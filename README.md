[![Documentation Status](https://readthedocs.org/projects/btso-cloned/badge/?version=latest)](https://btso-cloned.readthedocs.io/en/latest/?badge=latest)

# Welcome to branch `btso/package`

This branch contains all test to be run to check if the package contain any errors. Full documentation of the package can be found [here](https://btso-cloned.readthedocs.io/en/latest/?badge=latest.) 


## Getting started
### Install btso

btso relies on other packages to function correctly. All required package could be seen in ```requirements.txt``` file. The suggested way to setup btso package is to create a fresh Conda environment by following these steps:
1. Install Miniconda or update your conda installation with conda update conda
2. Create a new environment with `conda create -n <environmentName> python=3.10`
3. Activate the environmemt with `conda activate <environmentName>` on Mac/Linux or activate <environmentName> on Windows
4. Clone btso package with `git clone https://`
5. Install btso with `pip install -e <path_to_btso>`
6. Verify your btso installation by running the automated tests. Go to your btso top-level/root folder in terminal and type `python -m unittest` or `python -m unittest discover <path_to_test_directory>`, if the first code doesn't work.

### Usage


### Acknowledgements
Copyright &copy; 2022 by Solar Institut Jülich, all rights reserved.
