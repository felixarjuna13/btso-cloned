====================
Heating Technologies
====================

Heat Pump
---------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`\dot{Q}_{HP,nom}`. The heat flow of the heat pump must be below the nominal power as follows:

.. math::
    \dot{Q}_{HP} \leq \dot{Q}_{HP}^{[\frac{t}{l_{yr}}]}

The power demand is calculated with a constant coefficient of performance:

.. math::
    P_{HP}^t = \frac{\dot{Q}_{HP}}{COP}

Air-Water Heat Pump
--------------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`\dot{Q}_{Air-HP,nom}`. The heat flow of the air-water heat pump must be below the nominal power multiplied by a constant or a time-dependent factor as follows:

.. math::
    \dot{Q}_{Air-HP,h} + \dot{Q}_{Air-HP,c} +\frac{\dot{Q}_{Air-HP,c}}{COP_c^t} \leq f_{Air-HP} \cdot \dot{Q}_{Air-HP,nom}^{[\frac{t}{l_{yr}}]}


The electrical demand is calculated with a constant or time-dependent coefficient of performace:

.. math::
    P_{Air-HP,h} = \frac{\dot{Q}_{Air-HP,h}}{COP_h^t} + \frac{\dot{Q}_{Air-HP,c}}{COP_c^t}

When a minimum power or a binary power is considered, the following equations are applied:

.. math::
    \dot{Q}_{Air-HP,h} + \dot{Q}_{Air-HP,c} +\frac{\dot{Q}_{Air-HP,c}}{COP_c^t} \leq \infty \cdot b_{Air-HP}^t

.. math::
    \dot{Q}_{Air-HP,h} + \dot{Q}_{Air-HP,c} +\frac{\dot{Q}_{Air-HP,c}}{COP_c^t} \geq \dot{Q}_{Air-HP,nom}^t

with:

.. math::
    \dot{Q}_{Air-HP,on} + \dot{Q}_{Air-HP,off} =  f_{min} \cdot f_{Air-HP}^t \cdot \dot{Q}_{Air-HP,nom}^{[\frac{t}{l_{yr}}]}

.. math::
    \dot{Q}_{Air-HP,on} \leq \infty \cdot b_{Air-HP}^t

.. math::
    \dot{Q}_{Air-HP,off} + \infty \cdot b_{Air-HP}^t \leq \infty

Boiler
-------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nominal power :math:`\dot{Q}_{Boil,nom}`.

The heat output must remain below the rated output

.. math::
    {\dot{Q}_{Boil}}^t \leq \dot{Q}_{Boil,nom}^{[\frac{t}{l_{yr}}]}


The mass flow rate of the fired fluid is calculated by the thermal efficiency of the boiler

.. math::
    {M_{Boil}}^t = \frac{{\dot{Q}_{Boil}}^t}{\eta_{th}}

Geothermal
----------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the total length of the geothermal plant :math:`L_{Geo}`.

The equation uses a constant factor :math:`\dot{q}_{Geo}`

.. math::
    {\dot{Q}_{Geo_+}}^t + {\dot{Q}_{Geo_-}}^t \leq \dot{q}_{Geo} \cdot L_{Geo}^{[\frac{t}{l_{yr}}]}

Combined Heat and Power
-----------------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`P_{CHP,nom}`. The electrical power of the CHP must remain below the nomimal power.

.. math::
    P_{CHP}^t \leq P_{CHP,nom}^{[\frac{t}{l_{yr}}]}

The mass flow rate of the burnt liquid is calculated by the electrical efficiency of CHP as follows:

.. math::
    M_{CHP}^t = \frac{P_{CHP}^t}{\eta_{el}}

The generated heat is calculated with a constant thermal efficiency as follows:

.. math::
    \dot{Q}_{CHP}^t = M_{CHP}^t \cdot \eta_{th}

The electricity sold must be less than the electricity generated.

.. math::
    P_{sell,CHP}^t \leq P_{CHP}^t

If a ninimum power considered, the following equations apply:

.. math::
    P_{CHP,X}^t = P_{CHP,max}^t -  P_{CHP}^t

.. math::
    P_{CHP,X}^t \leq P_{CHP,max}^t -  P_{CHP,min}^t

.. math::
    P_{CHP,min}^t + P_{CHP,min,tr}^t = f_{min}\cdot P_{CHP,nom}^{[\frac{t}{l_{yr}}]}

.. math::
    P_{CHP,max}^t + P_{CHP,max,tr}^t = P_{CHP,nom}^{[\frac{t}{l_{yr}}]}

.. math::
    P_{CHP,min}^t \leq \infty \cdot b_{CHP,min}^t

.. math::
    P_{CHP,max}^t \leq \infty \cdot b_{CHP,min}^t

.. math::
    P_{CHP,min,tr}^t + \infty \cdot b_{CHP,min}^t \leq \infty

.. math::
    P_{CHP,max,tr}^t + \infty \cdot b_{CHP,min}^t \leq \infty

If, in addition, a minimum runtime is considered, the following equation applies to guarantee the minimum number of time steps :math:`N_{min}`.

.. math::
    \frac{1}{N_{min}-1} \cdot \sum_{i=t-(N_{min}-1)}^{t-1} b_{CHP,min}^i \leq b_{CHP,min}^t + b_{CHP,min}^{t-N_{min}}

Solar Thermal Absorber / Collector
----------------------------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The following equations are applied for a solar thermal collector (sts=stc) and a solar thermal absorber (sts=sta). The reference dimension is the total area :math:`A_{sts}`. The area used for heating or cooling must remain below the total area.

.. math::
    A_{sts,HT,h}^t + A_{sts,HT,c}^t + A_{sts,CT,h}^t + A_{sts,CT,c}^t \leq A_{sts}

The equation for the hot water storage tank is applied for all hot water storage factors above zero

.. math::
    \dot{Q}_{sts,HT,heat} = f_{sts,HT}^t \cdot A_{sts,HT,h}^t \;\forall\; f_{sts,HT}^t > 0

The cooling equation is applied for all hot water storage factors below zero

.. math::
    \dot{Q}_{sts,HT,cool} = -f_{sts,HT}^t \cdot A_{sts,HT,c}^t \;\forall\; f_{sts,HT}^t < 0

The equation for the heating cooling tank is applied for all cooling tank factors above zero

.. math::
    \dot{Q}_{sts,CT,heat} = f_{sts,CT}^t \cdot A_{sts,CT,h}^t \;\forall\; f_{sts,CT}^t > 0

The cooling tank equation is applied for all cooling tank factors below zero

.. math::
    \dot{Q}_{sts,CT,cool} = -f_{sts,CT}^t \cdot A_{sts,CT,c}^t \;\forall\; f_{sts,CT}^t < 0

