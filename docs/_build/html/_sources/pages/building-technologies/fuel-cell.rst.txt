Fuel Cell
---------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nominal power :math:`P_{FC,max}`.

The equations for the fuel cell is as follows: 

.. math::
    {P_{FC}}^t \leq P_{FC,max}^{[\frac{t}{l_{yr}}]}

The discharge quantity is calculated based on the electrical efficiency of the fuel cell

.. math::
    {P_{dis_{h_2s}}}^t = \frac{{P_{FC}}^t}{\eta_{el}}

When using waste heat, the following equation is applied

.. math::
    {\dot{Q}_{FC}}^t = {P_{dis,h_2s}}^t \cdot \eta_{th}
