(My_target)= ## Explicit targets

# How the component costs or emissions calculated

The component costs/emissions are the total sum over all calculated years. A linear price to the component reference variable $`S_{c,ref}`$ and binary installation costs $`{b_{ref}}^i`$ are considered. 

```math
C_{grid} = \sum_{i=1}^{n_{yrs}} {S_{c,ref}}^i \cdot c_{lin} + 
{b_{ref}}^i \cdot c_{con}
```

\
To ensure that the component is only considered when the installation cost is used, an inequality constraint has been introduced. 

```math
{S_{c,ref}}^i \leq {b_{ref}}^i \cdot \infty
```

\
The number of calculated years for each component was calculated as follows 

```math
N_c = max\left(\frac{t_c}{t_{bd}}, 1\right)
```

The size of the component is then the sum over the number of calculated years with a component, starting with the first year 

```math
{S_{c}}^i =  \sum_{j=max(i-N_c,1)}^{i}{S_{c,ref}}^j
```