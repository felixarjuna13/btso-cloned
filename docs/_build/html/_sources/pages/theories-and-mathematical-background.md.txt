# Theories and Mathematical Backgrounds

## How btso works? 
The mathematical formulation for the Building Technology Size Optimization and also how the objective formulation looks like are described here. The building technologies for electricity production under consideration are photovoltaics, wind turbine, fuel cell, electrolysis  with the technology for heat production such as boiler, geothermal heat pump, air-water heat pump, solar panel and geothermal energy. Combined heat and power are also considered that produced both electricity and heat. For the storage systems are technologies such as battery storage, hydrogen storage, heat and cold storage taken into account. Detailed description of the storage system could be seen here. 

The objective function is the minimization of the combination of network interaction costs and component costs and also the emissions emitted by the technology. The mathematical formulation our problem are:

**_Objective Function_**

$$min \; (C_{Component}+C_{Grid})$$

First equation is here \\(\mathbf{I}_n\\).

with
**_Component costs/emissions_**
```math
C_{Component} = C_{Air_{HP}} + C_{HT} + C_{PV} + C_{Batt} + C_{STC} + C_{STA} + C_{HP} + C_{CT} + C_{P_{el}}+C_{Boil}+ C_{Wind} + C_{FC} + C_{ES} + C_{H_2S} + C_{Q_h}+ C_{Q_c}
```

**_Network interaction costs/emissions_**
```math
C_{Grid} = \sum_{i=1}^{l_t} \delta {t_h}^t \cdot ({P_{buy}}^t\cdot {c_{buy}}^t - {P_{sell_{PV}}}^t\cdot {c_{sell_{PV}}}^t - {P_{sell_{Wind}}}^t\cdot {c_{sell_{Wind}}}^t - {P_{sell_{CHP}}}^t\cdot {c_{sell_{CHP}}}^t + {M_{Boil}}^t\cdot {c_{Fos}}^t + {M_{CHP}}^t\cdot {c_{Fos}}^t)
```
<br>

**_Energy balance constraints_**
```math
{P_{buy}}^t - {P_{sell_{PV}}}^t - {P_{sell_{Wind}}}^t - {P_{sell_{CHP}}}^t - {P_{Air-HP}}^t - {P_{el}}^t + \newline {P_{PV}}^t - {P_{ch}}^t + {P_{dis}}^t - {P_{HP}}^t + {P_{CHP}}^t +{P_{Wind}}^t + {P_{FC}}^t - {P_{ES}}^t = {P_{el,d}}^t
```

<br>
The detailed mathematical formulation for each building technologies are described in the corresponding page. The costs/emissions equations applies for all building technologies, so it can be represented once in this [page](/home/building-technologies/component-costs-or-emissions). 
