.. btso documentation master file, created by
   sphinx-quickstart on Mon Jul 18 14:38:10 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to btso's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/getting-started
   pages/theories-and-mathematical-background
   pages/component-costs-or-emissions
   pages/building-technologies/electricity-technologies
   pages/building-technologies/heating-technologies
   pages/building-technologies/storage-technologies
   pages/list-of-abbreviations


Links
==================

* :ref:`genindex`
* :ref:`search`
* Source code: https://git.fh-aachen.de/tb5152e/btso/-/tree/package
