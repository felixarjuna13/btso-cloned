Combined Heat and Power
========================

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`P_{CHP,nom}`. The electrical power of the CHP must remain below the nomimal power.

.. math::
    P_{CHP}^t \leq P_{CHP,nom}^{[\frac{t}{l_{yr}}]}

The mass flow rate of the burnt liquid is calculated by the electrical efficiency of CHP as follows:

.. math::
    M_{CHP}^t = \frac{P_{CHP}^t}{\eta_{el}}

The generated heat is calculated with a constant thermal efficiency as follows:

.. math::
    \dot{Q}_{CHP}^t = M_{CHP}^t \cdot \eta_{th}

The electricity sold must be less than the electricity generated.

.. math::
    P_{sell,CHP}^t \leq P_{CHP}^t

If a ninimum power considered, the following equations apply: 

.. math::
    P_{CHP,X}^t = P_{CHP,max}^t -  P_{CHP}^t

.. math::
    P_{CHP,X}^t \leq P_{CHP,max}^t -  P_{CHP,min}^t

.. math::
    P_{CHP,min}^t + P_{CHP,min,tr}^t = f_{min}\cdot P_{CHP,nom}^{[\frac{t}{l_{yr}}]}

.. math::
    P_{CHP,max}^t + P_{CHP,max,tr}^t = P_{CHP,nom}^{[\frac{t}{l_{yr}}]}

.. math::
    P_{CHP,min}^t \leq \infty \cdot b_{CHP,min}^t

.. math::
    P_{CHP,max}^t \leq \infty \cdot b_{CHP,min}^t

.. math::
    P_{CHP,min,tr}^t + \infty \cdot b_{CHP,min}^t \leq \infty

.. math::
    P_{CHP,max,tr}^t + \infty \cdot b_{CHP,min}^t \leq \infty

If, in addition, a minimum runtime is considered, the following equation applies to guarantee the minimum number of time steps :math:`N_{min}`.

.. math::
    \frac{1}{N_{min}-1} \cdot \sum_{i=t-(N_{min}-1)}^{t-1} b_{CHP,min}^i \leq b_{CHP,min}^t + b_{CHP,min}^{t-N_{min}}
