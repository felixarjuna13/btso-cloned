Electrolysis
============

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`P_{ES,max}`.

The electrolysis power must for each time remain below the nomimal power 

.. math::
    {P_{ES}}^t \leq P_{ES,max}^{[\frac{t}{l_{yr}}]}

The discharge quantity is calculated based on the electrical efficiency of the electrolysis

.. math::
    {P_{ch_{h_2s}}}^t = {{P_{ES}}^t}\cdot{\eta_{el}}

When using waste heat, the following equation is applied

.. math::
    {\dot{Q}_{ES}}^t = {P_{ES}}^t \cdot \eta_{th}

..
    All variables needed: [:math:`{\dot{Q}_{ES}}`](/home/list-of-abbreviations), [:math:`P_{ES,max}`](/home/list-of-abbreviations), [:math:`P_{ES}`](/home/list-of-abbreviations), [:math:`{P_{ch_{h_2s}}}`](/home/list-of-abbreviations), [:math:`{\eta_{el}}`](/home/list-of-abbreviations) and [:math:`{\eta_{th}}`](/home/list-of-abbreviations)

..
    | Symbols | Description | Unit
    | :---:   | :---        | :---:
    $`{\dot{Q}_{ES}}`$   | Charge power for the hydrogen storage                    |$`W`$
    $`P_{ES,max}`$       | Nominal power for the electrolysis technology            |$`W`$
    $`P_{ES}`$           | Actual power power for the electrolysis technology       |$`W`$
    $`{\eta_{el}}`$      | Electrical Efficiency  |$`-`$
    $`{\eta_{th}}`$      | Thermal Efficiency     |$`-`$