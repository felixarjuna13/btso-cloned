Photovoltaic
============

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the photovoltaic area :math:`A_{PV}`.

The equations for the photovoltaic is as follows:

.. math::
    {P_{PV}}^t \leq A_{PV}^{[\frac{t}{l_{yr}}]} \cdot {p_{PV}}^t

with :math:`p_{PV}` as the calculated photovoltaic power each hour of the year from the imported data.

The constraints for the photovoltaic limits that for each hour of the year no more electricity can be sold then the one produced and is as follows: 

.. math::
    {P_{sell_{PV}}}^t \leq {P_{PV}}^t
