Wind Turbine
============

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the wind turbine maximal power :math:`P_{Wind,d}`.

The equations for the wind turbine is as follows:

.. math::
    {P_{Wind}}^t \leq P_{Wind,d}^{[\frac{t}{l_{yr}}]} \cdot {p_{Wind}}^t

with :math:`p_{Wind}` as the calculated wind turbine power each hour of the year from the imported data.

The constraints for the wind turbine limits that for each hour of the year no more electricity can be sold then the one produced and is as follows: 

.. math::
    {P_{sell_{Wind}}}^t \leq {P_{Wind}}^t


