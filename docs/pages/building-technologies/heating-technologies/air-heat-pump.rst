Heat Pump
---------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`\dot{Q}_{HP,nom}`. The heat flow of the heat pump must be below the nominal power as follows:

.. math::
    \dot{Q}_{HP} \leq \dot{Q}_{HP}^{[\frac{t}{l_{yr}}]}

The power demand is calculated with a constant coefficient of performance: 

.. math::
    P_{HP}^t = \frac{\dot{Q}_{HP}}{COP}
