Air-Water Heat Pump
--------------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nomimal power :math:`\dot{Q}_{Air-HP,nom}`. The heat flow of the air-water heat pump must be below the nominal power multiplied by a constant or a time-dependent factor as follows:

.. math::
    \dot{Q}_{Air-HP,h} + \dot{Q}_{Air-HP,c} +\frac{\dot{Q}_{Air-HP,c}}{COP_c^t} \leq f_{Air-HP} \cdot \dot{Q}_{Air-HP,nom}^{[\frac{t}{l_{yr}}]}


The electrical demand is calculated with a constant or time-dependent coefficient of performace: 

.. math::
    P_{Air-HP,h} = \frac{\dot{Q}_{Air-HP,h}}{COP_h^t} + \frac{\dot{Q}_{Air-HP,c}}{COP_c^t}

When a minimum power or a binary power is considered, the following equations are applied: 

.. math::
    \dot{Q}_{Air-HP,h} + \dot{Q}_{Air-HP,c} +\frac{\dot{Q}_{Air-HP,c}}{COP_c^t} \leq \infty \cdot b_{Air-HP}^t

.. math::
    \dot{Q}_{Air-HP,h} + \dot{Q}_{Air-HP,c} +\frac{\dot{Q}_{Air-HP,c}}{COP_c^t} \geq \dot{Q}_{Air-HP,nom}^t

with:

.. math::
    \dot{Q}_{Air-HP,on} + \dot{Q}_{Air-HP,off} =  f_{min} \cdot f_{Air-HP}^t \cdot \dot{Q}_{Air-HP,nom}^{[\frac{t}{l_{yr}}]}

.. math::
    \dot{Q}_{Air-HP,on} \leq \infty \cdot b_{Air-HP}^t

.. math::
    \dot{Q}_{Air-HP,off} + \infty \cdot b_{Air-HP}^t \leq \infty
