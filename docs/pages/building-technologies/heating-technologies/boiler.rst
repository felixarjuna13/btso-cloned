Boiler
-------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the nominal power :math:`\dot{Q}_{Boil,nom}`.

The heat output must remain below the rated output

.. math::
    {\dot{Q}_{Boil}}^t \leq \dot{Q}_{Boil,nom}^{[\frac{t}{l_{yr}}]}


The mass flow rate of the fired fluid is calculated by the thermal efficiency of the boiler

.. math::
    {M_{Boil}}^t = \frac{{\dot{Q}_{Boil}}^t}{\eta_{th}}
