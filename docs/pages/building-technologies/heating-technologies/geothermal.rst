Geothermal
----------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the total length of the geothermal plant :math:`L_{Geo}`.

The equation uses a constant factor :math:`\dot{q}_{Geo}`

.. math::
    {\dot{Q}_{Geo_+}}^t + {\dot{Q}_{Geo_-}}^t \leq \dot{q}_{Geo} \cdot L_{Geo}^{[\frac{t}{l_{yr}}]}

