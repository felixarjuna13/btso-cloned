Solar Thermal Absorber / Collector
----------------------------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The following equations are applied for a solar thermal collector (sts=stc) and a solar thermal absorber (sts=sta). The reference dimension is the total area :math:`A_{sts}`. The area used for heating or cooling must remain below the total area.

.. math::
    A_{sts,HT,h}^t + A_{sts,HT,c}^t + A_{sts,CT,h}^t + A_{sts,CT,c}^t \leq A_{sts}

The equation for the hot water storage tank is applied for all hot water storage factors above zero

.. math::
    \dot{Q}_{sts,HT,heat} = f_{sts,HT}^t \cdot A_{sts,HT,h}^t \;\forall\; f_{sts,HT}^t > 0

The cooling equation is applied for all hot water storage factors below zero

.. math::
    \dot{Q}_{sts,HT,cool} = -f_{sts,HT}^t \cdot A_{sts,HT,c}^t \;\forall\; f_{sts,HT}^t < 0

The equation for the heating cooling tank is applied for all cooling tank factors above zero

.. math::
    \dot{Q}_{sts,CT,heat} = f_{sts,CT}^t \cdot A_{sts,CT,h}^t \;\forall\; f_{sts,CT}^t > 0

The cooling tank equation is applied for all cooling tank factors below zero

.. math::
    \dot{Q}_{sts,CT,cool} = -f_{sts,CT}^t \cdot A_{sts,CT,c}^t \;\forall\; f_{sts,CT}^t < 0
