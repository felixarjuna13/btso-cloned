Battery Storage
---------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the maximum storage capacity :math:`E_{Batt,max}`. The storage equation described in :doc:`storage systems page <storage-systems>` are used.

The power for charging and discharging the battery system is implemented as follows:

.. math::
    \sum P_t = P_{ch}^t\cdot \eta_{ch} - \frac{P_{dis}^t}{\eta_{dis}}

The maximum discharge rate is limited as follows:

.. math::
    {P_{dis}}^t \leq {f_{dis}}^t \cdot E_{batt,max}^{[\frac{t}{l_{yr}}]}

The minimum discharge rate is limited as follows:

.. math::
    {P_{ch}}^t \leq {f_{ch}}^t \cdot E_{batt,max}^{[\frac{t}{l_{yr}}]}
