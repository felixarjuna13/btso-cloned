Cold Storage
---------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the maximum storage capacity :math:`E_{CT,max}`. The storage equation described in :doc:`storage systems page <storage-systems>` are used. 

The power for charging and discharging the heat storage is implemented as follows:

.. math:: 
    \sum \dot{Q}^t = \; &\dot{Q}_{geo_+}^t + \dot{Q}_{geo_-}^t + \dot{Q}_{HP}^t + \dot{Q}_{Air-HP,c}^t + \dot{Q}_{stc,CT,heat}^t - \\ &\dot{Q}_{stc,CT,cool}^t - \dot{Q}_{sta,CT,cool}^t + \dot{Q}_{sta,CT,heat}^t

The maximum discharge rate is limited as follows: 

.. math::
    \dot{Q}_{geo_-}^t + \dot{Q}_{HP}^t - P_{HP}^t + \dot{Q}_{Air-HP,c}^t + \newline \dot{Q}_{stc,CT,cool}^t + \dot{Q}_{sta,CT,cool}^t  \leq f_{dis}^t \cdot E_{CT,max}^{[\frac{t}{l_{yr}}]}

The maximum charge rate is limited as follows: 

.. math::
    \dot{Q}_{geo_+}^t + \dot{Q}_{stc,CT,heat}^t + \dot{Q}_{sta,CT,heat}^t \leq f_{ch}^t \cdot E_{CT,max}^{[\frac{t}{l_{yr}}]}
