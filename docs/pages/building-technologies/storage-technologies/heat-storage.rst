Heat Storage
---------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the maximum storage capacity :math:`E_{HT,max}`. The storage equation described in :doc:`storage systems page <storage-systems>` are used. 

The power for charging and discharging the heat storage is implemented as follows:

.. math::
    \sum \dot{Q}^t = \; &\dot{Q}_{el}^t + \dot{Q}_{Air-HP,h}^t + \dot{Q}_{HP}^t + \dot{Q}_{Boil}^t + \dot{Q}_{CHP}^t + \dot{Q}_{ES}^t + \dot{Q}_{FC}^t + \dot{Q}_{stc,HT,heat}^t - \\
    &\dot{Q}_{stc,HT,cool}^t - \dot{Q}_{sta,HT,cool}^t + \dot{Q}_{sta,HT,heat}^t - \dot{Q}_{h}^t

The maximum discharge rate is limited as follows: 

.. math::
    \dot{Q}_{stc,HT,cool}^t + \dot{Q}_{sta,HT,cool}^t + \dot{Q}_{h}^t \leq f_{dis}^t \cdot E_{HT,max}^{[\frac{t}{l_{yr}}]}

The minimum discharge rate is limited as follows: 

.. math::
    &\dot{Q}_{el}^t + \dot{Q}_{Air-HP,h}^t + \dot{Q}_{HP}^t + \dot{Q}_{Boil}^t + \dot{Q}_{CHP}^t +  \dot{Q}_{ES}^t + \\ &\dot{Q}_{FC}^t + \dot{Q}_{stc,HT,heat}^t +
    \dot{Q}_{stc,HT,cool}^t \leq f_{ch}^t \cdot E_{HT,max}^{[\frac{t}{l_{yr}}]}