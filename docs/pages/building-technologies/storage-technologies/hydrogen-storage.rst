Hydrogen Storage
----------------

The cost of the component was explained in :doc:`component costs or emissions page </pages/component-costs-or-emissions>`. The reference size is the maximum storage capacity :math:`E_{H_2S,max}`. The storage equation described in :doc:`storage systems page <storage-systems>` are used. 

The power for charging and discharging the heat storage is implemented as follows:

.. math::
    \sum P^t = P_{ch,h_2s}^t\cdot\eta_{ch} - \frac{P_{dis,h_2s}^t}{\eta_{dis}}

The maximum discharge rate is limited as follows: 

.. math::
    P_{dis,h_2s}^t \leq f_{dis}^t \cdot E_{H_2S,max}^{[\frac{t}{l_{yr}}]}

The maximum charge rate is limited as follows: 

.. math::
    P_{ch,h_2s}^t \leq f_{ch}^t \cdot E_{H_2S,max}^{[\frac{t}{l_{yr}}]}
