---------------
Storage Systems
---------------

For the solution of the differential equation of the storage system are three different methods considered. These are *Forward Euler*, *Central Euler* and *Backward Euler*. The Central Euler method is also called Crack-Nicolsen Approach.

The first Equation constraints states that the storage system must be smaller that its maximum capacity.

.. math::
    E_{st}^t \leq E_{st,ref}^{[\frac{t}{l_{yr}}]}

Forward Euler
-------------

.. math::
    E_{st}^t - E_{st}^{t-1} \cdot \eta_{self}^{t-1} = \sum (P^{t-1} \;or \; \dot{Q}^{t-1}) \cdot \delta_{t_h}^{t-1}

Startpoint

.. math::
    E_{st}^{t_{start}} = f_{SOC} \cdot E_{st,ref}^{[\frac{t}{l_{yr}}]}

If start- and endpoint are the same: 

.. math::
    E_{st}^{t_{start}} - E_{st}^{t_{end}}\cdot \eta_{self}^{t_{end}} =
    \sum(P^{t_{end}}\;or\;\dot{Q}^{t_{end}}) \cdot \delta_{t_h}^{t_{end}}


Backward Euler
-------------

.. math::
    E_{st}^t - E_{st}^{t-1} \cdot \eta_{self}^{t-1} = \sum (P^{t} \;or \; \dot{Q}^{t}) \cdot \delta_{t_h}^{t}

Startpoint

.. math::
    E_{st}^{t_{start}} = f_{SOC} \cdot E_{st,ref}^{[\frac{t}{l_{yr}}]} \cdot \eta_{self}^{t_{start}} + \sum(P^{t_{start}}\;or\;\dot{Q}^{t_{start}}) \cdot \delta_{t_h}^{t_{start}}

If start- and endpoint are the same: 

.. math::
    E_{st}^{t_{start}} - E_{st}^{t_{end}} \cdot \eta_{self}^{t_{end}} =
    \sum(P^{t_{start}}\;or\;\dot{Q}^{t_{start}}) \cdot \delta_{t_h}^{t_{start}}

Central Euler
-------------

.. math::
    E_{st}^t - E_{st}^{t-1} \cdot \eta_{self}^{t-1} = \sum (P^{t-1} \;or \; \dot{Q}^{t-1}) \cdot  \frac{\delta_{t_h}^{t-1}}{2} +
    \sum (P^{t} \;or \; \dot{Q}^{t}) \cdot  \frac{\delta_{t_h}^{t}}{2}

Startpoint

.. math::
    E_{st}^{t_{start}} = f_{SOC} \cdot E_{st,ref}^{[\frac{t}{l_{yr}}]} \cdot \eta_{self}^{t_{start}} + \sum(P^{t_{start}}\;or\;\dot{Q}^{t_{start}}) \cdot \frac{\delta_{t_h}^{t_{start}}}{2}

If start- and endpoint are the same: 

.. math::
    E_{st}^{t_{start}} - E_{st}^{t_{end}} \cdot \eta_{self}^{t_{end}} =
    \sum(P^{t_{start}}\;or\;\dot{Q}^{t_{start}}) \cdot \frac{\delta_{t_h}^{t_{start}}}{2} +
    \sum(P^{t_{start}}\;or\;\dot{Q}^{t_{start}}) \cdot \frac{\delta_{t_h}^{t_{end}}}{2}
