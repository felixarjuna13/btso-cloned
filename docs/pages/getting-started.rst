===============
Getting started
===============

Install btso
===============

btso relies on other packages to function correctly. All required package could be seen in :code:`requirements.txt` file. The suggested way to setup btso package is to create a fresh Conda environment by following these steps:

#. Install Miniconda or update your conda installation with :code:`conda update conda`
#. Create a new environment with :code:`conda create -n <environmentName> python=3.10`
#. Activate the environmemt with :code:`conda activate <environmentName>` on Mac/Linux or :code:`activate <environmentName>` on Windows
#. Clone btso package with :code:`git clone https://`
#. Install btso with :code:`pip install -e <path_to_btso>`
#. Verify your btso installation by running the automated tests. Go to your btso top-level/root folder in terminal and type :code:`python -m unittest` or :code:`python -m unittest discover <path_to_test_directory>`, if the first code doesn't work.