from math import cos, sin, radians, degrees, asin, acos  # type: ignore
from numpy import zeros, real, array, ndarray, float64 as np_float  # type: ignore
from numpy.typing import NDArray  # type: ignore
from typing import Union, List


def solar_collector(eta: float, k1: float, k2: float, t_amb: ndarray, t_fl: Union[float, ndarray], irr: ndarray) \
        -> ndarray:
    """
    calculation of solar thermal heat gain with simple quadratic loss model
    :param eta: optical collector efficiency [-]
    :param k1: linear heat loss coefficient [W/m²K]
    :param k2: quadratic heat loss coefficient [W/m²K²]
    :param t_amb: ambient temperature [°C]
    :param t_fl: fluid temperature [°C]
    :param irr: surface irradiation [W/m²]
    :return: solar thermal heat gain [W/m²]
    """
    # determine temperature difference between fluid temperature and ambient temperature
    d_t: ndarray = t_fl - t_amb

    q_st: ndarray = eta * irr - d_t * (k1 + d_t * k2)
    return q_st


def calc_pv(eta: float, gamma_t: float, t_amb: ndarray, t_noct: float, irr: ndarray) -> ndarray:
    t_ref: float = 25.
    t_c: ndarray = t_amb + (t_noct - t_ref) * irr / 800
    power: ndarray = eta * irr * (1 + gamma_t * (t_c - t_ref))
    return power


def calc_radiation(longitude, latitude, longitude_hour, rho, beta, gamma, diff_model, irradiation_direct,
                   irradiation_diffuse):
    i_0 = 1360.8  # solar constant
    cosd_latitude = cos(radians(latitude))
    sind_latitude = sin(radians(latitude))
    len_yr = len(irradiation_direct)
    hours = range(1, len_yr + 1)
    sind_beta = sin(radians(beta))
    cosd_beta = cos(radians(beta))

    cosd_theta_gen = zeros(len_yr)
    theta_gen = zeros(len_yr)
    irradiation_direct_area = zeros(len_yr)
    irradiation_diffuse_area = zeros(len_yr)
    irradiation_reflected_area = zeros(len_yr)

    for i in range(len_yr):
        # Calculation of day of the year
        day = int(hours[i] / 24 + 1)
        # extraterrestrial irradiation
        e_0 = i_0 * (1 + 0.0334 * cos(0.0172 * day - 0.04747))
        # Calculation DIN parameter
        j = 360 * day / 365
        # Calculation of solar Declination
        delta = (0.3948 - 23.2559 * cos(radians(j + 9.1)) - 0.3915 * cos(radians(2 * j + 5.4)) - 0.1764 * cos(
            radians(3 * j + 26)))
        # Calculation of equation of time
        equation_of_time = (0.0066 + 7.3525 * cos(radians(j + 85.9)) + 9.9359 * cos(radians(2 * j + 108.9)) + 0.3387 *
                            cos(radians(3 * j + 105.2)))
        # Calculation true local time
        th = -1
        t_sol = hours[i] + th + 4 / 60 * (longitude_hour - longitude) + equation_of_time / 60 - (day - 1) * 24
        # Calculation hour angle
        omega = (12 - t_sol) * 15
        gamma_s = degrees(
            asin(cos(radians(omega)) * cosd_latitude * cos(radians(delta)) + sind_latitude * sin(radians(delta))))
        cosd_gamma_s = cos(radians(gamma_s))
        sind_gamma_s = sin(radians(gamma_s))

        # Calculate angle of zenit
        theta_hor = 90 - gamma_s
        # Calculation solar azimuth
        #        print(t_sol)
        if t_sol > 12:  # then
            alpha_s = 180 + degrees(
                acos((sind_gamma_s * sind_latitude - sin(radians(delta))) / (cosd_gamma_s * cosd_latitude)))
        else:
            alpha_s = 180 - degrees(
                acos((sind_gamma_s * sind_latitude - sin(radians(delta))) / (cosd_gamma_s * cosd_latitude)))
        air_mass = real(1 / (cos(radians(theta_hor)) + 0.50572 * (96.07995 - theta_hor) ** -1.6364))
        if irradiation_direct.item(i) > 0 or irradiation_diffuse.item(i) > 0:
            cosd_theta_gen.itemset(i, (
                        -cosd_gamma_s * sind_beta * cos(radians(alpha_s - gamma) + sind_gamma_s * cosd_beta)))
            theta_gen.itemset(i, degrees(acos(cosd_theta_gen.item(i))))

            if gamma_s > 10:
                irradiation_direct_area.itemset(i, max(irradiation_direct[i] * cosd_theta_gen.item(i) * air_mass, 0))
            else:
                if air_mass != 0 and cosd_theta_gen.item(i) != 0:
                    irradiation_direct_area.itemset(i, irradiation_direct[i] / cosd_theta_gen.item(i) / air_mass)
                else:
                    irradiation_direct_area.itemset(i, irradiation_direct[i])
                irradiation_direct_area.itemset(i, max(min(irradiation_direct_area.item(i), e_0,
                                                           irradiation_direct.item(i) * 2), 0))
            if irradiation_diffuse.item(i) > 0:
                if diff_model < 2:  # isotropic model
                    irradiation_diffuse_area.itemset(i, Calc_isotropic_diffuse_Irradiation(
                        irradiation_diffuse.item(i), cosd_beta))
                elif diff_model < 3:  # Klucher Model
                    irradiation_diffuse_area.itemset(i, Calc_Klucher_diffuse_Irradiation(
                        irradiation_diffuse.item(i), irradiation_direct.item(i), cosd_theta_gen.item(i), cosd_beta,
                        beta, gamma_s))
                elif diff_model < 4:  # Hay Davis Model
                    irradiation_diffuse_area.itemset(i, Calc_Hay_Davis_diffuse_Irradiation(irradiation_diffuse.item(i),
                                                                                           irradiation_direct.item(i),
                                                                                           e_0, cosd_theta_gen.item(i),
                                                                                           cosd_beta, air_mass))
                elif diff_model < 5:  # Reindl Model
                    irradiation_diffuse_area.itemset(i, Calc_Reindl_diffuse_Irradiation(irradiation_diffuse.item(i),
                                                                                        irradiation_direct.item(i), e_0,
                                                                                        cosd_theta_gen.item(i),
                                                                                        cosd_beta, beta, air_mass))
                elif diff_model < 6:  # Perez Model
                    irradiation_diffuse_area.itemset(i, Calc_Perez_diffuse_Irradiation(irradiation_diffuse.item(i),
                                                                                       irradiation_direct.item(i), e_0,
                                                                                       cosd_beta, sind_gamma_s,
                                                                                       sind_beta, theta_hor, air_mass))
                else:  # isotropic model
                    irradiation_diffuse_area.itemset(i, Calc_isotropic_diffuse_Irradiation(irradiation_diffuse.item(i),
                                                                                           cosd_beta))

            irradiation_reflected_area.itemset(i,
                                               (irradiation_direct.item(i) + irradiation_diffuse.item(i)) * rho * 0.5 *
                                               (1 - cosd_beta))

    irradiation_total_area = (irradiation_direct_area + irradiation_diffuse_area + irradiation_reflected_area)

    return irradiation_total_area


# Functions for different diffuse radiation model
# Good Summary:
# 'Comparison of Modelled and Measured Tilted Solar irr for Photovoltaic Applications' by
# Riyad Mubarak, Martin Hofmann, Stefan Riechelmann and Gunther Seckmeyer


def Calc_isotropic_diffuse_Irradiation(I_diff, cosd_beta):
    # function for Calculation of diffuse Irradiation with an isotropic Model
    # Liu, B.; Jordan, R. Daily insolation on surfaces tilted towards equator. ASHRAE J. 1961, 10, 526–541.
    # calc diffuse Irradiation with isotropic model
    Irradiation_diffuse = I_diff * 0.5 * (1 + cosd_beta)
    return Irradiation_diffuse


def Calc_Klucher_diffuse_Irradiation(I_diff, I_dir, cosd_theta_gen, cosd_beta, beta, gamma_s):
    # function for Calculation of diffuse Irradiation with Klucher Model
    # Klucher, T.M. Evaluation of models to predict insolation on tilted surfaces. Sol. Energy 1979, 23, 111–114.
    x = I_diff / (I_diff + I_dir)  # calc once not 2 times --> better performance
    F = 1 - x * x  # calc Klucher modulating factor
    cosd_gamma_s = cos(radians(gamma_s))  # calc once not 3 times --> better performance
    sind_beta_2 = sin(radians(beta / 2))  # calc once not 3 times --> better performance
    # calc diffuse Irradiation with Klucher model
    Irradiation_diffuse = (I_diff * 0.5 * (1 + cosd_beta) * (1 + F * sind_beta_2 * sind_beta_2 * sind_beta_2) * (
                1 + F * cosd_theta_gen *
                cosd_theta_gen *
                cosd_gamma_s *
                cosd_gamma_s *
                cosd_gamma_s))
    return Irradiation_diffuse


def Calc_Hay_Davis_diffuse_Irradiation(I_diff, I_dir, E_0, cosd_theta_gen, cosd_beta, AirMass):
    # function for Calculation of diffuse Irradiation with Hay-Davis Model
    # Hay, J.E.; Davies, J.A. Calculation of the solar radiation incident on an inclined surface. In Proceedings of the
    # First Canadian Solar Radiation Data Workshop, Toronto, ON, Canada, 17–19 April 1978; pp. 59–72.
    # calc the transmittance of beam irradiance through the atmosphere
    A = I_dir / E_0
    # calc diffuse Irradiation with Hay Davis model
    Irradiation_diffuse = I_diff * (A * cosd_theta_gen * AirMass + (1 - A) * 0.5 * (1 + cosd_beta))
    return Irradiation_diffuse


def Calc_Reindl_diffuse_Irradiation(I_diff, I_dir, E_0, cosd_theta_gen, cosd_beta, beta, AirMass):
    # function for Calculation of diffuse Irradiation with Reindl Model
    # Reindl, D.T.; Beckman,W.A.; Duffie, J.A. Evaluation of hourly tilted surface radiation models. Sol. Energy 1990,
    # 45, 9–17.
    from math import sqrt
    # calc the transmittance of beam irradiance through the atmosphere
    A = I_dir / E_0
    sind_beta_2 = sin(radians(beta / 2))  # calc once not 3 times --> better performance
    # calc diffuse Irradiation with Reindl model
    Irradiation_diffuse = I_diff * (
                A * cosd_theta_gen * AirMass + (1 - A) * 0.5 * (1 + cosd_beta) * (1 + sqrt(I_dir / (I_dir + I_diff)) *
                                                                                  sind_beta_2 * sind_beta_2 *
                                                                                  sind_beta_2))
    return Irradiation_diffuse


def Calc_Perez_diffuse_Irradiation(I_diff, I_dir, E_0, cosd_beta, sind_gamma_s, sind_beta, theta_hor, AirMass):
    # function for Calculation of diffuse Irradiation with Perez Model
    # Perez, R.; Ineichen, P.; Seals, R.; Michalsky, J.; Stewart, R. Modeling daylight availability and irradiance
    # components from direct and global irradiance. Sol. Energy 1990, 44, 271–289.
    k = 1.041  # Konstant for Perez Model
    theta_hor_rad = radians(theta_hor)  # Cast from degree to radians
    k_theta_hor_3 = k * theta_hor_rad * theta_hor_rad * theta_hor_rad  # calc ^3 once cause it`s faster
    # Calculate Himmelsklarheitsindex
    epsilon = real(((I_diff + I_dir * AirMass) / I_diff + k_theta_hor_3) / (1 + k_theta_hor_3))
    # Calculate Helligkeitsindex
    Delta = AirMass * I_diff / E_0
    # calc Perez-Factors F1 and F2
    [F_1, F_2] = Calc_F_1_2(epsilon, Delta, theta_hor_rad)
    # calc Perez factors a and b
    a = max(0, sind_gamma_s)
    b = max(0.087, sind_gamma_s)
    # calc diffuse Irradiation with Perez model
    Irradiation_diffuse = abs(I_diff * (0.5 * (1 + cosd_beta) * (1 - F_1) + a / b * F_1 + F_2 * sind_beta))
    return Irradiation_diffuse


def Calc_F_1_2(epsilon, Delta, theta_hor):
    # Calculation of F1 and F2 for Perez Model
    if epsilon < 1.065:
        F_11 = -0.008
        F_12 = 0.588
        F_13 = -0.062
        F_21 = -0.06
        F_22 = 0.072
        F_23 = -0.022
    elif epsilon < 1.23:
        F_11 = 0.13
        F_12 = 0.683
        F_13 = -0.151
        F_21 = -0.019
        F_22 = 0.066
        F_23 = -0.029
    elif epsilon < 1.5:
        F_11 = 0.33
        F_12 = 0.487
        F_13 = -0.221
        F_21 = 0.055
        F_22 = -0.064
        F_23 = -0.026
    elif epsilon < 1.95:
        F_11 = 0.568
        F_12 = 0.187
        F_13 = -0.295
        F_21 = 0.109
        F_22 = -0.152
        F_23 = -0.014
    elif epsilon < 2.8:
        F_11 = 0.873
        F_12 = -0.392
        F_13 = -0.362
        F_21 = 0.226
        F_22 = -0.462
        F_23 = 0.001
    elif epsilon < 4.5:
        F_11 = 1.132
        F_12 = -1.237
        F_13 = -0.412
        F_21 = 0.288
        F_22 = -0.823
        F_23 = 0.056
    elif epsilon < 6.2:
        F_11 = 1.06
        F_12 = -1.6
        F_13 = -0.359
        F_21 = 0.265
        F_22 = -1.127
        F_23 = 0.131
    else:
        F_11 = 0.678
        F_12 = -0.372
        F_13 = -0.25
        F_21 = 0.156
        F_22 = -1.377
        F_23 = 0.251

    F_1 = max(F_11 + F_12 * Delta + F_13 * theta_hor, 0)
    F_2 = F_21 + F_22 * Delta + F_23 * theta_hor
    return F_1, F_2


def Calc_Reflection(Irradiation_direct, Irradiation_diffuse, Irradiation_reflected, beta, theta_gen,
                    Incident_Angle_Modifier):
    Irradiation_direct_refl = Calc_direct_Reflection(Irradiation_direct, theta_gen, Incident_Angle_Modifier)
    Irradiation_diffuse_refl = zeros(len(Irradiation_diffuse))
    Irradiation_refl_refl = zeros(len(Irradiation_reflected))

    cosd_theta_diff = cos(radians(59.68 - 0.1388 * beta + 0.001497 * beta * beta))
    cosd_theta_refl = cos(radians(90 - 0.5788 * beta + 0.002693 * beta * beta))
    refl_refl = min(max(1 - (1 - Incident_Angle_Modifier) * (1 / cosd_theta_refl - 1), 0), 1)
    refl_diff = min(max(1 - (1 - Incident_Angle_Modifier) * (1 / cosd_theta_diff - 1), 0), 1)
    for i in range(len(theta_gen)):
        Irradiation_diffuse_refl.itemset(i, Irradiation_diffuse.item(i) * refl_diff)
        Irradiation_refl_refl.itemset(i, Irradiation_reflected.item(i) * refl_refl)

    return Irradiation_direct_refl, Irradiation_diffuse_refl, Irradiation_refl_refl


def Calc_direct_Reflection(Irradiation_direct, theta_gen, Incident_Angle_Modifier):
    Irradiation_direct_refl = zeros(len(Irradiation_direct))
    for i in range(len(theta_gen)):
        if Irradiation_direct.item(i) > 0:
            cosd_theta_gen = cos(radians(theta_gen.item(i)))
            reflection = min(max(1 - (1 - Incident_Angle_Modifier) * (1 / cosd_theta_gen - 1), 0), 1)
            Irradiation_direct_refl.itemset(i, Irradiation_direct.item(i) * reflection)

    return Irradiation_direct_refl


def CalcWindPower(Wind: array, NominalPower: float, NominalVelocity: float,
                  MinimalVelocity: float, MaximalVelocity: float):
    eta = NominalPower / NominalVelocity / NominalVelocity / NominalVelocity
    res = zeros(len(Wind))
    for idx, val in enumerate(Wind):
        if MinimalVelocity < val < MaximalVelocity:
            res[idx] = eta * val * val * val
            continue

    return res / NominalPower


def calc_wind_at_height(wind: NDArray[np_float], height: float, model: int, constant_idx: int,
                        constant_user: float) -> NDArray[np_float]:
    """
    calculation of wind at wind turbine height with one of two models. \n
    :param wind: wind at 10 m height as an array
    :param height: height of wind turbine in meter [m]
    :param model: wind calculation model: 0 Hellmann; 1; Logarithmic
    :param constant_idx: constant to be used in the model as index of list; Hellmann; [0.27, 0.34, 0.60, str_user] Logarithmic; [0.0002, 0.005, 0.03, 0.1, 0.25, 0.5, 1, 2, str_user]
    :param constant_user: user defined constant
    :return: array with wind at turbine height [m/s]
    """
    from math import log
    str_user: float = -1
    list_entries_hell: List[float] = [0.27, 0.34, 0.60, str_user]
    list_entries_log: List[float] = [0.0002, 0.005, 0.03, 0.1, 0.25, 0.5, 1.0, 2.0, str_user]
    if model < 1:
        constant = list_entries_log[constant_idx]
        constant = constant_user if str_user == constant else constant
        wind_factor = log(height / constant) / log(10 / constant)
        res = wind * wind_factor
        return res
    constant = list_entries_hell[constant_idx]
    constant = constant_user if str_user == constant else constant
    wind_factor = (height / 10) ** constant
    res = wind * wind_factor
    return res
