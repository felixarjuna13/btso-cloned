from numpy import array, linspace, ones, zeros, float64 as np_float, maximum  # type: ignore
from abc import abstractmethod, ABC
from numpy.typing import NDArray  # type: ignore
from pickle import load as pk_load, dump as pk_dump, HIGHEST_PROTOCOL  # type: ignore
from pandas import read_csv as pd_read_csv, to_datetime as pd_to_datetime, concat as pd_concat  # type: ignore
from pandas import MultiIndex as pd_MultiIndex, DataFrame as pdDataFrame, Series as pdSeries, DatetimeIndex  # type: ignore
from gurobipy import GRB  # type: ignore
from btso.CalculationModels import calc_pv, calc_radiation, solar_collector, CalcWindPower, calc_wind_at_height
from numpy_financial import npv as npf_npv  # type: ignore
from typing import Union, List, Tuple, Callable, Sequence, Optional
from btso.TypicalPeriodsAggregation import CreateTP as TD_CreateTD
from math import isclose  # type: ignore
from dataclasses import dataclass, field  # type: ignore

default_array = zeros(0)  # create default matrix

# indexes of different kinds of energy
IDX_ELECTRICAL = 0
IDX_HEATING = 1
IDX_COOLING = 2
IDX_HYDROGEN = 3
IDX_FUEL = 4


def read_data(data: Union[pdDataFrame, pdSeries], column_name: str, unit: float = None) -> NDArray[np_float]:
    """
    read column from data (pandas DataFrame) and multiply with unit values
    :param data: pandas Dataframe where to select column from
    :param column_name: column name which should be selected in Dataframe
    :param unit: multiplication factor (default = 1.)
    :return: column in dataframe as array multiplied with unit
    """
    # set default value for unit
    unit = 1. if unit is None else unit
    # try to get data by column name and multiply and with the unit
    try:
        data = data[column_name] * unit
    except KeyError as e:
        # raise Key Error and print message
        print(f'wrong column name: {column_name}')
        raise e
    # cast data to a numpy array if it is a pandas series
    if isinstance(data, pdSeries):
        data = array(data.to_list())
        data.reshape(len(data))
    return data


def npv(value: float, time_step_years: int, rate_of_interest: float) -> float:
    """
    Net present value calculation for value with the time step of the year and the rate of interest. \n
    :param value: value to calculate net present value for
    :param time_step_years: years to calculate net present value for [#]
    :param rate_of_interest: rate of interest [-]
    :return: float which is net present value of value
    """
    vec: array = ones(time_step_years) * value  # create array with value entries and length of time_step_years
    val: float = npf_npv(rate_of_interest / 100, vec)  # calculate net present value for given rate of interest
    return val


def check_efficiency(list_of_efficiency: List[float]) -> None:
    """
    check if the entries in the list of efficiencies is decreasing and not increasing
    :param list_of_efficiency: list of efficiencies
    :return: None
    """
    before = max(list_of_efficiency)
    for i in list_of_efficiency:
        if i > before:
            raise ValueError(f'list of efficiencies {list_of_efficiency} is not decreasing')
        before = i


@dataclass(slots=True)
class Variable:
    idx: Union[List[int], int] = -1
    value: Union[NDArray, float] = -1.


@dataclass(slots=True)
class Node:
    """
    node class to collect variable connected in one node\n
    :param kind: 0 = electrical, 1 = heating, 2 = cooling, 3 = Hydrogen, 4 = fuel
    """
    kind: int  # 0 = electrical, 1 = heating, 2 = cooling, 3 = Hydrogen, 4 = fuel
    variables: List[Variable] = field(default_factory=lambda: [])
    values: List[float] = field(default_factory=lambda: [])
    demands: List[NDArray[np_float]] = field(default_factory=lambda: [])

    def __post_init__(self):
        self.variables = []
        self.values = []
        self.demands = []

    def append(self, variable: Variable, value: float):
        self.variables.append(variable)
        self.values.append(value)

    def append_demand(self, demand: NDArray[np_float]):
        self.demands.append(demand)

    def reset(self):
        self.variables = []
        self.values = []
        self.demands = []

    def __getitem__(self, idx: int):
        return self.variables[idx]


@dataclass(slots=True)  # type: ignore
class BTClass:
    """
    Base class for building technology components. Implementing linear prices and minimal as well as maximal values.
    """
    # set default values
    rate_of_interest: float = 0.0
    building_lifetime: int = 20
    change_price: bool = False
    maintenance_val: float = 0
    pr_lin_val: float = 10
    pr_con_val: float = 0
    life: int = 20
    pr_lin: NDArray[np_float] = default_array
    pr_con: NDArray[np_float] = default_array
    maintenance: float = 0.
    min: bool = False
    min_val: float = 0.
    max: bool = False
    max_val: float = GRB.INFINITY
    size: Variable = Variable()
    size_yr: Variable = Variable()
    size_bin: Variable = Variable()
    costs: Variable = Variable()

    def __post_init__(self):
        [setattr(self, var, Variable()) for var in self.__slots__ if isinstance(getattr(self, var), Variable)]

    def set_prices(self, rate_of_interest: float, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                   life: int = 20, building_lifetime: int = 50, change_price: bool = False):
        """
        Base class for building technology components. Implementing linear prices and minimal as well as maximal values
        :param rate_of_interest: rate of interest [-]
        :param pr_lin_val: linear component price [€/size]
        :param pr_con_val: constant component price [€]
        :param maintenance_val: maintenance costs as percentage of component costs [%]
        :param life: component lifetime in years [a]
        :param building_lifetime: building or quarter lifetime in years [a]
        :param change_price: True if components price should change during building lifetime [True, False]
        """
        # save inputs to class variables
        self.rate_of_interest: float = rate_of_interest
        self.building_lifetime: int = building_lifetime
        self.change_price: bool = change_price
        self.maintenance_val: float = maintenance_val
        self.pr_lin_val: float = pr_lin_val
        self.pr_con_val: float = pr_con_val
        self.life: int = life
        # determine prices considering the rate of interest
        self._calc_price()

    def _calc_price(self) -> None:
        """
        calculate prices from inputs\n
        :return: None
        """
        # cast maintenance from percentage to real
        self.maintenance = self.maintenance_val / 100
        # to be deleted
        if self.building_lifetime < 1:
            red: list = [1, 0.7672, 0.5948, 0.3879, 0.3879, 0.3879, 0.3879, 0.3879, 0.3879] if \
                self.change_price else [1, 1, 1, 1, 1, 1, 1, 1, 1]
            self.pr_lin: list = [self.pr_lin_val * i for i in red]
            self.pr_con: list = [self.pr_con_val * i for i in red]
            return
        # initialize costs vector
        vec = zeros(int(self.building_lifetime))
        # set linear costs for component
        vec[0] = self.pr_lin_val
        vec[0:int(self.building_lifetime):int(self.life)] = self.pr_lin_val
        # add maintenance costs
        vec[:] += self.maintenance * self.pr_lin_val
        # calculate costs considering the rate of interest
        self.pr_lin: list = [npf_npv(self.rate_of_interest / 100, vec)]
        # initialize costs vector
        vec = zeros(int(self.building_lifetime))
        # set constant costs for component
        vec[0] = self.pr_con_val
        vec[0:int(self.building_lifetime):int(self.life)] = self.pr_con_val
        # add maintenance costs
        vec[:] += self.maintenance * self.pr_con_val
        # calculate costs considering the rate of interest
        self.pr_con: list = [npf_npv(self.rate_of_interest / 100, vec)]

    def set_min(self, minimal_value: float) -> None:
        """
        set the minimal value\n
        :param minimal_value: minimal value of component as float
        :return:None
        """
        # activate minimal value
        self.min: bool = True
        # set minimal value
        self.min_val: float = minimal_value

    def set_max(self, maximal_value: float) -> None:
        """
        set the maximal value\n
        :param maximal_value: maximal value of component as float
        :return: None
        """
        # activate maximal value
        self.max: bool = True
        # set maximal value
        self.max_val: float = maximal_value

    def set_lists(self, values: Tuple[list, list, list, int]) -> None:
        """
        set gurobi variable list names \n
        :param values: tuple containing size, binaries and costs variable lists
        """
        # set variable to class variables as lists or int
        self.size.idx = values[0]
        self.size_bin.idx = values[1]
        self.size_yr.idx = values[2]
        self.costs.idx = values[3]

    def get_results(self, x: NDArray[np_float], def_val: NDArray[np_float]):
        if self.size.idx != -1:
            for var in [getattr(self, var) for var in self.__slots__ if isinstance(getattr(self, var), Variable)]:
                var.value = x[var.idx] if var.idx != -1 else def_val

    @abstractmethod
    def calc(self, data: pdDataFrame):
        pass

    def __eq__(self, other) -> bool:
        """
        equality function to check if values are equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has the same values as other
        :return: boolean which is true if self has the same values as other
        """
        # if not of same class return false
        if not isinstance(other, self.__class__):
            return False
        # compare all slot values if one not match return false
        for i in self.__slots__:  # type: ignore
            # get object by name
            obj = getattr(self, i)
            # just compare object if it is not an array
            if not isinstance(obj, NDArray):
                # if the object values does not math return False
                if obj != getattr(other, i):
                    return False
        # if all match return true
        return True

    def __ne__(self, other) -> bool:
        """
        not equality function to check if values are not equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has not the same values as other
        """
        # return the opposite of the equality function
        return not self.__eq__(other)


@dataclass(slots=True)
class NodeConnection(BTClass):
    """
    Node connection class to connect two nodes of the same type
    """
    node_start: Node = Node(-1)
    node_end: Node = Node(-1)
    power_input_start: Variable = Variable()
    power_input_end: Variable = Variable()
    power_output_start: Variable = Variable()
    power_output_end: Variable = Variable()
    bidirectional: bool = False
    eta_start_2_end: float = 1
    eta_end_2_start: float = 1

    def set_nodes(self, start_node: Node, end_node: Node):
        self.node_start = start_node
        self.node_end = end_node

    def activate_bidirectional(self):
        self.bidirectional = True

    def set_efficiencies(self, efficiency_start_2_end: float, efficiency_end_2_start: float = None):
        self.eta_start_2_end = efficiency_start_2_end
        self.eta_end_2_start = efficiency_end_2_start if efficiency_end_2_start is not None else 1.0

    def calc(self, data: pdDataFrame) -> None:
        self._calc_price()
        # check if nodes have the same type
        if self.node_start.kind != self.node_end.kind:
            raise TypeError(f'start node type {self.node_start.kind} is not equal to end node type {self.node_end.kind}'
                            )
        self.node_start.append(self.power_input_start, -1)
        self.node_end.append(self.power_input_start, self.eta_start_2_end)
        if self.bidirectional:
            self.node_end.append(self.power_input_end, -1)
            self.node_start.append(self.power_input_end, self.eta_end_2_start)


@dataclass(slots=True)
class IrrData:
    """
    irradiation class to calculate surface irradiation
    """

    # set default values
    diff_irr: NDArray[np_float] = default_array  # array of diffuse irradiation
    diff_irr_line: str = ''  # column name of diffuse irradiation in pandas pv_data frame
    dir_irr: NDArray[np_float] = default_array  # array of direct irradiation
    dir_irr_line: str = ''  # column name of direct irradiation in pandas pv_data frame
    # Diffuse radiation model (1 = isotropic model, 2 = Klucher Model, 3 = Hay Davis Model, 4 = Reindl Model, 5 =
    # Perez Model
    irr_model: int = 3
    ground: float = 0.  # ground reflectance [-]
    pos: float = 0.  # Position [°S] 0°--> S 90° -->W
    slope: float = 0.  # Slope [°]
    lath: float = 0.  # latitude hour [°]
    lat: float = 0.  # latitude [°]
    long: float = 0.  # longitude [°]
    irr: NDArray[np_float] = default_array  # array of surface irradiation
    irr_line: str = ''  # column name of surface irradiation in pandas pv_data frame
    get_surf_irr: bool = False  # boolean which is True if surface irradiation is known in pandas pv_data frame

    def set_irr_line(self, irr_line: str) -> None:
        """
        set irradiation Line if known
        :param irr_line: column name of surface irradiation in pandas pv_data frame
        :return: None
        """
        # activate boolean for surface irradiation known
        self.get_surf_irr: bool = True
        # save irradiation column name
        self.irr_line: str = irr_line

    def set_diff_dir_irr_line(self, longitude: float, latitude: float, latitude_hour: float, slope: float,
                              position: float, dir_irr_line: str, diff_irr_line: str, ground_reflectance: float = 0.2,
                              irr_model: int = 3) -> None:
        """
        set values for surface irradiation calculation
        :param longitude: longitude [°]
        :param latitude: latitude [°]
        :param latitude_hour: latitude hour [°]
        :param slope: Slope [°]
        :param position: Position [°S] 0°--> S 90° -->W
        :param dir_irr_line: column name of direct irradiation in pandas pv_data frame
        :param diff_irr_line: column name of diffuse irradiation in pandas pv_data frame
        :param ground_reflectance: ground reflectance [-]
        :param irr_model: Diffuse radiation model (1 = isotropic model, 2 = Klucher Model, 3 = Hay Davis Model, 4 =
        Reindl Model, 5 = Perez Model
        :return: None
        """
        # deactivate boolean for surface irradiation known
        self.get_surf_irr: bool = False
        # store values
        self.long = longitude
        self.lat = latitude
        self.lath = latitude_hour
        self.slope = slope
        self.pos = position
        self.ground = ground_reflectance
        self.irr_model = irr_model
        self.dir_irr_line = dir_irr_line
        self.diff_irr_line = diff_irr_line

    def calc_irr(self, data: pdDataFrame) -> None:
        """
        get or calculate surface irradiation
        :param data: pandas dataframe of which to take the pv_data from
        :return: None
        """
        # if surface irradiation is known just read pv_data from pv_data frame
        if self.get_surf_irr:
            self.irr: NDArray[np_float] = read_data(data, self.irr_line, 1)
            return
        # Otherwise, read direct and diffuse irradiation from pv_data frame
        self.dir_irr: NDArray[np_float] = read_data(data, self.dir_irr_line, 1)
        self.diff_irr: NDArray[np_float] = read_data(data, self.diff_irr_line, 1)
        # calculate surface irradiation
        self.irr: NDArray[np_float] = calc_radiation(self.long, self.lat, self.lath, self.ground, self.slope, self.pos,
                                                     self.irr_model, self.dir_irr, self.diff_irr)


@dataclass(slots=True)  # type: ignore
class Storage(BTClass, ABC):
    """
    general storage system class
    """

    # set default values
    max_charge_val: float = 100.
    max_discharge_val: float = 100.
    max_charge_use: bool = True
    max_discharge_use: bool = True
    eta_self_discharge_val: float = 0.
    eta_charge: float = 1.
    eta_discharge: float = 1.
    start: bool = False
    start_val: float = 0.
    eta_self_discharge: NDArray[np_float] = default_array
    max_charge: NDArray[np_float] = default_array
    max_discharge: NDArray[np_float] = default_array
    power_charge: Variable = Variable()
    power_discharge: Variable = Variable()
    energy: Variable = Variable()
    energy_d: Variable = Variable()

    node: Node = Node(-1)

    def set_node(self, node: Node):
        self.node = node

    def set_start(self, start_value: float = None) -> None:
        """
        set year starting method if no start value is used the start value of the year has to be the same as the end of
        the year value. Otherwise, the start value is used as start value for the storage.\n
        :param start_value:
        :return:
        """
        # if no start value is used deactivate the start boolean and used start value = end value
        if start_value is None:
            self.start: bool = False
            return
        # Otherwise, set start value and activate boolean
        self.start: bool = True
        self.start_val: float = start_value

    def set_values(self, eta_self: float = None, eta_charge: float = None, eta_discharge: float = None,
                   max_charge: float = None, max_discharge: float = None) -> None:
        """
        set values for self discharging rate, efficiencies and maximal rates \n
        :param eta_self: self discharging rate [%/h] (default = 0)
        :param eta_charge: charging efficiency [%] (default = 100)
        :param eta_discharge: discharging efficiency [%] (default = 100)
        :param max_charge: maximal charging rate [%/h] (default = not used)
        :param max_discharge: maximal discharge rate [%/h] (default = not used)
        :return: None
        """
        # set efficiencies with default values if None is given
        self.eta_self_discharge_val = 0 if eta_self is None else eta_self
        self.eta_charge = 1 if eta_charge is None else eta_charge / 100
        self.eta_discharge = 1 if eta_discharge is None else eta_discharge / 100
        # set maximal and minimal charging fraction if given
        self.max_charge_use: bool = max_charge is not None
        self.max_charge_val = max_charge if max_charge is not None else -1
        self.max_discharge_use: bool = max_discharge is not None
        self.max_discharge_val = max_discharge if max_discharge is not None else -1

    def _calc_rates(self, dt_h: NDArray[np_float]) -> None:
        """
        calculation of maximal charging and discharging rates and self discharging rate\n
        :param dt_h: time step array in [h]
        """
        # calculate general prices
        self._calc_price()
        # set variables to node
        self.node.append(self.power_charge, -1)
        self.node.append(self.power_discharge, 1)
        # determine self discharge rate
        self.eta_self_discharge: NDArray[np_float] = (1 - self.eta_self_discharge_val / 100 * dt_h)
        # determine maximal charging and discharging rates if wanted to
        if self.max_charge_use:
            self.max_charge_use = not(isclose(max(dt_h), min(dt_h), rel_tol=0.0001) and
                                      isclose(max(dt_h), self.max_charge_val / 100, rel_tol=0.0001))
            self.max_charge: NDArray[np_float] = (self.max_charge_val / 100) * dt_h if self.max_charge_use else \
                default_array
        if self.max_discharge_use:
            self.max_discharge_use = not (isclose(max(dt_h), min(dt_h), rel_tol=0.0001) and
                                          isclose(max(dt_h), self.max_discharge_val / 100, rel_tol=0.0001))
            self.max_discharge: NDArray[np_float] = (self.max_discharge_val / 100) * dt_h if \
                self.max_discharge_use else default_array


@dataclass(slots=True)  # type: ignore
class ThermalStorage(Storage, ABC):
    """
    thermal storage system class
    """
    # set default values
    determine_eta_self: bool = False
    th_cap: float = 4182.  # thermal capacity of the fluid [J/kg K]
    density: float = 1000.  # density of the fluid [kg/m³]
    heat_loss: float = 9.5  # thermal heat loss [W/K]
    t_min: float = 35.  # minimal storage temperature
    t_diff: float = 10.  # temperature difference of thermal storage [K]
    t_ambient: float = 20.  # temperature around the storage
    use_max_ch: bool = False
    use_max_dis: bool = False
    t_depending_loss: bool = False
    num_t_steps: int = 1
    t_array: NDArray[np_float] = default_array
    th_l: Variable = Variable()
    th_n: Variable = Variable()
    th_l_on: Variable = Variable()
    th_l_of: Variable = Variable()
    th_b: Variable = Variable()
    pr_lin_kwh: float = 0.

    def set_thermal_properties(self, th_cap: float = None, density: float = 1000.,
                               temperature_difference: float = 10.) -> None:
        """
        set thermal properties of thermal storage\n
        :param th_cap: thermal capacity of the fluid [J/kg K] (default = 4182 J/kgK)
        :param density: density of the fluid [kg/m³] (default = 1000 kg/m²)
        :param temperature_difference: temperature difference of thermal storage [K] (default = 10 K)
        """
        # set default values if no value is given
        self.th_cap: float = 4182. if th_cap is None else th_cap
        self.density: float = 1000. if density is None else density
        self.t_diff: float = 10. if temperature_difference is None else temperature_difference

    def determine_self_discharge(self, heat_loss: float, minimal_temperature: float = None,
                                 ambient_temperature: float = None) -> None:
        """
        determine self discharge by heat loss factor [W/K]
        :param heat_loss: heat loss factor [W/K]
        :param minimal_temperature: minimal storage temperature [°C] (default = 35 °C)
        :param ambient_temperature: ambient temperature [°C] (default = 20 °C)
        """
        # set default values if no value is given
        self.determine_eta_self: bool = True
        self.heat_loss: float = heat_loss
        self.t_min: float = 35 if minimal_temperature is None else minimal_temperature
        self.t_ambient: float = 20 if ambient_temperature is None else ambient_temperature

    def set_temperature_depending_loss(self, number_of_temperatures_steps) -> None:
        self.t_depending_loss: bool = True
        self.num_t_steps: int = number_of_temperatures_steps

    def set_maximal_dis_charging(self, maximal_charging: float = None, maximal_discharging: float = None) -> None:
        maximal_charging = 100. if maximal_charging is None else maximal_charging
        self.use_max_ch: bool = False if maximal_charging is None else True
        maximal_discharging = 100. if maximal_discharging is None else maximal_discharging
        self.use_max_dis: bool = False if maximal_discharging is None else True
        self.set_values(max_charge=maximal_charging, max_discharge=maximal_discharging)

    def _calc_temperatures(self, dt_h: NDArray[np_float]) -> None:
        self._calc_rates(dt_h)
        self.pr_lin_kwh: float = self.pr_lin_val
        self.pr_lin: list = [i / self.density / self.th_cap / self.t_diff * 3600 * 1000 for i in self.pr_lin]
        if self.t_depending_loss:
            num_nt = self.num_t_steps
            self.t_array: NDArray[np_float] = (self.t_min + self.t_diff * linspace(1, num_nt, num_nt) / num_nt -
                                               self.t_diff / (2 * num_nt))
            self.eta_self_discharge: NDArray[np_float] = (1 - self.heat_loss * (self.t_array - self.t_ambient) * 3600 /
                                                          (self.density * self.th_cap * self.t_diff))
            return
        if self.determine_eta_self:
            self.eta_self_discharge = 1 - (self.heat_loss * ((self.t_min + self.t_diff) - self.t_ambient)
                                           * 3600 * dt_h / (self.density * 1 * self.th_cap * self.t_diff))


@dataclass(slots=True)
class SellingPower:
    """
    Selling Power class
    """
    # set init variables of parent class
    time_step_years: int
    sell_power: bool = False
    cSellConst: bool = False
    cSellVal: float = 0.
    c_sell_c: float = 0.
    c_sell_f_Line: str = ''
    c_sell_f: NDArray[np_float] = default_array
    power: Variable = Variable()
    power_sell: Variable = Variable()

    node: Node = Node(-1)

    def set_node(self, node: Node):
        self.node = node

    def __post_init__(self):
        [setattr(self, var, Variable()) for var in self.__slots__ if isinstance(getattr(self, var), Variable)]

    def set_constant_selling_price(self, price: float) -> None:
        """
        set constant selling price\n
        :param price: reward for selling of energy
        """
        # just consider selling if price is not 0
        if not isclose(price, 0, rel_tol=0.000001):
            self.sell_power: bool = True
            self.cSellConst: bool = True
            self.cSellVal: float = price
            return
        self.sell_power: bool = False
        self.cSellConst: bool = False

    def set_flexible_selling_price(self, column_name: str) -> None:
        """
        set hourly flexible selling price by get it from pandas dataframe \n
        :param column_name: column name of pandas dataframe including selling price
        """
        self.sell_power: bool = True
        self.cSellConst: bool = False
        self.c_sell_f_Line: str = column_name

    def calc_selling_prices(self, data: pdDataFrame, rate_of_interest: float):
        self.time_step_years: int = int(self.time_step_years)
        if self.sell_power:
            self.node.append(self.power_sell, -1)
            if self.cSellConst:
                self.c_sell_c: float = npv(self.cSellVal, self.time_step_years, rate_of_interest)
                return
            self.c_sell_f: NDArray[np_float] = read_data(data, self.c_sell_f_Line, 1) * self.time_step_years

    def get_sell_power_results(self, x: NDArray[np_float]):
        if self.sell_power:
            self.power_sell.value = x[self.power_sell.idx]


@dataclass(slots=True)
class ProducedPower:

    calc_power: bool = False
    PowerLine: str = ''
    power: NDArray[np_float] = default_array
    power_var: Variable = Variable()
    f_unit_power: float = 1.

    node: Node = Node(-1)

    def __post_init__(self):
        [setattr(self, var, Variable()) for var in self.__slots__ if isinstance(getattr(self, var), Variable)]

    def set_node(self, node: Node):
        self.node = node

    def calc_power_function(self, data: pdDataFrame) -> NDArray:
        pass

    def get_power_from_data(self, column_name: str, unit_factor: float = None):
        self.calc_power: bool = False
        self.PowerLine: str = column_name
        self.f_unit_power: float = unit_factor if unit_factor is not None else 1.

    def calculate_power(self, data: pdDataFrame) -> None:
        self.node.append(self.power_var, 1)
        if not self.calc_power:
            self.power: NDArray[np_float] = read_data(data, self.PowerLine, self.f_unit_power)
            return
        self.power = self.calc_power_function(data)

    def get_produced_power_results(self, x: NDArray[np_float]):
        if self.power_var.idx != 0:
            self.power_var.value = x[self.power_var.idx]


@dataclass(slots=True)
class AirHPData(BTClass):
    """
    air water heat pump class
    """
    # initialize values
    constant_cop: bool = False
    cop: float = 0.
    cop_2: float = 0.
    lin: float = 0.
    con: float = 0.
    t_amb_line: str = ''
    t_amb: NDArray[np_float] = default_array
    th_power: NDArray[np_float] = default_array
    th_power_2: NDArray[np_float] = default_array
    el_flexible: bool = False
    el_lin: float = 0.
    el_con: float = 0.
    num_nt: int = 1
    min_temp_hot_tank: float = 0.
    temp_diff_hot_tank: float = 0.
    temps_hot_tank: NDArray[np_float] = default_array
    el_power: NDArray[np_float] = default_array
    cop_flex: NDArray[np_float] = default_array
    binary_status: bool = False
    minimal_runtime: bool = False
    minimal_runtime_val: int = 1
    minimal_power: bool = False
    minimal_power_val: float = 0.
    cooling: bool = False
    cop_cool: float = 1.
    import_flexible_heat: bool = False
    th_power_line: str = ''
    th_power_line_2: str = ''
    cop_flex_cool: NDArray[np_float] = default_array
    el_power_frac: float = 0.
    flexible_cop: bool = False
    eta_exergy: float = 0.
    t_hot: float = 0.
    t_hot_2: float = 0.
    second_node: bool = False
    power_el_var: Variable = Variable()
    power_heat: Variable = Variable()
    power_heat_2: Variable = Variable()
    power_cool: Variable = Variable()
    binary: Variable = Variable()
    electrical_node: Node = Node(-1)
    heating_node: Node = Node(-1)
    heating_node_2: Node = Node(-1)
    cooling_node: Node = Node(-1)

    def set_constant_cop(self, cop: float, cop_second_node: float = None) -> None:
        """
        set a constant cop of air water heat pump
        :param cop: coefficient of performance [-]
        :param cop_second_node: coefficient of performance for second node[-]
        :return: None
        """
        self.constant_cop: bool = True  # activate constant cop
        self.cop: float = cop  # set cop
        self.cop_2: float = cop_second_node if cop_second_node is not None else self.cop_2

    def activate_second_heating_node(self, second_node: Node) -> None:
        """
        activate second temperature level for a second heating node
        :param second_node: node for second heating level
        :return: None
        """
        self.second_node = True
        self.heating_node_2 = second_node

    def set_flexible_cop(self, t_amb_column: str, eta_exergy: float, t_hot: float = 40) -> None:
        """
        set ambient temperature depending cop (cop = eta_exergy * t_hot / t_amb
        :param t_amb_column: ambient temperature column in data file
        :param eta_exergy: exergetic efficiency
        :param t_hot: hot side temperature
        """
        self.constant_cop: bool = False  # deactivate constant cop
        self.import_flexible_heat: bool = False  # activate that the flexible heat should be read from pv_data frame
        self.flexible_cop: bool = True
        self.t_amb_line: str = t_amb_column
        self.eta_exergy: float = eta_exergy
        self.t_hot: float = t_hot
        self.th_power: float = 1_000.

    def set_flexible_heat_and_cop(self, electrical_power_fraction: float, thermal_power_column_name: str) -> None:
        """
        set flexible cop which is imported from pandas dataframe by column with name thermal_power_column_name
        :param electrical_power_fraction: electrical power fraction [Wel/kWth]
        :param thermal_power_column_name: pandas dataframe column name with thermal power values [W/kWth]
        :return: None
        """
        self.constant_cop: bool = False  # deactivate constant cop
        self.import_flexible_heat: bool = True  # activate that the flexible heat should be read from pv_data frame
        self.el_power_frac: float = electrical_power_fraction  # set electrical power fraction in Wel per kWth
        self.el_power: float = self.el_power_frac  # set electrical power
        self.th_power_line: str = thermal_power_column_name  # set column name of which thermal power should be read

    def calculate_flexible_heat_and_cop(self, electrical_power_fraction: float, linear_factor: float,
                                        constant_factor: float, ambient_temperature_column_name: str) -> None:
        """
        set values for temperature depending on cop calculation \n
        th_power = linear_factor*t_ambient + constant_factor \n
        cop = th_power/electrical_power_fraction \n
        :param electrical_power_fraction: electrical power fraction [Wel/kWth]
        :param linear_factor: linear thermal power factor [W/Wth °C]
        :param constant_factor: constant thermal power factor [W/kWth]
        :param ambient_temperature_column_name: ambient temperature column name in pandas dataframe [°C]
        :return: None
        """
        self.constant_cop: bool = False  # deactivate constant cop
        self.import_flexible_heat: bool = False  # deactivate that the flexible heat should be read from pv_data frame
        self.el_power_frac: float = electrical_power_fraction  # set electrical power fraction
        self.el_power: float = self.el_power_frac  # set electrical power
        self.lin: float = linear_factor  # set linear ambient temperature coefficient
        self.con: float = constant_factor  # set constant coefficient
        self.t_amb_line: str = ambient_temperature_column_name  # set  ambient temperature column name in dataframe

    def set_warm_side_temperature_depending_cop(self, linear_factor: float, constant_factor: float,
                                                number_of_temperatures_steps: int, hot_tank_minimal_temperature: float,
                                                hot_tank_temperature_difference: float) -> None:
        self.el_flexible: bool = True
        self.el_lin: float = linear_factor
        self.el_con: float = constant_factor
        self.num_nt: int = number_of_temperatures_steps
        self.min_temp_hot_tank: float = hot_tank_minimal_temperature
        self.temp_diff_hot_tank: float = hot_tank_temperature_difference

    def set_minimal_runtime(self, runtime: int, minimal_power: float) -> None:
        self.minimal_runtime: bool = True
        self.minimal_runtime_val: int = runtime
        self.set_minimal_power(minimal_power)

    def set_minimal_power(self, minimal_power: float) -> None:
        self.minimal_power: bool = True
        self.minimal_power_val: float = minimal_power

    def set_nodes(self, electrical_node: Node, heating_node: Node, cooling_node: Node):
        self.electrical_node = electrical_node
        self.heating_node = heating_node
        self.cooling_node = cooling_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_cooling_node(self, cooling_node: Node):
        self.cooling_node = cooling_node

    def set_heating_node(self, heating_node: Node):
        self.heating_node = heating_node

    def activate_binary_status(self) -> None:
        self.binary_status: bool = True
        self.minimal_power_val: float = 1

    def activate_cooling(self) -> None:
        self.cooling: bool = True

    def calc(self, data: pdDataFrame) -> None:
        self._calc_price()
        self.electrical_node.append(self.power_el_var, -1)
        self.heating_node.append(self.power_heat, 1)
        if self.second_node:
            self.heating_node_2.append(self.power_heat_2, 1)
        if not self.constant_cop and not self.import_flexible_heat and not self.flexible_cop:
            self.t_amb: NDArray[np_float] = read_data(data, self.t_amb_line, 1)
            self.th_power: NDArray[np_float] = self.lin * self.t_amb + self.con
        if not self.constant_cop and self.import_flexible_heat and not self.flexible_cop:
            self.th_power: NDArray[np_float] = read_data(data, self.th_power_line, 1)
        if self.flexible_cop:
            self.t_amb: NDArray[np_float] = read_data(data, self.t_amb_line, 1)
            cop: NDArray[np_float] = self.eta_exergy * (self.t_hot + 273.15) / (self.t_hot - self.t_amb)
            self.el_power: NDArray[np_float] = self.th_power / cop
        if self.el_flexible:
            num_nt = self.num_nt
            self.temps_hot_tank: NDArray[np_float] = (self.min_temp_hot_tank + self.temp_diff_hot_tank *
                                                      linspace(1, num_nt, num_nt) / num_nt -
                                                      self.temp_diff_hot_tank / (2 * num_nt))
            self.el_power: NDArray[np_float] = self.el_con + self.el_lin * self.temps_hot_tank
        if not self.el_flexible and not self.constant_cop:
            self.cop_flex: NDArray[np_float] = self.th_power / self.el_power
        if self.cooling:
            self.cooling_node.append(self.power_cool, 1)
            if self.constant_cop:
                self.cop_cool: float = self.cop - 1
            if not self.el_flexible and not self.constant_cop:
                self.cop_flex_cool: NDArray[np_float] = (self.th_power - self.el_power) / self.el_power


@dataclass(slots=True)
class GeoHPData(BTClass):
    """
    water-water heat pump class
    """
    # initialize values
    constant_cop: bool = False
    second_node: bool = False
    cop: float = 0.
    cop_2: float = 0.
    binary_status: bool = False
    minimal_power: bool = False
    minimal_power_val: float = 1.
    power_heat: Variable = Variable()
    power_heat_2: Variable = Variable()
    power_el: Variable = Variable()
    power_cool: Variable = Variable()
    binary: Variable = Variable()

    cool_node: Node = Node(-1)
    heat_node: Node = Node(-1)
    heat_node_2: Node = Node(-1)
    electrical_node: Node = Node(-1)

    def set_node(self, electrical_node: Node, heat_node: Node, cool_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.cool_node = cool_node

    def get_results(self, x: NDArray[np_float], def_val: NDArray[np_float]):
        super(GeoHPData, self).get_results(x, def_val)
        self.power_cool.value = self.power_heat.value - self.power_el.value + (
            self.power_heat_2.value if self.second_node else 0)

    def activate_binary_status(self) -> None:
        self.binary_status: bool = True
        self.minimal_power_val: float = 1

    def set_minimal_power(self, minimal_power: float) -> None:
        self.minimal_power: bool = True
        self.minimal_power_val: float = minimal_power

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heating_node(self, heating_node: Node):
        self.heat_node = heating_node

    def set_first_heating_node(self, heating_node: Node):
        self.set_heating_node(heating_node)

    def set_second_heating_node(self, heating_node: Node):
        self.heat_node_2 = heating_node

    def set_first_constant_cop(self, cop: float) -> None:
        self.set_constant_cop(cop)

    def set_second_constant_cop(self, cop: float) -> None:
        self.cop_2 = cop

    def activate_second_node(self, first_node: Node, second_node: Node, first_cop: float, second_cop: float):
        self.second_node = True
        self.set_first_heating_node(first_node)
        self.set_second_heating_node(second_node)
        self.set_first_constant_cop(first_cop)
        self.set_second_constant_cop(second_cop)

    def set_cooling_node(self, cooling_node: Node):
        self.cool_node = cooling_node

    def set_constant_cop(self, cop: float) -> None:
        """
        set a constant cop of air water heat pump
        :param cop: coefficient of performance [-]
        :return: None
        """
        self.constant_cop: bool = True  # activate constant cop
        self.cop: float = cop  # set cop

    def calc(self, data: pdDataFrame) -> None:
        self._calc_price()
        self.electrical_node.append(self.power_el, -1)
        self.heat_node.append(self.power_heat, -1 if self.heat_node.kind == IDX_COOLING else 1)
        self.cool_node.append(self.power_heat, 1 if self.cool_node.kind == IDX_COOLING else -1)
        self.cool_node.append(self.power_el, -1 if self.cool_node.kind == IDX_COOLING else 1)
        if self.second_node:
            self.heat_node_2.append(self.power_heat_2, -1 if self.heat_node_2.kind == IDX_COOLING else 1)
            self.cool_node.append(self.power_heat_2, 1 if self.cool_node.kind == IDX_COOLING else -1)


@dataclass(slots=True)
class GeoData(BTClass):
    """
    geothermal system class
    """

    # initialize values
    Const: bool = False
    Gain: float = 0.
    frac_el: float = 0.
    Stor: bool = False
    TEarth: float = 0.
    Cap: float = 0.
    Earth: float = 0.
    RHole: float = 0.
    NRadial: int = 1
    RLam: float = 0.
    RbStar: float = 0.
    REarth: float = 0.
    t_min_cold_tank: float = 0.
    t_max_cold_tank: float = 0.
    eta_i: NDArray[np_float] = default_array
    eta_a: NDArray[np_float] = default_array
    CapPerLMin: NDArray[np_float] = default_array
    CapPerLMax: NDArray[np_float] = default_array
    power_heat: Variable = Variable()
    power_cool: Variable = Variable()
    power_el: Variable = Variable()

    cool_node: Node = Node(-1)
    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)

    def set_node(self, electrical_node: Node, heat_node: Node, cool_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.cool_node = cool_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heating_node(self, heating_node: Node):
        self.heat_node = heating_node

    def set_cooling_node(self, cooling_node: Node):
        self.cool_node = cooling_node

    def set_constant_gain(self, gain: float):
        self.Const: bool = True
        self.Stor: bool = False
        self.Gain: float = gain

    def set_electrical_demand(self, fraction: float) -> None:
        """
        Aktivate electrical demand fraction of borehole
        :param fraction: electrical demand fraction of borehole [Wel/Wth]
        """
        self.frac_el: float = fraction

    def simulate_storage(self, t_earth: float, th_cap: float, r_earth: float, r_borehole: float, n_radial: int,
                         th_conductivity: float, borehole_resistance: float, t_min_cold_tank: float = 0.,
                         t_max_cold_tank: float = 10.) -> None:
        """
        simulate geothermal system with a radial storage
        :param t_earth: Earth Temperature [°C]
        :param th_cap: heat capacity of soil [MJ/m³K]:
        :param r_earth: to considering earth radius [m]
        :param r_borehole: borehole radius [m]
        :param n_radial: number of radial regions [-]
        :param th_conductivity: thermal conductivity of soil [W/mK]
        :param borehole_resistance: total borehole resistance [mK/W]
        :param t_min_cold_tank: minimal cold tank temperature [°C]
        :param t_max_cold_tank: maximal cold tank temperature [°C]
        :return: None
        """
        self.Const: bool = False
        self.Stor: bool = True
        self.TEarth: float = t_earth
        self.Cap: float = th_cap
        self.REarth: float = r_earth
        self.RHole: float = r_borehole
        self.NRadial: int = n_radial
        self.RLam: float = th_conductivity
        self.RbStar: float = borehole_resistance
        self.t_min_cold_tank: float = t_min_cold_tank
        self.t_max_cold_tank: float = t_max_cold_tank

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.cool_node.append(self.power_cool, 1 if self.cool_node.kind == IDX_COOLING else -1)
        self.heat_node.append(self.power_heat, -1 if self.heat_node.kind == IDX_COOLING else 1)
        if self.frac_el > 0:
            self.electrical_node.append(self.power_el, -1)
        if self.Const:
            return
        from math import pi
        from numpy import log
        r_b = linspace(self.RHole, self.REarth, self.NRadial + 1)
        r_m = zeros(self.NRadial + 2)
        r_m[1:-1] = (r_b[0:-1] + r_b[1:]) / 2
        r_m[0] = r_b[0]
        r_m[-1] = r_b[-1]
        area = pi * (r_b[1:] * r_b[1:] - r_b[0:-1] * r_b[0:-1])
        log_r = log(r_m[1:] / r_m[0:-1])
        self.eta_i = 2 * pi * self.RLam * 3600 / (log_r[0:-1] * self.Cap * area * 1_000_000)
        self.eta_a = 2 * pi * self.RLam * 3600 / (log_r[1:] * self.Cap * area * 1_000_000)
        self.eta_i[0] = 1/(self.RbStar+log_r[0]/(2 * pi * self.RLam)) * (self.TEarth-self.t_min_cold_tank)
        self.CapPerLMin = area * self.Cap * (self.TEarth - self.t_max_cold_tank)/3.6
        self.CapPerLMax = area * self.Cap * (self.TEarth - self.t_min_cold_tank)/3.6
        return


@dataclass(slots=True)
class BatData(Storage):
    """
    battery storage system class
    """

    def calc(self, dt_h: NDArray[np_float]):
        self._calc_rates(dt_h)


@dataclass(slots=True)
class HTData(ThermalStorage):
    """
    hot tank class
    """

    def calc(self, dt_h: array):
        self._calc_price()
        self._calc_temperatures(dt_h)


@dataclass(slots=True)
class CTData(ThermalStorage):
    """
    cold tank class
    """

    def calc(self, dt_h: array):
        self._calc_price()
        self._calc_temperatures(dt_h)


@dataclass(slots=True)
class PVData(BTClass):
    """
    photovoltaic class\n
    """
    time_step_years: int = 20
    selling_power: SellingPower = SellingPower(time_step_years)
    irradiation_data: IrrData = IrrData()
    produced_power: ProducedPower = ProducedPower()
    Eff: float = 0.18
    TCoe: float = 0.002
    pr: float = 0.8
    t_noct: float = 44.
    t_ambient_line: str = ''
    t_ambient: array = default_array
    use_el_demand: bool = False
    el_demand: float = 0.
    power_demand: Variable = Variable()

    electrical_node: Node = Node(-1)

    def __post_init__(self):
        super(PVData, self).__post_init__()
        self.selling_power = SellingPower(self.time_step_years)
        self.produced_power = ProducedPower()

    def set_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_time_step_years(self, time_step_years: int):
        self.time_step_years: int = time_step_years
        self.selling_power: SellingPower = SellingPower(self.time_step_years)

    def get_results(self, x: NDArray, def_val: NDArray[np_float]):
        if self.produced_power.power_var.idx != 0:
            super(PVData, self).get_results(x, def_val)
            self.selling_power.get_sell_power_results(x)
            self.produced_power.get_produced_power_results(x)

    def set_electrical_demand(self, power_frac: float) -> None:
        """
        set constant electrical demand for example of the inverter as a fraction of area.\n
        :param power_frac: fraction of electrical power demand per area [kW/m²]
        """
        self.use_el_demand: bool = True
        self.el_demand: float = power_frac

    def set_calculate_power(self, t_ambient_column_name: str, efficiency: float, t_coefficient: float,
                            t_noct: float = 44., perf_ratio: float = 0.8) -> None:
        """
        calculate power with efficiency depending on ambient temperature model \n
        t_c = t_ambient + (t_noct - 25) * irr / 800 \n
        power = efficiency * irr * (1 + t_coefficient * (t_c - 25)) * perf_ratio \n
        :param t_ambient_column_name: column name of pandas dataframe which contains the ambient temperature
        :param efficiency: Efficiency [-]
        :param t_coefficient: temperature coefficient [%/°C]
        :param t_noct: temperature under NOCT conditions [°C]
        :param perf_ratio: performance ratio [-]
        """
        self.produced_power.calc_power = True
        self.t_ambient_line: str = t_ambient_column_name
        self.Eff: float = efficiency
        self.TCoe: float = t_coefficient
        self.t_noct: float = t_noct
        self.pr: float = perf_ratio

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.max_val: float = self.max_val if self.max else 357_582 * 1_000_000
        self.selling_power.set_node(self.electrical_node)
        self.selling_power.calc_selling_prices(data, self.rate_of_interest)
        self.produced_power.set_node(self.electrical_node)
        self.produced_power.calculate_power(data)
        if self.use_el_demand:
            self.electrical_node.append(self.power_demand, -1)

    def calc_power_function(self, data: pdDataFrame) -> NDArray:
        self.irradiation_data.calc_irr(data)
        self.t_ambient: NDArray[np_float] = read_data(data, self.t_ambient_line, 1)
        return calc_pv(self.Eff, self.TCoe, self.t_ambient, self.t_noct, self.irradiation_data.irr) * self.pr


@dataclass(slots=True)
class BoilerData(BTClass):
    """
    boiler class\n
    """
    eta: float = 1.
    power_fuel: Variable = Variable()
    power_heat: Variable = Variable()

    fuel_node: Node = Node(-1)
    heat_node: Node = Node(-1)

    def set_nodes(self, heat_node: Node, fuel_node: Node):
        self.heat_node = heat_node
        self.fuel_node = fuel_node

    def set_heat_node(self, heat_node: Node):
        self.heat_node = heat_node

    def set_fuel_node(self, fuel_node: Node):
        self.fuel_node = fuel_node

    def set_efficiency(self, efficiency: float = 1.) -> None:
        self.eta: float = efficiency

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.heat_node.append(self.power_heat, 1)
        self.fuel_node.append(self.power_fuel, -1)


@dataclass(slots=True)
class CHPData(BTClass):
    """
    Combined heat and power class
    """
    time_step_years: int = 20
    selling_power: SellingPower = SellingPower(time_step_years)
    eta_el: float = 0.
    eta_th: float = 0.
    minimal_power: bool = False
    minimal_power_val: float = 0.
    minimal_runtime: bool = False
    minimal_runtime_val: int = 1
    is_limit_set: bool = False
    heat_demand: str = ''
    el_demand: str = ''
    limit_max: bool = False
    active_max_gradient_up: bool = False
    max_gradient_up: float = -1.0
    active_max_gradient_down: bool = False
    max_gradient_down: float = -1.0
    limit_array: NDArray[np_float] = default_array
    power_fuel: Variable = Variable()
    power_heat: Variable = Variable()
    power_el: Variable = Variable()
    power_el_min: Variable = Variable()
    power_el_min_tr: Variable = Variable()
    power_el_max: Variable = Variable()
    power_el_max_tr: Variable = Variable()
    power_el_binary: Variable = Variable()

    fuel_node: Node = Node(-1)
    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)

    def set_nodes(self, electrical_node: Node, heat_node: Node, fuel_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.fuel_node = fuel_node

    def set_heat_node(self, heat_node: Node):
        self.heat_node = heat_node

    def set_fuel_node(self, fuel_node: Node):
        self.fuel_node = fuel_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_time_step_years(self, time_step_years: int):
        self.time_step_years: int = time_step_years
        self.selling_power: SellingPower = SellingPower(self.time_step_years)

    def get_results(self, x: NDArray, def_val: NDArray[np_float]):
        if self.power_fuel.idx != 0:
            super(CHPData, self).get_results(x, def_val)
            self.selling_power.get_sell_power_results(x)

    def set_efficiencies(self, electrical_efficiency: float, thermal_efficiency: float):
        self.eta_el: float = electrical_efficiency
        self.eta_th: float = thermal_efficiency

    def set_minimal_runtime(self, runtime: int, minimal_power: float) -> None:
        self.minimal_runtime: bool = True
        self.minimal_runtime_val: int = runtime
        self.set_minimal_power(minimal_power)

    def set_minimal_power(self, minimal_power: float) -> None:
        self.minimal_power: bool = True
        self.minimal_power_val: float = minimal_power

    def activate_binary(self) -> None:
        self.set_minimal_power(1)

    def activate_max_gradient_up(self, max_gradient: float) -> None:
        """
        activate the maximal upward gradient per hour\n
        :param max_gradient: maximal gradient for upward power per component size [1/h]
        :return:
        """
        # activate boolean
        self.active_max_gradient_up = True
        # save max gradient
        self.max_gradient_up = max_gradient

    def activate_max_gradient_down(self, max_gradient: float) -> None:
        """
        activate the maximal downward gradient per hour\n
        :param max_gradient: maximal gradient for downward power per component size [1/h]
        :return:
        """
        # activate boolean
        self.active_max_gradient_down = True
        # save max gradient
        self.max_gradient_down = max_gradient

    def set_limit(self, limit_max: bool = None, electrical_demand_line: str = None, heat_demand_line: str = None):
        self.is_limit_set = True
        self.limit_max = limit_max if limit_max is not None else False
        self.el_demand = electrical_demand_line
        self.heat_demand = heat_demand_line

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.electrical_node.append(self.power_el, 1)
        self.heat_node.append(self.power_heat, 1)
        self.fuel_node.append(self.power_fuel, -1)
        self.selling_power.set_node(self.electrical_node)
        self.selling_power.calc_selling_prices(data, self.rate_of_interest)
        if self.is_limit_set:
            el_demand: NDArray[np_float] = read_data(data, self.el_demand, 1) if self.el_demand is not None else \
                default_array
            heat_demand: NDArray[np_float] = read_data(data, self.heat_demand, 1) * self.eta_el / self.eta_th if \
                self.heat_demand is not None else default_array
            if self.limit_max:
                max_val: float = el_demand.max() if self.el_demand is not None else 0
                max_val = max(heat_demand.max(), max_val) if self.heat_demand is not None else max_val
                self.set_max(max_val)
            if self.el_demand is not None:
                if self.heat_demand is not None:
                    self.limit_array: NDArray[np_float] = maximum(heat_demand, el_demand)
                    return
                self.limit_array = el_demand
                return
            self.limit_array = heat_demand


@dataclass(slots=True)
class WindData(BTClass):
    """
    wind turbine class\n
    """
    time_step_years: int = 20
    selling_power: SellingPower = SellingPower(0)
    produced_power: ProducedPower = ProducedPower()
    # set default values
    read_velocity: bool = False
    nominal_power: float = 0.
    nominal_velocity: float = 0.
    minimal_velocity: float = 0.
    maximal_velocity: float = 0.
    height: float = 0.
    wind_model: int = 0
    wind_model_constant: int = 0
    wind_model_constant_user: float = 0.
    VelocityHeightLine: str = ''
    velocity_line: str = ''
    velocity_at_height: NDArray[np_float] = default_array
    velocity: NDArray[np_float] = default_array

    node: Node = Node(-1)

    def __post_init__(self):
        super(WindData, self).__post_init__()
        self.selling_power = SellingPower(self.time_step_years)
        self.produced_power = ProducedPower()

    def set_node(self, node: Node):
        self.node = node

    def set_time_step_years(self, time_step_years: int):
        self.time_step_years: int = time_step_years
        self.selling_power: SellingPower = SellingPower(self.time_step_years)

    def get_results(self, x: NDArray[np_float], def_val: NDArray[np_float]):
        if self.size.idx != 0:
            super(WindData, self).get_results(x, def_val)
            self.selling_power.get_sell_power_results(x)
            self.produced_power.get_produced_power_results(x)

    def calculate_power(self, nom_power: float, velocity_at_nom_power: float, minimal_velocity: float,
                        maximal_velocity: float) -> None:
        """
        calculate wind turbine power per kW
        :param nom_power: nominal power [W]
        :param velocity_at_nom_power: velocity at nominal power [m/s]
        :param minimal_velocity: minimal velocity [m/s]
        :param maximal_velocity: maximal velocity [m/s]
        """
        self.produced_power.calc_power = True
        self.nominal_power: float = nom_power
        self.nominal_velocity: float = velocity_at_nom_power
        self.minimal_velocity: float = minimal_velocity
        self.maximal_velocity: float = maximal_velocity

    def calc_velocity_at_turbine(self, velocity_line: str, rotor_height: float, wind_model: int,
                                 wind_model_constant_index: int, wind_model_constant_user: float = 0.5) -> None:
        """
        calculate the velocity at rotor height by one of two models. velocity on 10 m height is needed. \n
        :param velocity_line: column name of velocity on 10 m height in pv_data frame [m/s]
        :param rotor_height: height of rotor [m]
        :param wind_model: 0 = logarithmic wind profile; 1 = power law of Hellmann
        :param wind_model_constant_index: Hellmann indexes = [0.27, 0.34, 0.60, StrUser]; log indexes =
        [0.0002, 0.005, 0.03, 0.1, 0.25, 0.5, 1, 2, StrUser]
        :param wind_model_constant_user: if wind_model_constant_index selected StrUser index a user defined value can
        be used
        """
        self.read_velocity: bool = False
        self.velocity_line: str = velocity_line
        self.height: float = rotor_height
        self.wind_model: int = wind_model
        self.wind_model_constant: int = wind_model_constant_index
        self.wind_model_constant_user: float = wind_model_constant_user

    def get_velocity_from_data(self, column_name: str) -> None:
        self.read_velocity: bool = True
        self.velocity_line: str = column_name

    def calc(self, data: pdDataFrame) -> None:
        self._calc_price()
        self.selling_power.set_node(self.node)
        self.selling_power.calc_selling_prices(data, self.rate_of_interest)
        self.produced_power.set_node(self.node)
        self.produced_power.calculate_power(data)

    def calc_power_function(self, data: pdDataFrame) -> NDArray:
        if self.read_velocity:
            self.velocity_at_height: NDArray[np_float] = read_data(data, self.VelocityHeightLine, 1)
        else:
            self.velocity: NDArray[np_float] = read_data(data, self.velocity_line, 1)
            self.velocity_at_height: NDArray[np_float] = calc_wind_at_height(self.velocity, self.height,
                                                                             self.wind_model, self.wind_model_constant,
                                                                             self.wind_model_constant_user)
        return CalcWindPower(self.velocity_at_height, self.nominal_power, self.nominal_velocity, self.minimal_velocity,
                             self.maximal_velocity)


@dataclass(slots=True)
class FuelCellData(BTClass):
    """
    fuel cell class\n
    """

    eta_el: float = 0.4
    use_waste_heat: bool = False
    eta_th: float = 0.2
    use_constant_eta: bool = True
    use_weak_flexible_eta: bool = False
    use_linear_flexible_eta: bool = False
    use_constant_flexible_eta: bool = False
    list_eta_flex: List[float] = field(default_factory=lambda: [])
    list_eta_flex_use: List[float] = field(default_factory=lambda: [])
    len_eta_flex: int = 0
    power_heat: Variable = Variable()
    power_hydro: Variable = Variable()
    power_el: Variable = Variable()

    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)
    hydro_node: Node = Node(-1)

    def set_nodes(self, electrical_node: Node, heat_node: Node, hydro_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.hydro_node = hydro_node

    def set_hydrogen_node(self, hydrogen_node: Node):
        self.hydro_node = hydrogen_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heating_node(self, heating_node: Node):
        self.heat_node = heating_node

    def set_electrical_efficiency(self, electrical_efficiency: float) -> None:
        self.use_weak_flexible_eta: bool = False
        self.eta_el: float = electrical_efficiency

    def set_use_waste_heat(self, thermal_efficiency: float) -> None:
        self.use_waste_heat: bool = True
        self.eta_th: float = thermal_efficiency

    def set_flexible_weak_efficiency(self, list_eta_flex: List[float]) -> None:
        """
        set list of flexible efficiencies
        :param list_eta_flex: list of floats with electrical efficiency with equidistant space
        :return: None
        """
        check_efficiency(list_eta_flex)
        self.use_weak_flexible_eta: bool = True
        self.use_constant_eta: bool = False
        self.list_eta_flex = list_eta_flex

    def set_flexible_linear_efficiency(self, list_eta_flex: List[float]) -> None:
        """
        set list of flexible efficiencies
        :param list_eta_flex: list of floats with electrical efficiency with equidistant space
        :return: None
        """
        self.use_linear_flexible_eta: bool = True
        self.use_constant_eta: bool = False
        self.list_eta_flex = list_eta_flex

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.electrical_node.append(self.power_el, 1)
        self.hydro_node.append(self.power_hydro, -1)
        if self.use_waste_heat:
            self.heat_node.append(self.power_heat, 1)
        if self.use_weak_flexible_eta or self.use_linear_flexible_eta:
            self.len_eta_flex = len(self.list_eta_flex)
            c = 1
            self.list_eta_flex_use = []
            for e in self.list_eta_flex:
                self.list_eta_flex_use.append(c * e - sum(self.list_eta_flex_use))
                c += 1


@dataclass(slots=True)
class ElectrolysisData(BTClass):
    """
    Electrolysis cell class\n
    """

    eta_el: float = 0.4
    use_waste_heat: bool = False
    eta_th: float = 0.2
    power_heat: Variable = Variable()
    power_hydro: Variable = Variable()
    power_el: Variable = Variable()

    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)
    hydro_node: Node = Node(-1)

    def set_nodes(self, electrical_node: Node, heat_node: Node, hydro_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.hydro_node = hydro_node

    def set_hydrogen_node(self, hydrogen_node: Node):
        self.hydro_node = hydrogen_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heating_node(self, heating_node: Node):
        self.heat_node = heating_node

    def set_electrical_efficiency(self, electrical_efficiency: float) -> None:
        self.eta_el: float = electrical_efficiency

    def set_use_waste_heat(self, thermal_efficiency: float) -> None:
        self.use_waste_heat: bool = True
        self.eta_th: float = thermal_efficiency

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.electrical_node.append(self.power_el, -1)
        self.hydro_node.append(self.power_hydro, 1)
        if self.use_waste_heat:
            self.heat_node.append(self.power_heat, 1)


@dataclass(slots=True)
class H2StorageData(Storage):
    """
    hydrogen storage system class
    """

    def calc(self, dt_h: NDArray[np_float]):
        self._calc_rates(dt_h)


@dataclass(slots=True)
class SolarThermal(BTClass):
    """
    General solar thermal collector or absorber class\n
    """

    irradiation_data: IrrData = IrrData()
    calc_power: bool = False
    eta: float = 0.9
    k1: float = 4.2
    k2: float = 0.02
    t_hot: float = -273
    t_cold: float = -273
    t_ambient_line: str = ''
    data_line_cold: str = ''
    data_line_warm: str = ''
    heat_warm: bool = False
    heat_cold: bool = False
    cool_warm: bool = False
    cool_cold: bool = False
    power_warm: NDArray[np_float] = default_array
    power_cold: NDArray[np_float] = default_array
    t_ambient: NDArray[np_float] = default_array
    power_heat_hot: Variable = Variable()
    power_cool_hot: Variable = Variable()
    power_heat_cold: Variable = Variable()
    power_cool_cold: Variable = Variable()

    heat_node: Node = Node(-1)
    cool_node: Node = Node(-1)

    def set_nodes(self, heat_node: Node, cool_node: Node):
        self.heat_node = heat_node
        self.cool_node = cool_node

    def set_heating_node(self, heating_node: Node):
        self.heat_node = heating_node

    def set_cooling_node(self, cooling_node: Node):
        self.cool_node = cooling_node

    def set_connections(self, heat_warm_side: bool = True, cool_warm_side: bool = False, heat_cold_side: bool = False,
                        cool_cold_side: bool = False) -> None:
        self.heat_warm: bool = heat_warm_side
        self.heat_cold: bool = heat_cold_side
        self.cool_warm: bool = cool_warm_side
        self.cool_cold: bool = cool_cold_side

    def get_power_from_data(self, column_name_warm_side: str = None, column_name_cold_side: str = None):
        self.calc_power: bool = False
        self.data_line_warm: str = column_name_warm_side if column_name_warm_side is not None else self.data_line_warm
        self.data_line_cold: str = column_name_cold_side if column_name_cold_side is not None else self.data_line_cold

    def calculate_power(self, optical_efficiency: float, linear_th_loss: float, quadratic_th_loss: float,
                        column_name_ambient_temperature: str, temperature_warm_side: float = None,
                        temperature_cold_side: float = None):
        self.eta = optical_efficiency
        self.k1 = linear_th_loss
        self.k2 = quadratic_th_loss
        self.t_ambient_line: str = column_name_ambient_temperature
        self.t_hot: float = temperature_warm_side if temperature_warm_side is not None else self.t_hot
        self.t_cold: float = temperature_cold_side if temperature_cold_side is not None else self.t_cold

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.heat_node.append(self.power_heat_hot, 1) if self.heat_warm else None
        self.heat_node.append(self.power_cool_hot, -1) if self.cool_warm else None
        self.cool_node.append(self.power_cool_cold, 1) if self.cool_cold else None
        self.cool_node.append(self.power_heat_cold, -1) if self.heat_cold else None
        if not self.calc_power:
            if self.heat_warm or self.cool_warm:
                self.power_warm: NDArray[np_float] = read_data(data, self.data_line_warm, 1)
            if self.cool_cold or self.heat_cold:
                self.power_cold: NDArray[np_float] = read_data(data, self.data_line_cold, 1)
            return
        self.irradiation_data.calc_irr(data)
        self.t_ambient: NDArray[np_float] = read_data(data, self.t_ambient_line, 1) if self.calc_power else None
        self.power_warm: NDArray[np_float] = solar_collector(self.eta, self.k1, self.k2, self.t_ambient, self.t_hot,
                                                             self.irradiation_data.irr) if self.calc_power \
            else self.power_warm
        self.power_cold: NDArray[np_float] = solar_collector(self.eta, self.k1, self.k2, self.t_ambient, self.t_cold,
                                                             self.irradiation_data.irr) if self.calc_power \
            else self.power_cold


@dataclass(slots=True)
class STCData(SolarThermal):
    """
    Solar thermal collector class\n
    """


@dataclass(slots=True)
class STAData(SolarThermal):
    """
    Solar thermal absorber class\n
    """


@dataclass(slots=True)
class PVTData(SolarThermal):
    """
    photovoltaic + solar thermal absorber class\n
    """
    time_step_years: int = 20
    selling_power: SellingPower = SellingPower(time_step_years)
    produced_power: ProducedPower = ProducedPower()
    PowerLine: str = ''
    power: array = default_array
    calc_power: bool = False
    Eff: float = 0.18
    TCoe: float = 0.002
    pr: float = 0.8
    t_noct: float = 44.
    t_ambient_line: str = ''
    t_ambient: array = default_array
    use_el_demand: bool = False
    el_demand: float = 0.
    power_li: Variable = Variable()
    power_sell_li: Variable = Variable()
    power_demand_li: Variable = Variable()

    def set_time_step_years(self, time_step_years: int):
        self.time_step_years: int = time_step_years
        self.selling_power: SellingPower = SellingPower(self.time_step_years)

    def calc(self, data: pdDataFrame):
        self.selling_power.calc_selling_prices(data, self.rate_of_interest)
        self.produced_power.calculate_power(data)
        self._calc_price()
        if not self.calc_power:
            if self.heat_warm or self.cool_warm:
                self.power_warm: NDArray[np_float] = read_data(data, self.data_line_warm, 1)
            if self.cool_cold or self.heat_cold:
                self.power_cold: NDArray[np_float] = read_data(data, self.data_line_cold, 1)
            return
        self.irradiation_data.calc_irr(data)
        self.t_ambient: NDArray[np_float] = read_data(data, self.t_ambient_line, 1) if self.calc_power else \
            default_array
        self.power_warm = solar_collector(self.eta, self.k1, self.k2, self.t_ambient, self.t_hot,
                                          self.irradiation_data.irr) if self.calc_power else self.power_warm
        self.power_cold = solar_collector(self.eta, self.k1, self.k2, self.t_ambient, self.t_cold,
                                          self.irradiation_data.irr) if self.calc_power else self.power_cold


@dataclass(slots=True)
class ACData(BTClass):
    """
    Absorption chiller class\n
    """

    heat_frac: float = 0.
    el_frac: float = 0.
    power_heat: Variable = Variable()
    power_cool: Variable = Variable()
    power_el: Variable = Variable()
    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)
    cool_node: Node = Node(-1)

    def set_nodes(self, electrical_node: Node, heat_node: Node, cool_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.cool_node = cool_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heating_node(self, heat_node: Node):
        self.heat_node = heat_node

    def set_cooling_node(self, cool_node: Node):
        self.cool_node = cool_node

    def set_heat_frac(self, frac: float) -> None:
        """
        set thermal efficiency of absorption chiller
        :param frac: fraction of cooling power to thermal input normally 0.4 < x < 0.6 [-]
        """
        self.heat_frac: float = frac

    def set_el_frac(self, frac: float) -> None:
        """
        set electrical efficiency of absorption chiller
        :param frac: fraction of electrical power to cooling power [-]
        """
        self.el_frac: float = frac

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.heat_node.append(self.power_heat, -1)
        self.cool_node.append(self.power_cool, 1)
        if self.el_frac > 0:
            self.electrical_node.append(self.power_el, -1)


@dataclass(slots=True)
class ECData(BTClass):
    """
    Electrical chiller class\n
    """

    constant_cop: bool = False
    cop: float = 0.
    import_flexible_heat: bool = False
    el_power_frac: float = 0.
    el_power: float = 0.
    th_power_line: str = ''
    lin: float = 0.
    con: float = 0.
    t_amb_line: str = ''
    t_amb: NDArray[np_float] = default_array
    th_power: NDArray[np_float] = default_array
    cop_flex: NDArray[np_float] = default_array
    power_el: Variable = Variable()
    power_cool: Variable = Variable()

    electrical_node: Node = Node(-1)
    cool_node: Node = Node(-1)

    def set_nodes(self, electrical_node: Node, cool_node: Node):
        self.electrical_node = electrical_node
        self.cool_node = cool_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_cooling_node(self, cooling_node: Node):
        self.cool_node = cooling_node

    def set_constant_cop(self, cop: float) -> None:
        """
        set constant coefficient of performance for electrical chiller.\n
        :param cop: fraction heating power by electrical input [-]
        """
        self.constant_cop: bool = True  # activate constant cop
        self.cop: float = cop

    def set_flexible_cop(self, electrical_power_fraction: float, thermal_power_column_name: str) -> None:
        """
        set flexible cop which is imported from pandas dataframe by column with name thermal_power_column_name
        :param electrical_power_fraction: electrical power fraction [Wel/kWth]
        :param thermal_power_column_name: pandas dataframe column name with thermal power values [W/kWth]
        :return: None
        """
        self.constant_cop: bool = False  # deactivate constant cop
        self.import_flexible_heat: bool = True  # activate that the flexible heat should be read from pv_data frame
        self.el_power_frac: float = electrical_power_fraction  # set electrical power fraction in Wel per kWth
        self.el_power: float = self.el_power_frac  # set electrical power
        self.th_power_line: str = thermal_power_column_name  # set column name of which thermal power should be read

    def calculate_flexible_cop(self, electrical_power_fraction: float, linear_factor: float, constant_factor: float,
                               ambient_temperature_column_name: str) -> None:
        """
        set values for temperature depending on cop calculation \n
        th_power = linear_factor*t_ambient + constant_factor \n
        cop = th_power/electrical_power_fraction \n
        :param electrical_power_fraction: electrical power fraction [Wel/kWth]
        :param linear_factor: linear thermal power factor [W/Wth °C]
        :param constant_factor: constant thermal power factor [W/kWth]
        :param ambient_temperature_column_name: ambient temperature column name in pandas dataframe [°C]
        :return: None
        """
        self.constant_cop: bool = False  # deactivate constant cop
        self.import_flexible_heat: bool = False  # deactivate that the flexible heat should be read from pv_data frame
        self.el_power_frac: float = electrical_power_fraction  # set electrical power fraction
        self.el_power: float = self.el_power_frac  # set electrical power
        self.lin: float = linear_factor  # set linear ambient temperature coefficient
        self.con: float = constant_factor  # set constant coefficient
        self.t_amb_line: str = ambient_temperature_column_name  # set  ambient temperature column name in dataframe

    def calc(self, data: pdDataFrame) -> None:
        self._calc_price()
        self.electrical_node.append(self.power_el, -1)
        self.cool_node.append(self.power_cool, 1)
        if not self.constant_cop and not self.import_flexible_heat:
            self.t_amb: NDArray[np_float] = read_data(data, self.t_amb_line, 1)
            self.th_power: NDArray[np_float] = self.lin * self.t_amb + self.con
        if not self.constant_cop and self.import_flexible_heat:
            self.th_power: NDArray[np_float] = read_data(data, self.th_power_line, 1)
        if not self.constant_cop:
            self.cop_flex: NDArray[np_float] = self.th_power / self.el_power


@dataclass(slots=True)
class EHData(BTClass):
    """
    Electrical heater class\n
    """

    eta: float = 1.
    power_heat: Variable = Variable()
    power_el: Variable = Variable()
    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)

    def get_results(self, x: NDArray[np_float], def_val: NDArray[np_float]):
        super(EHData, self).get_results(x, def_val)
        self.power_el.value = self.power_heat.value / self.eta

    def set_nodes(self, electrical_node: Node, heat_node: Node):
        self.heat_node = heat_node
        self.electrical_node = electrical_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heat_node(self, heat_node: Node):
        self.heat_node = heat_node

    def set_efficiency(self, eta: float) -> None:
        """
        set thermal efficiency of electrical heater.\n
        :param eta: fraction heating power by electrical input [-]
        """
        self.eta: float = eta

    def calc(self, data: pdDataFrame):
        self._calc_price()
        self.heat_node.append(self.power_heat, 1)
        self.electrical_node.append(self.power_heat, -1 / self.eta)


@dataclass(slots=True)
class Demand:
    """
    class for demand data
    """

    building_life: int
    rate_of_interest: float
    use: bool = False
    multi: bool = False
    line: str = ''
    lines: List[str] = field(default_factory=lambda: [])
    lifetimes: NDArray[np_float] = default_array
    prices: NDArray[np_float] = default_array
    prices_val: NDArray[np_float] = default_array
    demand: NDArray[np_float] = default_array
    demand_var: Variable = Variable()
    binary: Variable = Variable()
    costs: Variable = Variable()
    unit_val: float = 1.
    node: Node = Node(-1)

    def __post_init__(self):
        [setattr(self, var, Variable()) for var in self.__slots__ if isinstance(getattr(self, var), Variable)]

    def set_node(self, node: Node):
        self.node = node

    def set_single_demand(self, column_name, unit_val: float = None):
        self.use: bool = True
        self.multi: bool = False
        self.line: str = column_name
        self.unit_val: float = 1 if unit_val is None else unit_val

    def set_multiple_demand(self, column_names: List[str], prices: List[float], lifetimes: List[float] = None,
                            unit_val: float = None):
        self.use: bool = True
        self.multi: bool = True
        self.lifetimes: list = [self.building_life] * len(prices) if lifetimes is None else lifetimes
        self.prices_val: List[float] = prices
        self.prices: List[float] = [self.npv(p, l) for p, l in zip(self.prices_val, self.lifetimes)]
        self.lines: List[str] = column_names
        self.unit_val: float = 1 if unit_val is None else unit_val

    def npv(self, price: float, life: int):
        # initialize costs vector
        vec = zeros(int(self.building_life))
        # set constant costs for component
        vec[0] = price
        vec[0:int(self.building_life):int(life)] = price
        # calculate costs considering the rate of interest
        return npf_npv(self.rate_of_interest / 100, vec)

    def calc(self, data: pdDataFrame):
        if not self.use:
            return
        if not self.multi:
            self.demand: NDArray[np_float] = read_data(data, self.line, self.unit_val) if self.use else default_array
            self.demand: NDArray[np_float] = self.demand * -1 if sum(self.demand) < 0 else self.demand
            self.node.append_demand(self.demand)
        if self.multi:
            self.node.append(self.demand_var, -1)
            li: list = self.prices
            n: int = len(li)
            d: NDArray[np_float] = read_data(data, self.lines[0], 1)
            d = d * -1 if sum(d) < 0 else d
            self.demand: NDArray[np_float] = zeros((len(d), n))
            self.demand[:, 0] = d
            for i in range(1, n):
                d = read_data(data, self.lines[i], 1)
                d = d * -1 if sum(d) < 0 else d
                self.demand[:, i] = d

    def get_results(self, x: NDArray[np_float], def_val: NDArray[np_float]):
        if self.demand_var.idx != 0:
            self.demand_var.value = x[self.demand_var.idx] if self.multi else def_val
            self.binary.value = x[self.binary.idx] if self.multi else def_val
            self.costs.value = x[self.costs.idx] if self.multi else def_val

    def __eq__(self, other) -> bool:
        """
        equality function to check if values are equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has the same values as other
        """
        # if not of same class return false
        if not isinstance(other, DataStorage):
            return False
        # compare all slot values if one not match return false
        for i in self.__slots__:  # type: ignore
            # get object by name
            obj = getattr(self, i)
            # just compare object if it is not an array
            if not isinstance(obj, NDArray):
                # if the object values does not math return False
                if obj != getattr(other, i):
                    return False
        # if all match return true
        return True

    def __ne__(self, other) -> bool:
        """
        not equality function to check if values are not equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has not the same values as other
        """
        # return the opposite of the equality function
        return not self.__eq__(other)


@dataclass(slots=True)
class MultipleDemands(Demand):

    electric: bool = False
    heating: bool = False
    cooling: bool = False
    hydrogen: bool = False
    fuel: bool = False
    el_lines: List[str] = field(default_factory=lambda: [])
    heat_lines: List[str] = field(default_factory=lambda: [])
    cool_lines: List[str] = field(default_factory=lambda: [])
    hydro_lines: List[str] = field(default_factory=lambda: [])
    fuel_lines: List[str] = field(default_factory=lambda: [])
    el_demand: NDArray[np_float] = default_array
    heat_demand: NDArray[np_float] = default_array
    cool_demand: NDArray[np_float] = default_array
    hydro_demand: NDArray[np_float] = default_array
    fuel_demand: NDArray[np_float] = default_array
    demand_heat: Variable = Variable()
    demand_hydro: Variable = Variable()
    demand_cool: Variable = Variable()
    demand_el: Variable = Variable()
    demand_fuel: Variable = Variable()

    heat_node: Node = Node(-1)
    electrical_node: Node = Node(-1)
    cool_node: Node = Node(-1)
    hydrogen_node: Node = Node(-1)
    fuel_node: Node = Node(-1)

    def set_nodes(self, electrical_node: Node, heat_node: Node, cool_node: Node, hydrogen_node: Node, fuel_node: Node):
        self.electrical_node = electrical_node
        self.heat_node = heat_node
        self.cool_node = cool_node
        self.hydrogen_node = hydrogen_node
        self.fuel_node = fuel_node

    def set_electrical_node(self, electrical_node: Node):
        self.electrical_node = electrical_node

    def set_heating_node(self, heat_node: Node):
        self.heat_node = heat_node

    def set_cooling_node(self, cool_node: Node):
        self.cool_node = cool_node

    def set_hydrogen_node(self, hydrogen_node: Node):
        self.hydrogen_node = hydrogen_node

    def set_fuel_node(self, fuel_node: Node):
        self.fuel_node = fuel_node

    def get_results(self, x: NDArray, def_val: NDArray[np_float]):
        super().get_results(x, def_val)
        self.demand_el.value = self.demand_var.idx
        self.demand_heat.value = x[self.demand_heat.idx] if self.multi else def_val
        self.demand_cool.value = x[self.demand_cool.idx] if self.multi else def_val
        self.demand_hydro.value = x[self.demand_hydro.idx] if self.multi else def_val

    def set_demands(self, prices: List[float], lifetimes: List[float] = None,
                    column_names_electrical: List[str] = None,
                    column_names_heating: List[str] = None, column_names_cooling: List[str] = None,
                    column_names_hydrogen: List[str] = None, column_names_fuel: List[str] = None):
        self.use: bool = True
        self.multi: bool = True
        self.electric: bool = True if column_names_electrical is not None else False
        self.heating: bool = True if column_names_heating is not None else False
        self.cooling: bool = True if column_names_cooling is not None else False
        self.hydrogen: bool = True if column_names_hydrogen is not None else False
        self.fuel: bool = True if column_names_fuel is not None else False
        self.el_lines: list = column_names_electrical if column_names_electrical is not None else []
        self.heat_lines: list = column_names_heating if column_names_heating is not None else []
        self.cool_lines: list = column_names_cooling if column_names_cooling is not None else []
        self.hydro_lines: list = column_names_hydrogen if column_names_hydrogen is not None else []
        self.fuel_lines: list = column_names_fuel if column_names_fuel is not None else []
        self.lifetimes: list = [self.building_life] * len(prices) if lifetimes is None else lifetimes
        self.prices_val: list = prices
        self.prices: list = [p / l * self.building_life for p, l in zip(self.prices_val, self.lifetimes)]

    def calc(self, data: pdDataFrame):
        if not self.use:
            return
        li_mul: list = self.prices
        n_mul: int = len(li_mul)
        if self.electric:
            pel1 = read_data(data, self.el_lines[0], 1)
            self.el_demand: NDArray[np_float] = zeros((len(pel1), n_mul), dtype=np_float)
            self.el_demand[:, 0] = pel1
            self.electrical_node.append(self.demand_el, -1)
        if self.heating:
            qh1 = read_data(data, self.heat_lines[0], 1)
            self.heat_demand: NDArray[np_float] = zeros((len(qh1), n_mul), dtype=np_float)
            self.heat_demand[:, 0] = qh1
            self.heat_node.append(self.demand_heat, 1 if self.heat_node.kind == IDX_COOLING else -1)
        if self.cooling:
            qc1 = read_data(data, self.cool_lines[0], 1)
            qc2: NDArray[np_float] = -1 * qc1 if min(qc1) < 0 else qc1
            self.cool_demand: NDArray[np_float] = zeros((len(qc2), n_mul), dtype=np_float)
            self.cool_demand[:, 0] = qc2
            self.cool_node.append(self.demand_cool, -1 if self.cool_node.kind == IDX_COOLING else 1)
        if self.hydrogen:
            m_h2_1 = read_data(data, self.hydro_lines[0], 1)
            self.hydro_demand: NDArray[np_float] = zeros((len(m_h2_1), n_mul), dtype=np_float)
            self.hydro_demand[:, 0] = m_h2_1
            self.hydrogen_node.append(self.demand_hydro, -1)
        if self.fuel:
            m_fuel_1 = read_data(data, self.fuel_lines[0], 1)
            self.fuel_demand: NDArray[np_float] = zeros((len(m_fuel_1), n_mul), dtype=np_float)
            self.fuel_demand[:, 0] = m_fuel_1
            self.fuel_node.append(self.demand_fuel, -1)
        for i in range(1, n_mul):
            if self.electric:
                pel2: NDArray[np_float] = read_data(data, self.el_lines[i], 1)
                self.el_demand[:, i] = pel2
            if self.heating:
                qh2: NDArray[np_float] = read_data(data, self.heat_lines[i], 1)
                self.heat_demand[:, i] = qh2
            if self.cooling:
                qc3: NDArray[np_float] = read_data(data, self.cool_lines[i], 1)
                qc4: NDArray[np_float] = -1 * qc3 if min(qc3) < 0 else qc3
                self.cool_demand[:, i] = qc4
            if self.hydrogen:
                m_h2_1 = read_data(data, self.hydro_lines[i], 1)
                m_h2_2: NDArray[np_float] = -1 * m_h2_1 if min(m_h2_1) < 0 else m_h2_1
                self.hydro_demand[:, i] = m_h2_2
            if self.fuel:
                m_fuel_1 = read_data(data, self.fuel_lines[i], 1)
                m_fuel_2: NDArray[np_float] = -1 * m_fuel_1 if min(m_fuel_1) < 0 else m_fuel_1
                self.fuel_demand[:, i] = m_fuel_2


@dataclass(slots=True)
class ExternalConnection:
    use: bool = False
    buy: bool = False
    sell: bool = False
    const_price: bool = False
    const_reward: bool = False
    price: float = 0.
    price_value: float = 0.
    price_line: str = ''
    price_array: Union[NDArray, None] = None
    use_power_price: bool = False
    power_price_val: float = 0.
    power_price: float = 0.
    reward: float = 0.
    reward_value: float = 0.
    reward_line: str = ''
    reward_array: Union[NDArray, None] = None
    max_frac: float = -1
    max_amount: Union[Demand, float, None] = None
    base_price: float = 0
    base_price_val: float = 0
    use_base_price: bool = False

    power_buy: Variable = Variable()
    power_sell: Variable = Variable()
    base_price_var: Variable = Variable()
    cost_var: Variable = Variable()
    max_power: Variable = Variable()

    node: Node = Node(-1)

    def __post_init__(self):
        [setattr(self, var, Variable()) for var in self.__slots__ if isinstance(getattr(self, var), Variable)]

    def set_node(self, node: Node):
        self.node = node

    def set_price(self, price: Union[str, float] = None, reward: Union[str, float] = None, power_price: float = None,
                  base_price: float = None):
        if price is not None:
            if isinstance(price, str):
                self.set_flexible_price(price)
            else:
                self.set_constant_price(price)
        if reward is not None:
            if isinstance(reward, str):
                self.set_flexible_reward(reward)
            else:
                self.set_constant_reward(reward)

        if power_price is not None:
            self.set_power_price(power_price)

        if base_price is not None:
            self.set_base_price(base_price)

    def get_results(self, x: NDArray[np_float]):
        if self.power_buy.idx != 0:
            self.power_buy.value = x[self.power_buy.idx]
        if self.power_sell.idx != 0:
            self.power_sell.value = x[self.power_sell.idx]

    def set_base_price(self, base_price: float):
        self.use_base_price: bool = True
        self.base_price_val: float = base_price

    def set_constant_price(self, price: float):
        self.use: bool = True
        self.buy: bool = True
        self.const_price: bool = True
        self.price_value: float = price

    def set_flexible_price(self, column_name: str):
        self.use: bool = True
        self.buy: bool = True
        self.const_price: bool = False
        self.price_line: str = column_name

    def set_constant_reward(self, reward: float):
        self.use: bool = True
        self.sell: bool = True
        self.const_reward: bool = True
        self.reward_value: float = reward

    def set_flexible_reward(self, column_name: str):
        self.use: bool = True
        self.sell: bool = True
        self.const_reward: bool = False
        self.reward_line: str = column_name

    def set_power_price(self, price: float):
        self.use_power_price: bool = True
        self.power_price_val: float = price

    def set_maximal_usage_amount(self, fraction: float, amount: Union[Demand, float]) -> None:
        """
        set maximal amount that can be used\n
        :param fraction: fraction of maximal amount
        :param amount: Either float or demand class, if float then the fraction is taken from float, if Demand class
        then it is a fraction of yearly demand.
        """
        # set maximal fraction
        self.max_frac: float = fraction
        # set amount
        self.max_amount: Union[Demand, float] = amount

    def calc(self, data: pdDataFrame, time_step_years: int, rate_of_interest: float):
        if not self.use:
            return
        if self.buy:
            self.node.append(self.power_buy, 1)
            self.price: float = npv(self.price_value, time_step_years, rate_of_interest) if self.const_price else 0.
            self.price_array: NDArray[np_float] = (read_data(data, self.price_line, 1) * time_step_years) if \
                not self.const_price else None
        self.power_price: float = npv(self.power_price_val, time_step_years, rate_of_interest) if \
            self.use_power_price else 0.
        self.base_price: float = npv(self.base_price_val, time_step_years, rate_of_interest) if \
            self.use_base_price else 0.
        if self.sell:
            self.node.append(self.power_sell, -1)
            self.reward: float = npv(self.reward_value, time_step_years, rate_of_interest) if self.const_reward else 0.
            self.reward_array: NDArray[np_float] = (read_data(data, self.reward_line, 1) * time_step_years) if not \
                self.const_reward else None

    def __eq__(self, other) -> bool:
        """
        equality function to check if values are equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has the same values as other
        """
        # if not of same class return false
        if not isinstance(other, DataStorage):
            return False
        # compare all slot values if one not match return false
        for i in self.__slots__:  # type: ignore
            # get object by name
            obj = getattr(self, i)
            # just compare object if it is not an array
            if not isinstance(obj, NDArray[np_float]):
                # if the object values does not math return False
                if obj != getattr(other, i):
                    return False
        # if all match return true
        return True

    def __ne__(self, other) -> bool:
        """
        not equality function to check if values are not equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has not the same values as other
        """
        # return the opposite of the equality function
        return not self.__eq__(other)


@dataclass(slots=True)
class DataStorage:
    """
    datastorage as input for optimization. \n
    """
    # save filename to variable
    filename: str = ''
    # test if the filename works
    raw: pdDataFrame = pdDataFrame()
    # save input variables
    rate_of_interest: float = 0.
    building_lifetime: int = 60
    time_step_years: int = 60
    time_step_per_year: int = 8760
    change_bt: bool = False
    change_building_once: bool = False
    # determine number of years to be calculated
    number_of_years: int = 0
    # unused variables for later implementations
    ChangeBuildingPrice: bool = False
    TL: int = 0
    # set typical periods parameters to false and empty
    use_tp: bool = False
    tp_data: Union[TD_CreateTD, None] = None
    # set default solver settings
    Solver: str = 'gurobi'
    threads: int = 0
    TimeLimit: float = 3600.
    MIPGap: float = 0
    PreSolve: int = 0
    Heuristic: float = 0
    # get dgl solver
    eul_for: bool = True
    eul_bac: bool = False
    eul_cen: bool = False
    # set integration rule
    integration_rule: int = 0
    # set lists of demands to empty lists
    ElDem: List[Demand] = field(default_factory=lambda: [])
    HeatD: List[Demand] = field(default_factory=lambda: [])
    CoolD: List[Demand] = field(default_factory=lambda: [])
    HydrogenD: List[Demand] = field(default_factory=lambda: [])
    FuelD: List[Demand] = field(default_factory=lambda: [])
    MultD: List[MultipleDemands] = field(default_factory=lambda: [])
    # set lists of components to empty lists
    AirHP: List[AirHPData] = field(default_factory=lambda: [])
    HT: List[HTData] = field(default_factory=lambda: [])
    PV: List[PVData] = field(default_factory=lambda: [])
    Bat: List[BatData] = field(default_factory=lambda: [])
    STC: List[STCData] = field(default_factory=lambda: [])
    STA: List[STAData] = field(default_factory=lambda: [])
    Geo: List[GeoData] = field(default_factory=lambda: [])
    GeoHP: List[GeoHPData] = field(default_factory=lambda: [])
    CT: List[CTData] = field(default_factory=lambda: [])
    Boiler: List[BoilerData] = field(default_factory=lambda: [])
    CHP: List[CHPData] = field(default_factory=lambda: [])
    Wind: List[WindData] = field(default_factory=lambda: [])
    FuelCell: List[FuelCellData] = field(default_factory=lambda: [])
    Electrolysis: List[ElectrolysisData] = field(default_factory=lambda: [])
    H2Storage: List[H2StorageData] = field(default_factory=lambda: [])
    AC: List[ACData] = field(default_factory=lambda: [])
    EC: List[ECData] = field(default_factory=lambda: [])
    EH: List[EHData] = field(default_factory=lambda: [])
    # set lists of external connections to empty lists
    electrical_grids: List[ExternalConnection] = field(default_factory=lambda: [])
    district_heat: List[ExternalConnection] = field(default_factory=lambda: [])
    district_cool: List[ExternalConnection] = field(default_factory=lambda: [])
    district_hydro: List[ExternalConnection] = field(default_factory=lambda: [])
    fuel_connection: List[ExternalConnection] = field(default_factory=lambda: [])
    # create node lists
    electrical_nodes: List[Node] = field(default_factory=lambda: [])
    heat_nodes: List[Node] = field(default_factory=lambda: [])
    cool_nodes: List[Node] = field(default_factory=lambda: [])
    hydro_nodes: List[Node] = field(default_factory=lambda: [])
    fuel_nodes: List[Node] = field(default_factory=lambda: [])
    # create node connections
    inverter: List[NodeConnection] = field(default_factory=lambda: [])
    fuel_pipe: List[NodeConnection] = field(default_factory=lambda: [])
    hydrogen_pipe: List[NodeConnection] = field(default_factory=lambda: [])
    heating_pipe: List[NodeConnection] = field(default_factory=lambda: [])
    cooling_pipe: List[NodeConnection] = field(default_factory=lambda: [])
    # create default arrays (data array, time step width array and time step width array in hours)
    data: NDArray[np_float] = default_array
    dt: NDArray[np_float] = default_array
    dt_in_h: NDArray[np_float] = default_array
    # init results dictionary
    res: dict = field(default_factory=lambda: {})

    def set_file_rate_time_steps(self, filename: str, rate_of_interest: float, building_lifetime: int, time_step: int,
                                 time_step_per_year: int, change_building_technology: bool = None,
                                 change_building_technology_once: bool = None) -> None:
        """
        set filename and time steps and other general settings \n
        :param filename: filename of data csv file
        :param rate_of_interest: considered rate of interest [-]
        :param building_lifetime: building or energy system lifetime [a]
        :param time_step: yearly time step of data [a] if time_step < building_lifetime the datafile is split
        :param time_step_per_year: time step of datafile per year [#]
        :param change_building_technology: should the technology be changeable during the lifetime [True, False]
        :param change_building_technology_once: should the technology be changeable during the lifetime and just
        changed if component lifetime ends [True, False]
        """
        # save filename to variable
        self.filename: str = filename
        # test if the filename works
        try:
            self.raw: pdDataFrame = pd_read_csv(self.filename, index_col=0)
        except FileNotFoundError:
            print('no data file selected')
            raise FileNotFoundError
        # save input variables
        self.rate_of_interest: float = rate_of_interest
        self.building_lifetime: int = building_lifetime
        self.time_step_years: int = time_step
        self.time_step_per_year: int = time_step_per_year
        self.change_bt: bool = False if change_building_technology is None else change_building_technology
        self.change_building_once: bool = False if change_building_technology_once is None else \
            change_building_technology_once
        # determine number of years to be calculated
        self.number_of_years: int = int(self.building_lifetime / self.time_step_years)
        # unused variables for later implementations
        self.ChangeBuildingPrice: bool = False
        self.TL: int = 0 if self.change_bt else self.building_lifetime

    @staticmethod
    def _get_node(node_list: List[Node], kind: int) -> Node:
        """
        check if a node in the list already exists otherwise create one\n
        :param node_list: node list
        :param kind: 0 = electrical, 1 = heating, 2 = cooling, 3 = Hydrogen, 4 = fuel
        :return:
        """
        if node_list:
            return node_list[0]
        node = Node(kind)
        node_list.append(node)
        return node

    @staticmethod
    def _add_node(node_list: List[Node], kind: int) -> Node:
        """
        create a node in the list and return it \n
        :param node_list: node list
        :param kind: 0 = electrical, 1 = heating, 2 = cooling, 3 = Hydrogen, 4 = fuel
        :return:
        """
        node = Node(kind)
        node_list.append(node)
        return node

    def _get_heat_node(self) -> Node:
        return self._get_node(self.heat_nodes, IDX_HEATING)

    def _get_cool_node(self) -> Node:
        return self._get_node(self.cool_nodes, IDX_COOLING)

    def _get_electrical_node(self) -> Node:
        return self._get_node(self.electrical_nodes, IDX_ELECTRICAL)

    def _get_hydrogen_node(self) -> Node:
        return self._get_node(self.hydro_nodes, IDX_HYDROGEN)

    def _get_fuel_node(self) -> Node:
        return self._get_node(self.fuel_nodes, IDX_FUEL)

    def add_electrical_node(self) -> Node:
        return self._add_node(self.electrical_nodes, IDX_ELECTRICAL)

    def add_heating_node(self) -> Node:
        return self._add_node(self.heat_nodes, IDX_HEATING)

    def add_cooling_node(self) -> Node:
        return self._add_node(self.cool_nodes, IDX_COOLING)

    def add_hydrogen_node(self) -> Node:
        return self._add_node(self.hydro_nodes, IDX_HYDROGEN)

    def add_fuel_node(self) -> Node:
        return self._add_node(self.fuel_nodes, IDX_FUEL)

    def set_integration_rule(self, rule: int) -> None:
        """
        Set the way how the integration over time should work
        :param rule: 0: Riemann sum; 1: trapezoid rule
        """
        self.integration_rule: int = rule

    def import_typical_periods(self, filename: str) -> None:
        """
        import typical days from file with filename.\n
        :param filename: filename of which the typical days class should be imported from
        """
        self.use_tp: bool = True  # activate typical periods consideration
        # open file and read typical days class
        with open(filename, "rb") as f:
            self.tp_data: TD_CreateTD = pk_load(f)

    def export_typical_periods(self, filename: str) -> None:
        """
        export typical days from file with filename.\n
        :param filename: filename of which the typical days class should be exported to
        """
        # open file and save typical days class
        with open(filename, "wb") as f:
            pk_dump(self.tp_data, f, HIGHEST_PROTOCOL)

    def calculate_typical_periods(self, no_typical_periods: int, hours_per_period: int, cluster_method_idx: int,
                                  add_extreme_method_idx: int = None, list_max: list = None, list_min: list = None,
                                  solver: str = 'glpk', ts_seasonal: bool = None, ts_seasonal_plus: bool = None,
                                  tp_seasonal_simple: bool = None)\
            -> TD_CreateTD:
        """
        create typical periods class\n
        :param no_typical_periods: number of typical periods [#]
        :param hours_per_period: hours per period [#]
        :param cluster_method_idx: cluster method [0,1,2,3]; 1=averaging, 2= hierarchical, 3=k_means, 4=k_medoids
        :param add_extreme_method_idx: should a seasonal storage model be considered? [True, False]
        :param list_max: list of column name of integrated maximal periods [[str, str]]
        :param list_min: list of column name of integrated minimal periods [[str, str]]
        :param solver: MILP solver of pyomo for example 'cbc', 'glpk', 'gurobi'
        :param ts_seasonal: should a seasonal storage model be considered? [True, False]
        :param ts_seasonal_plus: should the seasonal + storage model be considered? [True, False]
        :param tp_seasonal_simple: should the simple seasonal storage model be considered? [True, False]
        :return: typical days
        """

        self.use_tp: bool = True  # activate typical periods consideration
        # initialize creation typical period class
        self.tp_data: TD_CreateTD = TD_CreateTD(self.raw, no_typical_periods, hours_per_period, cluster_method_idx,
                                                ts_seasonal, ts_seasonal_plus, tp_seasonal_simple, solver,
                                                add_extreme_method_idx, list_max, list_min)
        return self.tp_data

    def set_typical_periods(self, typical_periods: TD_CreateTD) -> None:
        """
        set typical days from already existing typical periods class.\n
        :param typical_periods: typical periods
        """
        self.use_tp: bool = True  # activate typical periods consideration
        self.tp_data: TD_CreateTD = typical_periods

    def set_de_model(self, model: int) -> None:
        """
        set the differential equation model\n
        :param model: 1=Euler forward; 2=Euler backward, 3=Euler central
        """
        self.eul_for: bool = True if model == 1 else False
        self.eul_bac: bool = True if model == 2 else False
        self.eul_cen: bool = True if model == 3 else False

    def add_single_electrical_demand(self, column_name: str, unit_val: float = None) -> Demand:
        """
        add single electrical demand by column name\n
        :param column_name: column name of electrical demand in csv file
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be single demand
        demand.set_single_demand(column_name, unit_val)
        # set node
        demand.set_node(self._get_electrical_node())
        # append the demand to electrical demand list
        self.ElDem.append(demand)
        # return demand class
        return demand

    def add_multiple_electrical_demand(self, column_names: list, prices: list, lifetimes: list = None,
                                       unit_val: float = None) -> Demand:
        """
        add multiple electrical demands by column names\n
        :param column_names: column names of electrical demands in csv file
        :param prices: prices of different demand
        :param lifetimes: lifetimes of different demands
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be multiple demands
        demand.set_multiple_demand(column_names, prices, lifetimes, unit_val)
        # set node
        demand.set_node(self._get_electrical_node())
        # append the demand to electrical demand list
        self.ElDem.append(demand)
        # return demand class
        return demand

    def add_single_fuel_demand(self, column_name: str, unit_val: float = None) -> Demand:
        """
        add single fuel demand by column name\n
        :param column_name: column name of fuel demand in csv file
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be single demand
        demand.set_single_demand(column_name, unit_val)
        # set node
        demand.set_node(self._get_fuel_node())
        # append the demand to electrical demand list
        self.FuelD.append(demand)
        # return demand class
        return demand

    def add_multiple_fuel_demand(self, column_names: list, prices: list, lifetimes: list = None,
                                 unit_val: float = None) -> Demand:
        """
        add multiple fuel demands by column names\n
        :param column_names: column names of fuel demands in csv file
        :param prices: prices of different demand
        :param lifetimes: lifetimes of different demands
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be multiple demands
        demand.set_multiple_demand(column_names, prices, lifetimes, unit_val)
        # set node
        demand.set_node(self._get_fuel_node())
        # append the demand to electrical demand list
        self.FuelD.append(demand)
        # return demand class
        return demand

    def add_single_heating_demand(self, column_name, unit_val: float = None) -> Demand:
        """
        add single heating demand by column name\n
        :param column_name: column name of heating demand in csv file
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be single demand
        demand.set_single_demand(column_name, unit_val)
        # set node
        demand.set_node(self._get_heat_node())
        # append the demand to heating demand list
        self.HeatD.append(demand)
        # return demand class
        return demand

    def add_multiple_heating_demand(self, column_names: list, prices: list, lifetimes: list = None,
                                    unit_val: float = None) -> Demand:
        """
        add multiple heating demands by column names\n
        :param column_names: column names of heating demands in csv file
        :param prices: prices of different demand
        :param lifetimes: lifetimes of different demands
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be multiple demands
        demand.set_multiple_demand(column_names, prices, lifetimes, unit_val)
        # set node
        demand.set_node(self._get_heat_node())
        # append the demand to heating demand list
        self.HeatD.append(demand)
        # return demand class
        return demand

    def add_single_hydrogen_demand(self, column_name, unit_val: float = None) -> Demand:
        """
        add single heating hydrogen by column name\n
        :param column_name: column name of hydrogen demand in csv file
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be single demand
        demand.set_single_demand(column_name, unit_val)
        # set node
        demand.set_node(self._get_hydrogen_node())
        # append the demand to hydrogen demand list
        self.HydrogenD.append(demand)
        # return demand class
        return demand

    def add_multiple_hydrogen_demand(self, column_names: list, prices: list, lifetimes: list = None,
                                     unit_val: float = None) -> Demand:
        """
        add multiple hydrogen demands by column names\n
        :param column_names: column names of hydrogen demands in csv file
        :param prices: prices of different demand
        :param lifetimes: lifetimes of different demands
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be multiple demands
        demand.set_multiple_demand(column_names, prices, lifetimes, unit_val)
        # set node
        demand.set_node(self._get_hydrogen_node())
        # append the demand to hydrogen demand list
        self.HydrogenD.append(demand)
        # return demand class
        return demand

    def add_single_cooling_demand(self, column_name, unit_val: float = None) -> Demand:
        """
        add single heating cooling by column name\n
        :param column_name: column name of cooling demand in csv file
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be single demand
        demand.set_single_demand(column_name, unit_val)
        # set node
        demand.set_node(self._get_cool_node())
        # append the demand to cooling demand list
        self.CoolD.append(demand)
        # return demand class
        return demand

    def add_multiple_cooling_demand(self, column_names: list, prices: list, lifetimes: list = None,
                                    unit_val: float = None) -> Demand:
        """
        add multiple cooling demands by column names\n
        :param column_names: column names of cooling demands in csv file
        :param prices: prices of different demands
        :param lifetimes: lifetimes of different demands
        :param unit_val: unit value of data to get kW (default = 1.)
        :return: Demand class
        """
        # create demand class with building lifetime and rate of interest
        demand = Demand(self.building_lifetime, self.rate_of_interest)
        # set demand to be multiple demands
        demand.set_multiple_demand(column_names, prices, lifetimes, unit_val)
        # set node
        demand.set_node(self._get_cool_node())
        # append the demand to cooling demand list
        self.CoolD.append(demand)
        # return demand class
        return demand

    def add_linked_multiple_demands(self, prices: List[float], lifetimes: List[float] = None,
                                    column_names_electrical: List[str] = None,
                                    column_names_heating: List[str] = None, column_names_cooling: List[str] = None,
                                    column_names_hydrogen: List[str] = None, column_names_fuel: List[str] = None) \
            -> MultipleDemands:
        """
        add multiple demands by column names\n
        :param prices: prices of different demands
        :param lifetimes: lifetimes of different demands
        :param column_names_electrical: column names of electrical demands in csv file
        :param column_names_heating: column names of heating demands in csv file
        :param column_names_cooling: column names of cooling demands in csv file
        :param column_names_hydrogen: column names of hydrogen demands in csv file
        :param column_names_fuel: column names of fuel demands in csv file
        :return: MultipleDemands class
        """
        # create multiple demand class with building lifetime and rate of interest
        demand = MultipleDemands(self.building_lifetime, self.rate_of_interest)
        # set demand to be linked multiple demands
        demand.set_demands(prices, lifetimes, column_names_electrical, column_names_heating, column_names_cooling,
                           column_names_hydrogen, column_names_fuel)
        demand.set_nodes(self._get_electrical_node(), self._get_heat_node(), self._get_cool_node(),
                         self._get_hydrogen_node(), self._get_fuel_node())
        # append the demand to multiple demand list
        self.MultD.append(demand)
        # return demand class
        return demand

    @staticmethod
    def _delete_item(list_of_item: List[BTClass], component: BTClass) -> bool:
        if list_of_item:
            try:
                list_of_item.remove(component)
                return True
            except ValueError:
                return False
        return False

    def add_air_hp(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            AirHPData:
        """
        add air heat pump to system\n
        :param pr_lin_val: linear price value [€/kW]
        :param pr_con_val: constant price value [€]
        :param maintenance_val: maintenance value as fraction of investment costs [-]
        :param life: component lifetime [a]
        :return: AirHPData class
        """
        # create air heat pump class
        air_hp_data = AirHPData()
        air_hp_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                               self.building_lifetime)
        # set nodes
        air_hp_data.set_nodes(self._get_electrical_node(), self._get_heat_node(), self._get_cool_node())
        # append air heat pump class to list
        self.AirHP.append(air_hp_data)
        # return class
        return air_hp_data

    def get_air_hp_by_index(self, idx: int) -> AirHPData:
        return self.AirHP[idx]

    def del_air_hp_by_class(self, air_hp: AirHPData) -> bool:
        return self._delete_item(self.AirHP, air_hp)  # type: ignore

    def del_air_hp_by_index(self, idx: int) -> bool:
        try:
            del self.AirHP[idx]
            return True
        except IndexError:
            print(f'Index ({idx}) out of range')
            return False

    def add_hot_tank(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> HTData:
        ht_data: HTData = HTData()
        ht_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life, self.building_lifetime)
        ht_data.set_node(self._get_heat_node())
        self.HT.append(ht_data)
        return ht_data

    def add_cold_tank(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            CTData:
        ct_data: CTData = CTData()
        ct_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life, self.building_lifetime)
        ct_data.set_node(self._get_cool_node())
        self.CT.append(ct_data)
        return ct_data

    def add_battery(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> BatData:
        bat_data: BatData = BatData()
        bat_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                            self.building_lifetime)
        bat_data.set_node(self._get_electrical_node())
        self.Bat.append(bat_data)
        return bat_data

    def add_hydrogen_storage(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                             life: int = 20) -> H2StorageData:
        h2_storage_data: H2StorageData = H2StorageData()
        h2_storage_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                                   self.building_lifetime)
        h2_storage_data.set_node(self._get_hydrogen_node())
        self.H2Storage.append(h2_storage_data)
        return h2_storage_data

    def add_fuel_cell(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            FuelCellData:
        fc_data: FuelCellData = FuelCellData()
        fc_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life, self.building_lifetime)
        fc_data.set_nodes(self._get_electrical_node(), self._get_heat_node(), self._get_hydrogen_node())
        self.FuelCell.append(fc_data)
        return fc_data

    def add_electrolysis(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            ElectrolysisData:
        es_data: ElectrolysisData = ElectrolysisData()
        es_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life, self.building_lifetime)
        es_data.set_nodes(self._get_electrical_node(), self._get_heat_node(), self._get_hydrogen_node())
        self.Electrolysis.append(es_data)
        return es_data

    def add_wind_turbine(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            WindData:
        wind_data: WindData = WindData()
        wind_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                             self.building_lifetime)
        wind_data.set_time_step_years(self.time_step_years)
        wind_data.set_node(self._get_electrical_node())
        self.Wind.append(wind_data)
        return wind_data

    def add_combined_heat_power(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                                life: int = 20) -> CHPData:
        chp_data: CHPData = CHPData()
        chp_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                            self.building_lifetime)
        chp_data.set_time_step_years(self.time_step_years)
        chp_data.set_nodes(self._get_electrical_node(), self._get_heat_node(), self._get_fuel_node())
        self.CHP.append(chp_data)
        return chp_data

    def add_photovoltaic(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            PVData:
        """

        :rtype: object
        """
        pv_data: PVData = PVData()
        pv_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                           self.building_lifetime)
        pv_data.set_time_step_years(self.time_step_years)
        pv_data.set_node(self._get_electrical_node())
        self.PV.append(pv_data)
        return pv_data

    def add_solar_thermal_collector(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                                    life: int = 20) -> STCData:
        stc_data: STCData = STCData()
        stc_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                            self.building_lifetime)
        stc_data.set_nodes(self._get_heat_node(), self._get_cool_node())
        self.STC.append(stc_data)
        return stc_data

    def add_solar_thermal_absorber(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                                   life: int = 20) -> STAData:
        sta_data: STAData = STAData()
        sta_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                            self.building_lifetime)
        sta_data.set_nodes(self._get_heat_node(), self._get_cool_node())
        self.STA.append(sta_data)
        return sta_data

    def add_geothermal_system(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                              life: int = 20) -> GeoData:
        geo_data: GeoData = GeoData()
        geo_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                            self.building_lifetime)
        geo_data.set_node(self._get_electrical_node(), self._get_cool_node(), self._get_cool_node())
        self.Geo.append(geo_data)
        return geo_data

    def add_geothermal_heat_pump(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                                 life: int = 20) -> GeoHPData:
        geo_hp_data: GeoHPData = GeoHPData()
        geo_hp_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                               self.building_lifetime)
        geo_hp_data.set_node(self._get_electrical_node(), self._get_heat_node(), self._get_cool_node())
        self.GeoHP.append(geo_hp_data)
        return geo_hp_data

    def add_boiler(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0., life: int = 20) -> \
            BoilerData:
        boiler_data: BoilerData = BoilerData()
        boiler_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                               self.building_lifetime)
        boiler_data.set_nodes(self._get_heat_node(), self._get_fuel_node())
        self.Boiler.append(boiler_data)
        return boiler_data

    def add_absorption_chiller(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                               life: int = 20) -> ACData:
        ac_data: ACData = ACData()
        ac_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life, self.building_lifetime)
        ac_data.set_nodes(self._get_electrical_node(), self._get_heat_node(), self._get_cool_node())
        self.AC.append(ac_data)
        return ac_data

    def add_electrical_chiller(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                               life: int = 20) -> ECData:
        ec_data: ECData = ECData()
        ec_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life, self.building_lifetime)
        ec_data.set_nodes(self._get_electrical_node(), self._get_cool_node())
        self.EC.append(ec_data)
        return ec_data

    def add_electrical_heater(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                              life: int = 20) -> EHData:
        el_heater_data: EHData = EHData()
        el_heater_data.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                                  self.building_lifetime)
        el_heater_data.set_nodes(self._get_electrical_node(), self._get_heat_node())
        self.EH.append(el_heater_data)
        return el_heater_data

    def set_gurobi_solver(self, threads: int = None, time_limit: float = None, mip_gap: float = None,
                          pre_solve: int = None, heuristic: float = None) -> None:
        """
        set optimization solver to gurobi with the following options:\n
        :param threads: number of threads (default = 2)
        :param time_limit: time limit of optimization (default = 3600 s)
        :param mip_gap: mip gap of optimization [0...1] (default= 0.01)
        :param pre_solve: pre solve usage [0,1,2] 0: no pre solve, 2: maximal pre solve (default = 1
        :param heuristic: heuristic fraction [0...1] 0: no heuristic, 1: maximal heuristic (default = 0.01)
        """
        self.Solver: str = 'gurobi'
        self.threads: int = 2 if threads is None else threads
        self.TimeLimit: float = 3600. if time_limit is None else time_limit
        self.MIPGap: float = 0.01 if mip_gap is None else mip_gap
        self.PreSolve: int = 1 if pre_solve is None else pre_solve
        self.Heuristic: float = 0.01 if heuristic is None else heuristic

    def set_glpk_solver(self, time_limit: float = 3600, mip_gap: float = 0.01, pre_solve: bool = True):
        self.Solver: str = 'glpk'
        self.TimeLimit: float = time_limit
        self.MIPGap: float = mip_gap
        self.PreSolve: int = int(pre_solve)

    def set_cplex_solver(self, threads: int = 2, time_limit: float = 3600, mip_gap: float = 0.01,
                         pre_solve: bool = True):
        self.Solver: str = 'cplex'
        self.threads: int = threads
        self.TimeLimit: float = time_limit
        self.MIPGap: float = mip_gap
        self.PreSolve: int = int(pre_solve)

    def set_txt_solver(self) -> None:
        """
        read the solver to choose from 'solver.txt' file. This function is needed for the validation.
        """
        # with open('solver.txt', 'r') as f:
        solver = 'gurobi'  # f.read()
        if solver == 'gurobi':
            return self.set_gurobi_solver(12, 600, 0.01, 1, 0.01)
        if solver == 'cplex':
            return self.set_cplex_solver(12, 600, 0.01, True)
        self.set_glpk_solver(600, 0.01, True)

    def add_electrical_grid(self, price: Union[str, float, int] = None, reward: Union[str, float, int] = None,
                            demand_charge: float = None, base_price: float = None) -> ExternalConnection:
        """
        add electrical grid node.\n
        :param price: buying price as float or if flexible and taken from data as column name
        :param reward: selling reward as float or if flexible and taken from data as column name
        :param demand_charge: demand charge of maximal power which is taken or taken into node
        :param base_price: base price to even use the electrical grid
        :return: external node class
        """
        # create external node class for electrical grid
        el_grid: ExternalConnection = ExternalConnection()
        el_grid.set_price(price, reward, demand_charge, base_price)
        # set node
        el_grid.set_node(self._get_electrical_node())
        # append class to list of electrical grids
        self.electrical_grids.append(el_grid)
        # return electrical grid external node class
        return el_grid

    def add_district_heating(self, price: Union[str, float, int] = None, reward: Union[str, float, int] = None,
                             power_price: float = None, base_price: float = None) -> ExternalConnection:
        dis_heat: ExternalConnection = ExternalConnection()
        dis_heat.set_price(price, reward, power_price, base_price)
        # set node
        dis_heat.set_node(self._get_heat_node())
        self.district_heat.append(dis_heat)
        return dis_heat

    def add_district_cooling(self, price: Union[str, float, int] = None, reward: Union[str, float, int] = None,
                             power_price: float = None, base_price: float = None) -> ExternalConnection:
        dis_cool: ExternalConnection = ExternalConnection()
        dis_cool.set_price(price, reward, power_price, base_price)
        # set node
        dis_cool.set_node(self._get_cool_node())
        self.district_cool.append(dis_cool)
        return dis_cool

    def add_district_hydro(self, price: Union[str, float, int] = None, reward: Union[str, float, int] = None,
                           power_price: float = None, base_price: float = None) -> ExternalConnection:
        dis_hydro: ExternalConnection = ExternalConnection()
        dis_hydro.set_price(price, reward, power_price, base_price)
        # set node
        dis_hydro.set_node(self._get_hydrogen_node())
        self.district_hydro.append(dis_hydro)
        return dis_hydro

    def add_fuel_connection(self, price: Union[str, float, int] = None, reward: Union[str, float, int] = None,
                            power_price: float = None, base_price: float = None) -> ExternalConnection:
        fuel_connection: ExternalConnection = ExternalConnection()
        fuel_connection.set_price(price, reward, power_price, base_price)
        # set node
        fuel_connection.set_node(self._get_fuel_node())
        self.fuel_connection.append(fuel_connection)
        return fuel_connection

    def add_inverter(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                     life: int = 20, start_node: Node = Node(-1), end_node: Node = Node(-1)) -> NodeConnection:
        return self.add_node_connection(pr_lin_val, pr_con_val, maintenance_val, life,
                                        self.inverter, self._get_electrical_node, start_node, end_node)

    def add_fuel_pipe(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                      life: int = 20, start_node: Node = Node(-1), end_node: Node = Node(-1)) -> NodeConnection:
        return self.add_node_connection(pr_lin_val, pr_con_val, maintenance_val, life,
                                        self.fuel_pipe, self._get_fuel_node, start_node, end_node)

    def add_hydrogen_pipe(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                          life: int = 20, start_node: Node = Node(-1), end_node: Node = Node(-1)) -> NodeConnection:
        return self.add_node_connection(pr_lin_val, pr_con_val, maintenance_val, life,
                                        self.hydrogen_pipe, self._get_hydrogen_node, start_node, end_node)

    def add_heating_pipe(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                         life: int = 20, start_node: Node = Node(-1), end_node: Node = Node(-1)) -> NodeConnection:
        return self.add_node_connection(pr_lin_val, pr_con_val, maintenance_val, life,
                                        self.heating_pipe, self._get_heat_node, start_node, end_node)

    def add_cooling_pipe(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float = 0.,
                         life: int = 20, start_node: Node = Node(-1), end_node: Node = Node(-1)) -> NodeConnection:
        return self.add_node_connection(pr_lin_val, pr_con_val, maintenance_val, life,
                                        self.cooling_pipe, self._get_cool_node, start_node, end_node)

    def add_node_connection(self, pr_lin_val: float, pr_con_val: float, maintenance_val: float,
                            life: int, connection_list: List[NodeConnection],
                            function_2_get_default_node: Callable, start_node: Node = Node(-1),
                            end_node: Node = Node(-1)):
        connection: NodeConnection = NodeConnection()
        connection.set_prices(self.rate_of_interest, pr_lin_val, pr_con_val, maintenance_val, life,
                              self.building_lifetime)
        connection_list.append(connection)
        start_node = start_node if start_node is not None else function_2_get_default_node()
        end_node = end_node if end_node is not None else function_2_get_default_node()
        connection.set_nodes(start_node, end_node)
        return connection

    def calc(self):
        try:
            self.raw: pdDataFrame = pd_read_csv(self.filename, index_col=0)
            # elf.raw = self.raw.round(4).drop_duplicates()
            self.raw = self.raw[~self.raw.index.duplicated(keep='first')]
            date: DatetimeIndex = pd_to_datetime(self.raw.index)
            self.data: array = self.raw.to_numpy()
        except FileNotFoundError:
            print('no file selected')
            raise FileNotFoundError
        reshape: bool = self.raw.shape[0] == self.time_step_per_year
        self.raw: pdDataFrame = pd_concat([self.raw] * self.number_of_years) if reshape else self.raw

        if self.use_tp and not self.tp_data.calculated:
            self.tp_data.aggregate()
            self.raw = self.tp_data.typical_periods
        elif self.use_tp:
            self.raw: pdDataFrame = self.tp_data.typical_periods
            if isinstance(self.raw.index, pd_MultiIndex):
                self.raw.index = range(len(self.raw))

        if not self.use_tp:
            # Date: datetime = pd_to_datetime(self.raw.index)
            self.raw.index = pd_to_datetime(self.raw.index)
            self.dt: NDArray[np_float] = \
                self.raw.reset_index()[self.raw.index.name].diff().dt.total_seconds().fillna(0).values
            self.dt[0] = self.dt[1] if self.integration_rule == 0 else self.dt[0]
        else:
            self.dt: NDArray[np_float] = ones(len(self.raw)) * (date[1] - date[0]).total_seconds()

        self.dt_in_h: NDArray[np_float] = self.dt / 3600
        self.dt_in_h: NDArray[np_float] = self.dt_in_h.repeat(self.number_of_years) if reshape else self.dt_in_h

        [node.reset() for node in self.electrical_nodes]
        [node.reset() for node in self.heat_nodes]
        [node.reset() for node in self.cool_nodes]
        [node.reset() for node in self.hydro_nodes]
        [node.reset() for node in self.fuel_nodes]

        [node_connection.calc(self.raw) for node_connection in self.inverter]
        [node_connection.calc(self.raw) for node_connection in self.heating_pipe]
        [node_connection.calc(self.raw) for node_connection in self.cooling_pipe]
        [node_connection.calc(self.raw) for node_connection in self.fuel_pipe]
        [node_connection.calc(self.raw) for node_connection in self.hydrogen_pipe]

        [demand.calc(self.raw) for demand in self.ElDem]
        [demand.calc(self.raw) for demand in self.HeatD]
        [demand.calc(self.raw) for demand in self.CoolD]
        [demand.calc(self.raw) for demand in self.HydrogenD]
        [demand.calc(self.raw) for demand in self.FuelD]
        [demand.calc(self.raw) for demand in self.MultD]

        [item.calc(self.raw, self.time_step_years, self.rate_of_interest) for item in self.electrical_grids]
        [item.calc(self.raw, self.time_step_years, self.rate_of_interest) for item in self.district_heat]
        [item.calc(self.raw, self.time_step_years, self.rate_of_interest) for item in self.district_cool]
        [item.calc(self.raw, self.time_step_years, self.rate_of_interest) for item in self.district_hydro]
        [item.calc(self.raw, self.time_step_years, self.rate_of_interest) for item in self.fuel_connection]

        [item.calc(self.raw) for item in self.AirHP]
        [item.calc(self.raw) for item in self.PV]
        [item.calc(self.dt_in_h) for item in self.Bat]
        [item.calc(self.dt_in_h) for item in self.HT]
        [item.calc(self.dt_in_h) for item in self.CT]
        [item.calc(self.raw) for item in self.STC]
        [item.calc(self.raw) for item in self.STA]
        [item.calc(self.raw) for item in self.CHP]
        [item.calc(self.raw) for item in self.Boiler]
        [item.calc(self.raw) for item in self.Electrolysis]
        [item.calc(self.raw) for item in self.FuelCell]
        [item.calc(self.dt_in_h) for item in self.H2Storage]
        [item.calc(self.raw) for item in self.Geo]
        [item.calc(self.raw) for item in self.GeoHP]
        [item.calc(self.raw) for item in self.Wind]
        [item.calc(self.raw) for item in self.AC]
        [item.calc(self.raw) for item in self.EC]
        [item.calc(self.raw) for item in self.EH]

        from btso.Optimization import MILProblem
        mp: MILProblem = MILProblem(self)
        self.res = mp.calc()

    def __eq__(self, other) -> bool:
        """
        equality function to check if values are equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has the same values as other
        """
        # if not of same class return false
        if not isinstance(other, DataStorage):
            return False
        # compare all slot values if one not match return false
        for i in self.__slots__:  # type: ignore
            # get object by name
            obj = getattr(self, i)
            # just compare object if it is not an array
            if not isinstance(obj, NDArray) and not isinstance(obj, pdDataFrame):
                # if the object values does not math return False
                if obj != getattr(other, i):
                    return False
        # if all match return true
        return True

    def __ne__(self, other) -> bool:
        """
        not equality function to check if values are not equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has not the same values as other
        """
        # return the opposite of the equality function
        return not self.__eq__(other)
