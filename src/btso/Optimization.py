from typing import Union, List, Sequence, Tuple, Optional
from numpy import zeros, ones, uint32 as np_int, empty, array, min as np_min  # type: ignore
from numpy import float64 as np_float, int64 as np_int, ndarray  # type: ignore
from numpy.typing import NDArray as NDArray  # type: ignore
from gurobipy import Model as gp_Model, GurobiError as gp_GurobiError, GRB  # type: ignore
import scipy.sparse as sp  # type: ignore
from copy import deepcopy as dc  # type: ignore
from btso.DataStorage import DataStorage, BTClass, ExternalConnection, Demand, AirHPData, PVData, BatData, HTData, \
    ECData, CTData, H2StorageData, ACData, EHData, WindData, BoilerData, CHPData, FuelCellData, ElectrolysisData, \
    SolarThermal, GeoData, GeoHPData, MultipleDemands, Storage, PVTData, SellingPower, ProducedPower, Node, \
    NodeConnection, default_array


class ExternalVariable:
    __slots__ = 'var', 'price', 'buy'

    def __init__(self, var: list, prices: NDArray, buy: bool) -> None:
        """
        external connection variable class \n
        :param var: variable of external connection
        :param prices: price to be bought or sold
        :param buy: boolean True if buying variable else selling
        """
        self.var: list = var
        self.price: NDArray = prices
        self.buy: bool = buy


class MILProblem:
    __slots__ = ('ds', '__len_yr', '__len_d', '__ZeroA', '__OnesA', 'li_costs', '__ranLenYr', '__ranLenD',
                 '__ranNumYr', '__ArYrHr', '__ArYrD',  'lb', 'ub', 'type', 'VarName', 'obj', 'val', 'row', 'col',
                 'equal', 'rhs', 'constraint_number', 'x', 'heating_list', 'H2_list', 'cooling_list', 'electrical_list',
                 'inv_list', 'weights', 'fuel_list', 'test')
    # define infinite value
    inf = 1_111_111_111_111  # GRB.INFINITY

    def __init__(self, ds: DataStorage) -> None:
        """
        mixed integer problem calculation class \n
        :param ds: Datastorage containing all necessary information for the optimization
        """
        # save data storage
        self.ds = ds
        # create lists for lower and upper bound values, objective inclusion, type and name of variables
        self.lb: list = []
        self.ub: list = []
        self.obj: list = []
        self.type: list = []
        self.VarName: list = []
        # create lists for array values, row number, column number, equality condition and equality value
        # A * x <= rhs with A of values node_val in row and column (needed to create sparse matrix)
        # equal is either '=' or '<'
        self.val: list = []
        self.row: list = []
        self.col: list = []
        self.equal: list = []
        self.rhs: list = []
        # init row number counter
        self.constraint_number: int = 0
        # init results vector
        self.x: NDArray = zeros(0)
        # determine length of data and range for this length
        self.__len_yr: int = self.ds.raw.shape[0]
        self.__ranLenYr: range = range(self.__len_yr)
        # determine length of typical period year data and range for this
        len_d: int = len(self.ds.tp_data.match_periods) if \
            self.ds.use_tp and self.ds.tp_data.tp_seasonal and not self.ds.tp_data.tp_seasonal_simple else 0
        # determine length of original year of typical period
        self.__len_d: int = len(self.ds.tp_data.matching.PeriodNum) if \
            self.ds.use_tp and self.ds.tp_data.tp_seasonal and self.ds.tp_data.tp_seasonal_simple else len_d
        self.__ranLenD: range = range(self.__len_d if self.ds.use_tp and self.ds.tp_data.tp_seasonal else 0)
        # list and ranges for the consideration of a change in the building technology
        self.__ranNumYr: range = range(self.ds.number_of_years) if self.ds.change_bt else range(1)
        self.__ArYrHr: Sequence[int] = [int(i / self.ds.time_step_per_year) for i in self.__ranLenYr] if \
            self.ds.change_bt else zeros(self.__len_yr, np_int)
        self.__ArYrD: list = [int(i / self.ds.time_step_per_year) for i in self.__ranLenD] if \
            (self.ds.change_bt and self.ds.use_tp and self.ds.tp_data.tp_seasonal) else (
            zeros(self.__len_d, np_int) if self.ds.use_tp and self.ds.tp_data.tp_seasonal else None)
        # init zeros and one's array for data length
        self.__ZeroA: NDArray = zeros(self.__len_yr)
        self.__OnesA: NDArray = ones(self.__len_yr)
        # weights for typical period calculation
        self.weights: NDArray = array([self.ds.tp_data.weights[int(t / self.ds.tp_data.hours_per_period /
                                                                   self.ds.dt_in_h[0])]
                                       for t in self.__ranLenYr]) if self.ds.use_tp else self.__OnesA
        # init list for electrical grid node
        self.electrical_list: list = []
        # init list for heating node
        self.heating_list: list = []
        # init list for cooling node
        self.cooling_list: list = []
        # init list for H2 node
        self.H2_list: list = []
        # init list for H2 node
        self.fuel_list: list = []
        # init list of investment costs
        self.inv_list: list = []
        # init list of costs variables
        self.li_costs: list = []
        ###
        # create demands
        ###
        # create electrical demand
        [self.create_demands(demand, f'Pel_{idx}_', self.electrical_list) for idx, demand in enumerate(self.ds.ElDem)]
        # create heating demand
        [self.create_demands(demand, f'Qh_{idx}_', self.heating_list) for idx, demand in enumerate(self.ds.HeatD)]
        # create cooling demand
        [self.create_demands(demand, f'Qc_{idx}_', self.cooling_list) for idx, demand in enumerate(self.ds.CoolD)]
        # create hydrogen demand
        [self.create_demands(demand, f'h2_d_{idx}_', self.H2_list) for idx, demand in enumerate(self.ds.HydrogenD)]
        # create fuel demand
        [self.create_demands(demand, f'fuel_d_{idx}_', self.H2_list) for idx, demand in enumerate(self.ds.FuelD)]
        # create multiple demands
        [self.create_multiple_demands(demand, f'multi_d_{idx}_') for idx, demand in enumerate(self.ds.MultD)]
        ###
        # create external connections
        ###
        # create electrical grid connection
        [self.create_external_connection(item, f'P_el_{idx}_', self.electrical_list) for idx, item in
         enumerate(self.ds.electrical_grids)]
        # create district heating connection
        [self.create_external_connection(item, f'q_district_heat_{idx}_', self.heating_list) for idx, item in
         enumerate(self.ds.district_heat)]
        # create district cooling connection
        [self.create_external_connection(item, f'q_district_cool_{idx}_', self.cooling_list) for idx, item in
         enumerate(self.ds.district_cool)]
        # create district hydrogen connection
        [self.create_external_connection(item, f'm_district_hydro_{idx}_', self.H2_list) for idx, item in
         enumerate(self.ds.district_hydro)]
        # create district fuel connection
        [self.create_external_connection(item, f'm_fuel_{idx}_', self.fuel_list) for idx, item in
         enumerate(self.ds.fuel_connection)]

    def create_demands(self, demand: Demand, name: str, node: list) -> bool:
        """
        creation of demand constraints and variable if necessary\n
        :param demand: Demand class
        :param name: unique name of demand
        :param node: node of where to add the demand to
        """
        # get demand array if it is used
        data: NDArray = demand.demand if demand.use else self.__ZeroA
        # just append array to node and return if no multiple demands are considered
        if not demand.multi:
            node.append(data)
            return True
        # create range for all multiple demands considered
        ra_le = range(len(data[0, :]))
        # create demand variable and add to node (P_d)
        demand.demand_var.idx = self.create_var(True, f"{name}_", False, self.__ranLenYr,
                                                lb=-MILProblem.inf if np_min(data) < 0 else 0)
        # create one binary for every multiple demand considered (b_d)
        demand.binary.idx = [self.create_var(True, f"b_{name}_{t}", True)[0] for t in ra_le]
        # constraint for linking the demand with the binaries
        # P_d = sum(b_d[i] * data[i])
        self.add_constraint_var(1, demand.demand_var.idx, False, self.__ranLenYr)
        for i in ra_le:
            self.add_constraint_var(-1 * data[:, i], demand.binary.idx[i], False, self.__ranLenYr)
        self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)
        # Constraint to ensure that one binary is selected
        # sum(b_d[i]) = 1
        [self.add_constraint_var(1, demand.binary.idx[i], True) for i in ra_le]
        self.set_constraint_rhs(1, True, True)
        # create costs variable (C_d)
        demand.costs.idx = self.create_var(True, f"Costs_{name}", False)[0]
        # append costs variable to investment costs list
        self.inv_list.append(demand.costs.idx)
        # constraint to link binaries to costs
        # C_d = sum (b_d[i] * price[i])
        self.add_constraint_var(1, demand.costs.idx, True)
        [self.add_constraint_var(-1 * demand.prices[i], demand.binary.idx[i], True) for i in ra_le]
        self.set_constraint_rhs(0, True, True)
        return True

    def create_multiple_demands(self, demand: MultipleDemands, name: str) -> bool:
        # get demands
        p_el = demand.el_demand
        q_h = demand.heat_demand
        q_c = demand.cool_demand
        h2_d = demand.hydro_demand
        fuel_d = demand.fuel_demand
        # determine number of scenarios
        ra_le = range(max(len(p_el[0, :]) if p_el is not default_array else 0, len(q_h[0, :]) if q_h is not default_array else 0,
                          len(q_c[0, :]) if q_c is not default_array else 0, len(h2_d[0, :]) if h2_d is not default_array else 0,
                          len(fuel_d[0, :]) if fuel_d is not default_array else 0))
        # create demand variables
        demand.demand_el.idx = self.create_var(demand.electric, f"Pel_{name}_", False, self.__ranLenYr)
        demand.demand_heat.idx = self.create_var(demand.heating, f"Qh_{name}_", False, self.__ranLenYr)
        demand.demand_cool.idx = self.create_var(demand.cooling, f"Qc_{name}_", False, self.__ranLenYr)
        demand.demand_hydro.idx = self.create_var(demand.hydrogen, f"h2_d_{name}_", False, self.__ranLenYr)
        demand.demand_fuel.idx = self.create_var(demand.fuel, f"fuel_d_{name}_", False, self.__ranLenYr)
        # create binary which select the scenario
        demand.binary.idx = self.create_var(True, f'bin_{name}_', True, ra_le)
        # add electrical demand constraint
        if demand.electric:
            # link binary to demand
            # sum(demand(i) * bin(i)) == 0
            self.add_constraint_var(1, demand.demand_el.idx, False, self.__ranLenYr)
            for i in ra_le:
                self.add_constraint_var(-1 * p_el[:, i], demand.binary.idx[i], False, self.__ranLenYr)
            self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)
        # add heating demand constraint
        if demand.heating:
            # link binary to demand
            # sum(demand(i) * bin(i)) == 0
            self.add_constraint_var(1, demand.demand_heat.idx, False, self.__ranLenYr)
            for i in ra_le:
                self.add_constraint_var(-1 * q_h[:, i], demand.binary.idx[i], False, self.__ranLenYr)
            self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)
        # add cooling demand constraint
        if demand.cooling:
            # link binary to demand
            # sum(demand(i) * bin(i)) == 0
            self.add_constraint_var(1, demand.demand_cool.idx, False, self.__ranLenYr)
            for i in ra_le:
                self.add_constraint_var(-1 * q_c[:, i], demand.binary.idx[i], False, self.__ranLenYr)
            self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)
        # add hydrogen demand constraint
        if demand.hydrogen:
            # link binary to demand
            # sum(demand(i) * bin(i)) == 0
            self.add_constraint_var(1, demand.demand_hydro.idx, False, self.__ranLenYr)
            for i in ra_le:
                self.add_constraint_var(-1 * h2_d[:, i], demand.binary.idx[i], False, self.__ranLenYr)
            self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)

        # add fuel demand constraint
        if demand.fuel:
            # link binary to demand
            # sum(demand(i) * bin(i)) == 0
            self.add_constraint_var(1, demand.demand_fuel.idx, False, self.__ranLenYr)
            for i in ra_le:
                self.add_constraint_var(-1 * fuel_d[:, i], demand.binary.idx[i], False, self.__ranLenYr)
            self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)
        # add constraint so that just one scenario can be selected
        # sum(bin(i)) == 1
        [self.add_constraint_var(1, demand.binary.idx[i], True) for i in ra_le]
        self.set_constraint_rhs(1, True, True)
        # create costs variable
        demand.costs.idx = self.create_var(True, f'C_{name}_', False)[0]
        # add costs variable to investment list
        self.inv_list.append(demand.costs.idx)
        # set costs constraint
        # costs == sum(price(i) * bin(i))
        [self.add_constraint_var(-1 * demand.prices[i], demand.binary.idx[i], True) for i in ra_le]
        self.add_constraint_var(1, demand.costs.idx, True)
        self.set_constraint_rhs(0, True, True)
        return True

    def create_external_connection(self, external_connection: ExternalConnection, name: str, node: list) -> bool:
        """
        function to create external connection constraints and variables (such as the electrical grid)\n
        :param external_connection: external connection class
        :param name: unique external connection name
        :param node: node where to add the buy and sell power to
        """
        # return if the external connection is unused
        if not external_connection.use:
            return False
        # create variable and add to node if the energy should be bought or sold
        li_buy: List[int] = self.create_var(external_connection.buy, f"{name}_buy_", False, self.__ranLenYr,
                                            node=node, node_val=1)
        li_sell: List[int] = self.create_var(external_connection.sell, f"{name}_sell_", False, self.__ranLenYr,
                                             node=node, node_val=-1)
        # create costs variable
        external_connection.cost_var.idx = self.create_var(True, f'{name}_costs', False,
                                                           lb=-MILProblem.inf if external_connection.sell else 0)[0]
        # add costs to investment costs list
        self.inv_list.append(external_connection.cost_var.idx)
        # create base load setting
        if external_connection.use_base_price:
            external_connection.base_price_var.idx = self.create_var(True, f'{name}_base_price', True)[0]
            # set limits of buy variable
            # p_buy <= bin * inf
            if external_connection.buy:
                self.add_constraint_var(1, li_buy, False, self.__ranLenYr)
                self.add_constraint_var(-1_000_000, [external_connection.base_price_var.idx] * self.__len_yr, False,
                                        self.__ranLenYr)
                self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # set limits of sell variable
            # p_sell <= bin * inf
            if external_connection.sell:
                self.add_constraint_var(1, li_sell, False, self.__ranLenYr)
                self.add_constraint_var(-1_000_000, [external_connection.base_price_var.idx] * self.__len_yr, False,
                                        self.__ranLenYr)
                self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # check if energy should be bought
        if external_connection.buy:
            # add list to class variable
            external_connection.power_buy.idx = li_buy
            # limit maximal buy able amount
            if external_connection.max_frac > 0:
                # weights for time series calculation
                weights = [self.ds.tp_data.weights[int(t / self.ds.tp_data.hours_per_period / self.ds.dt_in_h[0])] for
                           t in self.__ranLenYr] if self.ds.use_tp else self.__OnesA
                # add limiting constraint
                # sum(P_buy) <= max_amount or sum(demand) * factor
                [self.add_constraint_var(w, i, True) for i, w in zip(li_buy, weights)]
                if isinstance(external_connection.max_amount, float):
                    # if amount is a fixed value add them to rhs vector
                    self.set_constraint_rhs(external_connection.max_amount, False, True)
                else:
                    # add constant value if no multiple scenario is used
                    if not external_connection.max_amount.multi:
                        self.set_constraint_rhs(external_connection.max_frac * sum(
                            external_connection.max_amount.demand * weights), False, True)
                    else:
                        [self.add_constraint_var(-external_connection.max_frac, i, True) for i in
                         external_connection.max_amount.demand]
                        self.set_constraint_rhs(0, False, True)
        # check if energy should be sold
        if external_connection.sell:
            # add list to class variable
            external_connection.power_sell.idx = li_sell
        # determine demand charge price if it should be considered
        if external_connection.use_power_price:
            # create variable for the maximal amount of power used
            external_connection.max_power.idx = self.create_var(True, f'P_{name}_max_power', False)[0]
            # limit buy power
            # p_buy[t] < max_power
            if external_connection.buy:
                self.add_constraint_var(1, li_buy, False, self.__ranLenYr)
                self.add_constraint_var(-1, [external_connection.max_power.idx] * self.__len_yr, False,
                                        self.__ranLenYr)
                self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # limit sell power
            # p_sell[t] < max_power
            if external_connection.sell:
                self.add_constraint_var(1, li_sell, False, self.__ranLenYr)
                self.add_constraint_var(-1, [external_connection.max_power.idx] * self.__len_yr, False,
                                        self.__ranLenYr)
                self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # determine costs
            # c = power_max * price [X/kW]

        self.add_constraint_var(1, external_connection.cost_var.idx, True)
        if external_connection.use_power_price:
            self.add_constraint_var(-1 * external_connection.power_price, external_connection.max_power.idx, True)
        if external_connection.use_base_price:
            self.add_constraint_var(-1 * external_connection.base_price, external_connection.base_price_var.idx, True)
        if external_connection.buy:
            # create buy price array with either constant or flexible values
            buy: NDArray = self.__OnesA * external_connection.price if external_connection.const_price \
                else external_connection.price_array
            self.add_external_connection_constraint(True, li_buy, buy)
        if external_connection.sell:
            # create reward price array with either constant or flexible values
            reward: NDArray = self.__OnesA * external_connection.reward if external_connection.const_reward \
                else external_connection.reward_array
            self.add_external_connection_constraint(False, li_sell, reward)
        self.set_constraint_rhs(0, True, True)
        return True

    def add_external_connection_constraint(self, buy: bool, variable_indexes: List[int], price: NDArray[np_float]):
        if self.ds.integration_rule == 0:
            [self.add_constraint_var(price, var, True) for price, var in
             zip(price * self.ds.dt_in_h * self.weights * (-1 if buy else 1), variable_indexes)]
            return
        [self.add_constraint_var(price, var, True) for price, var in zip(
            price[0:-1] * self.ds.dt_in_h[1:] * self.weights[0:-1] * -0.5, variable_indexes[0:-1])]
        [self.add_constraint_var(price, var, True) for price, var in zip(
            price[0:-1] * self.ds.dt_in_h[1:] * self.weights[0:-1] * -0.5, variable_indexes[1:])]

    def add_constraint_var(self, value: Union[int, float, List[float], NDArray], variable: Union[int, List[int], None],
                           single: bool = True, ran: Union[range, List[float]] = None):
        """
        add single or multiple variable to the current constraint with the value. \n
        :param value: value of with the variable should be multiplied with
        :param variable: variable index or index list to be added to the current constraint
        :param single: add a single or multiple variables
        :param ran: if multiple variables add this is the range or list of added numbers
        """
        # check if a single variable should be added to the constraint or multiple
        if single:
            # add value, variable and current row number to the corresponding lists
            self.val.append(value)
            self.col.append(variable)
            self.row.append(self.constraint_number)
            return
        # determine length of input range or list
        length = ran.stop if isinstance(ran, range) else len(ran) if isinstance(ran, list) else 0
        # extend value list by either length times the values if the values is a float, int, etc, otherwise extend
        # with the input list or array
        self.val.extend([value] * length) if (not(isinstance(value, list)) and not(isinstance(value, ndarray))) \
            else self.val.extend(value)
        # extend list either with the input list or length times the same variable index
        self.col.extend(variable) if isinstance(variable, list) else self.col.extend([variable] * length)
        # extend row with row number either by the given range or the determined new range by list length
        if isinstance(ran, range):
            self.row.extend([self.constraint_number + t for t in ran])
            return
        self.row.extend([self.constraint_number + t for t in range(length)])

    def set_constraint_rhs(self, rhs: Union[int, float, list, NDArray], equality: bool,
                           single: bool = True, ran: Union[range, list] = range(0)) -> None:
        """
        set result vector values and equality condition (A * x <= rhs)\n
        :param rhs: results vector values or list/array of values
        :param equality: equality or inequality constraint
        :param single: add a single or multiple variables
        :param ran: if multiple variables add this is the range or list of added numbers
        """
        # check if single entry should be created
        if single:
            # append either equality (=) or inequality (<) sign to equal list
            self.equal.append('=') if equality else self.equal.append('<')
            # add value to rhs list
            self.rhs.append(rhs)
            # count constraint number
            self.constraint_number += 1
            return
        # determine length of input range or list
        length = ran.stop if isinstance(ran, range) else len(ran)
        # append either equality (=) or inequality (<) sign to equal list
        self.equal.extend(['='] * length) if equality else self.equal.extend(['<'] * length)
        # add value or values to rhs list
        self.rhs.extend([rhs] * length) if isinstance(rhs, (float, int)) else self.rhs.extend(rhs)
        # add length to constraint number
        self.constraint_number += length

    def create_var(self, used: bool, name: str, binary: bool, ran: range = None, lb: float = None,
                   ub: float = None, *, obj: bool = False, node: list = None, node_val: float = None) \
            -> List[int]:
        """
        create a single or multiple continuous or binary variable/s\n
        :param used: boolean to indicate if the variable should be created
        :param name: name of variable
        :param binary: should the variable be binary (true) or continuous (False)
        :param ran: range of which the variable should be created for. if None a single value is created
        :param obj: boolean which is true if the variable is a part of the objective function
        :param lb: lower bound of variable
        :param ub: upper bound of variable
        :param node: node of which the variable id part of
        :param node_val: value for the node to be added
        :return: list or int of variable index/es or None
        """
        # return None if variable should not be created
        if not used:
            return []
        # create single variable if no range is given
        if ran is None:
            # set default values for lower and upper bound if they are not given
            lb = 0 if lb is None else lb
            ub = (MILProblem.inf if not binary else 1) if ub is None else ub
            # determine start variable index as length of lower bounds list
            start = len(self.lb)
            # add lower and upper bound to lists
            self.lb.append(lb)
            self.ub.append(ub)
            # add objective value 1= used, 0 = not used in objective
            self.obj.append(1) if obj else self.obj.append(0)
            # set type to either continuous or binary and add to list
            self.type.append(GRB.CONTINUOUS) if not binary else self.type.append(GRB.BINARY)
            # append variable name
            self.VarName.append(name)
            # return variable index
            return [start]
        # set default values for lower and upper bound if they are not given
        lb = 0 if lb is None else lb
        ub = (MILProblem.inf if not binary else 1) if ub is None else ub
        # determine start variable index as length of lower bounds list
        start = len(self.lb)
        # determine length by given range
        length = self.__len_yr if ran == self.__ranLenYr else self.__len_d if ran == self.__ranLenD else len(ran)
        # add lower and upper bounds to lists
        self.lb.extend([lb] * length)
        self.ub.extend([ub] * length)
        # add objective value 1= used, 0 = not used in objective
        self.obj.extend([1] * length) if obj else self.obj.extend([0] * length)
        # set types to either continuous or binary and add to list
        self.type.extend([GRB.CONTINUOUS] * length if not binary else [GRB.BINARY] * length)
        # append variable names
        self.VarName.extend([f'{name}{t}' for t in ran])
        # determine variable indexes
        var_index: List[int] = [start + t for t in ran]
        # append to node if node is given
        node.append([var_index, node_val]) if node is not None else None
        # return variable indexes
        return var_index

    def create_storage_variables(self, storage: Storage, name: str, idx: int, node: list) -> None:
        """
        create storage variables for charging, discharging and energy stored\n
        :param storage: Storage class
        :param name: name of storage
        :param idx: index of component
        :param node: list of node power variables
        """
        # create charging power variable
        storage.power_charge.idx = self.create_var(True, f"P_{name}_ch_{idx}_", False, self.__ranLenYr,
                                                   node=node, node_val=-1)
        # create discharging power variable
        storage.power_discharge.idx = self.create_var(True, f"P_{name}_dis_{idx}_", False, self.__ranLenYr,
                                                      node=node, node_val=1)
        # determine if a seasonal storage model is used and then allow negative lower bound
        lb = -self.inf if self.ds.use_tp and self.ds.tp_data.tp_seasonal and not self.ds.tp_data.tp_seasonal_simple \
            else 0
        # determine length which is different for simple seasonal storage model
        ran = self.__ranLenD if self.ds.use_tp and self.ds.tp_data.tp_seasonal and self.ds.tp_data.tp_seasonal_simple \
            else self.__ranLenYr
        # create stored energy variable
        storage.energy.idx = self.create_var(True, f"E_{name}_{idx}_", False, ran, lb=lb)
        # create stored energy variable for start of period status if seasonal storage model is used
        storage.energy_d.idx = self.create_var(self.ds.use_tp and self.ds.tp_data.tp_seasonal and
                                               not self.ds.tp_data.tp_seasonal_simple, f"E_{name}_d_{idx}_", False,
                                               self.__ranLenD)

    def create_storage_constraints(self, storage: Storage, name: str, idx: int) -> None:
        """
        create storage constraints for charging, discharging the storage and limiting the maximal capacity and charging
        and discharging rates\n
        :param storage: Storage class
        :param name: name of storage
        :param idx: index of component
        """
        if self.ds.use_tp and self.ds.tp_data.tp_seasonal_simple:
            self.add_constraint_var(1, storage.energy.idx, False, self.__ranLenD)
            self.add_constraint_var(-1, [storage.size.idx[0] for _ in self.__ranLenD], False,
                                    self.__ranLenD)
            self.set_constraint_rhs(0, False, False, self.__ranLenD)
        elif not(self.ds.use_tp and self.ds.tp_data.tp_seasonal and not self.ds.tp_data.tp_seasonal_simple):
            # limit maximal energy storage state
            # E_t <= E_max
            self.add_constraint_var(1, storage.energy.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1, [storage.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # limit maximal charging power if used
        if storage.max_charge_use:
            # limit maximal charging power
            # P_charge <= f_max * E_max
            self.add_constraint_var(1, storage.power_charge.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * storage.max_charge,
                                    [storage.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # limit maximal discharging power if used
        if storage.max_discharge_use:
            # limit maximal discharging power
            # P_discharge <= f_max * E_max
            self.add_constraint_var(1, storage.power_discharge.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * storage.max_discharge,
                                    [storage.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        if not storage.max_charge_use and not storage.max_discharge_use:
            self.add_constraint_var(1 / storage.eta_discharge, storage.power_discharge.idx, False, self.__ranLenYr)
            self.add_constraint_var(storage.eta_charge, storage.power_charge.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1,
                                    [storage.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # create constraints depending on differential equation method selected
        self.de_storage(storage, f'{name}_{idx}')
        """
        if self.ds.eul_bac:
            self.dgl_backward(storage, f'{name}_{idx}')
            return
        if self.ds.eul_for:
            self.dgl_forward(storage, f'{name}_{idx}')
            return
        self.dgl_central(storage, f'{name}_{idx}')
        return
        """

    def create_solar_thermal(self, st_data: SolarThermal, idx: int) -> bool:
        """
        create solar thermal system in optimization problem\n
        :param st_data: solar thermal system class
        :param idx: index of solar thermal system
        """
        # get relative solar thermal gain per area for cold and hot side in kW/m²
        q_st_a_heat = st_data.power_warm / 1000 if st_data.heat_warm or st_data.cool_warm else None
        q_st_a_cold = st_data.power_cold / 1000 if st_data.heat_cold or st_data.cool_cold else None
        # create variable heat, cool the hot or cold side
        st_data.power_heat_hot.idx = self.create_var(st_data.heat_warm, f"Q_st_heat_hot_{idx}_", False, self.__ranLenYr,
                                                     node=self.heating_list, node_val=1)
        st_data.power_cool_hot.idx = self.create_var(st_data.cool_warm, f"Q_st_cool_hot_{idx}_", False, self.__ranLenYr,
                                                     node=self.heating_list, node_val=-1)
        st_data.power_heat_cold.idx = self.create_var(st_data.heat_cold, f"Q_st_heat_cold_{idx}_", False,
                                                      self.__ranLenYr, node=self.cooling_list, node_val=-1)
        st_data.power_cool_cold.idx = self.create_var(st_data.cool_cold, f"Q_st_cool_cold_{idx}_", False,
                                                      self.__ranLenYr, node=self.cooling_list, node_val=1)
        # create reference size and costs variables
        st_data.set_lists(self.create_bt_costs(st_data, f'A_st_{idx}_', 10 ** 12))
        # init area heat hot tank
        a_heat_hot = self.create_var(st_data.heat_warm, f"A_st_heat_hot_{idx}_", False, self.__ranLenYr)
        # solar thermal system gain has to be below possible heat gain
        if st_data.heat_warm:
            # constraint
            # q_heat_hot = q_st_a_heat * a_heat_hot
            self.add_constraint_var(1, st_data.power_heat_hot.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * q_st_a_heat.clip(0), a_heat_hot, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # init area cool hot tank
        a_cool_hot = self.create_var(st_data.cool_warm, f"A_st_cool_hot_{idx}_", False, self.__ranLenYr)
        # solar thermal system loss has to be below possible heat loss
        if st_data.cool_warm:
            # constraint
            # q_cool_hot = q_st_a_heat * a_cool_hot
            self.add_constraint_var(1, st_data.power_cool_hot.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * (q_st_a_heat * -1).clip(0), a_cool_hot, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # init area heat cold tank
        a_heat_cold = self.create_var(st_data.heat_cold, f"A_st_heat_cold_{idx}_", False, self.__ranLenYr)
        # solar thermal system gain has to be below possible heat gain
        if st_data.heat_cold:
            # constraint
            # q_heat_cold = q_st_a_cold * a_heat_cold
            self.add_constraint_var(1, st_data.power_heat_cold.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * q_st_a_cold.clip(0), a_heat_cold, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # init area cool cold tank
        a_cool_cold = self.create_var(st_data.cool_cold, f"A_st_cool_hot_{idx}_", False, self.__ranLenYr)
        # solar thermal system loss has to be below possible heat loss
        if st_data.cool_cold:
            # constraint
            # q_cool_cold = q_st_a_cold * a_cool_cold
            self.add_constraint_var(1, st_data.power_cool_cold.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * (q_st_a_cold * -1).clip(0), a_cool_cold, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # limit summation of area to total solar thermal collector area
        # a_heat_hot + a_cool_hot + a_heat_cold + a_cool_cold <= a_tot
        self.add_constraint_var(1, a_heat_hot, False, self.__ranLenYr) if st_data.heat_warm else None
        self.add_constraint_var(1, a_cool_hot, False, self.__ranLenYr) if st_data.cool_warm else None
        self.add_constraint_var(1, a_heat_cold, False, self.__ranLenYr) if st_data.heat_cold else None
        self.add_constraint_var(1, a_cool_cold, False, self.__ranLenYr) if st_data.cool_cold else None
        self.add_constraint_var(-1, [st_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                self.__ranLenYr)
        self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        return True

    def create_bat(self, bat_data: BatData, idx: int) -> bool:
        """
        create battery in optimization problem\n
        :param bat_data: battery data class
        :param idx: index of battery system
        :return: None
        """
        # create storage variables in optimization problem with name H2, index idx and add them to node H2
        self.create_storage_variables(bat_data, 'bat', idx, self.electrical_list)
        # create reference size and costs variables
        bat_data.set_lists(self.create_bt_costs(bat_data, f'E_bat_{idx}_', max(bat_data.max_val if bat_data.max else 0,
                                                                               1_000_000)))
        # create storage constraints in optimization problem with name bat and index idx
        self.create_storage_constraints(bat_data, 'bat', idx)
        return True

    def create_h2s(self, h2_storage_data: H2StorageData, idx: int) -> bool:
        """
        create hydrogen storage in optimization problem\n
        :param h2_storage_data: hydrogen storage data class
        :param idx: index of hydrogen storage system
        :return: None
        """
        # create storage variables in optimization problem with name H2, index idx and add them to node H2
        self.create_storage_variables(h2_storage_data, 'H2', idx, self.H2_list)
        # create reference size and costs variables
        h2_storage_data.set_lists(self.create_bt_costs(h2_storage_data, f'E_H2_{idx}_', 1_000_000))
        # create storage constraints in optimization problem with name H2 and index idx
        self.create_storage_constraints(h2_storage_data, 'H2', idx)
        return True

    def create_pv(self, pv_data: PVData, idx: int) -> bool:
        """
        create photovoltaic in optimization problem\n
        :param pv_data: photovoltaic data class
        :param idx: index of photovoltaic system
        """
        # create reference size and costs variables
        pv_data.set_lists(self.create_bt_costs(pv_data, f'A_pv_{idx}_', pv_data.max_val))
        self.create_production_variable(pv_data.produced_power, f"P_pv_{idx}_")
        # Constraint limiting the pv power variable to relative power produced multiplied by total pv area
        # P <= power_rel * A_tot
        self.create_limit_to_component_size_constraint(pv_data, pv_data.produced_power.power_var.idx,
                                                       pv_data.produced_power.power)

        # add variable and constraints if electrical power should be sold
        if pv_data.selling_power.sell_power:
            pv_data.selling_power.power = pv_data.produced_power.power_var
            self.create_sell_variable_constraints(pv_data.selling_power, f"P_sell_PV_{idx}_")
        # add variable and constraints for a constant standby demand
        if pv_data.use_el_demand:
            # create constant demand variable
            pv_data.power_demand.idx = self.create_var(True, f"p_pv_d_{idx}_", False, self.__ranLenYr)
            # set power demand as fraction of total area
            # P_d == frac * A_tot
            self.add_constraint_var(1, pv_data.power_demand.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * pv_data.el_demand,
                                    [pv_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        return True

    def create_absorption_chiller(self, ac_data: ACData, idx: int) -> bool:
        """
        create absorption chiller in optimization problem\n
        :param ac_data: absorption chiller data class
        :param idx: index of absorption chiller system
        """
        # create variable for hot side heat input
        ac_data.power_heat.idx = self.create_var(True, f'q_absorption_heat_{idx}_', False, self.__ranLenYr)
        # create variable for cold side heat input
        ac_data.power_cool.idx = self.create_var(True, f'q_absorption_cool_{idx}_', False, self.__ranLenYr)
        # create variable for electrical demand if considered
        ac_data.power_el.idx = self.create_var(ac_data.el_frac > 0, f'p_absorption_{idx}_', False, self.__ranLenYr)
        # create reference size and costs variables
        ac_data.set_lists(self.create_bt_costs(ac_data, f'q_abs_ref_{idx}_', 1_000_000))
        # limit maximal cooling power to system size
        # q_absorption_cool <= q_abs_ref
        self.create_limit_to_component_size_constraint(ac_data, ac_data.power_cool.idx)
        # link heat demand to cooling power
        # q_absorption_cool == heat_frac * q_absorption_heat
        self.add_constraint_var(1, ac_data.power_cool.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 * ac_data.heat_frac, ac_data.power_heat.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # link cooling power to electrical demand if considered
        if ac_data.el_frac > 0:
            # q_absorption_cool == el_frac * q_absorption_heat
            self.add_constraint_var(1, ac_data.power_el.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * ac_data.el_frac, ac_data.power_cool.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        return True

    def create_electrical_chiller(self, ec_data: ECData, idx: int) -> bool:
        """
        create electrical chiller in optimization problem \n
        :param ec_data: electrical chiller data class
        :param idx: index of eletrical chiller system
        """
        # create variable for electrical demand
        ec_data.power_el.idx = self.create_var(True, f'p_ec_{idx}_', False, self.__ranLenYr)
        # create variable for cooling power
        ec_data.power_cool.idx = self.create_var(True, f'q_ec_{idx}_', False, self.__ranLenYr)
        # create reference size and costs variables
        ec_data.set_lists(self.create_bt_costs(ec_data, f'ec_ref_{idx}_', 1_000_000))
        # determine cooling power limitation factor either constant one or flexible
        q_ec_rel = ec_data.th_power / 1000 if not ec_data.constant_cop else self.__OnesA
        # determine coefficient of performance either constant or time step specific 
        cop = ec_data.cop_flex if not ec_data.constant_cop else self.__OnesA * ec_data.cop
        # limit maximal cooling power to system size times limiting factor
        # q_ec <= q_ec_rel * q_ec_ref
        self.create_limit_to_component_size_constraint(ec_data, ec_data.power_cool.idx, q_ec_rel)
        # link cooling power and electrical demand by the coefficient of performance
        # q_ec == cop * p_ec
        self.add_constraint_var(1, ec_data.power_cool.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 * cop, ec_data.power_el.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        return True

    def create_electrical_heater(self, el_heater_data: EHData, idx: int) -> bool:
        """
        create electrical heater in optimization problem \n
        :param el_heater_data: electrical heater class
        :param idx: index of electrical heater
        """
        # create variable for heat produced
        el_heater_data.power_heat.idx = self.create_var(True, 'q_el_heat_', False, self.__ranLenYr)
        # create reference size and costs variables
        el_heater_data.set_lists(self.create_bt_costs(el_heater_data, f'q_eh_ref_{idx}_', 1_000_000))
        # limit maximal heating power to system size
        # q_ec <= q_ec_ref
        self.create_limit_to_component_size_constraint(el_heater_data, el_heater_data.power_heat.idx)
        return True

    def create_air_hp(self, air_hp_data: AirHPData, idx: int) -> bool:
        """
        create air heat pump in optimization problem \n
        :param air_hp_data: air heat pump data class
        :param idx: index of air heat pump system
        """
        # create variable for electrical demand
        air_hp_data.power_el_var.idx = self.create_var(True, f"P_hp_{idx}_", False, self.__ranLenYr)
        # create varaible for heating power
        air_hp_data.power_heat.idx = self.create_var(True, f"Q_hp_{idx}_", False, self.__ranLenYr)
        # create binary for every time step if a minimal power or binary heat pump status should be considered
        air_hp_data.binary.idx = self.create_var(air_hp_data.binary_status or air_hp_data.minimal_power,
                                                 f"b_Q_hp_{idx}_", True, self.__ranLenYr)
        # create cooling power variable if a cooling power should be considered
        air_hp_data.power_cool.idx = self.create_var(air_hp_data.cooling, f"Q_hp_cool_{idx}_", False, self.__ranLenYr)
        # determine max value
        max_val = min(100_000., air_hp_data.max_val)
        # create reference size and costs variables
        air_hp_data.set_lists(self.create_bt_costs(air_hp_data, f'Q_hp_{idx}', max_val))
        # determine heating power limitation factor either constant one or flexible
        q_hp_rel = air_hp_data.th_power / 1000 if not air_hp_data.constant_cop else self.__OnesA
        # determine either constant or time step specific coefficient of performance for heating
        cop = air_hp_data.cop_flex if not air_hp_data.constant_cop else self.__OnesA * air_hp_data.cop
        # determine either constant or time step specific coefficient of performance for cooling
        cop_c = air_hp_data.cop_flex_cool if not air_hp_data.constant_cop else self.__OnesA * air_hp_data.cop_cool
        # limit heating power to system size times limiting factor
        # p_heat + (1 + 1 / cop_c) * q_cool <= q_hp_rel * q_hp
        self.add_constraint_var(1, air_hp_data.power_heat.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 * q_hp_rel, [air_hp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr],
                                False, self.__ranLenYr)
        if air_hp_data.cooling:
            self.add_constraint_var(1 + 1 / cop_c, air_hp_data.power_cool.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(self.__ZeroA, False, False, self.__ranLenYr)
        # link electrical demand and heating power by coefficient of performance
        # p_hp = q_heat / cop + q_cool / cop_c
        self.add_constraint_var(1, air_hp_data.power_el_var.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 / cop, air_hp_data.power_heat.idx, False, self.__ranLenYr)
        if air_hp_data.cooling:
            self.add_constraint_var(-1 / cop_c, air_hp_data.power_cool.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(self.__ZeroA, True, False, self.__ranLenYr)
        # add constraints for binary of minimal power consideration
        if air_hp_data.binary_status or air_hp_data.minimal_power:
            # link heating / cooling power to the binary
            # q_heat + q_cool * (1 + 1 / cop_c) <= bin * max_val
            self.add_constraint_var(1, air_hp_data.power_heat.idx, False, self.__ranLenYr)
            if air_hp_data.power_cool.idx is not None:
                self.add_constraint_var((1 + 1 / cop_c), air_hp_data.power_cool.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * max_val, air_hp_data.binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # create on and off variable
            q_on = self.create_var(True, f'Q_on_HP_{idx}_', False, self.__ranLenYr)
            q_of = self.create_var(True, f'Q_of_HP_{idx}_', False, self.__ranLenYr)
            # set minimal value of heating and cooling power
            # q_on <= q_heat + q_cool * (1 + 1 / cop_c)
            self.add_constraint_var(-1, air_hp_data.power_heat.idx, False, self.__ranLenYr)
            if air_hp_data.power_cool.idx is not None:
                self.add_constraint_var(-1, air_hp_data.power_cool.idx, False, self.__ranLenYr)
                self.add_constraint_var(-1 / cop_c, air_hp_data.power_cool.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, q_on, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # link on and off variable to minimal power produced
            # q_on + q_of == min_val * q_hp_rel * q_hp
            self.add_constraint_var(1, q_of, False, self.__ranLenYr)
            self.add_constraint_var(1, q_on, False, self.__ranLenYr)
            self.add_constraint_var(-1 * air_hp_data.minimal_power_val * q_hp_rel,
                                    [air_hp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # q_on can just be used if binary is true
            # q_on <= bin * max_val
            self.add_constraint_var(1, q_on, False, self.__ranLenYr)
            self.add_constraint_var(- max_val, air_hp_data.binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # q_of can just be used if binary is False
            # q_of <= (1-bin) * max_val
            self.add_constraint_var(1, q_of, False, self.__ranLenYr)
            self.add_constraint_var(max_val, air_hp_data.binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(max_val, False, False, self.__ranLenYr)
        return True

    def create_geo_hp(self, geo_hp_data: GeoHPData, idx: int) -> bool:
        """
        create water/water or geothermal heat pump in optimization problem \n
        :param geo_hp_data: geothermal heat pump data class
        :param idx: index of geothermal heat pump system
        """
        # create variable for electrical power demand
        geo_hp_data.power_el.idx = self.create_var(True, f"P_GeoHP_{idx}_", False, self.__ranLenYr,
                                                   node=self.electrical_list, node_val=-1)
        # create variable for heating power
        geo_hp_data.power_heat.idx = self.create_var(True, f"Q_GeoHP_{idx}_", False, self.__ranLenYr,
                                                     node=self.heating_list, node_val=1)
        # create variable for heating power
        geo_hp_data.power_heat_2.idx = self.create_var(geo_hp_data.second_node, f"Q_GeoHP_sec_{idx}_", False,
                                                       self.__ranLenYr, node=self.heating_list, node_val=1)
        # create binary for every time step if a minimal power or binary heat pump status should be considered
        geo_hp_data.binary.idx = self.create_var(geo_hp_data.binary_status or geo_hp_data.minimal_power,
                                                 f"b_Q_hp_{idx}_", True, self.__ranLenYr)
        # add cooling power as subtraction of heating and cooling power to cooling node
        self.cooling_list.append([geo_hp_data.power_heat.idx, 1])
        self.cooling_list.append([geo_hp_data.power_el.idx, -1])
        # create reference size and costs variables
        geo_hp_data.set_lists(self.create_bt_costs(geo_hp_data, f'Q_GeoHP_{idx}_', 1_000_000))
        # limit heating power to system size
        # q_heat + q_heat_2 <= q_hp
        self.add_constraint_var(1, geo_hp_data.power_heat.idx, False, self.__ranLenYr)
        if geo_hp_data.second_node:
            self.add_constraint_var(1, geo_hp_data.power_heat_2.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1, [geo_hp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                self.__ranLenYr)
        self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # link heating power and electrical demand with coefficient of performance
        # q_el == q_heat / cop
        self.add_constraint_var(1, geo_hp_data.power_el.idx, False, self.__ranLenYr)
        if geo_hp_data.second_node:
            self.add_constraint_var(-1 / geo_hp_data.cop_2, geo_hp_data.power_heat_2.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 / geo_hp_data.cop, geo_hp_data.power_heat.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # add constraints for binary of minimal power consideration
        if geo_hp_data.binary_status or geo_hp_data.minimal_power:
            # determine max value
            max_val = min(100_000., geo_hp_data.max_val)
            # link heating / cooling power to the binary
            # q_heat + q_cool * (1 + 1 / cop_c) <= bin * max_val
            self.add_constraint_var(1, geo_hp_data.power_heat.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * max_val, geo_hp_data.binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # create on and off variable
            q_on = self.create_var(True, f'Q_on_geo_HP_{idx}_', False, self.__ranLenYr)
            q_of = self.create_var(True, f'Q_of_geo_HP_{idx}_', False, self.__ranLenYr)
            # set minimal value of heating and cooling power
            # q_on <= q_heat + q_cool * (1 + 1 / cop_c)
            self.add_constraint_var(-1, geo_hp_data.power_heat.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, q_on, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # link on and off variable to minimal power produced
            # q_on + q_of == min_val * q_hp_rel * q_hp
            self.add_constraint_var(1, q_of, False, self.__ranLenYr)
            self.add_constraint_var(1, q_on, False, self.__ranLenYr)
            self.add_constraint_var(-1 * geo_hp_data.minimal_power_val,
                                    [geo_hp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # q_on can just be used if binary is true
            # q_on <= bin * max_val
            self.add_constraint_var(1, q_on, False, self.__ranLenYr)
            self.add_constraint_var(- max_val, geo_hp_data.binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # q_of can just be used if binary is False
            # q_of <= (1-bin) * max_val
            self.add_constraint_var(1, q_of, False, self.__ranLenYr)
            self.add_constraint_var(max_val, geo_hp_data.binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(max_val, False, False, self.__ranLenYr)
        return True

    def create_geo(self, geo_data: GeoData, idx: int) -> bool:
        """
        create geothermal system in optimization problem \n
        :param geo_data: geothermal system data class
        :param idx: index of geothermal system
        """
        # create heating power variable
        geo_data.power_heat.idx = self.create_var(True, f"Q_geo_Pos_{idx}_", False, self.__ranLenYr)
        # create cooling power variable
        geo_data.power_cool.idx = self.create_var(True, f"Q_geo_Neg_{idx}_", False, self.__ranLenYr)
        # create electrical demand variable
        geo_data.power_el.idx = self.create_var(geo_data.frac_el > 0, f'p_geo_{idx}_', False, self.__ranLenYr)
        # create reference size and costs variables
        geo_data.set_lists(self.create_bt_costs(geo_data, f'L_Geo_{idx}_', 1_000_000))
        # add electrical demand constraint
        if geo_data.frac_el > 0:
            # set electrical demand to the sum of heating and cooling power times the power fraction
            # p_el = (q_heat + q_cool) * frac_el
            self.add_constraint_var(1 * geo_data.frac_el, geo_data.power_heat.idx, False, self.__ranLenYr)
            self.add_constraint_var(1 * geo_data.frac_el, geo_data.power_cool.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1, geo_data.power_el.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # set constant geothermal gain
        if geo_data.Const:
            # the heating/cooling power is limited to the length times the constant gain factor
            # q_heat + q_cool <= gain * L_geo
            self.add_constraint_var(1, geo_data.power_heat.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, geo_data.power_cool.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * geo_data.Gain / 1000, [geo_data.size.idx[self.__ArYrHr[t]] for
                                                                t in self.__ranLenYr], False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            return True
        # to be explained later
        geo_stor: NDArray[np_int] = empty((self.__len_yr, geo_data.NRadial), dtype=int) if not geo_data.Const else \
            empty(0)
        self.add_constraint_var(1, geo_data.power_heat.idx, False, self.__ranLenYr)
        self.add_constraint_var(1, geo_data.power_cool.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 * float(geo_data.eta_i[0]), [geo_data.size.idx[self.__ArYrHr[t]] for t in
                                                                self.__ranLenYr], False, self.__ranLenYr)
        self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # [self.m.addConstr(geo_data.power_heat_li[t] + geo_data.power_cool_li[t] - self.ds.GeoData.eta_i[0] *
        #                  geo_data.size.idx[self.__ArYrHr[t]] <= 0, f"bal_Q_geo_{t}") for t in self.__ranLenYr]
        for i in range(geo_data.NRadial):
            geo_stor[:, i] = self.create_var(True, f'GeoStor__{idx}_{i}_', False, self.__ranLenYr, -1_000_000.)
        for i in range(geo_data.NRadial):
            self.add_constraint_var(1, geo_stor[:, i], False, self.__ranLenYr)
            self.add_constraint_var(-1 * float(geo_data.CapPerLMax[i]), [geo_data.size.idx[self.__ArYrHr[t]]
                                                                         for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # [self.m.addConstr(geo_stor[t, i] - self.ds.GeoData.CapPerLMax[i] * geo_data.size.idx[self.__ArYrHr[t]]
            #                  <= 0, f"bal_Q_geo_max_{t}_{i}") for t in self.__ranLenYr]
            self.add_constraint_var(-1, geo_stor[:, i], False, self.__ranLenYr)
            self.add_constraint_var(float(geo_data.CapPerLMin[i]), [geo_data.size.idx[self.__ArYrHr[t]] for t in
                                                                    self.__ranLenYr], False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # [self.m.addConstr(-1*geo_stor[t, i] + self.ds.GeoData.CapPerLMin[i] * geo_data.size.idx[self.__ArYrHr[t]]
            #                  <= 0, f"bal_Q_geo_min_{t}_{i}") for t in self.__ranLenYr]

            if i < geo_data.NRadial - 1:
                if i > 0:
                    self.add_constraint_var(float(1 + geo_data.eta_i[i] + geo_data.eta_a[i]),
                                            geo_stor[:, i], False, self.__ranLenYr)
                    self.add_constraint_var(-1, [geo_stor[t - 1, i] for t in self.__ranLenYr], False,
                                            self.__ranLenYr)
                    self.add_constraint_var(-1 * float(geo_data.eta_i[i]), geo_stor[:, i - 1], False,
                                            self.__ranLenYr)
                    self.add_constraint_var(-1 * float(geo_data.eta_a[i]), geo_stor[:, i + 1], False,
                                            self.__ranLenYr)
                    self.set_constraint_rhs(0, True, False, self.__ranLenYr)
                    # [self.m.addConstr(geo_stor[t, i]*(1 + self.ds.GeoData.eta_i[i] + self.ds.GeoData.eta_a[i]) -
                    #                  geo_stor[t-1, i] - self.ds.GeoData.eta_i[i] * geo_stor[t, i-1]
                    #                  - self.ds.GeoData.eta_a[i] * geo_stor[t, i+1] == 0, f"bal_Q_geo_{t}_{i}")
                    # for t in self.__ranLenYr]
                    continue
                self.add_constraint_var(float(1 + geo_data.eta_a[i]), geo_stor[:, i], False,
                                        self.__ranLenYr)
                self.add_constraint_var(-1, [geo_stor[t - 1, i] for t in self.__ranLenYr], False,
                                        self.__ranLenYr)
                self.add_constraint_var(-1, geo_data.power_heat.idx, False, self.__ranLenYr)
                self.add_constraint_var(1, geo_data.power_cool.idx, False, self.__ranLenYr)
                self.add_constraint_var(-1 * float(geo_data.eta_a[i]), geo_stor[:, i + 1], False,
                                        self.__ranLenYr)
                self.set_constraint_rhs(0, True, False, self.__ranLenYr)
                # [self.m.addConstr(geo_stor[t, i] * (1 + self.ds.GeoData.eta_a[i]) - geo_stor[t - 1, i] -
                #                  geo_data.power_heat_li[t] + geo_data.power_cool_li[t] - self.ds.GeoData.eta_a[i] *
                #                  geo_stor[t, i + 1] == 0, f"bal_Q_geo_{t}_{i}") for t in self.__ranLenYr]
                continue
            self.add_constraint_var(float(1 + geo_data.eta_i[i] + geo_data.eta_a[i]),
                                    geo_stor[:, i], False, self.__ranLenYr)
            self.add_constraint_var(-1, [geo_stor[t - 1, i] for t in self.__ranLenYr], False, self.__ranLenYr)
            self.add_constraint_var(-1 * float(geo_data.eta_i[i]), geo_stor[:, i - 1], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # [self.m.addConstr(geo_stor[t, i] * (1 + self.ds.GeoData.eta_i[i] + self.ds.GeoData.eta_a[i]) -
            #                  geo_stor[t - 1, i] - self.ds.GeoData.eta_i[i] * geo_stor[t, i - 1] == 0,
            #                  f"bal_Q_geo_{t}_{i}") for t in self.__ranLenYr]
            # + self.ds.GeoData.eta_a[i] *
            #                               geo_data.size.idx[self.__ArYrHr[t]]
        return True

    def create_hot_tank(self, hot_tank_data: HTData, idx: int) -> bool:
        """
        create hot tank in optimization problem\n
        :param hot_tank_data: hot tank data class
        :param idx: index of hot tank system
        :return: None
        """
        # create storage variables in optimization problem with name hot_tank, index idx and add them to heating node
        self.create_storage_variables(hot_tank_data, 'hot_tank', idx, self.heating_list)
        # determine maximal value
        max_val = 100_000. if not hot_tank_data.max else hot_tank_data.max_val
        # create reference size and costs variables
        hot_tank_data.set_lists(self.create_bt_costs(hot_tank_data, f'Q_HT_{idx}_', max_val))
        # create storage constraints in optimization problem with name hot_tank and index idx
        self.create_storage_constraints(hot_tank_data, 'hot_tank', idx)
        return True

    def create_cold_tank(self, cold_tank_data: CTData, idx: int) -> bool:
        """
        create cold tank in optimization problem\n
        :param cold_tank_data: cold tank data class
        :param idx: index of cold tank system
        :return: None
        """
        # create storage variables in optimization problem with name cold_tank, index idx and add them to cooling node
        self.create_storage_variables(cold_tank_data, 'cold_tank', idx, self.cooling_list)
        # determine maximal value
        max_val = 100_000. if not cold_tank_data.max else cold_tank_data.max_val
        # create reference size and costs variables
        cold_tank_data.set_lists(self.create_bt_costs(cold_tank_data, f'Q_CT_{idx}_', max_val))
        # create storage constraints in optimization problem with name cold_tank and index idx
        self.create_storage_constraints(cold_tank_data, 'cold_tank', idx)
        return True

    def create_boiler(self, boiler_data: BoilerData, idx: int) -> bool:
        """
        create boiler in optimization problem\n
        :param boiler_data: boiler data class
        :param idx: index of boiler system
        :return: None
        """
        # create fuel power variable
        boiler_data.power_fuel.idx = self.create_var(True, f"m_boiler_{idx}_", False, self.__ranLenYr)
        # create thermal power variable
        boiler_data.power_heat.idx = self.create_var(True, f"q_boiler_{idx}_", False, self.__ranLenYr)
        # create reference size and costs variables
        boiler_data.set_lists(self.create_bt_costs(boiler_data, f'q_boiler_{idx}_',
                                                   boiler_data.max_val if boiler_data.max else 1_000_000.))
        # limit heating power to system size
        # q_heat <= q_ref
        self.create_limit_to_component_size_constraint(boiler_data, boiler_data.power_heat.idx)
        # link fuel power to thermal power
        # q_heat * eta = m_fuel
        self.add_constraint_var(boiler_data.eta, boiler_data.power_fuel.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1, boiler_data.power_heat.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        return True

    def create_fuel_cell(self, fc_data: FuelCellData, idx: int) -> bool:
        """
        create fuel cell in optimization problem\n
        :param fc_data: fuel cell data class
        :param idx: index of fuel cell system
        :return: None
        """
        # create hydrogen power demand variable
        fc_data.power_hydro.idx = self.create_var(True, f"M_FC_{idx}_", False, self.__ranLenYr)
        # create electrical power variable
        fc_data.power_el.idx = self.create_var(True, f"P_FC_{idx}_", False, self.__ranLenYr)
        # create thermal power production variable if waste heat should be used
        fc_data.power_heat.idx = self.create_var(fc_data.use_waste_heat, f"Q_FC_{idx}_", False, self.__ranLenYr)

        # create reference size and costs variables
        fc_data.set_lists(self.create_bt_costs(fc_data, f'FC_{idx}_',
                                               fc_data.max_val if fc_data.max else 1_000_000.))
        # limit electrical power to system size
        # p_el <= p_ref
        self.create_limit_to_component_size_constraint(fc_data, fc_data.power_el.idx)
        # link electrical power and hydrogen demand
        # eta_el * m_hydro = p_el
        if fc_data.use_constant_eta:
            self.create_constant_efficiency(fc_data.power_hydro.idx, fc_data.power_el.idx, fc_data.eta_el)
        else:
            if fc_data.use_weak_flexible_eta:
                self.create_weak_flexible_efficiency(fc_data, fc_data.power_el.idx, fc_data.power_hydro.idx,
                                                     fc_data.list_eta_flex_use, 'flex_fc', idx)
            if fc_data.use_constant_flexible_eta:
                self.create_constant_flexible_efficiency(fc_data, fc_data.power_el.idx, fc_data.power_hydro.idx,
                                                         fc_data.list_eta_flex_use, 'flex_fc', idx)
            if fc_data.use_linear_flexible_eta:
                self.create_linear_flexible_efficiency(fc_data, fc_data.power_el.idx, fc_data.power_hydro.idx,
                                                       fc_data.list_eta_flex_use, 'flex_fc', idx)

        # add waste heat usage constraints if it should be used
        if fc_data.use_waste_heat:
            # link hydrogen demand and thermal power with the thermal efficiency eta_th
            # q_heat <= eta_th * m_hydro
            self.create_constant_efficiency(fc_data.power_heat.idx, fc_data.power_hydro.idx, 1/fc_data.eta_th, False)
        return True

    def create_constant_efficiency(self, power_in: list[int], power_out: list[int], efficiency: float,
                                   equality: bool = True) -> None:
        self.add_constraint_var(1 * efficiency, power_in, False, self.__ranLenYr)
        self.add_constraint_var(-1, power_out, False, self.__ranLenYr)
        self.set_constraint_rhs(0, equality, False, self.__ranLenYr)

    def create_weak_flexible_efficiency(self, data_class: BTClass, input: list[int], output: list[int],
                                        efficiency: list[float], name: str, idx: int) -> None:
        p_i: List[List[int]] = [self.create_var(True, f"P_{name}_{i}_{idx}_", False, self.__ranLenYr) for i in
                                range(len(efficiency))]
        for p in p_i:
            self.add_constraint_var(1, p, False, self.__ranLenYr)
        self.add_constraint_var(-1, input, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)

        for p, eta in zip(p_i, efficiency):
            self.add_constraint_var(1 / eta, p, False, self.__ranLenYr)
        self.add_constraint_var(-1, output, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)

        for p in p_i:
            self.create_limit_to_component_size_constraint(data_class, p, 1 / len(efficiency))

    def create_constant_flexible_efficiency(self, data_class: BTClass, input: list[int], output: list[int],
                                            efficiency: list[float], name: str, idx: int) -> None:

        len_n = len(efficiency)
        ran_n = range(len_n)
        p_i: List[List[int]] = [self.create_var(True, f"P_{name}_{i}_{idx}_", False, self.__ranLenYr) for i in
                                ran_n]
        p_tr_i: List[List[int]] = [self.create_var(True, f"P_tr_{name}_{i}_{idx}_", False, self.__ranLenYr) for i in
                                   ran_n]
        b_i: List[List[int]] = [self.create_var(True, f"B_{name}_{i}_{idx}_", True, self.__ranLenYr) for i in
                                ran_n]

        for p in p_i:
            self.add_constraint_var(1, p, False, self.__ranLenYr)
        self.add_constraint_var(-1, input, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)

        for p, eta in zip(p_i, efficiency):
            self.add_constraint_var(1 / eta, p, False, self.__ranLenYr)
        self.add_constraint_var(-1, output, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)

        c = 0
        data_class_size_idx = [data_class.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr]
        max_val = data_class.max_val if data_class.max else 1_000_000
        for p, p_tr, b in zip(p_i, p_tr_i, b_i):
            # Pi + Ptri > c/n*P_ref
            self.add_constraint_var(c/len_n, data_class_size_idx, False, self.__ranLenYr)
            self.add_constraint_var(-1, p, False, self.__ranLenYr)
            self.add_constraint_var(-1, p_tr, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # Pi + Ptri < (c+1)/n*P_ref
            self.add_constraint_var(-1 * (c + 1) / len_n, data_class_size_idx, False, self.__ranLenYr)
            self.add_constraint_var(1, p, False, self.__ranLenYr)
            self.add_constraint_var(1, p_tr, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)

            self.add_constraint_var(-1 * (c + 1) / len_n * max_val, b, False, self.__ranLenYr)
            self.add_constraint_var(1, p, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            c += 1

        for b in b_i:
            self.add_constraint_var(1, b, False, self.__ranLenYr)
        self.set_constraint_rhs(1, True, False, self.__ranLenYr)

    def create_linear_flexible_efficiency(self, data_class: BTClass, input: list[int], output: list[int],
                                          efficiency: list[float], name: str, idx: int) -> None:

        len_n = len(efficiency)
        ran_n = range(len_n)
        p_i: List[List[int]] = [self.create_var(True, f"P_{name}_{i}_{idx}_", False, self.__ranLenYr) for i in
                                ran_n]
        b_i: List[List[int]] = [self.create_var(True, f"B_{name}_{i}_{idx}_", True, self.__ranLenYr) for i in
                                ran_n]

        for p in p_i:
            self.add_constraint_var(1, p, False, self.__ranLenYr)
        self.add_constraint_var(-1, input, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)

        for p, eta in zip(p_i, efficiency):
            self.add_constraint_var(1 / eta, p, False, self.__ranLenYr)
        self.add_constraint_var(-1, output, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)

        data_class_size_idx = [data_class.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr]
        max_val = data_class.max_val if data_class.max else 1_000_000
        for p, b in zip(p_i, b_i):
            # Pi < c/n*P_ref
            self.add_constraint_var(-1/len_n, data_class_size_idx, False, self.__ranLenYr)
            self.add_constraint_var(1, p, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)

            self.add_constraint_var(-max_val / len_n, b, False, self.__ranLenYr)
            self.add_constraint_var(-1, p, False, self.__ranLenYr)
            self.add_constraint_var(1 / len_n, data_class_size_idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)

        for p, b in zip(p_i[1:], b_i[:-1]):
            self.add_constraint_var(max_val / len_n, b, False, self.__ranLenYr)
            self.add_constraint_var(1, p, False, self.__ranLenYr)
            self.set_constraint_rhs(max_val / len_n, False, False, self.__ranLenYr)

        self.add_constraint_var(1, b_i[-1], False, self.__ranLenYr)
        self.set_constraint_rhs(1, True, False, self.__ranLenYr)

    def create_electrolysis(self, es_data: ElectrolysisData, idx: int) -> bool:
        """
        create electrolysis in  optimization problem\n
        :param es_data: electrolysis data class
        :param idx: index of electrolysis system
        :return: None
        """
        # create hydrogen power demand variable
        es_data.power_hydro.idx = self.create_var(True, f"M_ES_{idx}_", False, self.__ranLenYr)
        # create electrical power variable
        es_data.power_el.idx = self.create_var(True, f"P_ES_{idx}_", False, self.__ranLenYr)
        # create thermal power production variable if waste heat should be used
        es_data.power_heat.idx = self.create_var(es_data.use_waste_heat, f"Q_ES_{idx}_", False, self.__ranLenYr)
        # create reference size and costs variables
        es_data.set_lists(self.create_bt_costs(es_data, f'ES_{idx}_',
                                               es_data.max_val if es_data.max else 1_000_000.))
        # limit electrical power to system size
        # p_el <= p_ref
        self.create_limit_to_component_size_constraint(es_data, es_data.power_el.idx)
        # link electrical power and hydrogen produced
        # p_el * eta_el == m_hydro
        self.add_constraint_var(1, es_data.power_hydro.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1 * es_data.eta_el, es_data.power_el.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # add waste heat usage constraints if it should be used
        if es_data.use_waste_heat:
            # link hydrogen demand and thermal power with the thermal efficiency eta_th
            # q_heat <= eta_th * m_hydro
            self.add_constraint_var(1, es_data.power_heat.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * es_data.eta_th, es_data.power_el.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        return True

    def create_chp(self, chp_data: CHPData, idx: int) -> bool:
        """
        create combined heat and power plant in optimization problem\n
        :param chp_data: chp data class
        :param idx: index of chp system
        :return: None
        """
        # create fuel power demand variable
        chp_data.power_fuel.idx = self.create_var(True, f"M_CHP_{idx}_", False, self.__ranLenYr, node=self.fuel_list,
                                                  node_val=-1)
        # create electrical power produced variable
        chp_data.power_el.idx = self.create_var(True, f"P_CHP_{idx}_", False, self.__ranLenYr,
                                                node=self.electrical_list, node_val=1)
        # create variables and binaries for limiting the minimal power
        chp_data.power_el_min.idx = self.create_var(chp_data.minimal_power, f"P_CHP_min_{idx}_", False, self.__ranLenYr)
        chp_data.power_el_min_tr.idx = self.create_var(chp_data.minimal_power, f"P_CHP_min_tr_{idx}_", False,
                                                       self.__ranLenYr)
        chp_data.power_el_max.idx = self.create_var(chp_data.minimal_power, f"P_CHP_max_{idx}_", False, self.__ranLenYr)
        chp_data.power_el_max_tr.idx = self.create_var(chp_data.minimal_power, f"P_CHP_max_tr_{idx}_", False,
                                                       self.__ranLenYr)
        chp_data.power_el_binary.idx = self.create_var(chp_data.minimal_power, f"b_P_CHP_min_{idx}_", True,
                                                       self.__ranLenYr)
        # determine maximal value
        max_val: float = chp_data.max_val if chp_data.max else 100_000.
        # create reference size and costs variables
        chp_data.set_lists(self.create_bt_costs(chp_data, f'CHP_{idx}_', max_val))
        # limit maximal electrical power to system size
        # p_el <= p_ref
        self.create_limit_to_component_size_constraint(chp_data, chp_data.power_el.idx)
        # link fuel power and electrical power with electrical efficiency eta_el
        # p_el = eta_el * m_fuel
        self.add_constraint_var(1 * chp_data.eta_el, chp_data.power_fuel.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1, chp_data.power_el.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # check if electrical power can be sold
        if chp_data.selling_power.sell_power:
            chp_data.selling_power.power = chp_data.power_el
            self.create_sell_variable_constraints(chp_data.selling_power, f"P_sell_CHP_{idx}_")
        # check if thermal power can be produce eta_th > 0
        if chp_data.eta_th > 0:
            # create thermal power produced variable
            chp_data.power_heat.idx = self.create_var(True, f"Q_CHP_{idx}_", False, self.__ranLenYr,
                                                      node=self.heating_list, node_val=1)
            # link fuel power demand and thermal power with thermal efficiency eta_th
            # p_heat = eta_th * m_fuel
            self.add_constraint_var(-1 * chp_data.eta_th, chp_data.power_fuel.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, chp_data.power_heat.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
        # check if the maximal upward gradient should be considered:
        if chp_data.active_max_gradient_up:
            # p_el[t] - p_el[t-1] <= f * size * dt
            self.add_constraint_var(1, chp_data.power_el.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1, [chp_data.power_el.idx[t - 1] for t in self.__ranLenYr], False, self.__ranLenYr)
            self.add_constraint_var(-1 * chp_data.max_gradient_up * self.ds.dt_in_h,
                                    [chp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # check if the maximal downward gradient should be considered:
        if chp_data.active_max_gradient_down:
            # p_el[t-1] - f * size * dt <=  p_el[t]
            self.add_constraint_var(-1, chp_data.power_el.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, [chp_data.power_el.idx[t - 1] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.add_constraint_var(-1 * chp_data.max_gradient_down * self.ds.dt_in_h,
                                    [chp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
        # check if a minimal power value should be considered
        if chp_data.minimal_power:
            # limit the minimal power with the creation of placeholder variable
            # 0/ f_min * p_ref <= p_el <= p_ref
            # create placeholder variable
            p_chp_x = self.create_var(True, f"P_CHP_X_{idx}_", False, self.__ranLenYr)
            # p_chp_x + p_el == p_max
            self.add_constraint_var(1, p_chp_x, False, self.__ranLenYr)
            self.add_constraint_var(1, chp_data.power_el.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1, chp_data.power_el_max.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # p_chp_x + p_min <= p_max
            self.add_constraint_var(1, p_chp_x, False, self.__ranLenYr)
            self.add_constraint_var(-1, chp_data.power_el_max.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, chp_data.power_el_min.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # p_min + p_min_tr = p_ref * f_min
            self.add_constraint_var(1, chp_data.power_el_min.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, chp_data.power_el_min_tr.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * chp_data.minimal_power_val,
                                    [chp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # p_max + p_max_tr = p_ref
            self.add_constraint_var(1, chp_data.power_el_max.idx, False, self.__ranLenYr)
            self.add_constraint_var(1, chp_data.power_el_max_tr.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1, [chp_data.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr],
                                    False, self.__ranLenYr)
            self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # determine max value as an array if wanted
            max_val: Optional[NDArray[np_float], float] = chp_data.limit_array if chp_data.is_limit_set else max_val
            # p_min <= bin * max_val
            self.add_constraint_var(1, chp_data.power_el_min.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * max_val * chp_data.minimal_power_val, chp_data.power_el_binary.idx, False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # p_max <= bin * max_val
            self.add_constraint_var(1, chp_data.power_el_max.idx, False, self.__ranLenYr)
            self.add_constraint_var(-1 * max_val, chp_data.power_el_binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(0, False, False, self.__ranLenYr)
            # get max of max_val
            max_val = max_val if isinstance(max_val, float) else max_val.max()
            # p_min_tr <= (1-bin) * max_val
            self.add_constraint_var(1, chp_data.power_el_min_tr.idx, False, self.__ranLenYr)
            self.add_constraint_var(max_val * chp_data.minimal_power_val, chp_data.power_el_binary.idx, False,
                                    self.__ranLenYr)
            self.set_constraint_rhs(max_val * chp_data.minimal_power_val, False, False, self.__ranLenYr)
            # p_max_tr <= (1-bin) * max_val
            self.add_constraint_var(1, chp_data.power_el_max_tr.idx, False, self.__ranLenYr)
            self.add_constraint_var(max_val, chp_data.power_el_binary.idx, False, self.__ranLenYr)
            self.set_constraint_rhs(max_val, False, False, self.__ranLenYr)
            # set minimal runtime like 4 hours if set to
            if chp_data.minimal_runtime:
                # create constraints to ensure a minimal runtime
                # sum(bin(i)) * f_time_steps for i in range(t-n-1, t)<= bin(t) + bin(t-n)
                # get number of time steps to run (n)
                n_time_steps = chp_data.minimal_runtime_val
                # determine factor
                f_time_steps = 1 / (n_time_steps - 1)
                for t in self.__ranLenYr:
                    self.add_constraint_var(-1, chp_data.power_el_binary.idx[t], True)
                    self.add_constraint_var(-1, chp_data.power_el_binary.idx[t - n_time_steps], True)
                    for i in range(t - n_time_steps + 1, t):
                        self.add_constraint_var(f_time_steps, chp_data.power_el_binary.idx[i], True)
                    self.set_constraint_rhs(0, False, True)
        return True

    def create_wind(self, wind_data: WindData, idx: int) -> bool:
        """
        create wind turbine in optimization problem\n
        :param wind_data: wind turbine data class
        :param idx: index of wind turbine
        :return: None
        """
        # create reference size and costs variables
        wind_data.set_lists(self.create_bt_costs(wind_data, f'Wind_{idx}_',
                                                 1_000_000 if not wind_data.max else wind_data.max_val))
        self.create_production_variable(wind_data.produced_power, f"P_wind_{idx}_")
        # limit wind power produced to system size
        # p_el <= p_rel * p_ref
        self.create_limit_to_component_size_constraint(wind_data, wind_data.produced_power.power_var.idx,
                                                       wind_data.produced_power.power)
        # check if power can be sold
        if wind_data.selling_power.sell_power:
            wind_data.selling_power.power = wind_data.produced_power.power_var
            self.create_sell_variable_constraints(wind_data.selling_power, f"P_sell_Wind_{idx}_")
        return True

    def create_sell_variable_constraints(self, selling_power: SellingPower, name: str):
        # determine either constant or time step specific selling reward
        c_sell = self.__OnesA * selling_power.c_sell_c if selling_power.cSellConst else selling_power.c_sell_f
        # create electrical power selling variable
        selling_power.power_sell.idx = self.create_var(True, name, False, self.__ranLenYr)
        # append selling power with reward to cost list
        self.li_costs.append(ExternalVariable(selling_power.power_sell.idx, c_sell, False))
        # limit selling power to the power produced by the chp
        # p_el_sell <= p_el
        self.add_constraint_var(1, selling_power.power_sell.idx, False, self.__ranLenYr)
        self.add_constraint_var(-1, selling_power.power.idx, False, self.__ranLenYr)
        self.set_constraint_rhs(0, False, False, self.__ranLenYr)

    def create_production_variable(self, production_data: ProducedPower, name: str):
        # create wind power produced variable
        production_data.power_var.idx = self.create_var(True, name, False, self.__ranLenYr)

    def create_limit_to_component_size_constraint(self, bt_class: BTClass, limiting_variable: List[int],
                                                  factor: Union[float, NDArray[np_float]] = None) -> None:
        factor = 1. if factor is None else factor
        # limit power produced to system size
        # p_el <= p_rel * p_ref
        self.add_constraint_var(1, limiting_variable, False, self.__ranLenYr)
        self.add_constraint_var(-1 * factor, [bt_class.size.idx[self.__ArYrHr[t]] for t in self.__ranLenYr], False,
                                self.__ranLenYr)
        self.set_constraint_rhs(0, False, False, self.__ranLenYr)

    def create_pvt(self, pvt_data: PVTData, idx: int) -> None:
        """
        create photovoltaic + solar thermal in optimization problem\n
        :param pvt_data: photovoltaic + solar thermal data class
        :param idx: index of wind turbine
        :return: None
        """
        self.create_solar_thermal(pvt_data, idx)

    def create_bt_costs(self, bt_data: BTClass, name: str, max_val: float) -> Tuple[List[int], List[int], List[int],
                                                                                    int]:
        """
        creation of building technology costs variables and constraints.\n
        :param bt_data: building technology class or parent of this (like PVData)
        :param name: name of building technology
        :param max_val: maximal value of building technology
        :return: list of reference size, list of binaries for constant costs, list of reference size during the years
        if change in technology is considered, int of costs variable
        """
        # create reference size of building technology one for each year considered
        var_ref: List[int] = self.create_var(True, f"{name}_ref", False, self.__ranNumYr, bt_data.min_val,
                                             bt_data.max_val)
        # create binary to apply constant costs if no minimal value is selected
        b_var_ref: List[int] = [1 if bt_data.min and bt_data.min_val > 0 else
                                self.create_var(True, f"b_{name}_{i}", True)[0] for i in self.__ranNumYr]
        """
        consideration of costs in steps (in Progress) 
        int_var = self.create_vars_flex_int(bt_data.st, f'{name}_Int_', self.__ranNumYr,
                                            0, int((bt_data.max_val - bt_data.min_val) / bt_data.StepWidthVal + 1))
        if bt_data.StepWidth:
            for i in self.__ranNumYr:
                self.add_constraint_var(1, var_ref[i], True)
                self.add_constraint_var(-1 * bt_data.StepWidthVal, int_var[i], True)
                self.set_constraint_rhs(bt_data.min_val, True, True) if bt_data.min and bt_data.min_val > 0 \
                    else self.set_constraint_rhs(0, True, True)
        """
        # check if building technology can be changed during the years considered
        if self.ds.change_bt:
            # run function to add variables and constraints for the consideration of a change in building technology
            var_ref_yr, c = self.create_change_bt(bt_data, var_ref, b_var_ref, max_val, name)
            return var_ref, b_var_ref, var_ref_yr, c
        # create link between binary and reference size to avoid that the technology exists without that the binary
        # is chosen
        # var_ref[0] <= b_var_ref[0] * max_val
        if b_var_ref[0] != 1:
            self.add_constraint_var(1, var_ref[0], True)
            self.add_constraint_var(-1 * max_val, b_var_ref[0], True)
            self.set_constraint_rhs(0, False, True)

        # create a costs variable
        c = self.create_var(True, f"Costs_{name}_ref", False)[0]
        # add costs variable to investment list so that it can be implemented later on
        self.inv_list.append(c)
        # link costs and reference size
        # c = ref_size * pr_lin + bin * pr_con
        self.add_constraint_var(1, c, True)
        self.add_constraint_var(-1 * bt_data.pr_lin[0] if isinstance(bt_data.pr_lin, list) else bt_data.pr_lin,
                                var_ref[0], True)
        if b_var_ref[0] != 1:
            self.add_constraint_var(-1 * bt_data.pr_con[0] if isinstance(bt_data.pr_con, list) else bt_data.pr_con,
                                    b_var_ref[0], True)
            self.set_constraint_rhs(0, True, True)
        else:
            self.set_constraint_rhs(bt_data.pr_con[0] if isinstance(bt_data.pr_con, list) else bt_data.pr_con, True,
                                    True)
        # return variable with an 0 entry for the change in technology variable
        return var_ref, b_var_ref, [0], c

    def create_change_bt(self, bt_data: BTClass, var_ref: list, b_var_ref: list, max_val: float, name: str) -> \
            Tuple[List[int], int]:
        """
        creation of variables and constraint for the consideration of a change in building technology during the years\n
        :param bt_data: building technology class or parent of this (like PVData)
        :param var_ref: reference size variable list
        :param b_var_ref: binaries list for constant costs consideration
        :param max_val: maximal value of building technology
        :param name: name of technology
        :return: list of reference size during the years if change in technology is considered, int of costs variable
        """
        # create variable for reference size during the years
        var_ref_yr: List[int] = self.create_var(True, f"{name}_ref_yr", False, self.__ranNumYr, bt_data.min_val,
                                                bt_data.max_val)
        # get component lifetime in time steps
        life_in_time_steps = max(int(bt_data.life / self.ds.time_step_years), 1)
        # var_ref[i] = sum(var_ref_yr[j] for j in technology lifetime)
        self.add_constraint_var(-1, var_ref, False, self.__ranNumYr)
        for t in self.__ranNumYr:
            for j in range(max(0, t + 1 - life_in_time_steps), t + 1):
                self.val.append(1)
                self.col.append(var_ref_yr[j])
                self.row.append(self.constraint_number + t)
        self.set_constraint_rhs(0, True, False, self.__ranNumYr)
        # check if technology should just be changed once during its lifetime
        if self.ds.change_building_once:
            # create binary of no binary exists
            b_var_ref = [self.create_var(True, f"b_{name}_", True, self.__ranNumYr)] if b_var_ref[0] == 1 else b_var_ref
            # avoid the binary to be more the one time be one during the technology lifetime
            # 1 >= sum(bin[i] for i in lifetime)
            for t in self.__ranNumYr:
                for j in range(max(0, t + 1 - life_in_time_steps), t + 1):
                    self.val.append(1)
                    self.col.append(b_var_ref[j])
                    self.row.append(self.constraint_number)
                self.set_constraint_rhs(1, False, True)

        # create link between binary and reference size to avoid that the technology exists without that the binary
        # is chosen
        # var_ref[i] <= b_var_ref[i] * max_val
        if b_var_ref[0] != 1:
            self.add_constraint_var(1, var_ref_yr, False, self.__ranNumYr)
            self.add_constraint_var(-1 * max_val, b_var_ref, False, self.__ranNumYr)
            self.set_constraint_rhs(0, False, False, self.__ranNumYr)
        # create a costs variable
        c = self.create_var(True, f"Costs_{name}_ref", False)[0]
        # add costs variable to investment list so that it can be implemented later on
        self.inv_list.append(c)
        # link costs and reference size
        # c = sum(ref_size[i] * pr_lin[i] + bin[i] * pr_con[i] for i in range of years)
        self.add_constraint_var(1, c, True)
        [self.add_constraint_var(-1 * bt_data.pr_lin[j], var_ref_yr[j], True) for j in self.__ranNumYr]
        if b_var_ref[0] != 1:
            [self.add_constraint_var(-1 * bt_data.pr_con[j], b_var_ref[j], True) for j in self.__ranNumYr]
            self.set_constraint_rhs(0, True, True)
            return var_ref_yr, c
        self.set_constraint_rhs(bt_data.pr_con[0] * self.ds.number_of_years, True, True)
        return var_ref_yr, c

    def de_storage(self, storage: Storage, name: str) -> None:
        """
        creating differential equations for the storage \n
        :param storage: storage class
        :param name: name of storage
        """
        # get list of energy status variables
        e_stor = storage.energy.idx
        # get self discharging efficiency
        eta = storage.eta_self_discharge
        # create list with charging and discharging variable and their efficiencies
        power_list_short = [[storage.power_charge.idx,
                             storage.eta_charge * (1 if self.ds.eul_for or self.ds.eul_bac else 0.5)],
                            [storage.power_discharge.idx,
                             (-1 if self.ds.eul_for or self.ds.eul_bac else -0.5) / storage.eta_discharge]]
        # check if typical days should be used
        if self.ds.use_tp:
            if self.ds.tp_data.tp_seasonal_simple:
                self.create_simple_seasonal_storage_equations(storage, e_stor, eta, power_list_short)
                return
            # check if a seasonal storage model should be considered
            if self.ds.tp_data.tp_seasonal:
                # set constraints for the seasonal storage model
                self.create_seasonal_storage_equations(storage, name, e_stor, eta, power_list_short)
                return
            # set constraints for the cyclic storage model
            self.create_cyclic_storage_equations(storage, name, e_stor, eta, power_list_short)
            return
        # set normal storage equations
        self.create_normal_storage_equations(storage, e_stor, eta, power_list_short)

    def create_cyclic_storage_equations(self, storage: Storage, name: str, e_stor: list, eta: NDArray,
                                        power_list_short: list):
        # start counter for the hour of the typical day
        hour = -1
        # get amount of hours per period
        hours_per_period = self.ds.tp_data.hours_per_period
        # determine amount of hours per period subtracted with one for the counting process
        hours_per_period_1 = hours_per_period - 1
        # check if a start value should be considered
        if storage.start:
            # get size of storage variable
            start_var = storage.size.idx
            # get relative start state of charge like 0.5
            start_val = storage.start_val
        # otherwise, create start state variable and limit it to the maximal storage size
        else:
            # create variable for start state of charge
            start_var = [self.create_var(True, f'E_start_{name}_{i}', False)[0] for i in storage.size.idx]
            # create range for entries in storage.size.idx
            ran_start = range(len(start_var))
            # set start value to one because it is now no longer a ratio
            start_val = 1
            # limit starting value to maximal storage size
            # start_var <= size
            self.add_constraint_var(1, start_var, False, ran_start)
            self.add_constraint_var(-1, storage.size.idx, False, ran_start)
            self.set_constraint_rhs(0, False, False, ran_start)
        # loop over every hour of the time series / typical periods hours to create the constraints
        for t in self.__ranLenYr:
            hour = hour + 1 if hour < hours_per_period_1 else 0
            # check if it is the first hour of the typical period
            if hour < 1:
                # add constraint for the start value
                # e_stor[start] = start_val * size +
                # euler backward
                # p_charge[start] * eta_charge - p_discharge[start] / eta_discharge
                # euler central
                # (p_charge[start]+p_charge[end])/2 * eta_charge - (p_discharge[start]+p_discharge[end])/2
                # / eta_discharge
                self.add_constraint_var(1, e_stor[t], True)
                self.add_constraint_var(-start_val, start_var[self.__ArYrHr[t]], True)
                if self.ds.eul_bac or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t], var[0][t], True) for var in
                     power_list_short]
                self.set_constraint_rhs(0, True, True)
            # check if it is not the last hour of the period
            if hour < hours_per_period_1:
                """
                add constraint to link state of charge with charging and discharging values depending on the de-
                solver selected
                e_stor[t+1] = e_stor[t] * eta + 
                euler forward:
                p_charge[t] * eta_charge - p_discharge[t] / eta_discharge
                euler backward:
                p_charge[t+1] * eta_charge - p_discharge[t+1] / eta_discharge
                euler central
                (p_charge[t] + p_charge[t+1])/2 * eta_charge - (p_discharge[t]+p_discharge[t+1])/2 / eta_discharge
                """
                self.add_constraint_var(1, e_stor[t + 1], True)
                self.add_constraint_var(-1 * eta[t], e_stor[t], True)
                if self.ds.eul_for or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t], var[0][t], True)
                     for var in power_list_short]
                if self.ds.eul_bac or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t + 1], var[0][t + 1], True)
                     for var in power_list_short]
                self.set_constraint_rhs(0, True, True)
                continue
            # last hour of the period
            # add constraint for the last value
            # (start_val * size) or start_var = e_stor[end] +
            # euler backward
            # p_charge[end] * eta_charge - p_discharge[end] / eta_discharge
            # euler central
            # (p_charge[start]+p_charge[end])/2 * eta_charge - (p_discharge[start]+p_discharge[end])/2
            # / eta_discharge
            self.add_constraint_var(-1 * eta[t], e_stor[t], True)
            self.add_constraint_var(start_val, start_var[self.__ArYrHr[t]], True)
            if self.ds.eul_for or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t], var[0][t], True)
                 for var in power_list_short]
            self.set_constraint_rhs(0, True, True)

    def create_normal_storage_equations(self, storage: Storage, e_stor: list, eta: NDArray,
                                        power_list_short: list) -> None:
        """
        create normal storage equations for the complete year depending on the differential equation solver selected. \n
        :param storage: Storage class to create the equations for
        :param e_stor: variable of stored energy
        :param eta: self discharging rate for every hour of the year
        :param power_list_short: list of variable for charging and discharging with their efficiencies
        """
        # determine range for all last hour of "year" entries
        ran_end = range(self.ds.time_step_per_year - 1, self.ds.time_step_per_year * self.ds.number_of_years,
                        self.ds.time_step_per_year)
        # determine range for all first hour of "year" entries
        ran_start = range(0, self.ds.time_step_per_year * self.ds.number_of_years, self.ds.time_step_per_year)
        # set storage constraints for "year" excluding the last entries
        for t in [i for i in self.__ranLenYr if not(i in ran_end)]:
            """
            add constraint to link state of charge with charging and discharging values depending on the de solver 
            selected
            e_stor[t+1] = e_stor[t] * eta + 
            euler forward:
            p_charge[t] * eta_charge - p_discharge[t] / eta_discharge
            euler backward:
            p_charge[t+1] * eta_charge - p_discharge[t+1] / eta_discharge
            euler central
            (p_charge[t] + p_charge[t+1])/2 * eta_charge - (p_discharge[t]+p_discharge[t+1])/2 / eta_discharge
            """
            self.add_constraint_var(1, e_stor[t + 1], True)
            self.add_constraint_var(-1 * eta[t], e_stor[t], True)
            if self.ds.eul_for or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t], var[0][t], True) for var in power_list_short]
            if self.ds.eul_bac or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t + 1], var[0][t + 1], True)
                 for var in power_list_short]
            self.set_constraint_rhs(0, True, True)
        # set constraint for the starting value
        if storage.start:
            # loop over all start entries
            for i, j in zip(ran_start, ran_end):
                # add constraint for the start value
                # e_stor[0] = start_val * size +
                # euler backward
                # p_charge[0] * eta_charge - p_discharge[0] / eta_discharge
                # euler central
                # p_charge[0]/2 * eta_charge - p_discharge[0]/2 / eta_discharge
                self.add_constraint_var(1, e_stor[i], True)
                self.add_constraint_var(-1 * storage.start_val * (1 if self.ds.eul_for else eta[i]),
                                        storage.size.idx[self.__ArYrHr[i]], True)
                if self.ds.eul_bac or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[i], var[0][i], True)
                     for var in power_list_short]
                if self.ds.eul_for or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[j], var[0][j], True)
                     for var in power_list_short]
                self.set_constraint_rhs(0, True, True)
            return
        # otherwise, use cyclic approach by looping over first and last entries
        for i, j in zip(ran_start, ran_end):
            # add constraint for the start value
            # e_stor[0] = eta * e_stor[last] +
            # euler forward
            # p_charge[last] * eta_charge - p_discharge[last] / eta_discharge
            # euler backward
            # p_charge[0] * eta_charge - p_discharge[0] / eta_discharge
            # euler central
            # (p_charge[0]+p_charge[last])/2 * eta_charge - (p_discharge[0] + p_discharge[last])/2 / eta_discharge
            self.add_constraint_var(1, e_stor[i], True)
            self.add_constraint_var(-1 * eta[j], e_stor[j], True)
            if self.ds.eul_bac or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[i], var[0][i], True) for var in power_list_short]
            if self.ds.eul_for or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[j], var[0][j], True) for var in power_list_short]
            self.set_constraint_rhs(0, True, True)

    def create_simple_seasonal_storage_equations(self, storage: Storage, e_stor: list, eta: NDArray,
                                                 power_list_short: list) -> None:
        """
        create simple seasonal storage equations for the tycial periods depending on the differential equation
        solver selected. \n
        :param storage: Storage class to create the equations for
        :param e_stor: variable of stored energy
        :param eta: self discharging rate for every hour of the year
        :param power_list_short: list of variable for charging and discharging with their efficiencies
        """
        # get last period of year to calculate old time step
        period = self.ds.tp_data.matching.PeriodNum[-1]
        # get hour within the last period to calculate old time step
        period_hour = self.ds.tp_data.matching.TimeStep[-1]
        # determine old time step
        t_old = period_hour + period * 24
        # set storage constraints for "year" excluding the last entries
        for t in self.__ranLenD:
            # get current period of year to calculate old time step
            period = self.ds.tp_data.matching.PeriodNum[t]
            # get hour within the current period to calculate old time step
            period_hour = self.ds.tp_data.matching.TimeStep[t]
            # determine current time step
            t_new = period_hour + period * 24
            """
            add constraint to link state of charge with charging and discharging values depending on the de solver 
            selected
            e_stor[t+1] = e_stor[t] * eta + 
            euler forward:
            p_charge[t] * eta_charge - p_discharge[t] / eta_discharge
            euler backward:
            p_charge[t+1] * eta_charge - p_discharge[t+1] / eta_discharge
            euler central
            (p_charge[t] + p_charge[t+1])/2 * eta_charge - (p_discharge[t]+p_discharge[t+1])/2 / eta_discharge
            """
            self.add_constraint_var(1, e_stor[t], True)
            self.add_constraint_var(-1 * eta[0], e_stor[t - 1], True)
            if self.ds.eul_for or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t_old], var[0][t_old], True) for var in
                 power_list_short]
            if self.ds.eul_bac or self.ds.eul_cen:
                [self.add_constraint_var(-1 * var[1] * self.ds.dt_in_h[t_new], var[0][t_new], True)
                 for var in power_list_short]
            self.set_constraint_rhs(0, True, True)
            # overwrite old time step
            t_old = t_new

    def create_seasonal_storage_equations(self, storage: Storage, name: str, e_stor: list, eta: NDArray,
                                          power_list_short: list) -> None:
        """
        create typical period seasonal storage constraints for the limits and linking of period to the yearly
        variables \n
        :param storage: storage class
        :param name: name of component
        :param e_stor: list of variables of stored or extracted energy within the period
        :param eta: self discharge rate
        :param power_list_short: list of variable for charging and discharging with their efficiencies
        """
        # get period variable
        period_var = storage.energy_d.idx
        # limit maximal energy storage state at the beginning /end of each period
        # E_d <= E_max
        self.add_constraint_var(1, period_var, False, self.__ranLenD)
        self.add_constraint_var(-1, [storage.size.idx[self.__ArYrD[t]] for t in self.__ranLenD], False, self.__ranLenD)
        self.set_constraint_rhs(0, False, False, self.__ranLenD)
        # calculate self discharge rate for one period
        eta_ts = eta[0] ** self.ds.tp_data.hours_per_period
        # init hour of period counter
        hour = -1
        # get amount of hours per period
        hours_per_period = self.ds.tp_data.hours_per_period
        # determine amount of hours per period subtracted with one for the counting process
        hours_per_period_1 = hours_per_period - 1
        # loop over all time steps and set constraints depending on which seasonal storage model is selected
        for t in self.__ranLenYr:
            # determine hour within the current period
            hour = hour + 1 if hour < hours_per_period_1 else 0
            # determine day / period of the current time step
            day = int(t / self.ds.tp_data.hours_per_period / self.ds.dt_in_h[0])
            # set constraints for every day / period represented by the current time steps period
            for day_j, MD in [(d, MD) for d, MD in enumerate(self.ds.tp_data.match_periods) if MD[0] == day]:
                # create within period limits:
                # 0 < E_d * eta_ts + dE_t < E_max
                # create limiting variable
                r = self.create_var(True, f"{name}_Min_{t}_{day_j}_k", False)[0]
                # r + E_d * eta_ts + dE_t = E_max
                self.add_constraint_var(1, r, True)
                self.add_constraint_var(1, e_stor[t], True)
                self.add_constraint_var(eta[0] ** self.ds.tp_data.matching.TimeStep[t], period_var[day_j], True)
                self.add_constraint_var(-1, storage.size.idx[self.__ArYrHr[t]], True)
                self.set_constraint_rhs(0, True, True)
                # r < E_max
                self.add_constraint_var(1, r, True)
                self.add_constraint_var(-1, storage.size.idx[self.__ArYrHr[t]], True)
                self.set_constraint_rhs(0, False, True)
                # if seasonal + model is used add more constraints
                if MD[1] > 1:
                    # calculate self discharge rate for more than one period
                    eta_ts_j = eta_ts ** MD[1]
                    # determine self discharging factor
                    f_self = (1 - eta_ts_j) / (1 - eta_ts) if eta_ts != 1 else MD[1]  # (1 + eta_ts ^1 + eta_ts^2+.)
                    # create within period limits:
                    # 0 < E_d-1 * (eta_ts - eta_ts_j / f_self) + E_d / f_self + dE_t * eta_ts < E_max
                    # create limiting variable
                    r = self.create_var(True, f"{name}_Min_{t}_2bd_{day_j}_k", False)[0]
                    # r < E_max
                    self.add_constraint_var(1, r, True)
                    self.add_constraint_var(-1, storage.size.idx[self.__ArYrHr[t]], True)
                    self.set_constraint_rhs(0, False, True)
                    # r + E_d-1 * (eta_ts - eta_ts_j / f_self) + E_d / f_self + dE_t * eta_ts = E_max
                    self.add_constraint_var(1, r, True)
                    self.add_constraint_var(eta_ts_j / (eta_ts * f_self) *
                                            eta[0] ** self.ds.tp_data.matching.TimeStep[t], period_var[day_j], True)
                    self.add_constraint_var((1 - 1 / f_self)/eta_ts*eta[0] ** self.ds.tp_data.matching.TimeStep[t],
                                            period_var[(day_j + 1) if day_j + 1 < self.__len_d else 0], True)
                    self.add_constraint_var(1, e_stor[t], True)
                    self.add_constraint_var(-1, storage.size.idx[self.__ArYrHr[t]], True)
                    self.set_constraint_rhs(0, True, True)
            # set constraints for charging and discharging within the period
            # set first value for the extracted or stored energy of period to zero
            if hour < 1:
                # 0 = e_stor(t=start) +
                # for backward and central euler (otherwise they are unused or not completely used)
                # p_charge[t] * eta_charge - p_discharge[t] / eta_discharge
                self.add_constraint_var(1, e_stor[t], True)
                if self.ds.eul_bac or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1], var[0][t], True) for var in power_list_short]
                self.set_constraint_rhs(0, True, True)
            # set charging and discharging equation
            if hour < hours_per_period_1:
                # e_stor(t+1) = e_stor(t) +
                # forward euler
                # p_charge[t] * eta_charge - p_discharge[t] / eta_discharge
                # backward euler
                # p_charge[t + 1] * eta_charge - p_discharge[t + 1] / eta_discharge
                # central euler
                # (p_charge[t] + p_charge[t + 1])/2 * eta_charge - (p_discharge[t] + p_discharge[t + 1])/2 /
                # eta_discharge
                self.add_constraint_var(1, e_stor[t + 1], True)
                self.add_constraint_var(-1 * eta[t], e_stor[t], True)
                if self.ds.eul_for or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1], var[0][t], True) for var in power_list_short]
                if self.ds.eul_bac or self.ds.eul_cen:
                    [self.add_constraint_var(-1 * var[1], var[0][t + 1], True) for var in power_list_short]
                self.set_constraint_rhs(0, True, True)
            else:
                # link last hour of the period to the next yearly state variable
                for day_j, MD in [(d, MD) for d, MD in enumerate(self.ds.tp_data.match_periods) if MD[0] == day]:
                    # determine self discharging factor for more than one period in a row
                    md_j = (1 - eta_ts ** MD[1]) / (1 - eta_ts) if eta_ts != 1 else MD[1]
                    # determine self discharging rate for more than one period in a row
                    eta_i = eta_ts ** MD[1]
                    # e_d(t+1) = e_d(t) * eta_i + eta * md_j * e_stor(t=end) +
                    # for forward and central euler (otherwise they are unused or not completely used)
                    # (p_charge[t] * eta_charge - p_discharge[t] / eta_discharge) * eta * md_j
                    self.add_constraint_var(1, period_var[(day_j + 1) if day_j + 1 < self.__len_d else 0], True)
                    self.add_constraint_var(-1 * eta_i, period_var[day_j], True)
                    self.add_constraint_var(-1 * md_j * eta[t], e_stor[t], True)
                    if self.ds.eul_for or self.ds.eul_cen:
                        [self.add_constraint_var(-1 * var[1] * md_j * self.ds.dt_in_h[t], var[0][t], True)
                         for var in power_list_short]
                    self.set_constraint_rhs(0, True, True)

    def create_node_connection(self, node_connection: NodeConnection, idx: int, name: str):
        node_connection.set_lists(self.create_bt_costs(node_connection, f'{name}_pipe_{idx}_',
                                                       1_000_000 if not node_connection.max else
                                                       node_connection.max_val))
        node_connection.power_input_start.idx = self.create_var(True, f'{name}_{idx}_input_start_', False,
                                                                self.__ranLenYr)
        node_connection.power_input_end.idx = self.create_var(node_connection.bidirectional,
                                                              f'{name}_{idx}_input_end_', False, self.__ranLenYr)
        self.create_limit_to_component_size_constraint(node_connection, node_connection.power_input_start.idx,
                                                       self.__OnesA)
        if node_connection.bidirectional:
            self.create_limit_to_component_size_constraint(node_connection, node_connection.power_input_end.idx,
                                                           self.__OnesA)

    def create_balance(self, node: Node) -> bool:
        """
        create balance for the node
        :param node: node with power variables
        """
        # set equality constraint
        # sum(power) == demand
        # sum up all entries in node which are variables
        [self.add_constraint_var(val, var.idx, False, self.__ranLenYr) for var, val in zip(node.variables,
                                                                                           node.values)]
        # create a deep copy of zero array to avoid overwriting
        demand_sum = dc(self.__ZeroA)
        # sum up all entries which are float / arrays
        for demand in node.demands:
            demand_sum += demand
        self.set_constraint_rhs(demand_sum, True, False, self.__ranLenYr)
        return True

    def calc(self) -> dict:
        """
        start calculation of optimization model\n
        :return: results dict
        """
        # create variables and constraints for every component considered
        [self.create_pv(item, idx) for idx, item in enumerate(self.ds.PV)]
        [self.create_bat(item, idx) for idx, item in enumerate(self.ds.Bat)]
        [self.create_air_hp(item, idx) for idx, item in enumerate(self.ds.AirHP)]
        [self.create_geo_hp(item, idx) for idx, item in enumerate(self.ds.GeoHP)]
        [self.create_geo(item, idx) for idx, item in enumerate(self.ds.Geo)]
        [self.create_solar_thermal(item, idx) for idx, item in enumerate(self.ds.STC)]
        [self.create_solar_thermal(item, idx) for idx, item in enumerate(self.ds.STA)]
        [self.create_boiler(item, idx) for idx, item in enumerate(self.ds.Boiler)]
        [self.create_chp(item, idx) for idx, item in enumerate(self.ds.CHP)]
        [self.create_wind(item, idx) for idx, item in enumerate(self.ds.Wind)]
        [self.create_fuel_cell(item, idx) for idx, item in enumerate(self.ds.FuelCell)]
        [self.create_h2s(item, idx) for idx, item in enumerate(self.ds.H2Storage)]
        [self.create_electrolysis(item, idx) for idx, item in enumerate(self.ds.Electrolysis)]
        [self.create_absorption_chiller(item, idx) for idx, item in enumerate(self.ds.AC)]
        [self.create_electrical_chiller(item, idx) for idx, item in enumerate(self.ds.EC)]
        [self.create_electrical_heater(item, idx) for idx, item in enumerate(self.ds.EH)]
        [self.create_hot_tank(item, idx) for idx, item in enumerate(self.ds.HT)]
        [self.create_cold_tank(item, idx) for idx, item in enumerate(self.ds.CT)]
        # create node connections
        [self.create_node_connection(node_connection, idx, 'electrical')
         for idx, node_connection in enumerate(self.ds.inverter)]
        [self.create_node_connection(node_connection, idx, 'heating')
         for idx, node_connection in enumerate(self.ds.heating_pipe)]
        [self.create_node_connection(node_connection, idx, 'cooling')
         for idx, node_connection in enumerate(self.ds.cooling_pipe)]
        [self.create_node_connection(node_connection, idx, 'hydrogen')
         for idx, node_connection in enumerate(self.ds.hydrogen_pipe)]
        [self.create_node_connection(node_connection, idx, 'fuel')
         for idx, node_connection in enumerate(self.ds.fuel_pipe)]
        # create node balances
        [self.create_balance(node) for node in self.ds.electrical_nodes]
        [self.create_balance(node) for node in self.ds.heat_nodes]
        [self.create_balance(node) for node in self.ds.cool_nodes]
        [self.create_balance(node) for node in self.ds.hydro_nodes]
        [self.create_balance(node) for node in self.ds.fuel_nodes]
        # create external connection costs constraints and variables
        if self.li_costs:
            # initialize grid interaction variable
            grid_inter: List[int] = self.create_var(True, 'grid_inter_', False, self.__ranLenYr, -MILProblem.inf,
                                                    obj=True)
            # determine external connection costs by Riemann sum
            if self.ds.integration_rule == 0:
                # add constraint for riemann sum
                # grid_inter = sum(1/-1 * price * weights * dt * var) for var in  li_costs
                self.add_constraint_var(1, grid_inter, False, self.__ranLenYr)
                [self.add_constraint_var(i.price * self.ds.dt_in_h * self.weights * (-1 if i.buy else 1), i.var, False,
                                         self.__ranLenYr) for i in self.li_costs]
                self.set_constraint_rhs(0, True, False, self.__ranLenYr)
            # determine external connection costs by trapezoid rule
            else:
                # add constraint for trapezoid rule
                # grid_inter[t] = sum(0.5/-0.5 * price[t] * weights[t] * dt[t] * var[t]) for var in  li_costs +
                # sum(0.5/-0.5 * price[t + 1] * weights[t + 1] * dt[t + 1] * var[t + 1]) for var in  li_costs
                self.add_constraint_var(1, grid_inter[0:-1], False, self.__ranLenYr[0:-1])
                [self.add_constraint_var(i.price[0:-1] * self.ds.dt_in_h[1:] * self.weights[0:-1] * (-0.5 if i.buy else 0.5),
                                         i.var[0:-1], False, self.__ranLenYr[0:-1]) for i in self.li_costs]
                [self.add_constraint_var(i.price[0:-1] * self.ds.dt_in_h[1:] * self.weights[0:-1] * (-0.5 if i.buy else 0.5),
                                         i.var[1:], False, self.__ranLenYr[0:-1]) for i in self.li_costs]
                self.set_constraint_rhs(0, True, False, self.__ranLenYr[0:-1])
                # last value of grid_inter has to be zero
                # grid_inter(t=end) = 0
                self.add_constraint_var(1, grid_inter[-1], True)
                self.set_constraint_rhs(0, True, True)

        # create component cost variable
        costs_comp = self.create_var(True, "costs_comp", False, lb=-MILProblem.inf, obj=True)[0]
        # create cost constraint
        # costs_comp = sum(c in inv_list)
        self.add_constraint_var(1, costs_comp, True)
        [self.add_constraint_var(-1, c, True) for c in self.inv_list]
        self.set_constraint_rhs(0, True, True)

        ###
        # set pyomo variables
        ###
        if not(self.ds.Solver == 'gurobi'):
            import pyomo.environ as pye  # type: ignore
            import pyomo.opt as pyo  # type: ignore
            m = pye.ConcreteModel()
            variables: list = []
            sum_obj = 0
            m.non_negative_reals = pye.VarList(domain=pye.NonNegativeReals, initialize=0)
            if any([i < 0 for i in self.lb]):
                m.reals = pye.VarList(domain=pye.Reals, initialize=0)
            if any([i == GRB.BINARY for i in self.type]):
                m.binaries = pye.VarList(domain=pye.Binary, initialize=1)
            for lb, ub, types, obj in zip(self.lb, self.ub, self.type, self.obj):
                if types == GRB.CONTINUOUS and lb >= 0:
                    variable = m.non_negative_reals.add()
                elif types == GRB.CONTINUOUS:
                    variable = m.reals.add()
                else:
                    variable = m.binaries.add()
                variable.setlb(lb)
                variable.setub(ub)
                variables.append(variable)
                if obj == 1:
                    sum_obj += 1*variable
            m.obj = pye.Objective(sense=pye.minimize, expr=sum_obj)
            # create constraint matrix
            mat = sp.csr_matrix((self.val, (self.row, self.col)), shape=(self.constraint_number, len(variables)))
            rows, cols = mat.nonzero()
            sum_val = 0
            row_old = 0
            m.constraints = pye.ConstraintList()
            for row, col in zip(rows, cols):
                if row > row_old:
                    if self.equal[row_old] == '<':
                        m.constraints.add((sum_val <= self.rhs[row_old]))
                    else:
                        m.constraints.add((sum_val == self.rhs[row_old]))
                    sum_val = variables[col] * mat[row, col]
                    row_old = row
                    continue
                sum_val += variables[col] * mat[row, col]
            if self.equal[row_old] == '<':
                m.constraints.add((sum_val <= self.rhs[row_old]))
            else:
                m.constraints.add((sum_val == self.rhs[row_old]))
            solver = pyo.SolverFactory(self.ds.Solver)
            if self.ds.Solver == 'cplex':
                # https://ampl.com/products/solvers/solvers-we-sell/cplex/options/
                solver.options['timelimit'] = self.ds.TimeLimit
                solver.options['threads'] = self.ds.threads
                solver.options['presolve'] = self.ds.PreSolve  # [0/1]
                solver.options['mipgap'] = self.ds.MIPGap
            #elif self.ds.Solver == 'glpk':
            #    # http://www.maximalsoftware.com/solvopt/optglpk.html
            #    solver.options['tmlim'] = self.ds.TimeLimit
            #    solver.options['presol'] = self.ds.PreSolve  # [0/1]
            #    solver.options['mipgap'] = self.ds.MIPGap
            elif self.ds.Solver == 'gurobi':
                solver.options['TimeLimit'] = self.ds.TimeLimit
                solver.options['Presolve'] = self.ds.PreSolve
                solver.options['MIPGap'] = self.ds.MIPGap
                solver.options['Heuristics'] = self.ds.Heuristic
                solver.options['Threads'] = self.ds.threads
            result = solver.solve(m, tee=True)
            stat = result.solver.status
            values: NDArray = array([i.value for i in variables])
            num_vars = len([v for v in m.component_data_objects(pye.Var, active=True)])

            num_constraints = len([c for c in m.component_data_objects(pye.Constraint, active=True)])
            # create return dict
            res = {'time': result.solver.Time, 'Obj': pye.value(m.obj), 'GAP': 0, 'stat': stat, 'NumVars': num_vars,
                   'NumConstrs': num_constraints}
            # return results dict
            return self.get_results(values, res)
        ###
        # set Gurobi values, parameters and start optimization
        ###
        # Create an optimization model
        model = gp_Model("mip1")
        # create variables in gurobi model
        variables = model.addMVar(len(self.lb), self.lb, self.ub, self.obj, self.type, self.VarName)
        # create constraint matrix
        mat = sp.csr_matrix((self.val, (self.row, self.col)),
                            shape=(self.constraint_number, variables.shape[0]))  # type: ignore
        mat.eliminate_zeros()
        # set constraints in gurobi model
        model.addMConstr(mat, variables, self.equal, self.rhs, 'c')
        # set Gurobi parameter
        model.Params.OutputFlag = 0  # no print outs
        model.Params.Presolve = self.ds.PreSolve  # set pre solve
        model.Params.MIPGap = self.ds.MIPGap  # set MIP gap accuracy
        model.Params.TimeLimit = self.ds.TimeLimit  # set time limit
        model.Params.Heuristics = self.ds.Heuristic  # set heuristic value
        model.Params.Threads = self.ds.threads  # set usable threads
        # start optimization
        model.optimize()  # optimize!
        model.write('model.lp')
        # get mip gap if available (if binaries are used)
        try:
            mip_gap = model.MIPGAP
        except AttributeError:
            mip_gap = 0

        # get model result status and calc IIS if no result is found
        if model.Status == 3:
            stat = 'Infeasible'
            model.computeIIS()
            model.write('model.lp')
            model.write('problem.ilp')
        elif model.Status == 4:
            stat = 'Infeasible or unbound'
            model.write('model.lp')
            model.computeIIS()
            model.write('problem.ilp')
        elif model.Status == 5:
            stat = 'unbound'
        else:
            stat = 'solved'
        # return a nearly empty dict if no solution is found
        if not stat == 'solved':
            res = {'stat': stat}
            return res
        # try to get results vector
        try:
            self.x = variables.X  # type: ignore
        except gp_GurobiError:
            self.x = zeros(len(self.lb))
        values = self.x

        #print(f'size: {self.x[self.ds.FuelCell[0].size.idx]}')
        #print(self.x[self.test[0][0]].sum(), self.x[self.test[0][1]].sum())
        #for i, j, k, l in zip(self.test[0][0], self.test[0][1], self.test[1][0], self.test[1][1]):
        #    print(self.x[i], self.x[j], self.x[k], self.x[l])

        # create return dict
        res = {'time': model.Runtime, 'Obj': model.ObjVal, 'GAP': mip_gap, 'stat': stat, 'NumVars': model.NumVars,
               'NumConstrs': model.NumConstrs}

        return self.get_results(values, res)

    def get_results(self, values: NDArray, results_dict: dict) -> dict:
        # get results for every component considered
        [item.get_results(values, self.__ZeroA) for item in self.ds.AirHP]
        [item.get_results(values, self.__ZeroA) for item in self.ds.AC]
        [item.get_results(values, self.__ZeroA) for item in self.ds.PV]
        [item.get_results(values, self.__ZeroA) for item in self.ds.Bat]
        [item.get_results(values, self.__ZeroA) for item in self.ds.HT]
        [item.get_results(values, self.__ZeroA) for item in self.ds.CT]
        [item.get_results(values, self.__ZeroA) for item in self.ds.CHP]
        [item.get_results(values, self.__ZeroA) for item in self.ds.FuelCell]
        [item.get_results(values, self.__ZeroA) for item in self.ds.Electrolysis]
        [item.get_results(values, self.__ZeroA) for item in self.ds.H2Storage]
        [item.get_results(values, self.__ZeroA) for item in self.ds.Boiler]
        [item.get_results(values, self.__ZeroA) for item in self.ds.EH]
        [item.get_results(values, self.__ZeroA) for item in self.ds.Wind]
        [item.get_results(values, self.__ZeroA) for item in self.ds.STC]
        [item.get_results(values, self.__ZeroA) for item in self.ds.STA]
        [item.get_results(values, self.__ZeroA) for item in self.ds.EC]
        [item.get_results(values, self.__ZeroA) for item in self.ds.Geo]
        [item.get_results(values, self.__ZeroA) for item in self.ds.GeoHP]
        # get results for every demand considered
        [demand.get_results(values, self.__ZeroA) for demand in self.ds.ElDem]
        [demand.get_results(values, self.__ZeroA) for demand in self.ds.HeatD]
        [demand.get_results(values, self.__ZeroA) for demand in self.ds.CoolD]
        [demand.get_results(values, self.__ZeroA) for demand in self.ds.HydrogenD]
        [demand.get_results(values, self.__ZeroA) for demand in self.ds.FuelD]
        # get results for every external connection considered
        [connection.get_results(values) for connection in self.ds.electrical_grids]
        [connection.get_results(values) for connection in self.ds.district_heat]
        [connection.get_results(values) for connection in self.ds.district_cool]
        [connection.get_results(values) for connection in self.ds.district_hydro]
        [connection.get_results(values) for connection in self.ds.fuel_connection]
        # get node connection results
        [node_connection.get_results(values, self.__ZeroA) for node_connection in self.ds.inverter]
        [node_connection.get_results(values, self.__ZeroA) for node_connection in self.ds.hydrogen_pipe]
        [node_connection.get_results(values, self.__ZeroA) for node_connection in self.ds.fuel_pipe]
        [node_connection.get_results(values, self.__ZeroA) for node_connection in self.ds.heating_pipe]
        [node_connection.get_results(values, self.__ZeroA) for node_connection in self.ds.cooling_pipe]

        # return results dict
        return results_dict
