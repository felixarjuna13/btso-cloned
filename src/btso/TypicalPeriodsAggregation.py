from numpy import array_equal, ndarray, zeros  # type: ignore
from pandas import to_datetime as pd_to_datetime, DataFrame  # type: ignore
from typing import Optional


class CreateTP:
    """
    class to create typical periods using the TSAM module:
    https://github.com/FZJ-IEK3-VSA/tsam
    """
    __slots__ = ('data', 'no_typical_periods', 'hours_per_period', 'cluster_method', 'solver', 'extreme_period_method',
                 'list_max', 'list_min', 'tp_seasonal', 'tp_seasonal_plus', 'tp_seasonal_simple', 'df', 'weights',
                 'error', 'typical_periods', 'new_times', 'matching', 'match_period', 'match_periods', 'calculated',
                 'cluster_method_idx', 'extreme_period_method_idx')

    def __init__(self, data: DataFrame, no_typical_periods: int, hours_per_period: int, cluster_method_idx: int,
                 tp_seasonal: bool = None, tp_seasonal_plus: bool = None, tp_seasonal_simple: bool = None,
                 solver: str = None, extreme_period_method_idx: int = None, list_max: list = None,
                 list_min: list = None) -> None:
        """
        class to create typical periods using the TSAM module. \n
        :param data: data as a pandas DataFrame
        :param no_typical_periods: number of typical periods [#]
        :param hours_per_period: hours per period [#]
        :param cluster_method_idx: cluster method [0,1,2,3]; 1=averaging, 2= hierarchical, 3=k_means, 4=k_medoids
        :param tp_seasonal: should a seasonal storage model be considered? [True, False]
        :param tp_seasonal_plus: should the seasonal + storage model be considered? [True, False]
        :param tp_seasonal_simple: should the simple seasonal storage model be considered? [True, False]
        :param solver: milp solver of pyomo for example 'cbc', 'glpk', 'gurobi'
        :param extreme_period_method_idx: extreme period integration method; None= no, 0=append, 1=new_cluster_center,
         2=replace_cluster_center
        :param list_max: list of column name of integrated maximal periods [[str, str]]
        :param list_min: list of column name of integrated minimal periods [[str, str]]
        """
        self.data: DataFrame = data
        self.no_typical_periods: int = no_typical_periods
        self.hours_per_period: int = hours_per_period
        self.solver: str = 'glpk' if solver is None else solver
        self.cluster_method_idx: int = cluster_method_idx
        cl_idx = self.cluster_method_idx
        self.cluster_method: str = 'averaging' if cl_idx < 1 else 'hierarchical' if cl_idx < 2 else 'k_means' if \
            cl_idx < 3 else 'k_medoids'
        self.extreme_period_method_idx: Optional[int] = extreme_period_method_idx
        ex_idx: Optional[int] = extreme_period_method_idx
        self.extreme_period_method: str = 'None' if ex_idx is None else 'append' if ex_idx < 1 else \
            'new_cluster_center' if ex_idx < 2 else 'replace_cluster_center'
        self.list_max: list = [] if list_max is None else list_max
        self.list_min: list = [] if list_min is None else list_min
        tp_seasonal_init: bool = False if tp_seasonal is None else tp_seasonal
        self.tp_seasonal_simple: bool = False if tp_seasonal_simple is None else tp_seasonal_simple
        self.tp_seasonal_plus: bool = False if tp_seasonal_plus is None else tp_seasonal_plus
        self.tp_seasonal: bool = True if self.tp_seasonal_plus or self.tp_seasonal_simple else tp_seasonal_init
        # init so far unused variables
        self.df: Optional[DataFrame] = None
        self.weights: ndarray = zeros(0)
        self.error: Optional[DataFrame] = None
        self.typical_periods: Optional[DataFrame] = None
        self.new_times: Optional[DataFrame] = None
        self.matching: Optional[DataFrame] = None
        self.match_period: list = []
        self.match_periods: list = []
        self.calculated: bool = False

    def aggregate(self) -> None:
        import tsam.timeseriesaggregation as tsam  # type: ignore
        import pyutilib.subprocess.GlobalData  # type: ignore
        pyutilib.subprocess.GlobalData.DEFINE_SIGNAL_HANDLERS_DEFAULT = False
        date = pd_to_datetime(self.data.index)
        resolution = date[1] - date[0]
        resolution = resolution.total_seconds()/3600
        # init tsam module
        aggregation = tsam.TimeSeriesAggregation(self.data, noTypicalPeriods=self.no_typical_periods,
                                                 hoursPerPeriod=self.hours_per_period, resolution=resolution,
                                                 clusterMethod=self.cluster_method, segmentation=False,
                                                 sortValues=False, solver=self.solver,
                                                 extremePeriodMethod=self.extreme_period_method,
                                                 addPeakMin=self.list_min, addPeakMax=self.list_max)
        # create typical periods
        self.df = aggregation.createTypicalPeriods()
        # set calculation boolean to true to declare that the time series has been calculated
        self.calculated = True
        # get weights of typical periods
        self.weights = aggregation.clusterPeriodNoOccur
        # get the accuracy of typical periods
        self.error = aggregation.accuracyIndicators()
        # get typical periods data
        self.typical_periods = aggregation.typicalPeriods
        # predict original data using the typical periods
        self.new_times = aggregation.predictOriginalData()
        # get period matching to period to day of the year in an hourly resolution
        self.matching = aggregation.indexMatching()
        # determine matching to day of the year in a daily resolution
        self.match_period = [self.matching.PeriodNum[i] for i in range(0, self.matching.PeriodNum.count(),
                                                                       self.hours_per_period)]
        # determine days of the same period followed by each other
        self.aggregate_tp_seasonal()

    def aggregate_tp_seasonal(self) -> None:
        """
        aggregation of matching periods for seasonal storages and the seasonal storage plus model by counting equal
        matching days followed by each other or setting the number to one.
        """
        # check if the seasonal plus model should be considered
        if self.tp_seasonal_plus:
            # create a list of match periods and the number of appearances after each other
            self.match_periods = [[self.match_period[0], 1]]
            for i in range(1, len(self.match_period)):
                if self.match_period[i] == self.match_period[i - 1]:
                    self.match_periods[-1][1] += 1
                else:
                    self.match_periods.append([self.match_period[i], 1])
            return
        # create a list of match periods and set the number of appearances after each other to one
        self.match_periods = [[self.match_period[i], 1] for i in range(len(self.match_period))]

    def check_equality_lite(self, other) -> bool:
        """
        function to the equality without checking the data
        :param other: class to be compared with
        :return: boolean True if equal else False
        """
        if not isinstance(other, CreateTP):
            return False
        if not (array_equal(self.data, other.data)):
            return False
        if self.no_typical_periods != other.no_typical_periods:
            return False
        if self.hours_per_period != other.hours_per_period:
            return False
        if self.cluster_method != other.cluster_method:
            return False
        if self.solver != other.solver:
            return False
        if self.extreme_period_method != other.extreme_period_method:
            return False
        if self.list_max != other.list_max:
            return False
        if self.list_min != other.list_min:
            return False
        return True

    def __eq__(self, other) -> bool:
        """
        equality function to check if values are equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has the same values as other
        """
        # if not of same class return false
        if not isinstance(other, CreateTP):
            return False
        # compare all slot values if one not match return false
        for i in self.__slots__:
            # get object by name
            obj = getattr(self, i)
            # just compare object if it is not an array
            if not isinstance(obj, ndarray) and not isinstance(obj, DataFrame):
                # if the object values does not math return False
                if obj != getattr(other, i):
                    return False
        # if all match return true
        return True

    def __ne__(self, other) -> bool:
        """
        not equality function to check if values are not equal
        :param other: to compare Datastorage class
        :return: boolean which is true if self has not the same values as other
        """
        # return the opposite of the equality function
        return not self.__eq__(other)
