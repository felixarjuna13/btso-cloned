from btso.DataStorage import DataStorage
from pickle import dump, HIGHEST_PROTOCOL
from btso import FOLDER
import os
from timeit import default_timer as timer


def setup_typical_periods():
    os.chdir(settings_dir)

    lifetime = 20
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.calculate_typical_periods(15, 24, 4, 2, ['Heating', 'Cooling', 'Electricity', 'TAmb'], ['TAmb'], solver='gurobi')
    ds.calc()

    with open("typical_periods.pkl", "wb") as f:
        dump(ds.tp_data, f, HIGHEST_PROTOCOL)
        print('Export successful')


def setup_solver():
    for solver in ['gurobi']:
        with open(os.path.join(settings_dir, 'solver.txt'), 'w') as f:
            f.write(solver)


def create_directory(name):
    this_dir = os.path.dirname(__file__)
    new_dir = os.path.abspath(os.path.join(this_dir, name))
    if not os.path.exists(new_dir):
        os.mkdir(new_dir)
        print(f"*** {name} Directory created ***")

    return new_dir


if __name__ == 'DataStorage':
    start = timer()

    settings_dir = create_directory("settings")
    setup_solver()
    setup_typical_periods()

    end = timer()
    print(f"\nElapsed time for setup: { end - start } seconds")
