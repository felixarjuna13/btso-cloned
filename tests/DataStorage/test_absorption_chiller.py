from btso.DataStorage import DataStorage
from btso import FOLDER
import unittest
from pandas import read_csv
from scipy import optimize
from numpy import ones, ndarray
from pickle import dump, HIGHEST_PROTOCOL

"""
validation script for the absorption chiller
"""


class TestAbsorptionChiller(unittest.TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    cool_line: str = 'Cooling'
    buy_el: float = 0.18
    buy_heat: float = 0.04
    buy_cool: float = 0.20
    heat_frac: float = 0.5
    el_frac: float = 0.05
    price_lin: float = 1_000.
    price_con: float = 2_000.
    lifetime: int = 20
    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        # lifetime = 20
        # ds = DataStorage()
        # ds.set_file_rate_time_steps(f"{FOLDER}/data/data.csv", 0.0, lifetime, lifetime, 8760)
        # ds.calculate_typical_periods(15, 24, 4, 2, ['Heating', 'Cooling', 'Electricity', 'TAmb'], ['TAmb'],
        #                              solver='gurobi')
        # ds.calc()
        #
        # with open("typical_periods.pkl", "wb") as f:
        #     dump(ds.tp_data, f, HIGHEST_PROTOCOL)
        #     print('Export successful')
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_cooling_demand(self.cool_line)
        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_district_cooling(self.buy_cool)
        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.cool_line] * -1
        self.sum_d = self.demand.sum()

    def calc_costs(self, size: ndarray) -> float:
        size = size[0]
        q_ar = self.demand.clip(None, size).sum()
        return size * self.price_lin + (((self.sum_d - q_ar) * self.buy_cool + q_ar / self.heat_frac * self.buy_heat
                                         + q_ar * self.el_frac * self.buy_el) * self.lifetime)

    def calc_costs_const(self, size: ndarray) -> float:
        size = size[0]
        q_ar = self.demand.clip(None, size).sum()
        return size * self.price_lin + self.price_con + (
                ((self.sum_d - q_ar) * self.buy_cool + q_ar / self.heat_frac * self.buy_heat + q_ar * self.el_frac *
                 self.buy_el) * self.lifetime)

    def calc_costs_wo_frac_el(self, size: ndarray) -> float:
        size = size[0]
        q_ar = self.demand.clip(None, size).sum()
        return size * self.price_lin + self.price_con + (((self.sum_d - q_ar) * self.buy_cool + q_ar / self.heat_frac
                                                          * self.buy_heat) * self.lifetime)

    def test_standard(self):
        ad_data = self.ds.add_absorption_chiller(self.price_lin, 0, 0, self.lifetime)
        ad_data.set_heat_frac(self.heat_frac)
        ad_data.set_el_frac(self.el_frac)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        for q_cool, q_heat, p_el in zip(ad_data.power_cool.value, ad_data.power_heat.value, ad_data.power_el.value):
            self.assertAlmostEqual(q_cool, q_heat * self.heat_frac, delta=0.00001 * q_heat)
            self.assertAlmostEqual(p_el, q_cool * self.el_frac, delta=0.00001 * q_cool)

        res = optimize.minimize(self.calc_costs, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
        c = res.fun
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, ad_data.size.value, delta=0.000_01 * ad_data.size.value)
        print(f'Validation of absorption refrigerator successful : {int(obj)} == {int(c)}\n')

    def test_constant_price(self):
        ad_data = self.ds.add_absorption_chiller(self.price_lin, self.price_con, 0, self.lifetime)
        ad_data.set_heat_frac(self.heat_frac)
        ad_data.set_el_frac(self.el_frac)
        self.ds.add_electrical_grid(self.buy_el)
        # set solver
        self.ds.set_txt_solver()
        self.ds.calc()

        res = optimize.minimize(self.calc_costs_const, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
        c = res.fun
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, ad_data.size.value, delta=0.000_01 * ad_data.size.value)
        print(f'Validation of absorption refrigerator with constant price successful : {int(obj)} == {int(c)}\n')

    def test_wo_electrical_demand(self):
        ad_data = self.ds.add_absorption_chiller(self.price_lin, self.price_con, 0, self.lifetime)
        ad_data.set_heat_frac(self.heat_frac)
        # set solver
        self.ds.set_txt_solver()
        self.ds.calc()

        res = optimize.minimize(self.calc_costs_wo_frac_el, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
        c = res.fun
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, ad_data.size.value, delta=0.0015 * ad_data.size.value)
        print(f'Validation of absorption refrigerator without electrical demand successful : {int(obj)} == {int(c)}\n')


if __name__ == '__main__':
    tac = TestAbsorptionChiller()
    tac.test_standard()
    tac.test_constant_price()
    tac.test_wo_electrical_demand()
