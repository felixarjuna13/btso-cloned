from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv
from scipy import optimize
from numpy import ones, ndarray

"""
validation script for the air heat pump
"""


class TestAirHeatPump(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    cop = 4.0
    heat_line: str = 'Heating'
    cool_line: str = 'Cooling'
    air_hp_line: str = 'AirHP'
    p_el_hp: float = 221.
    buy_el: float = 0.18
    buy_cool: float = 0.13
    price_lin: float = 1_000.
    price_con: float = 2_000.
    t_amb_line: str = 'TAmb'
    eta_exergy: float = 0.45
    t_hot: float = 40
    lifetime: int = 20

    df = None
    demand = None
    demand_c = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        eh_data = self.ds.add_electrical_heater(0, 0, 0, self.lifetime)
        eh_data.set_efficiency(1)
        self.ds.add_electrical_grid(self.buy_el)
        # set solver
        self.ds.set_txt_solver()
        self.demand = self.df[self.heat_line]
        self.sum_d = sum(self.demand)
        self.demand_c = self.df[self.cool_line] * -1
        self.sum_c = sum(self.demand_c)

    def calc_costs_and_size_constant_cop(self):
        c_old = self.sum_d * self.buy_el * self.lifetime
        size_best: float = 0.
        for size in self.demand.sort_values(ascending=False):
            q_hp = self.demand.where(self.demand <= size).sum() + self.demand.where(self.demand > size).count() * size
            c_new = size * self.price_lin + (q_hp / self.cop + (self.sum_d - q_hp)) * self.buy_el * self.lifetime
            if c_new < c_old:
                c_old = c_new
                size_best = size
                continue
            if c_new > c_old:
                break
        return c_old, size_best

    def calc_costs_constant_cop_cooling(self, size: ndarray):
        size = size[0] / (1 + 1 / (self.cop - 1))
        q_hp = self.demand_c.where(self.demand_c <= size).sum() + self.demand.where(
            self.demand_c > size).count() * size
        c_new = size * (1 + 1 / (self.cop - 1)) * self.price_lin + (q_hp / (self.cop - 1) * self.buy_el +
                                                                    (self.sum_c - q_hp) * self.buy_cool) * self.lifetime
        return c_new

    def calc_costs_and_size_limiting_power(self):
        q_th = self.df[self.air_hp_line] / 1000
        c_old = self.sum_d * self.buy_el * self.lifetime
        cop_f = q_th / self.p_el_hp * 1000
        size_best: float = 0.
        for size in self.demand.sort_values(ascending=False):
            q_th_size = q_th * size
            sum_el_h = 0
            sum_el_hp = 0
            for h, q, c in zip(self.demand, q_th_size, cop_f):
                if h <= q:
                    sum_el_hp += h / c
                    continue
                sum_el_hp += q / c
                sum_el_h += h - q
            c_new = size * self.price_lin + self.price_con + (sum_el_hp + sum_el_h) * self.buy_el * self.lifetime
            if c_new < c_old:
                c_old = c_new
                size_best = size
                continue
            if c_new > c_old:
                break
        return c_old, size_best

    def calc_costs_and_size_exergetic_efficiency(self):
        c_old = self.sum_d * self.buy_el * self.lifetime
        t_amb = self.df[self.t_amb_line]
        cop_f = self.eta_exergy * (self.t_hot + 273.15) / (self.t_hot - t_amb)
        size_best: float = 0.
        for size in self.demand.sort_values(ascending=False):
            sum_el_h = 0
            sum_el_hp = 0
            for h, c in zip(self.demand, cop_f):
                if h <= size:
                    sum_el_hp += h / c
                    continue
                sum_el_hp += size / c
                sum_el_h += h - size
            c_new = size * self.price_lin + self.price_con + (sum_el_hp + sum_el_h) * self.buy_el * self.lifetime
            if c_new < c_old:
                c_old = c_new
                size_best = size
                continue
            if c_new > c_old:
                break
        return c_old, size_best

    def test_constant_cop(self):
        self.ds.add_single_heating_demand(self.heat_line)
        air_hp_data = self.ds.add_air_hp(self.price_lin * 10, 0, 0, self.lifetime)
        air_hp_data.set_constant_cop(self.cop)
        air_hp_data2 = self.ds.add_air_hp(self.price_lin, 0, 0, self.lifetime)
        air_hp_data2.set_constant_cop(self.cop)
        self.ds.calc()
        c, size = self.calc_costs_and_size_constant_cop()

        obj = self.ds.res["Obj"]

        size = air_hp_data2.size.value[0]
        for p_el, q_heat in zip(air_hp_data2.power_el_var.value, air_hp_data2.power_heat.value):
            self.assertAlmostEqual(p_el, q_heat / self.cop, delta=q_heat * 0.000_001)

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(size, air_hp_data2.size.value, delta=0.000_01 * air_hp_data2.size.value)
        print(f'Validation successful air heat pump constant cop: {int(c)} == {int(self.ds.res["Obj"])}\n')

    def test_limiting_power(self):
        self.ds.add_single_heating_demand(self.heat_line)
        air_hp_data = self.ds.add_air_hp(self.price_lin, self.price_con, 0, self.lifetime)
        air_hp_data.set_flexible_heat_and_cop(self.p_el_hp, self.air_hp_line)
        air_hp_data2 = self.ds.add_air_hp(self.price_lin / 1000, self.price_con, 0, self.lifetime)
        air_hp_data2.set_flexible_heat_and_cop(self.p_el_hp, self.air_hp_line)
        self.ds.del_air_hp_by_class(air_hp_data2)

        self.ds.calc()
        obj = self.ds.res["Obj"]
        c, size = self.calc_costs_and_size_limiting_power()

        size = air_hp_data.size.value[0]
        q_th = self.df[self.air_hp_line] / 1000
        cop_f = q_th / self.p_el_hp * 1000
        for p_el, q_heat, cop, q_max in zip(air_hp_data.power_el_var.value, air_hp_data.power_heat.value, cop_f,
                                            q_th):
            self.assertAlmostEqual(p_el, q_heat / cop, delta=q_heat * 0.000_001)
            self.assertLessEqual(q_heat, q_max * size * 1.000_001)

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(size, air_hp_data.size.value, delta=0.001 * air_hp_data.size.value)
        print(f'Validation successful air heat pump with limiting power: {int(c)} == {int(self.ds.res["Obj"])}\n')

    def test_exergetic_efficiency(self):
        self.ds.add_single_heating_demand(self.heat_line)
        air_hp_data = self.ds.add_air_hp(self.price_lin, self.price_con, 0, self.lifetime)
        air_hp_data.set_flexible_cop(self.t_amb_line, self.eta_exergy, self.t_hot)

        self.ds.calc()
        obj = self.ds.res["Obj"]
        c, size = self.calc_costs_and_size_exergetic_efficiency()

        size = air_hp_data.size.value[0]
        t_amb = self.df[self.t_amb_line]
        cop_f = self.eta_exergy * (self.t_hot + 273.15) / (self.t_hot - t_amb)

        for p_el, q_heat, cop in zip(air_hp_data.power_el_var.value, air_hp_data.power_heat.value, cop_f):
            self.assertAlmostEqual(p_el, q_heat / cop, delta=q_heat * 0.000_001)

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(size, air_hp_data.size.value, delta=0.001 * air_hp_data.size.value)
        print(f'Validation successful air heat pump with exergetic efficiency: {int(c)} == {int(self.ds.res["Obj"])}\n')

    def test_constant_cop_cooling(self):
        cn = self.ds.add_cooling_node()
        dec = self.ds.add_single_cooling_demand(self.cool_line, -1)
        dec.set_node(cn)
        dsc = self.ds.add_district_cooling(self.buy_cool)
        dsc.set_node(cn)
        air_hp_data = self.ds.add_air_hp(self.price_lin, 0, 0, self.lifetime)
        air_hp_data.set_constant_cop(self.cop)
        air_hp_data.activate_cooling()
        air_hp_data.set_cooling_node(cn)
        self.ds.calc()

        res = optimize.minimize(self.calc_costs_constant_cop_cooling, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
        c = res.fun
        obj = self.ds.res["Obj"]

        size = res.x
        for p_el, q_cool in zip(air_hp_data.power_el_var.value, air_hp_data.power_cool.value):
            self.assertAlmostEqual(p_el, q_cool / (self.cop - 1), delta=q_cool * 0.000_001)

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(size, air_hp_data.size.value, delta=0.000_01 * air_hp_data.size.value)
        print(f'Validation successful air heat pump for cooling constant cop: {int(c)} == {int(self.ds.res["Obj"])}\n')


if __name__ == '__main__':
    thp = TestAirHeatPump()
    thp.test_constant_cop()
    thp.test_limiting_power()
    thp.test_exergetic_efficiency()
