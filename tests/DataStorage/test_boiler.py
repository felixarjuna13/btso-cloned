from btso.DataStorage import DataStorage
from btso import FOLDER
from pandas import read_csv
from unittest import TestCase

"""
validation script for the boiler
"""


class TestBoiler(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    heat_line: str = 'Heating'
    fos_line: str = 'Fos'
    el_line: str = 'Buy'
    buy_el: float = 0.18
    buy_fuel: float = 0.06
    eta: float = 0.95
    price_lin: float = 100.
    price_con: float = 1_000.
    lifetime: int = 20

    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_fuel_connection(self.buy_fuel)
        # boiler_data2.set_constant_fuel_price(buy_fuel)
        eh_data = self.ds.add_electrical_heater(0, 0, 0, self.lifetime)
        eh_data.set_efficiency(1)
        self.ds.add_single_heating_demand(self.heat_line)
        self.ds.add_electrical_grid(self.buy_el)

    def calc_costs_and_size_standard(self):
        demand = self.df[self.heat_line]
        sum_d = sum(demand)
        c_old = sum_d * self.buy_el * self.lifetime
        size_opt = 0
        for size in demand.sort_values(ascending=False):
            q_boiler = demand.where(demand <= size).sum() + demand.where(demand > size).count() * size
            c_new = size * self.price_lin + (sum_d - q_boiler) * self.buy_el * self.lifetime + q_boiler / self.eta * \
                    self.buy_fuel * self.lifetime
            if c_new < c_old:
                size_opt = size
                c_old = c_new
                continue
            if c_new > c_old:
                break

        return c_old, size_opt

    def test_standard(self):
        boiler_data = self.ds.add_boiler(self.price_lin * 1_000, 0, 0, self.lifetime)
        boiler_data.set_efficiency(self.eta)
        # boiler_data.set_constant_fuel_price(buy_fuel)
        boiler_data2 = self.ds.add_boiler(self.price_lin, 0, 0, self.lifetime)
        boiler_data2.set_efficiency(self.eta)

        self.ds.calc()

        c_old, size_opt = self.calc_costs_and_size_standard()

        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c_old, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(size_opt, boiler_data2.size.value, delta=0.000_01 * boiler_data2.size.value)
        print(f'Validation successful with constant price boiler: {int(c_old)} == {int(obj)}')
