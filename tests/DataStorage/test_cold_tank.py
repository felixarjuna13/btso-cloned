from btso.DataStorage import DataStorage, Storage
from btso import FOLDER
from math import isclose
from pandas import read_csv, DataFrame
from scipy import optimize
from numpy import ones, array, zeros
from typing import Callable
from copy import deepcopy
from unittest import TestCase

"""
validation script for the cold tank
"""


class TestColdTank(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    stc_line: str = 'STAPowerCold'
    cool_line: str = 'Cooling'
    max_stc: float = 100_000.
    sell_stc: float = 0.06
    buy_cool: float = 0.18
    eta_self: float = 0.01
    eta_charge: float = 0.97
    eta_discharge: float = 0.95
    price_lin_stc: float = 200.
    price_con_stc: float = 2000.
    price_lin_ct: float = 5.
    price_con_ct: float = 350.
    lifetime: int = 20
    start_val: float = 0.2

    df = None
    demand = None
    demand_c = None
    stc_data = None
    cold_tank_data = None
    len_ye = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)

        # set solver
        self.ds.set_txt_solver()

        self.demand = -1 * self.df[self.cool_line]
        self.p_stc = (-1 * self.df[self.stc_line]).clip(0) * self.max_stc / 1000
        self.len_ye = len(self.demand)
        self.ran_yr: range = range(self.len_ye)

        self.stc_data = self.ds.add_solar_thermal_collector(self.price_lin_stc, self.price_con_stc, 0, self.lifetime)
        self.stc_data.get_power_from_data(self.stc_line, self.stc_line)
        self.stc_data.set_connections(False, False, False, True)
        self.stc_data.set_max(self.max_stc)
        self.cold_tank_data = self.ds.add_cold_tank(self.price_lin_ct, self.price_con_ct, 0, self.lifetime)
        self.cold_tank_data.set_values(self.eta_self * 100, self.eta_charge * 100, self.eta_discharge * 100)

        self.cool_demand = self.ds.add_single_cooling_demand(self.cool_line, -1)
        self.district_cooling = self.ds.add_district_cooling(self.buy_cool, self.sell_stc)

    def calc_costs_backward(self, size_bat: array, eta_self_internal: float):
        size_bat = size_bat[0]
        charge_status = self.start_val * size_bat
        e_sell = 0
        e_buy = 0
        for p, d in zip(self.p_stc, self.demand):
            charge_status = charge_status * (1 - eta_self_internal)
            bat_status_old = charge_status
            if p > d:
                charge_status = min(charge_status + (p - d) * self.eta_charge, size_bat)
                e_sell += max(p - d - (charge_status - bat_status_old) / self.eta_charge, 0)
                continue
            charge_status = max(charge_status + (p - d) / self.eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - charge_status) * self.eta_discharge, 0)

        return self.price_lin_stc * self.max_stc + self.price_con_stc + self.price_lin_ct * size_bat + self.price_con_ct + (
                e_buy * self.buy_cool - e_sell * self.sell_stc) * self.lifetime

    def calc_costs_backward_with_self_discharge(self, size_bat: array):
        return self.calc_costs_backward(size_bat, self.eta_self)

    def calc_costs_backward_without_self_discharge(self, size_bat: array):
        return self.calc_costs_backward(size_bat, 0)

    def calc_costs_forward(self, size_bat: float, eta_self_internal: float):
        charge_status = self.start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in self.ran_yr:
            p = self.p_stc[t - 1] if t > 0 else 0
            d = self.demand[t - 1] if t > 0 else 0
            charge_status = charge_status * (1 - eta_self_internal)
            bat_status_old = charge_status
            if p > d:
                charge_status = min(charge_status + (p - d) * self.eta_charge, size_bat)
                e_sell += max(p - d - (charge_status - bat_status_old) / self.eta_charge, 0)
                continue
            charge_status = max(charge_status + (p - d) / self.eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - charge_status) * self.eta_discharge, 0)

        return self.price_lin_stc * self.max_stc + self.price_con_stc + self.price_lin_ct * size_bat + self.price_con_ct + (
                e_buy * self.buy_cool - e_sell * self.sell_stc) * self.lifetime

    def calc_costs_forward_with_self_discharge(self, size_bat: array):
        return self.calc_costs_forward(size_bat, self.eta_self)

    def calc_costs_forward_without_self_discharge(self, size_bat: array):
        return self.calc_costs_forward(size_bat, 0)

    def calc_costs_center(self, size_bat: float, eta_self_internal: float):
        charge_status = self.start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in self.ran_yr:
            p = self.p_stc[t - 1] / 2 + self.p_stc[t] / 2 if t > 0 else self.p_stc[t] / 2
            d = self.demand[t - 1] / 2 + self.demand[t] / 2 if t > 0 else self.demand[t] / 2
            charge_status = charge_status * (1 - eta_self_internal)
            bat_status_old = charge_status
            if p > d:
                charge_status = min(charge_status + (p - d) * self.eta_charge, size_bat)
                e_sell += max(p - d - (charge_status - bat_status_old) / self.eta_charge, 0)
                continue
            charge_status = max(charge_status + (p - d) / self.eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - charge_status) * self.eta_discharge, 0)

        return self.price_lin_stc * self.max_stc + self.price_con_stc + self.price_lin_ct * size_bat + self.price_con_ct + (
                e_buy * self.buy_cool - e_sell * self.sell_stc) * self.lifetime

    def calc_costs_center_with_self_discharge(self, size_bat: array):
        return self.calc_costs_center(size_bat, self.eta_self)

    def calc_costs_center_without_self_discharge(self, size_bat: array):
        return self.calc_costs_center(size_bat, 0)

    def check_backward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        charge_status = zeros(8760)
        charge_status[0] = start_value * size_bat * (1 - self.eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - self.eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(8760):
            charge_status[t] = charge_status[0] if t == 0 else charge_status[t - 1] * (1 - self.eta_self)
            bat_status_old = charge_status[t]
            p = self.p_stc[t]
            d = self.demand[t]
            charge_status[t] = max(min(charge_status[t] + storage.power_charge.value[t] * self.eta_charge -
                                       storage.power_discharge.value[t] / self.eta_discharge, size_bat), 0)
            p_sell[t] = max(p - d - (storage.power_charge.value[t] - storage.power_discharge.value[t]), 0)
            p_buy[t] = max(d - p + (storage.power_charge.value[t] - storage.power_discharge.value[t]), 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_forward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        charge_status = zeros(8760)
        charge_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[8759] * (
                1 - self.eta_self)
        charge_status[0] = charge_status[0] + (storage.power_charge.value[-1] * self.eta_charge -
                                               storage.power_discharge.value[-1] / self.eta_discharge)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(1, 8760):
            charge_status[t] = charge_status[t - 1] * (1 - self.eta_self)
            bat_status_old = charge_status[t]
            p = self.p_stc[t - 1]
            d = self.demand[t - 1]
            if p > d:
                charge_status[t] = min(charge_status[t] + storage.power_charge.value[t - 1] * self.eta_charge, size_bat)
                p_sell[t - 1] = max(p - d - (charge_status[t] - bat_status_old) / self.eta_charge, 0)
            else:
                charge_status[t] = max(charge_status[t] - storage.power_discharge.value[t - 1] / self.eta_discharge, 0)
                p_buy[t - 1] = max(d - p - (bat_status_old - charge_status[t]) * self.eta_discharge, 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_central(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        charge_status = zeros(8760)
        charge_status[0] = start_value * size_bat * (1 - self.eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - self.eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        p = (self.p_stc[0])
        d = (self.demand[0])
        charge_status[0] = max(min(charge_status[0] + (storage.power_charge.value[0]) / 2 * self.eta_charge -
                                   (storage.power_discharge.value[0]) / 2 / self.eta_discharge, size_bat), 0)
        p_sell[0] = max(p - d - storage.power_charge.value[0] + storage.power_discharge.value[0], 0)
        p_buy[0] += max(d - p - storage.power_discharge.value[0] + storage.power_charge.value[0], 0)
        charge_status[0] = max(min(charge_status[0] + (storage.power_charge.value[-1]) / 2 * self.eta_charge -
                                   (storage.power_discharge.value[-1]) / 2 / self.eta_discharge, size_bat), 0)
        assert isclose(round(charge_status[0], 2), round(storage.energy.value[0], 2), rel_tol=0.0001)
        assert isclose(round(p_buy[0], 2), round(buy[0], 2), rel_tol=0.0001)
        assert isclose(round(p_sell[0], 2), round(sell[0], 2), rel_tol=0.0001)
        for t in range(1, 8760):
            charge_status[t] = charge_status[t - 1] * (1 - self.eta_self)
            p = self.p_stc[t]
            d = self.demand[t]
            charge_status[t] = max(min(charge_status[t] +
                                       (storage.power_charge.value[t] + storage.power_charge.value[
                                           t - 1]) / 2 * self.eta_charge
                                       - (storage.power_discharge.value[t] + storage.power_discharge.value[t - 1]) / 2 /
                                       self.eta_discharge, size_bat), 0)

            p_sell[t] = max(p - d - storage.power_charge.value[t] + storage.power_discharge.value[t], 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_fail(self, funct: Callable, storage: Storage, buy: array, sell: array, start_value: float = None):
        try:
            funct(storage, buy, sell, start_value)
        except AssertionError:
            return
        raise AssertionError

    def check_tp_cyclic_forward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        ran_end = range(self.ds.tp_data.hours_per_period - 1, length, self.ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_end = storage.energy.value[j] * (1 - self.eta_self) - \
                        storage.power_discharge.value[j] / storage.eta_discharge + \
                        storage.power_charge.value[j] * storage.eta_charge
            assert isclose(round(storage.energy.value[i], 2), round(power_end, 2), rel_tol=0.00001)
        size_bat = storage.size.value
        charge_status = zeros(length)
        charge_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            charge_status[t] = charge_status[t - 1] * (1 - self.eta_self)
            p = max(-1 * self.stc_data.power_cold[t - 1], 0) / 1000. * self.stc_data.size.value
            d = self.cool_demand.demand[t - 1]
            charge_status[t] = max(charge_status[t] - storage.power_discharge.value[t - 1] / self.eta_discharge +
                                   storage.power_charge.value[t - 1] * self.eta_charge, 0)
            p_buy[t - 1] = max(d - p - storage.power_discharge.value[t - 1] + storage.power_charge.value[t - 1], 0)
            p_sell[t - 1] = max(p - d + storage.power_discharge.value[t - 1] - storage.power_charge.value[t - 1], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_tp_cyclic_backward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        ran_end = range(self.ds.tp_data.hours_per_period - 1, length, self.ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] /
                           storage.eta_discharge - storage.power_charge.value[i] * storage.eta_charge)
            assert isclose(power_start, storage.energy.value[j] * (1 - self.eta_self), rel_tol=0.0001)
        size_bat = storage.size.value
        charge_status = zeros(length)
        charge_status[0] = (start_value * size_bat - storage.power_discharge.value[0] /
                            storage.eta_discharge + storage.power_charge.value[0] * storage.eta_charge) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            charge_status[t] = charge_status[t - 1] * (1 - self.eta_self)
            p = max(-1 * self.stc_data.power_cold[t], 0) / 1000. * self.stc_data.size.value
            d = self.cool_demand.demand[t]
            charge_status[t] = max(charge_status[t] - storage.power_discharge.value[t] / self.eta_discharge +
                                   storage.power_charge.value[t] * self.eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_tp_cyclic_central(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        ran_end = range(self.ds.tp_data.hours_per_period - 1, length, self.ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] / storage.eta_discharge / 2
                           - storage.power_charge.value[i] * storage.eta_charge / 2)
            power_end = (storage.energy.value[j] * (1 - self.eta_self) - storage.power_discharge.value[j] /
                         storage.eta_discharge / 2 + storage.power_charge.value[j] * storage.eta_charge / 2)
            assert isclose(round(power_start, 2), round(power_end, 2), rel_tol=0.0001)
        size_bat = storage.size.value
        charge_status = zeros(length)
        charge_status[0] = (start_value * size_bat - storage.power_discharge.value[0] / storage.eta_discharge / 2
                            + storage.power_charge.value[0] * storage.eta_charge / 2) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            charge_status[t] = charge_status[t - 1] * (1 - self.eta_self)
            p = max(-1 * self.stc_data.power_cold[t], 0) / 1000. * self.stc_data.size.value
            d = self.cool_demand.demand[t]
            charge_status[t] = max(charge_status[t] -
                                   (storage.power_discharge.value[t] + storage.power_discharge.value[t - 1]) / 2
                                   / self.eta_discharge + (storage.power_charge.value[t]
                                                           + storage.power_charge.value[t - 1]) / 2 * self.eta_charge,
                                   0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def test_standard_system(self):
        stc_data = self.ds.add_solar_thermal_collector(self.price_lin_stc, self.price_con_stc, 0, self.lifetime)
        stc_data.get_power_from_data(self.stc_line, self.stc_line)
        stc_data.set_connections(False, False, False, True)
        stc_data.set_max(self.max_stc)
        cold_tank_data = self.ds.add_cold_tank(self.price_lin_ct, self.price_con_ct, 0, self.lifetime)
        cold_tank_data.set_values(self.eta_self * 100, self.eta_charge * 100, self.eta_discharge * 100)
        cold_tank_data.set_start(self.start_val)

        self.ds.set_de_model(2)

        cool_demand = self.ds.add_single_cooling_demand(self.cool_line, -1)
        district_cooling = self.ds.add_district_cooling(self.buy_cool, self.sell_stc)

        self.ds.calc()

        self.assertAlmostEqual(stc_data.size.value, self.max_stc, delta=0.0001 * stc_data.size.value)
        print(f"Validation cold tank system successful: {stc_data.size.value} == {self.max_stc}")

    def test_standard_backward_with_start_value(self):
        self.cold_tank_data.set_start(self.start_val)
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_backward(self.cold_tank_data, self.district_cooling.power_buy.value,
                            self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)

        res_with = optimize.minimize(self.calc_costs_backward_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_backward_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of cold tank size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
            f' <= {int(c_calc_with)}')

    def test_standard_backward(self):
        self.cold_tank_data.start = False
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_backward(self.cold_tank_data, self.district_cooling.power_buy.value,
                            self.district_cooling.power_sell.value)
        self.check_fail(self.check_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)
        self.check_fail(self.check_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)

        res_with = optimize.minimize(self.calc_costs_backward_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_backward_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of cold tank size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
            f' <= {int(c_calc_with)}')

    def test_standard_forward_with_start_value(self):
        self.cold_tank_data.set_start(self.start_val)
        self.cold_tank_data.set_max(100_000)
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_forward(self.cold_tank_data, self.district_cooling.power_buy.value,
                           self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)

        res_with = optimize.minimize(self.calc_costs_forward_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_forward_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of cold tank size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_standard_forward(self):
        self.cold_tank_data.start = False
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_forward(self.cold_tank_data, self.district_cooling.power_buy.value,
                           self.district_cooling.power_sell.value)
        self.check_fail(self.check_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)
        self.check_fail(self.check_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)

        res_with = optimize.minimize(self.calc_costs_forward_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_forward_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of cold tank size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_standard_central_with_start_value(self):
        self.cold_tank_data.set_start(self.start_val)
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_central(self.cold_tank_data, self.district_cooling.power_buy.value,
                           self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)

        res_with = optimize.minimize(self.calc_costs_center_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_center_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of cold tank size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_standard_central(self):
        self.cold_tank_data.start = False
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_central(self.cold_tank_data, self.district_cooling.power_buy.value,
                           self.district_cooling.power_sell.value)
        self.check_fail(self.check_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)
        self.check_fail(self.check_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)

        res_with = optimize.minimize(self.calc_costs_center_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_center_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of cold tank size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_typical_cyclic_forward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.cold_tank_data.start = False
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_cyclic_forward(self.cold_tank_data, self.district_cooling.power_buy.value,
                                     self.district_cooling.power_sell.value)
        self.check_fail(self.check_tp_cyclic_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)
        self.check_fail(self.check_tp_cyclic_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)

        print('test of cyclic cold tank with forward euler successful!\n')

    def test_typical_cyclic_forward_with_start_value(self):
        self.cold_tank_data.set_start(self.start_val)
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_cyclic_forward(self.cold_tank_data, self.district_cooling.power_buy.value,
                                     self.district_cooling.power_sell.value,
                                     self.start_val)
        self.check_fail(self.check_tp_cyclic_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)

        print('test of cyclic cold tank with forward euler with start value successful!\n')

    def test_typical_cyclic_backward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.cold_tank_data.start = False
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_cyclic_backward(self.cold_tank_data, self.district_cooling.power_buy.value,
                                      self.district_cooling.power_sell.value)
        self.check_fail(self.check_tp_cyclic_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)
        self.check_fail(self.check_tp_cyclic_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)

        print('test of cyclic cold tank with backward euler successful!\n')

    def test_typical_cyclic_backward_with_start_value(self):
        self.cold_tank_data.set_start(self.start_val)
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_cyclic_backward(self.cold_tank_data, self.district_cooling.power_buy.value,
                                      self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_central, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)

        print('test of cyclic cold tank with backward euler with start value successful!\n')

    def test_typical_cyclic_central(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.cold_tank_data.start = False
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_cyclic_central(self.cold_tank_data, self.district_cooling.power_buy.value,
                                     self.district_cooling.power_sell.value)
        self.check_fail(self.check_tp_cyclic_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)
        self.check_fail(self.check_tp_cyclic_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value)

        print('test of cyclic cold tank with central euler successful!\n')

    def test_typical_cyclic_central_with_start_value(self):
        self.cold_tank_data.set_start(self.start_val)
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_cyclic_central(self.cold_tank_data, self.district_cooling.power_buy.value,
                                     self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_forward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_backward, self.cold_tank_data, self.district_cooling.power_buy.value,
                        self.district_cooling.power_sell.value, self.start_val)

        print('test of cyclic cold tank with central euler with start value successful!\n')

    def compare_ds_results_storage(self, ds_first: DataStorage, ds_second: DataStorage):
        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()

        ct_data1 = ds_first.CT[0]
        ds_cool1 = ds_first.district_cool[0]
        ct_data2 = ds_second.CT[0]
        ds_cool2 = ds_second.district_cool[0]
        assert isclose(ds_first.res['Obj'], ds_second.res['Obj'], rel_tol=0.000001)
        assert isclose(ct_data1.size.value[0], ct_data2.size.value[0], rel_tol=0.000001)

        for t in range(ds1.raw.shape[0]):
            assert isclose(round(ct_data1.power_charge.value[t], 2), round(ct_data2.power_charge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(ct_data1.power_discharge.value[t], 2), round(ct_data2.power_discharge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(ds_cool1.power_sell.value[t], 2), round(ds_cool2.power_sell.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(ds_cool1.power_buy.value[t], 2), round(ds_cool2.power_buy.value[t], 2),
                           rel_tol=0.00001)

    def check_tp_seasonal(self, storage: Storage, dgl: int):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        f = 0.5 if dgl == 3 else 1
        for i in ran_start:
            power_start = storage.power_discharge.value[i] / self.eta_discharge - \
                          storage.power_charge.value[i] * self.eta_charge if dgl > 1 else 0
            assert round(storage.energy.value[i] + power_start * f, 2) == 0

        period = self.ds.tp_data.matching['PeriodNum'][0]
        period_hour = self.ds.tp_data.matching['TimeStep'][0]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        charge_status = zeros(length)
        power_start = (storage.power_discharge.value[t_old] / self.eta_discharge -
                       storage.power_charge.value[t_old] * self.eta_charge) if dgl > 1 else 0
        charge_status[0] = storage.energy_d.value[0] - power_start * f
        day = 0
        hour = 0
        for i in range(1, length):
            period = self.ds.tp_data.matching['PeriodNum'][i]
            period_hour = self.ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            charge_status[i] = charge_status[i - 1] * (1 - self.eta_self)
            power_last = 0 if dgl == 2 else (storage.power_charge.value[t_old] * self.eta_charge -
                                             storage.power_discharge.value[t_old] / self.eta_discharge)
            power_current = 0 if dgl == 1 else (storage.power_charge.value[t] * self.eta_charge -
                                                storage.power_discharge.value[t] / self.eta_discharge)
            charge_status[i] = charge_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(charge_status[i], 2) >= 0
            assert round(charge_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(
                round(storage.energy_d.value[day] * (1 - self.eta_self) ** period_hour + storage.energy.value[t], 2),
                round(charge_status[i], 2), rel_tol=0.0001)

    def test_typical_seasonal_backward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_seasonal(self.cold_tank_data, 2)
        print('test of seasonal cold tank with backward euler successful!\n')

        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()
        ds1.calc()

        self.compare_ds_results_storage(self.ds, ds1)
        print('test of seasonal+ cold tank with backward euler successful!\n')

    def test_typical_seasonal_forward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_seasonal(self.cold_tank_data, 1)
        print('test of seasonal cold tank with forward euler successful!\n')

        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()
        ds1.calc()

        self.compare_ds_results_storage(self.ds, ds1)
        print('test of seasonal+ cold tank with forward euler successful!\n')

    def test_typical_seasonal_central(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_seasonal(self.cold_tank_data, 3)
        print('test of seasonal cold tank with central euler successful!\n')

        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()
        ds1.calc()

        self.compare_ds_results_storage(self.ds, ds1)
        print('test of seasonal+ cold tank with central euler successful!\n')

    def check_tp_seasonal_simple(self, storage: Storage, dgl: int):
        f = 0.5 if dgl == 3 else 1

        period = self.ds.tp_data.matching['PeriodNum'][-1]
        period_hour = self.ds.tp_data.matching['TimeStep'][-1]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        charge_status = zeros(length)
        charge_status[-1] = storage.energy.value[-1]
        day = 0
        hour = 0
        for i in range(length):
            period = self.ds.tp_data.matching['PeriodNum'][i]
            period_hour = self.ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            charge_status[i] = charge_status[i - 1] * (1 - self.eta_self)
            power_last = 0 if dgl == 2 else (
                        storage.power_charge.value[t_old] * self.eta_charge - storage.power_discharge.value[
                    t_old] / self.eta_discharge)
            power_current = 0 if dgl == 1 else (
                        storage.power_charge.value[t] * self.eta_charge - storage.power_discharge.value[
                    t] / self.eta_discharge)
            charge_status[i] = charge_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(charge_status[i], 2) >= 0
            assert round(charge_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(round(storage.energy.value[i], 2), round(charge_status[i], 2), rel_tol=0.0001)

    def test_typical_seasonal_simple_forward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.tp_seasonal_simple = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_seasonal_simple(self.cold_tank_data, 1)

        print('test of simple seasonal cold tank with forward euler successful!\n')

    def test_typical_seasonal_simple_backward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.tp_seasonal_simple = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_seasonal_simple(self.cold_tank_data, 2)

        print('test of simple seasonal cold tank with backward euler successful!\n')

    def test_typical_seasonal_simple_central(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.tp_seasonal_simple = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_seasonal_simple(self.cold_tank_data, 3)

        print('test of simple seasonal cold tank with central euler successful!\n')
