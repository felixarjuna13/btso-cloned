from btso.DataStorage import DataStorage
from btso import FOLDER
import unittest
from pandas import read_csv
from scipy import optimize
from numpy import ones, ndarray, array, nonzero
from numpy.typing import NDArray
from typing import Tuple
from math import isclose


class TestCHP(unittest.TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    heat_line: str = 'Heating'
    el_line: str = 'Electricity'
    sell_line: str = 'Sell'
    buy_line: str = 'Buy'
    fos_line: str = 'Fos'
    buy_el: float = 0.5
    buy_district: float = 0.08
    buy_fos: float = 0.06
    sel_chp: float = 0.1
    price_lin: float = 200.
    price_con: float = 2500.
    lifetime: int = 20
    eta_el: float = 0.3
    eta_th: float = 0.6
    min_power: float = 0.4
    max_gradient_up: float = 0.4
    max_gradient_down: float = 0.5

    df = None
    demand = None
    demand_el = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self):
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_heating_demand(self.heat_line)
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_fuel_connection(self.buy_fos)
        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.heat_line]
        self.sum_h = self.demand.sum()
        self.demand_el = self.df[self.el_line]
        self.sum_el = self.demand_el.sum()


    def calc_costs(self, size: ndarray) -> float:
        size = size[0] * self.eta_th / self.eta_el
        q_chp = array([max(min(d, size), 0) for d in self.demand])
        return ((-1 * q_chp.sum() / self.eta_th * self.eta_el * self.sel_chp + (self.sum_h - sum(q_chp)) *
                 self.buy_district + sum(q_chp) / self.eta_th * self.buy_fos) * self.lifetime
                ) + size * self.price_lin * self.eta_el / self.eta_th + self.price_con

    def calc_costs_flex(self) -> Tuple[float, float]:
        size = self.demand.max() * self.eta_el / self.eta_th
        # q_chp = array([max(min(d, size), 0) if s >= buy_fos / eta_el - b / eta_el * eta_th else 0 for d, s, b
        #               in zip(demand_h, df[sell_line], df[buy_line])])
        q_chp = self.sum_h
        c_sell = self.demand / self.eta_th * self.eta_el * self.df[self.sell_line]
        c_fos = q_chp * self.buy_fos
        c = ((-1 * c_sell.sum() + c_fos.sum() / self.eta_th) * self.lifetime) + size * self.price_lin + self.price_con
        return c, size

    def calc_costs_min_power(self, size: ndarray) -> float:
        size = size[0] * self.eta_th / self.eta_el
        q_chp: NDArray = array([max(min(d, size), 0) if d >= self.min_power * size else 0 for d in self.demand])
        #q_el = self.demand_el - q_chp / self.eta_th * self.eta_el
        c_sell = q_chp * self.sel_chp
        #q_sell = q_el.clip(None, 0).sum()
        #q_buy = q_el.clip(0).sum()
        return ((-1 * c_sell.sum() / self.eta_th * self.eta_el + (self.sum_h - sum(q_chp)) *
                 self.buy_district + sum(q_chp) / self.eta_th * self.buy_fos) * self.lifetime
                ) + size * self.price_lin * self.eta_el / self.eta_th + self.price_con

    def calc_costs_binary(self, size: ndarray) -> float:
        size = size[0] * self.eta_th / self.eta_el
        q_chp: NDArray = self.demand.where(self.demand >= size).count() * size
        c_sell = q_chp * self.sel_chp / self.eta_th * self.eta_el
        return ((-1 * c_sell + (self.sum_h - q_chp) *
                 self.buy_district + q_chp / self.eta_th * self.buy_fos) * self.lifetime
                ) + size * self.price_lin * self.eta_el / self.eta_th + self.price_con

    def calc_max_gradient_up(self, size: ndarray) -> float:
        size = size[0] * self.eta_th / self.eta_el
        q_chp = ones((len(self.demand), 1)) * self.demand.iloc[-1]
        for t in range(len(self.demand)):
            if self.demand[t] <= size:
                size_max = q_chp[t - 1] + self.max_gradient_up * size
                q_chp[t] = min(self.demand[t], size_max)
                continue
            if self.demand[t] > size:
                size_max = q_chp[t - 1] + self.max_gradient_up * size
                q_chp[t] = min(size, size_max)
        q_chp = q_chp.sum()
        c_sell = q_chp * self.sel_chp / self.eta_th * self.eta_el
        return ((-1 * c_sell + (self.sum_h - q_chp) *
                 self.buy_district + q_chp / self.eta_th * self.buy_fos) * self.lifetime
                ) + size * self.price_lin * self.eta_el / self.eta_th + self.price_con

    def calc_max_gradient_down(self, size: ndarray) -> float:
        size = max(size[0], 1) * self.eta_th / self.eta_el
        q_chp = ones((len(self.demand), 1)) * self.demand.iloc[-1]
        for t in range(len(self.demand)):
            if self.demand[t] <= size:
                size_min = q_chp[t - 1] - self.max_gradient_up * size
                if size_min > self.demand[t]:
                    q_chp[t - 1] = self.demand[t] + self.max_gradient_up * size
                    q_chp[t] = self.demand[t]
                    if q_chp[t - 1] + self.max_gradient_up * size < q_chp[t - 2]:
                        q_chp[t - 2] = q_chp[t-1] + self.max_gradient_up * size
                    continue
                q_chp[t] = min(max(self.demand[t], size_min), size)
                continue
            if self.demand[t] > size:
                size_min = q_chp[t - 1] - self.max_gradient_up * size
                q_chp[t] = max(size, size_min)

        q_chp = q_chp.sum()
        c_sell = q_chp * self.sel_chp / self.eta_th * self.eta_el
        return ((-1 * c_sell + (self.sum_h - q_chp) *
                 self.buy_district + q_chp / self.eta_th * self.buy_fos) * self.lifetime
                ) + size * self.price_lin * self.eta_el / self.eta_th + self.price_con

    def test_standard(self):
        self.setUpClass()
        chp_data = self.ds.add_combined_heat_power(self.price_lin * 1_000, self.price_con, 0, self.lifetime)
        chp_data.set_efficiencies(self.eta_el, self.eta_th)
        chp_data.selling_power.set_constant_selling_price(self.sel_chp)
        chp_data2 = self.ds.add_combined_heat_power(self.price_lin, self.price_con, 0, self.lifetime)
        chp_data2.set_efficiencies(self.eta_el, self.eta_th)
        chp_data2.selling_power.set_constant_selling_price(self.sel_chp)
        self.ds.add_district_heating(self.buy_district)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs, ones(1), method='SLSQP')
        c = res.fun
        obj = self.ds.res['Obj']

        for q_heat, p_el, m_fuel in zip(chp_data2.power_heat.value, chp_data2.power_el.value,
                                        chp_data2.power_fuel.value):
            self.assertAlmostEqual(q_heat, m_fuel * self.eta_th, delta=0.000_01 * m_fuel)
            self.assertAlmostEqual(p_el, m_fuel * self.eta_el, delta=0.000_01 * m_fuel)

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, chp_data2.size.value, delta=0.001 * chp_data2.size.value)

        print(f'Validation of CHP constant price successful: {int(c)} == {int(obj)}\n')

    def test_flexible_selling(self):
        self.setUpClass()
        chp_data = self.ds.add_combined_heat_power(self.price_lin, self.price_con, 0, self.lifetime)
        chp_data.set_efficiencies(self.eta_el, self.eta_th)
        chp_data.selling_power.set_flexible_selling_price(self.sell_line)
        # set solver
        self.ds.set_txt_solver()
        self.ds.calc()

        c, size = self.calc_costs_flex()
        obj = self.ds.res['Obj']
        self.assertAlmostEqual(size, chp_data.size.value, delta=0.01 * chp_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.01 * obj)

        print(f'Validation of CHP flexible price successful: {int(c)} == {int(obj)}\n')

    def test_min_power(self):
        self.setUpClass()
        chp_data = self.ds.add_combined_heat_power(self.price_lin, self.price_con, 0, self.lifetime)
        chp_data.set_efficiencies(self.eta_el, self.eta_th)
        chp_data.selling_power.set_constant_selling_price(self.sel_chp)
        chp_data.set_minimal_power(self.min_power)
        chp_data.set_limit(False, None, self.heat_line)
        self.ds.add_district_heating(self.buy_district)

        self.ds.calc()
        res = optimize.minimize(self.calc_costs_min_power, ones(1), method='SLSQP')
        c = res.fun
        obj = self.ds.res['Obj']

        size = chp_data.size.value[0]
        for power in chp_data.power_el.value:
            self.assertTrue(isclose(power, 0, rel_tol=0.00001) or size * self.min_power <= power <= size)

        self.assertAlmostEqual(c, obj, delta=0.01 * obj)
        self.assertAlmostEqual(res.x[0], chp_data.size.value, delta=0.01 * chp_data.size.value)
        print(f'Validation of CHP with minimal power successful: {int(c)} == {int(obj)}\n')

    def test_binary(self):
        self.setUpClass()

        chp_data = self.ds.add_combined_heat_power(self.price_lin, self.price_con, 0, self.lifetime)
        chp_data.set_efficiencies(self.eta_el, self.eta_th)
        chp_data.selling_power.set_constant_selling_price(self.sel_chp)
        chp_data.activate_binary()
        chp_data.set_limit(False, None, self.heat_line)
        self.ds.add_district_heating(self.buy_district)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs_binary, ones(1), method='SLSQP')
        c = res.fun
        obj = self.ds.res['Obj']

        size = chp_data.size.value[0]
        for power in chp_data.power_el.value:
            self.assertTrue(isclose(power, 0, rel_tol=0.00001) or isclose(power, size, rel_tol=0.00001))

        a = chp_data.power_heat.value / self.eta_th * self.eta_el
        print(f'{a[nonzero(a)].min()}; {chp_data.size.value}; {res.x}')

        self.assertAlmostEqual(c, obj, delta=self.ds.res['GAP'] * obj)
        self.assertLessEqual(obj, c)
        self.assertAlmostEqual(res.x[0], chp_data.size.value, delta=self.ds.res['GAP'] * 100 * chp_data.size.value)
        print(f'Validation of CHP with minimal power successful: {int(c)} == {int(obj)}\n')

    def test_max_gradient_up(self):
        self.setUpClass()

        chp_data = self.ds.add_combined_heat_power(self.price_lin, self.price_con, 0, self.lifetime)
        chp_data.set_efficiencies(self.eta_el, self.eta_th)
        chp_data.selling_power.set_constant_selling_price(self.sel_chp)
        chp_data.activate_max_gradient_up(self.max_gradient_up)
        self.ds.add_district_heating(self.buy_district)

        self.ds.calc()

        res = optimize.minimize(self.calc_max_gradient_up, ones(1), method='SLSQP')
        c = res.fun
        obj = self.ds.res['Obj']

        size = chp_data.size.value[0]
        power = chp_data.power_el.value
        for t in range(self.demand.size):
            self.assertLessEqual(power[t] - power[t - 1], size * self.max_gradient_up * 1.000001)

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x[0], chp_data.size.value, delta=0.0015 * chp_data.size.value)
        print(f'Validation of CHP with maximal gradient upward successful: {int(c)} == {int(obj)}\n')

    def test_max_gradient_down(self):
        self.setUpClass()

        chp_data = self.ds.add_combined_heat_power(self.price_lin, self.price_con, 0, self.lifetime)
        chp_data.set_efficiencies(self.eta_el, self.eta_th)
        chp_data.selling_power.set_constant_selling_price(self.sel_chp)
        chp_data.activate_max_gradient_down(self.max_gradient_down)
        self.ds.add_district_heating(self.buy_district)

        self.ds.calc()

        size = chp_data.size.value[0]
        power = chp_data.power_el.value
        for t in range(self.demand.size):
            self.assertLessEqual(power[t - 1] - power[t], size * self.max_gradient_down * 1.000001)

        res = optimize.minimize(self.calc_max_gradient_down, ones(1), method='SLSQP')
        c = res.fun
        obj = self.ds.res['Obj']

        self.assertAlmostEqual(c, obj, delta=0.0015 * obj)
        self.assertLessEqual(obj, c)
        self.assertAlmostEqual(res.x[0], chp_data.size.value, delta=0.015 * chp_data.size.value)
        print(f'Validation of CHP with maximal gradient downward successful: {int(c)} == {int(obj)}\n')


if __name__ == '__main__':
    chp = TestCHP()
    chp.test_standard()
    chp.test_flexible_selling()
    chp.test_min_power()
    chp.test_binary()
    chp.test_max_gradient_up()
    chp.test_max_gradient_down()
