from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the district cooling node and cooling demands
"""


class TestCooling(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    cool_line: str = 'Cooling'
    buy_line: str = 'Buy'
    buy_cool: float = 0.08
    buy_el: float = 0.30
    power_price = 20.
    lifetime: int = 20
    # variables for multiple demand checking
    cool_line_2: str = 'Electricity'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_sta: float = 100_000.
    sell_sta: float = 0.06
    price_lin: float = 200.
    sta_line: str = 'STAPowerCold'
    sell_line: str = 'Sell'

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)
        self.demand = self.df[self.cool_line] * -1

        # set solver
        self.ds.set_txt_solver()

    def test_constant_price(self):
        self.ds.add_single_cooling_demand(self.cool_line, -1)
        district_cool = self.ds.add_district_cooling(self.buy_cool, None, self.power_price)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = self.demand.sum() * self.buy_cool * self.lifetime + self.power_price * self.demand.max() * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of district cooling constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price(self):
        self.ds.add_single_cooling_demand(self.cool_line, -1)
        district_cool = self.ds.add_district_cooling(self.buy_cool, None, self.power_price)
        district_cool.use_power_price = False
        district_cool.set_flexible_price(self.buy_line)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = sum(self.demand * self.df[self.buy_line]) * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of district cooling flexible price successful: {int(c)} == {int(obj)}')

    def test_constant_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_cooling_demand(self.cool_line)
        district_cool = self.ds.add_district_cooling(self.buy_cool)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = self.demand.sum() * self.buy_cool * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of district cooling constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_cooling_demand(self.cool_line)

        district_cool = self.ds.add_district_cooling(self.buy_cool)
        district_cool.set_flexible_price(self.buy_line)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = sum(self.demand * self.df[self.buy_line]) * self.lifetime
        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of district cooling flexible price successful: {int(c)} == {int(obj)} in error of 'f'{err * 100} %')

    def test_multiple_demand_1(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_multiple_cooling_demand([self.cool_line, self.cool_line], [self.price_1, self.price_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.demand.sum() * self.buy_cool * self.lifetime + min(self.price_1, self.price_2)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple cooling demands 1 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_2(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_multiple_cooling_demand([self.cool_line, self.cool_line_2], [self.price_1, self.price_1])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = min(self.demand.sum(), self.df[self.cool_line_2].sum()) * self.buy_cool * self.lifetime + self.price_1

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple heating demands 2 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_3(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        #self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_multiple_cooling_demand([self.cool_line, self.cool_line], [self.price_1, self.price_1], [self.life_1, self.life_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.demand.sum() * self.buy_cool * self.lifetime + min(self.price_1 / self.life_1 * self.lifetime, self.price_1 / self.life_2 * self.lifetime)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple cooling demands 3 successful: {int(c)} == {int(obj)}')

    def test_constant_sell_price_with_sta(self):
        self.ds.add_district_cooling(self.buy_cool, self.sell_sta)
        sta_data = self.ds.add_solar_thermal_absorber(self.price_lin, 0, 0, self.lifetime)
        sta_data.get_power_from_data(column_name_cold_side=self.sta_line)
        sta_data.set_connections(False, False, False, True)
        sta_data.set_max(self.max_sta)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c = self.max_sta * self.price_lin - self.df[self.sta_line].where(self.df[self.sta_line] < 0).sum() * -1 * self.max_sta / 1000 * self.sell_sta * self.lifetime
        c_sell = self.price_lin / self.df[self.sta_line].where(self.df[self.sta_line] < 0).sum() / self.lifetime * 1000 * -1

        self.assertAlmostEqual(sta_data.size.value, self.max_sta, delta=0.000001*sta_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))
        self.assertLess(c_sell, self.sell_sta)

        print(f'Validation of district cooling constant reward successful: {c_sell} < {self.sell_sta} and {int(c)} == f"{int(obj)}"')

    def test_flexible_sell_price_with_sta(self):
        self.ds.add_district_cooling(self.buy_line, self.sell_line)
        sta_data = self.ds.add_solar_thermal_absorber(self.price_lin, 0, 0, self.lifetime)
        sta_data.get_power_from_data(column_name_cold_side=self.sta_line)
        sta_data.set_connections(False, False, False, True)
        sta_data.set_max(self.max_sta)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c_sell = (self.df[self.sta_line] * -1).clip(0) * self.df[self.sell_line]
        c = self.max_sta * self.price_lin - c_sell.where(self.df[self.sell_line] > 0).sum() * self.max_sta / 1000 * self.lifetime

        self.assertAlmostEqual(sta_data.size.value, self.max_sta, delta=0.000001*sta_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))

        print(f'Validation of district cooling flexible reward successful: {int(c)} == {int(obj)}')