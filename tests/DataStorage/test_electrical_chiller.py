from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv
from scipy import optimize
from numpy import ndarray, array, ones

"""
validation script for the electrical chiller
"""


class TestElectricalChiller(TestCase):
    data = f'{FOLDER}/data/data.csv'
    cool_line: str = 'Cooling'
    buy_el: float = 0.18
    buy_cool: float = 0.1
    cop: float = 3.
    price_lin: float = 100.
    price_con: float = 1_000.
    power_el: float = 200.
    thermal_power_line: str = 'ElectricChiller'
    lifetime: int = 20

    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_cooling_demand(self.cool_line)
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_district_cooling(self.buy_cool)

        # set solver
        self.ds.set_txt_solver()

        self.demand = -1 * self.df[self.cool_line]
        self.sum_d = sum(self.demand)

    def calc_costs_constant_cop(self, size: ndarray):
        size = size[0]
        q_ec = self.demand.where(self.demand <= size).sum() + self.demand.where(self.demand > size).count() * size
        return size * self.price_lin + (self.sum_d - q_ec) * self.buy_cool * self.lifetime + q_ec / self.cop * self.buy_el * self.lifetime

    def test_constant_cop(self):
        ec_data = self.ds.add_electrical_chiller(self.price_lin, 0, 0, self.lifetime)
        ec_data.set_constant_cop(self.cop)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs_constant_cop, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
        c = res.fun
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))

        print(f'Validation of electrical chiller with constant COP successful: {int(c)} == {int(obj)}')

    def calc_costs_flexible_cop(self, size: ndarray):
        th_power = self.df[self.thermal_power_line] / 1000
        cop_flex = th_power / self.power_el * 1000
        size = size[0]
        q_ec: ndarray = array([d if d <= (size * th) else th * size for d, th in zip(self.demand, th_power)])
        p_ec: float = (q_ec / cop_flex).sum()
        q_ec: float = q_ec.sum()
        return size * self.price_lin + self.price_con + ((self.sum_d - q_ec) * self.buy_cool + p_ec * self.buy_el) * self.lifetime

    def test_flexible_cop(self):
        ec_data = self.ds.add_electrical_chiller(self.price_lin, self.price_con, 0, self.lifetime)
        ec_data.set_flexible_cop(self.power_el, self.thermal_power_line)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs_flexible_cop, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
        c = res.fun
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))

        print(f'Validation of electrical chiller with flexible COP successful: {int(c)} == {int(obj)}')