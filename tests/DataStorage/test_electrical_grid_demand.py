from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the electrical grid node and the electrical demands
"""


class TestElectricalGridDemand(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    el_line: str = 'Electricity'
    buy_line: str = 'Buy'
    buy_el: float = 0.18
    buy_el_2: float = 0.36
    power_price: float = 15
    base_price: float = 9 * 12
    base_price_2: float = 9 * 12 * 2
    lifetime: int = 20
    # variables for multiple demand checking
    el_line_2: str = 'Heating'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_pv: float = 100_000.
    sell_pv: float = 0.06
    price_lin: float = 250.
    pv_line: str = 'PV'
    sell_line: str = 'Sell'

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)
        self.demand = self.df[self.el_line]

        # set solver
        self.ds.set_txt_solver()

    def test_constant_price(self):
        self.ds.add_single_electrical_demand(self.el_line)
        el_grid = self.ds.add_electrical_grid(self.buy_el, None, self.power_price, self.base_price)
        self.ds.add_electrical_grid(self.buy_el_2, None, self.power_price, self.base_price_2)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = self.demand.sum() * self.buy_el * self.lifetime + self.power_price * self.df[self.el_line].max() * self.lifetime + self.base_price * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price(self):
        self.ds.add_single_electrical_demand(self.el_line)
        el_grid = self.ds.add_electrical_grid(self.buy_el, None, self.power_price, self.base_price)
        el_grid.use_power_price = False
        el_grid.use_base_price = False
        el_grid.set_flexible_price(self.buy_line)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = sum(self.demand * self.df[self.buy_line]) * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrical grid flexible price successful: {int(c)} == {int(obj)}')

    def test_constant_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_electrical_demand(self.el_line)
        el_grid = self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = self.demand.sum() * self.buy_el * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_electrical_demand(self.el_line)

        el_grid = self.ds.add_electrical_grid(self.buy_el)
        el_grid.set_flexible_price(self.buy_line)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = sum(self.demand * self.df[self.buy_line]) * self.lifetime
        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of electrical grid flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')

    def test_multiple_demand_1(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_multiple_electrical_demand([self.el_line, self.el_line], [self.price_1, self.price_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.demand.sum() * self.buy_el * self.lifetime + min(self.price_1, self.price_2)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple electrical demands 1 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_2(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_multiple_electrical_demand([self.el_line, self.el_line_2], [self.price_1, self.price_1])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = min(self.demand.sum(), self.df[self.el_line_2].sum()) * self.buy_el * self.lifetime + self.price_1

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple electrical demands 2 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_3(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_multiple_electrical_demand([self.el_line, self.el_line], [self.price_1, self.price_1], [self.life_1, self.life_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.demand.sum() * self.buy_el * self.lifetime + min(self.price_1 / self.life_1 * self.lifetime, self.price_1 / self.life_2 * self.lifetime)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple electrical demands 3 successful: {int(c)} == {int(obj)}')

    def test_constant_sell_price_with_pv(self):
        self.ds.add_electrical_grid(self.buy_el, self.sell_pv)
        pv_data = self.ds.add_photovoltaic(self.price_lin, 0, 0, self.lifetime)
        pv_data.produced_power.get_power_from_data(self.pv_line, 1 / 1000)
        pv_data.selling_power.set_constant_selling_price(0)
        pv_data.set_max(self.max_pv)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c = self.max_pv * self.price_lin - self.df[self.pv_line].sum() * self.max_pv / 1000 * self.sell_pv * self.lifetime
        c_sell = self.price_lin / sum(self.df[self.pv_line]) / self.lifetime * 1000

        self.assertAlmostEqual(pv_data.size.value, self.max_pv, delta=0.000001*pv_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))
        self.assertLess(c_sell, self.sell_pv)

        print(f'Validation of electrical grid constant reward successful: {c_sell} < {self.sell_pv} and {int(c)} == '
              f'{int(obj)}')

    def test_flexible_sell_price_with_pv(self):
        self.ds.add_electrical_grid(self.buy_line, self.sell_line)
        pv_data = self.ds.add_photovoltaic(self.price_lin, 0, 0, self.lifetime)
        pv_data.produced_power.get_power_from_data(self.pv_line, 1 / 1000)
        pv_data.selling_power.set_constant_selling_price(0)
        pv_data.set_max(self.max_pv)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c_sell = self.df[self.pv_line] * self.df[self.sell_line]
        c = self.max_pv * self.price_lin - c_sell.where(self.df[self.sell_line] > 0).sum() * self.max_pv / 1000 * self.lifetime

        self.assertAlmostEqual(pv_data.size.value, self.max_pv, delta=0.000001*pv_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))

        print(f'Validation of electrical grid flexible reward successful: {int(c)} == {int(obj)}')