from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the electrical heater
"""


class TestElectricalHeater(TestCase):
    data = f'{FOLDER}/data/data.csv'
    heat_line: str = 'Heating'
    buy_el: float = 0.18
    eta: float = 0.96
    price_lin: float = 100.
    price_con: float = 1_000.
    lifetime: int = 20

    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_heating_demand(self.heat_line)
        self.ds.add_electrical_grid(self.buy_el)

        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.heat_line]
        self.sum_d = sum(self.demand)

    def test_standard(self):
        self.eh_data = self.ds.add_electrical_heater(self.price_lin, 0, 0, self.lifetime)
        self.eh_data.set_efficiency(self.eta)

        self.ds.calc()

        c = self.sum_d / self.eta * self.buy_el * self.lifetime + self.demand.max() * self.price_lin
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))

        print(f'Validation of electrical heater successful: {int(c)} == {int(obj)}')

    def test_standard_constant_price(self):
        eh_data = self.ds.add_electrical_heater(self.price_lin, self.price_con, 0, self.lifetime)
        eh_data.set_efficiency(self.eta)

        self.ds.calc()

        c = self.sum_d / self.eta * self.buy_el * self.lifetime + self.demand.max() * self.price_lin + self.price_con
        obj = self.ds.res["Obj"]

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))

        print(f'Validation of electrical chiller with constant price successful: {int(c)} == {obj}')