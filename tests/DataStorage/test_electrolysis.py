from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv
from scipy import optimize
from numpy import ones, ndarray

"""
validation script for the electrolysis
"""


class TestElectrolysis(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    hydro_line: str = 'Electricity'
    buy_hydro: float = 1.05
    buy_el: float = 0.15
    buy_heat: float = 0.08
    sell_heat: float = 0.06
    lifetime: int = 20
    # electrolysis variables
    price_lin: float = 200.
    price_con: float = 2000.
    eta_el: float = 0.4
    eta_th: float = 0.5

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_hydrogen_demand(self.hydro_line)
        self.ds.add_district_hydro(self.buy_hydro)
        self.ds.add_electrical_grid(self.buy_el)
        self.demand = self.df[self.hydro_line]
        self.sum_d: float = self.demand.sum()

        # set solver
        self.ds.set_txt_solver()

    def calc_costs(self, size: ndarray) -> float:
        size = size[0]
        size_m = size * self.eta_el
        q_es = self.demand.where(self.demand <= size_m).sum() + self.demand.where(self.demand > size_m).count() * size_m
        return size * self.price_lin + self.price_con + (q_es / self.eta_el * self.buy_el + (self.sum_d - q_es) * self.buy_hydro) * self.lifetime

    def calc_costs_wo_constant(self, size: ndarray) -> float:
        size = size[0]
        size_m = size * self.eta_el
        q_es = self.demand.where(self.demand <= size_m).sum() + self.demand.where(self.demand > size_m).count() * size_m
        return size * self.price_lin + (q_es / self.eta_el * self.buy_el + (self.sum_d - q_es) * self.buy_hydro) * self.lifetime

    def calc_costs_waste_heat(self, size: ndarray) -> float:
        size = size[0]
        size_m = size * self.eta_el
        q_es = self.demand.where(self.demand <= size_m).sum() + self.demand.where(self.demand > size_m).count() * size_m
        return size * self.price_lin + self.price_con + (q_es / self.eta_el * self.buy_el + (self.sum_d - q_es) * self.buy_hydro - q_es / self.eta_el * self.eta_th * self.sell_heat) * self.lifetime

    def test_standard(self):
        electrolysis_data = self.ds.add_electrolysis(self.price_lin, self.price_con, 0, self.lifetime)
        electrolysis_data.set_electrical_efficiency(self.eta_el)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        res = optimize.minimize(self.calc_costs, ones(1), method='SLSQP')
        c = res.fun

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of electrolysis successful: {int(c)} == {int(obj)}')

    def test_standard_without_constant_price(self):
        electrolysis_data = self.ds.add_electrolysis(self.price_lin, 0, 0, self.lifetime)
        electrolysis_data.set_electrical_efficiency(self.eta_el)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        res = optimize.minimize(self.calc_costs_wo_constant, ones(1), method='SLSQP')
        c = res.fun

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrolysis without constant price successful: {int(c)} == {int(obj)}')

    def test_waste_heat_utilization(self):
        electrolysis_data = self.ds.add_electrolysis(self.price_lin, self.price_con, 0, self.lifetime)
        electrolysis_data.set_electrical_efficiency(self.eta_el)
        electrolysis_data.set_use_waste_heat(self.eta_th)
        self.ds.add_district_heating(self.buy_heat, self.sell_heat)

        self.ds.calc()

        obj = self.ds.res['Obj']

        res = optimize.minimize(self.calc_costs_waste_heat, ones(1), method='SLSQP')
        c = res.fun

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        self.assertAlmostEqual(electrolysis_data.size.value, res.x, delta=0.001*electrolysis_data.size.value)
        print(f'Validation of electrolysis with waste heat use successful: {int(c)} == {int(obj)}')

