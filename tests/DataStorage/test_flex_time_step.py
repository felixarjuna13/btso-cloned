from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv, to_datetime as pd_to_datetime
from numpy import trapz

"""
validation script for the district heating node and the heating demands
"""


class TestFlexTimeStep(TestCase):
    data = f'{FOLDER}/data/data_flex_time_step.csv'
    heat_line: str = 'Heating'
    date: str = 'Date'
    buy_line: str = 'Buy'
    buy_heat: float = 0.08
    buy_el: float = 0.30
    power_price: float = 25.
    lifetime: int = 20

    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(f'{FOLDER}/data/data_flex_time_step.csv', 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_heating_demand(self.heat_line)
        self.ds.add_district_heating(self.buy_heat, None, self.power_price)
        self.ds.add_electrical_grid(self.buy_el)

        # set solver
        self.ds.set_txt_solver()

    def test_trapezoid_rule(self):
        self.ds.set_integration_rule(1)
        # start calculation
        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']
        self.df[self.date] = pd_to_datetime(self.df[self.date])
        self.df.set_index(self.date, inplace=True)

        # Calculate total costs manually
        sample_points = self.df.reset_index()[self.date].diff().dt.total_seconds().fillna(0).cumsum().values
        e_tot = trapz(self.df[self.heat_line], x=sample_points) / 3600
        c = e_tot * self.buy_heat * self.lifetime + self.power_price * self.df[self.heat_line].max() * \
            self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of flexible time step calculation trapezoid rule successful: {int(c)} == {int(obj)}')

    def test_riemann_sum(self):
        self.ds.set_integration_rule(0)
        # start calculation
        self.ds.calc()

        obj = self.ds.res["Obj"]
        self.df[self.date] = pd_to_datetime(self.df[self.date])
        self.df.set_index(self.date, inplace=True)

        # Calculate total cost manually
        sample_points = self.df.reset_index()[self.date].diff().dt.total_seconds().fillna(0).values
        e_tot = sum(self.df[self.heat_line] * sample_points) / 3600 + self.df[self.heat_line][0] * sample_points[1] / 3600
        c = e_tot * self.buy_heat * self.lifetime + self.power_price * self.df[self.heat_line].max() * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))

        print(f'Validation of flexible time step calculation Riemann sum successful: {int(c)} == {int(obj)}')