from btso.DataStorage import DataStorage
from btso import FOLDER
from pandas import read_csv
from unittest import TestCase

"""
validation script for the fuel buy and the fuel demands
"""


class TestFuel(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    # set variables which are used later on
    demand_line: str = 'Demand'
    buy_line: str = 'Buy'
    sell_line: str = 'Sell'
    buy_fuel: float = 0.08
    reward: float = 0.02
    power_price = 20.
    lifetime: int = 20
    # variables for multiple demand checking
    demand_line_2: str = 'Electricity'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.demand_line]
        self.demand_2 = self.df[self.demand_line_2]

    def test_single_demand_and_fix_buy_sell_fuel_and_limit(self):
        # set variables in Datastorage
        self.ds.add_single_fuel_demand(self.demand_line)
        self.ds.add_fuel_connection(self.buy_fuel, self.reward, self.power_price)
        # start calculation
        self.ds.calc()
        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually
        c = self.demand.clip(0).sum() * self.buy_fuel * self.lifetime + self.demand.clip(None, 0).sum() * \
            self.reward * self.lifetime + self.power_price * self.demand.max() * self.lifetime
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=0.000001*obj)
        print(f'Validation of district fuel constant price successful: {int(c)} == {int(obj)}')

    def test_single_demand_and_flexible_buy_sell_fuel(self):
        # set variables in Datastorage
        self.ds.add_single_fuel_demand(self.demand_line)
        self.ds.add_fuel_connection(self.buy_line, self.sell_line)
        # start calculation
        self.ds.calc()
        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually
        c = (sum(self.demand.clip(0) * self.df[self.buy_line]) + sum(self.demand.clip(None, 0) *
                                                                     self.df[self.sell_line])) * self.lifetime
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=0.000001*obj)
        print(f'Validation of district fuel flexible price successful: {int(c)} == {int(obj)}')

    def test_typical_periods_with_fixed_price(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_fuel_demand(self.demand_line_2)
        self.ds.add_fuel_connection(self.buy_fuel)
        # set solver
        self.ds.calc()

        obj = self.ds.res['Obj']
        c = (self.demand_2.clip(0).sum() * self.buy_fuel) * self.lifetime
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=0.000001 * obj)
        print(f'Validation of district fuel constant price and typical periods successful: {int(c)} == {int(obj)}')

    def test_typical_periods_with_flexible_price(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_fuel_demand(self.demand_line_2)
        self.ds.add_fuel_connection(self.buy_line)
        self.ds.calc()

        obj = self.ds.res['Obj']
        c = sum(self.demand_2 * self.df[self.buy_line]) * self.lifetime
        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=err * obj)
        print(f'Validation of district fuel flexible price and typical days successful: {int(c)} == {int(obj)} in error'
              f' of {err * 100} %')

    def test_multiple_demands_with_different_price(self):
        self.ds.add_fuel_connection(self.buy_fuel, self.reward)
        self.ds.add_multiple_fuel_demand([self.demand_line, self.demand_line], [self.price_1, self.price_2])
        self.ds.calc()

        c = (self.demand.clip(0).sum() * self.buy_fuel + self.demand.clip(None, 0).sum() * self.reward
             ) * self.lifetime + min(self.price_1, self.price_2)
        obj = self.ds.res['Obj']
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=0.000001 * obj)
        print(f'Validation of multiple fuel demands with different price successful: {int(c)} == {int(obj)}')

    def test_multiple_demands_with_different_demand(self):
        self.ds.add_fuel_connection(self.buy_fuel, self.reward)
        self.ds.add_multiple_fuel_demand([self.demand_line, self.demand_line_2], [self.price_1, self.price_1])
        self.ds.calc()

        c = (self.demand.clip(0).sum() * self.buy_fuel + self.demand.clip(None, 0).sum() * self.reward
             ) * self.lifetime + min(self.price_1, self.price_2)
        obj = self.ds.res['Obj']
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=0.000001 * obj)
        print(f'Validation of multiple fuel demands with different demand successful: {int(c)} == {int(obj)}')

    def test_multiple_demands_with_different_lifetime(self):
        self.ds.add_fuel_connection(self.buy_fuel, self.reward)
        self.ds.add_multiple_fuel_demand([self.demand_line, self.demand_line], [self.price_1, self.price_1],
                                         [self.life_1, self.life_2])
        self.ds.calc()

        c = (self.demand.clip(0).sum() * self.buy_fuel + self.demand.clip(None, 0).sum() * self.reward
             ) * self.lifetime + min(self.price_1, self.price_2)
        obj = self.ds.res['Obj']
        # check if validation worked
        self.assertAlmostEqual(c, obj, delta=0.000001 * obj)
        print(f'Validation of multiple fuel demands with different lifetime successful: {int(c)} == {int(obj)}')


if __name__ == '__main__':
    fuel = TestFuel()
    fuel.test_single_demand_and_fix_buy_sell_fuel_and_limit()
    fuel.test_single_demand_and_flexible_buy_sell_fuel()
    fuel.test_typical_periods_with_fixed_price()
    fuel.test_typical_periods_with_flexible_price()
    fuel.test_multiple_demands_with_different_price()
    fuel.test_multiple_demands_with_different_demand()
    fuel.test_multiple_demands_with_different_lifetime()
