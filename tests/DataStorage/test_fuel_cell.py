from btso.DataStorage import DataStorage
from btso import FOLDER
import unittest
from pandas import read_csv
from scipy import optimize
from numpy import ones, ndarray, zeros
from typing import List

"""
validation script for the fuel cell
"""


class TestFuelCell(unittest.TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    # set variables which are used later on
    el_line: str = 'Electricity'
    buy_hydro: float = 0.05
    buy_el: float = 0.15
    buy_heat: float = 0.08
    sell_heat: float = 0.06
    lifetime: int = 20
    # fuel cell variables
    price_lin: float = 2000.
    price_con: float = 2000.
    eta_el: float = 0.4
    eta_th: float = 0.5
    eta_flex: List[float] = [0.690921634, 0.660122413, 0.643802571, 0.630241605, 0.616389168, 0.6]
    eta_flex_raise: list[float] = [0.345, 0.64308741, 0.637389681, 0.625069439, 0.616761756, 0.588]
    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.add_district_hydro(self.buy_hydro)
        self.ds.add_electrical_grid(self.buy_el)
        # set solver
        self.ds.set_txt_solver()
        self.demand = self.df[self.el_line]
        self.sum_d = self.demand.sum()

    def calc_costs(self, size: ndarray) -> float:
        size = size[0]
        p_fc = self.demand.clip(None, size).sum()
        return size * self.price_lin + self.price_con + (p_fc / self.eta_el * self.buy_hydro + (self.sum_d - p_fc)
                                                         * self.buy_el) * self.lifetime

    def calc_costs_weak_flexible_efficiency(self, size: ndarray) -> float:
        return self.calc_costs_flexible_efficiency(size, self.eta_flex)

    def calc_costs_linear_flexible_efficiency(self, size: ndarray) -> float:
        return self.calc_costs_flexible_efficiency(size, self.eta_flex_raise)

    def calc_costs_flexible_efficiency(self, size, eta_flex):
        size = max(size[0], 0)
        p_fc = zeros(self.demand.size)
        p_el = zeros(self.demand.size)
        c = 1
        eta_flex_int = []
        for e in eta_flex:
            eta_flex_int.append(c * e - sum(eta_flex_int))
            c += 1
        len_eta = len(eta_flex)
        for t in range(self.demand.size):
            d = min(self.demand[t], size)
            p_el[t] = d
            p_fc[t] = min(d, 1 / len_eta * size) / eta_flex_int[0]
            p_fc[t] += max(min(d - 1 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[1]
            p_fc[t] += max(min(d - 2 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[2]
            p_fc[t] += max(min(d - 3 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[3]
            p_fc[t] += max(min(d - 4 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[4]
            p_fc[t] += max(min(d - 5 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[5]

        return size * self.price_lin + self.price_con + (p_fc.sum() * self.buy_hydro + (self.sum_d - p_el.sum())
                                                         * self.buy_el) * self.lifetime

    def calc_costs_wo_constant(self, size: ndarray) -> float:
        size = size[0]
        p_fc = self.demand.clip(None, size).sum()
        return size * self.price_lin + (p_fc / self.eta_el * self.buy_hydro + (self.sum_d - p_fc) *
                                        self.buy_el) * self.lifetime

    def calc_costs_waste_heat(self, size: ndarray) -> float:
        size = size[0]
        q_fc = self.demand.clip(None, size).sum()
        return size * self.price_lin + self.price_con + (q_fc / self.eta_el * self.buy_hydro + (self.sum_d - q_fc) *
                                                         self.buy_el - q_fc / self.eta_el * self.eta_th *
                                                         self.sell_heat) * self.lifetime

    def test_standard(self):
        fuel_cell_data = self.ds.add_fuel_cell(self.price_lin * 1_000, self.price_con, 0, self.lifetime)
        fuel_cell_data.set_electrical_efficiency(self.eta_el)
        fuel_cell_data2 = self.ds.add_fuel_cell(self.price_lin, self.price_con, 0, self.lifetime)
        fuel_cell_data2.set_electrical_efficiency(self.eta_el)
        # start calculation
        self.ds.calc()
        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually

        for q_hydro, p_el in zip(fuel_cell_data2.power_hydro.value, fuel_cell_data2.power_el.value):
            self.assertAlmostEqual(p_el, q_hydro * self.eta_el, delta=0.00001 * p_el)

        res = optimize.minimize(self.calc_costs, ones(1), method='SLSQP')
        c = res.fun
        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, fuel_cell_data2.size.value, delta=0.000_001 * fuel_cell_data2.size.value)
        print(f'Validation of fuel cell successful: {int(c)} == {int(obj)}\n')

    def test_wo_constant_price(self):
        fuel_cell_data = self.ds.add_fuel_cell(self.price_lin, 0, 0, self.lifetime)
        fuel_cell_data.set_electrical_efficiency(self.eta_el)
        # start calculation
        self.ds.calc()
        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually

        for q_hydro, p_el in zip(fuel_cell_data.power_hydro.value, fuel_cell_data.power_el.value):
            self.assertAlmostEqual(p_el, q_hydro * self.eta_el, delta=0.00001 * p_el)

        res = optimize.minimize(self.calc_costs_wo_constant, ones(1), method='SLSQP')
        c = res.fun
        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, fuel_cell_data.size.value, delta=0.000_001 * fuel_cell_data.size.value)
        print(f'Validation of fuel cell without constant price successful: {int(c)} == {int(obj)}\n')

    def test_with_waste_heat_use(self):
        fuel_cell_data = self.ds.add_fuel_cell(self.price_lin, self.price_con, 0, self.lifetime)
        fuel_cell_data.set_electrical_efficiency(self.eta_el)
        fuel_cell_data.set_use_waste_heat(self.eta_th)
        self.ds.add_district_heating(self.buy_heat, self.sell_heat)
        # start calculation
        self.ds.calc()
        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually

        for q_hydro, p_el, q_heat in zip(fuel_cell_data.power_hydro.value, fuel_cell_data.power_el.value,
                                         fuel_cell_data.power_heat.value):
            self.assertAlmostEqual(p_el, q_hydro * self.eta_el, delta=0.00001 * p_el)
            self.assertAlmostEqual(q_heat, q_hydro * self.eta_th, delta=0.00001 * q_heat)

        res = optimize.minimize(self.calc_costs_waste_heat, ones(1), method='SLSQP')
        c = res.fun
        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, fuel_cell_data.size.value, delta=0.000_001 * fuel_cell_data.size.value)
        print(f'Validation of fuel cell with waste heat use successful: {int(c)} == {int(obj)}\n')

    def test_weak_flexible_efficiency(self):
        fuel_cell_data = self.ds.add_fuel_cell(self.price_lin, self.price_con, 0, self.lifetime)
        self.assertRaises(ValueError, fuel_cell_data.set_flexible_weak_efficiency, self.eta_flex_raise)
        fuel_cell_data.set_flexible_weak_efficiency(self.eta_flex)
        # start calculation
        self.ds.calc()

        size = fuel_cell_data.size.value
        len_eta = len(self.eta_flex)
        c = 1
        eta_flex_int = []
        for e in self.eta_flex:
            eta_flex_int.append(c * e - sum(eta_flex_int))
            c += 1
        for q_hydro, p_el in zip(fuel_cell_data.power_hydro.value, fuel_cell_data.power_el.value):
            p_fc = min(p_el, 1 / len_eta * size) / eta_flex_int[0]
            p_fc += max(min(p_el - 1 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[1]
            p_fc += max(min(p_el - 2 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[2]
            p_fc += max(min(p_el - 3 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[3]
            p_fc += max(min(p_el - 4 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[4]
            p_fc += max(min(p_el - 5 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[5]
            self.assertAlmostEqual(q_hydro, p_fc, delta=0.00001 * p_el)

        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually

        res = optimize.minimize(self.calc_costs_weak_flexible_efficiency, ones(1), method='SLSQP', bounds=((0, size * 10),))
        c = res.fun

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, fuel_cell_data.size.value, delta=0.000_001 * fuel_cell_data.size.value)
        print(f'Validation of fuel cell with flexible efficiency successful: {int(c)} == {int(obj)}\n')

    def test_linear_flexible_efficiency(self):
        fuel_cell_data = self.ds.add_fuel_cell(self.price_lin, self.price_con, 0, self.lifetime)
        fuel_cell_data.set_flexible_linear_efficiency(self.eta_flex_raise)
        # start calculation
        self.ds.TimeLimit = 3600
        self.ds.MIPGap = 0.000001
        # self.ds.Heuristic = 0
        self.ds.calc()

        size = fuel_cell_data.size.value
        len_eta = len(self.eta_flex_raise)
        c = 1
        eta_flex_int = []
        for e in self.eta_flex_raise:
            eta_flex_int.append(c * e - sum(eta_flex_int))
            c += 1
        for q_hydro, p_el in zip(fuel_cell_data.power_hydro.value, fuel_cell_data.power_el.value):
            p_fc = min(p_el, 1 / len_eta * size) / eta_flex_int[0]
            p_fc += max(min(p_el - 1 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[1]
            p_fc += max(min(p_el - 2 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[2]
            p_fc += max(min(p_el - 3 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[3]
            p_fc += max(min(p_el - 4 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[4]
            p_fc += max(min(p_el - 5 / len_eta * size, 1 / len_eta * size), 0) / eta_flex_int[5]
            self.assertAlmostEqual(q_hydro, p_fc, delta=0.00001 * p_el)

        # get total costs result
        obj = self.ds.res['Obj']
        # Calculate total costs manually

        res = optimize.minimize(self.calc_costs_linear_flexible_efficiency, ones(1), method='SLSQP', bounds=((0, size * 10),))
        c = res.fun

        self.assertAlmostEqual(c, obj, delta=0.000_001 * obj)
        self.assertAlmostEqual(res.x, fuel_cell_data.size.value, delta=0.000_001 * fuel_cell_data.size.value)
        print(f'Validation of fuel cell with flexible efficiency successful: {int(c)} == {int(obj)}\n')


if __name__ == '__main__':
    tfc = TestFuelCell()
    tfc.test_standard()
    tfc.test_wo_constant_price()
    tfc.test_with_waste_heat_use()
    tfc.test_linear_flexible_efficiency()
    tfc.test_weak_flexible_efficiency()
