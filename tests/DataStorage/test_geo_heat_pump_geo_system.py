from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv
from scipy import optimize
from numpy import ones, zeros, ndarray

"""
validation script for the geothermal heatpump and geothermal system
"""


class TestGeoHeatPumpGeoSystem(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    cop = 5.1
    cop_2 = 3.2
    heat_line: str = 'Heating'
    heat_line_2: str = 'Electricity'
    cool_line: str = 'Cooling'
    gain_geo: float = 50.
    frac_el: float = 0.02
    buy_el: float = 0.18
    buy_cool: float = 0.08
    price_lin_hp: float = 900.
    price_lin_geo: float = 50.
    price_con_hp: float = 1200.
    price_con_geo: float = 2000.
    lifetime: int = 20

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)

        # set solver
        self.ds.set_txt_solver()

    def test_standard(self):
        self.geo_hp_data = self.ds.add_geothermal_heat_pump(self.price_lin_hp, self.price_con_hp, 0, self.lifetime)
        self.geo_hp_data.set_constant_cop(self.cop)

        self.geo_data = self.ds.add_geothermal_system(self.price_lin_geo, self.price_con_geo, 0, self.lifetime)
        self.geo_data.set_constant_gain(self.gain_geo)
        self.geo_data.set_electrical_demand(self.frac_el)
        self.eh_data = self.ds.add_electrical_heater(0, 0, 0, self.lifetime)
        self.eh_data.set_efficiency(1)
        self.ds.add_single_heating_demand(self.heat_line)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        demand = self.df[self.heat_line]
        sum_d = sum(demand)
        c_old = sum_d * self.buy_el * self.lifetime

        for size in demand.sort_values(ascending=False):
            q_hp = demand.where(demand <= size).sum() + demand.where(demand > size).count() * size
            c_new = size * self.price_lin_hp + self.price_con_hp + size * (1 - 1 / self.cop) / self.gain_geo * 1000 * self.price_lin_geo + (
                self.price_con_geo) + (q_hp / self.cop + q_hp * (1 - 1 / self.cop) * self.frac_el + (sum_d - q_hp)) * self.buy_el * self.lifetime
            if c_new < c_old:
                c_old = c_new
                continue
            if c_new > c_old:
                break

        # get total costs result
        obj = self.ds.res['Obj']

        self.assertAlmostEqual(c_old, obj, delta=0.0001*abs(obj))

        print(f'Validation of geothermal systems successful: {int(c_old):_.0f} == {int(obj):_.0f}')

    def test_standard_cooling_system(self):
        geo_data = self.ds.add_geothermal_system(self.price_lin_geo, 0, 0, self.lifetime)
        geo_data.set_constant_gain(self.gain_geo)

        self.ds.add_single_cooling_demand(self.cool_line)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.cool_line] * - 1
        sum_d = sum(demand)
        c_old = sum_d * self.buy_el * self.lifetime

        for size in demand.sort_values(ascending=False):
            q_hp = demand.where(demand <= size).sum() + demand.where(demand > size).count() * size
            c_new = size / self.gain_geo * 1000 * self.price_lin_geo + ((sum_d - q_hp) * self.buy_cool) * self.lifetime
            if c_new < c_old:
                c_old = c_new
                continue
            if c_new > c_old:
                break

        self.assertAlmostEqual(c_old, obj, delta=0.0001*abs(obj))
        print(f'Validation of geothermal system for cooling successful: {int(c_old):_.0f} == {int(obj):_.0f}')

    def test_cooling_system_with_heat_pump(self):
        geo_hp_data = self.ds.add_geothermal_heat_pump(self.price_lin_hp, self.price_con_hp, 0, self.lifetime)
        geo_hp_data.set_constant_cop(self.cop)
        geo_data = self.ds.add_geothermal_system(self.price_lin_geo, 0, 0, self.lifetime)
        geo_data.set_constant_gain(self.gain_geo)
        geo_data.set_node(self.ds.electrical_nodes[0], self.ds.heat_nodes[0], self.ds.heat_nodes[0])
        self.ds.add_single_cooling_demand(self.cool_line)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.cool_line] * - 1
        sum_d = sum(demand)

        def calc_costs(size_j: ndarray):
            size_j = size_j[0]
            q_hp = demand.where(demand <= size_j * (1 - 1 / self.cop)).sum() + demand.where(
                demand > size_j * (1 - 1 / self.cop)).count() * size_j * (1 - 1 / self.cop)
            return size_j * self.price_lin_hp + self.price_con_hp + size_j / self.gain_geo * 1000 * self.price_lin_geo + (
                    q_hp / (1 - 1 / self.cop) / self.cop) * self.buy_el * self.lifetime + (sum_d - q_hp) * self.buy_cool * self.lifetime

        res = optimize.minimize(calc_costs, ones(1), method='SLSQP')
        c = res.fun

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of geothermal system for cooling with heat pump use successful:'
              f' {int(c):_.0f} == {int(obj):_.0f}')

    def test_geo_heat_pump_with_two_heat_nodes(self):
        first_node = self.ds.add_heating_node()
        second_node = self.ds.add_heating_node()
        geo_hp_data = self.ds.add_geothermal_heat_pump(self.price_lin_hp, self.price_con_hp, 0, self.lifetime)
        geo_hp_data.activate_second_node(first_node, second_node, self.cop, self.cop_2)
        geo_data = self.ds.add_geothermal_system(self.price_lin_geo, self.price_con_geo, 0, self.lifetime)
        geo_data.set_constant_gain(self.gain_geo)
        heat_d = self.ds.add_single_heating_demand(self.heat_line)
        heat_d.set_node(first_node)
        heat_d_2 = self.ds.add_single_heating_demand(self.heat_line_2)
        heat_d_2.set_node(second_node)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.heat_line]
        demand_2 = self.df[self.heat_line_2]

        q_hp = demand.sum()
        q_hp_2 = demand_2.sum()
        q_geo = max(demand_2 * (1 - 1 / self.cop_2) + demand * (1 - 1 / self.cop))
        size_j = max(demand + demand_2)
        costs = size_j * self.price_lin_hp + self.price_con_hp + q_geo / self.gain_geo * 1000 * self.price_lin_geo + (
            self.price_con_geo) + (q_hp / self.cop + q_hp_2 / self.cop_2) * self.buy_el * self.lifetime

        self.assertAlmostEqual(costs, obj, delta=0.0001*abs(obj))
        print(f'Validation of geothermal heat pump with two heat nodes successful:'
              f' {int(costs):_.0f} == {int(obj):_.0f}')
