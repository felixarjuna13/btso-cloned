from btso.DataStorage import DataStorage, Storage
from btso import FOLDER
from math import isclose
from pandas import read_csv, DataFrame
from scipy import optimize
from numpy import ones, array, zeros
from typing import Callable
from copy import deepcopy
from unittest import TestCase

"""
validation script for the hydrogen storage
"""


class TestHydrogenStorage(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    pv_line: str = 'PV'
    h2_line: str = 'Electricity'
    max_pv: float = 100_000.
    sell_h2: float = 0.06
    buy_h2: float = 0.32
    eta_self: float = 0.01
    eta_charge: float = 0.97
    eta_discharge: float = 0.95
    price_lin_pv: float = 200.
    price_con_pv: float = 2000.
    price_lin_h2: float = 5.
    price_con_h2: float = 350.
    eta_electrolysis: float = 0.7
    price_lin_electrolysis: float = 500.
    price_con_electrolysis: float = 3500.
    lifetime: int = 20
    start_val: float = 0.2

    df = None
    ds = None
    demand = None
    demand_c = None
    len_ye = None

    p_pv = None
    pv_data = None
    h2_data = None
    max_electrolysis = None
    electrolysis_data = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)

        # Max Electrolysis Setup
        p_pv = self.df[self.pv_line] * self.max_pv / 1000
        self.max_electrolysis = max(p_pv)

        self.pv_data = self.ds.add_photovoltaic(self.price_lin_pv, self.price_con_pv, 0, self.lifetime)
        self.pv_data.produced_power.get_power_from_data(self.pv_line, 1 / 1000)
        self.pv_data.selling_power.set_constant_selling_price(0)
        self.pv_data.set_min(self.max_pv)
        self.pv_data.set_max(self.max_pv)
        self.h2_data = self.ds.add_hydrogen_storage(self.price_lin_h2, self.price_con_h2, 0, self.lifetime)
        self.h2_data.set_values(self.eta_self * 100, self.eta_charge * 100, self.eta_discharge * 100)

        self.electrolysis_data = self.ds.add_electrolysis(self.price_lin_electrolysis, self.price_con_electrolysis, 0,
                                                        self.lifetime)
        self.electrolysis_data.set_min(self.max_electrolysis)
        self.electrolysis_data.set_electrical_efficiency(self.eta_electrolysis)

        self.h2_demand = self.ds.add_single_hydrogen_demand(self.h2_line)
        self.h2_grid = self.ds.add_district_hydro(self.buy_h2, self.sell_h2)

        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.h2_line]
        self.p_pv = self.df[self.pv_line] * self.max_pv / 1000 * self.eta_electrolysis
        self.len_ye = len(self.demand)
        self.ran_yr: range = range(self.len_ye)

    def calc_costs_backward(self, size_bat: array, eta_self_internal: float):
        size_bat = size_bat[0]
        bat_status = self.start_val * size_bat
        e_sell = 0
        e_buy = 0
        for p, d in zip(self.p_pv, self.demand):
            bat_status = bat_status * (1 - eta_self_internal)
            bat_status_old = bat_status
            if p > d:
                bat_status = min(bat_status + (p - d) * self.eta_charge, size_bat)
                e_sell += max(p - d - (bat_status - bat_status_old) / self.eta_charge, 0)
                continue
            bat_status = max(bat_status + (p - d) / self.eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - bat_status) * self.eta_discharge, 0)

        return self.price_lin_pv * self.max_pv + self.price_con_pv + self.price_lin_h2 * size_bat + self.price_con_h2 + (self.price_lin_electrolysis * self.max_electrolysis + self.price_con_electrolysis) + (e_buy * self.buy_h2 - e_sell * self.sell_h2) * self.lifetime

    def calc_costs_backward_with_self_discharge(self, size_bat: array):
        return self.calc_costs_backward(size_bat, self.eta_self)

    def calc_costs_backward_without_self_discharge(self, size_bat: array):
        return self.calc_costs_backward(size_bat, 0)

    def calc_costs_forward(self, size_bat: float, eta_self_internal: float):
        bat_status = self.start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in self.ran_yr:
            p = self.p_pv[t - 1] if t > 0 else 0
            d = self.demand[t - 1] if t > 0 else 0
            bat_status = bat_status * (1 - eta_self_internal)
            bat_status_old = bat_status
            if p > d:
                bat_status = min(bat_status + (p - d) * self.eta_charge, size_bat)
                e_sell += max(p - d - (bat_status - bat_status_old) / self.eta_charge, 0)
                continue
            bat_status = max(bat_status + (p - d) / self.eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - bat_status) * self.eta_discharge, 0)

        return self.price_lin_pv * self.max_pv + self.price_con_pv + self.price_lin_h2 * size_bat + self.price_con_h2 + (self.price_lin_electrolysis * self.max_electrolysis + self.price_con_electrolysis) + (e_buy * self.buy_h2 - e_sell * self.sell_h2) * self.lifetime

    def calc_costs_forward_with_self_discharge(self, size_bat: array):
        return self.calc_costs_forward(size_bat, self.eta_self)

    def calc_costs_forward_without_self_discharge(self, size_bat: array):
        return self.calc_costs_forward(size_bat, 0)

    def calc_costs_center(self, size_bat: float, eta_self_internal: float):
        bat_status = self.start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in self.ran_yr:
            p = self.p_pv[t - 1] / 2 + self.p_pv[t] / 2 if t > 0 else self.p_pv[t] / 2
            d = self.demand[t - 1] / 2 + self.demand[t] / 2 if t > 0 else self.demand[t] / 2
            bat_status = bat_status * (1 - eta_self_internal)
            bat_status_old = bat_status
            if p > d:
                bat_status = min(bat_status + (p - d) * self.eta_charge, size_bat)
                e_sell += max(p - d - (bat_status - bat_status_old) / self.eta_charge, 0)
                continue
            bat_status = max(bat_status + (p - d) / self.eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - bat_status) * self.eta_discharge, 0)

        return self.price_lin_pv * self.max_pv + self.price_con_pv + self.price_lin_h2 * size_bat + self.price_con_h2 + (self.price_lin_electrolysis * self.max_electrolysis + self.price_con_electrolysis) + (e_buy * self.buy_h2 - e_sell * self.sell_h2) * self.lifetime

    def calc_costs_center_with_self_discharge(self, size_bat: array):
        return self.calc_costs_center(size_bat, self.eta_self)

    def calc_costs_center_without_self_discharge(self, size_bat: array):
        return self.calc_costs_center(size_bat, 0)

    def check_backward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        bat_status = zeros(8760)
        bat_status[0] = start_value * size_bat * (1 - self.eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - self.eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(8760):
            bat_status[t] = bat_status[0] if t == 0 else bat_status[t - 1] * (1 - self.eta_self)
            bat_status_old = bat_status[t]
            p = self.p_pv[t]
            d = self.demand[t]
            if p > d:
                bat_status[t] = min(bat_status[t] + storage.power_charge.value[t] * self.eta_charge, size_bat)
                p_sell[t] = max(p - d - (bat_status[t] - bat_status_old) / self.eta_charge, 0)
            else:
                bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t] / self.eta_discharge, 0)
                p_buy[t] = max(d - p - (bat_status_old - bat_status[t]) * self.eta_discharge, 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_forward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        bat_status = zeros(8760)
        bat_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[8759] * (
                    1 - self.eta_self)
        bat_status[0] = bat_status[0] + (storage.power_charge.value[-1] * self.eta_charge -
                                         storage.power_discharge.value[-1] / self.eta_discharge)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(1, 8760):
            bat_status[t] = bat_status[t - 1] * (1 - self.eta_self)
            bat_status_old = bat_status[t]
            p = self.p_pv[t - 1]
            d = self.demand[t - 1]
            if p > d:
                bat_status[t] = min(bat_status[t] + storage.power_charge.value[t - 1] * self.eta_charge, size_bat)
                p_sell[t - 1] = max(p - d - (bat_status[t] - bat_status_old) / self.eta_charge, 0)
            else:
                bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t - 1] / self.eta_discharge, 0)
                p_buy[t - 1] = max(d - p - (bat_status_old - bat_status[t]) * self.eta_discharge, 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_central(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        bat_status = zeros(8760)
        bat_status[0] = start_value * size_bat * (1 - self.eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - self.eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        p = (self.p_pv[0])
        d = (self.demand[0])
        if p > d:
            bat_status[0] = min(bat_status[0] + (storage.power_charge.value[0]) / 2 * self.eta_charge, size_bat)
            p_sell[0] = max(p - d - storage.power_charge.value[0], 0)
        else:
            bat_status[0] = max(bat_status[0] - (storage.power_discharge.value[0]) / 2 / self.eta_discharge, 0)
            p_buy[0] += max(d - p - storage.power_discharge.value[0], 0)
        p = (self.p_pv[8759])
        d = (self.demand[8759])
        if p > d:
            bat_status[0] = min(bat_status[0] + (storage.power_charge.value[-1]) / 2 * self.eta_charge, size_bat)
        else:
            bat_status[0] = max(bat_status[0] - (storage.power_discharge.value[-1]) / 2 / self.eta_discharge, 0)
        assert isclose(round(bat_status[0], 2), round(storage.energy.value[0], 2), rel_tol=0.0001)
        assert isclose(round(p_buy[0], 2), round(buy[0], 2), rel_tol=0.0001)
        assert isclose(round(p_sell[0], 2), round(sell[0], 2), rel_tol=0.0001)
        for t in range(1, 8760):
            bat_status[t] = bat_status[t - 1] * (1 - self.eta_self)
            p = self.p_pv[t]
            d = self.demand[t]
            if p > d:
                bat_status[t] = min(
                    bat_status[t] + (storage.power_charge.value[t] + storage.power_charge.value[t - 1]) / 2
                    * self.eta_charge - (storage.power_discharge.value[t] +
                                    storage.power_discharge.value[t - 1]) / 2 / self.eta_discharge, size_bat)
                p_sell[t] = max(p - d - storage.power_charge.value[t] + storage.power_discharge.value[t], 0)
            else:
                bat_status[t] = max(bat_status[t] - (storage.power_discharge.value[t] +
                                                     storage.power_discharge.value[t - 1]) / 2 / self.eta_discharge +
                                    (storage.power_charge.value[t] + storage.power_charge.value[
                                        t - 1]) / 2 * self.eta_charge, 0)
                p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    @staticmethod
    def check_fail(funct: Callable, storage: Storage, buy: array, sell: array, start_value: float = None):
        try:
            funct(storage, buy, sell, start_value)
        except AssertionError:
            return
        raise AssertionError

    def test_standard_system(self):
        self.h2_data.set_start(self.start_val)
        self.ds.set_de_model(2)
        self.ds.calc()

        self.assertAlmostEqual(self.pv_data.size.value, self.max_pv, delta=0.0001 * self.pv_data.size.value)
        print(f'pv_data.size.value: {self.pv_data.size.value}')
        print(f'h2_data.size.value: {self.h2_data.size.value}')
        print(f'electrolysis_data.size.value: {self.electrolysis_data.size.value}')
        print(f"Validation PV and Battery system successful: {self.pv_data.size.value} == {self.max_pv}")

    def test_standard_backward_with_start_value(self):
        self.h2_data.set_start(self.start_val)
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_backward(self.h2_data, self.h2_grid.power_buy.value, self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_forward, self.h2_data, self.h2_grid.power_buy.value, self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)

        res_with = optimize.minimize(self.calc_costs_backward_with_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_backward_without_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(f'Validation of hydrogen storage size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
              f' <= {int(c_calc_with)}')

    def test_standard_backward(self):
        self.h2_data.start = False
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_backward(self.h2_data, self.h2_grid.power_buy.value,
                            self.h2_grid.power_sell.value)
        self.check_fail(self.check_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)
        self.check_fail(self.check_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)

        res_with = optimize.minimize(self.calc_costs_backward_with_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_backward_without_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(f'Validation of hydrogen storage size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
              f' <= {int(c_calc_with)}')

    def test_standard_forward_with_start_value(self):
        self.h2_data.set_start(self.start_val)
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_forward(self.h2_data, self.h2_grid.power_buy.value,
                           self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)

        res_with = optimize.minimize(self.calc_costs_forward_with_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_forward_without_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of hydrogen storage size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_standard_forward(self):
        self.h2_data.start = False
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_forward(self.h2_data, self.h2_grid.power_buy.value,
                           self.h2_grid.power_sell.value)
        self.check_fail(self.check_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)
        self.check_fail(self.check_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)

        res_with = optimize.minimize(self.calc_costs_forward_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_forward_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of hydrogen storage size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_standard_central_with_start_value(self):
        self.h2_data.set_start(self.start_val)
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_central(self.h2_data, self.h2_grid.power_buy.value,
                           self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)

        res_with = optimize.minimize(self.calc_costs_center_with_self_discharge, ones(1), method='SLSQP',
                                     bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_center_without_self_discharge, ones(1), method='SLSQP',
                                        bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of hydrogen storage size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def test_standard_central(self):
        self.h2_data.start = False
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_central(self.h2_data, self.h2_grid.power_buy.value,
                           self.h2_grid.power_sell.value)
        self.check_fail(self.check_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)
        self.check_fail(self.check_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)

        res_with = optimize.minimize(self.calc_costs_center_with_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_with = res_with.fun
        res_without = optimize.minimize(self.calc_costs_center_without_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
        c_calc_without = res_without.fun
        c_opt = self.ds.res['Obj']

        self.assertLessEqual(c_opt, c_calc_with * 1.00001)
        self.assertLessEqual(c_calc_without, c_opt * 1.00001)

        print(
            f'Validation of hydrogen storage size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')

    def check_tp_cyclic_forward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        ran_end = range(self.ds.tp_data.hours_per_period - 1, length, self.ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_end = storage.energy.value[j] * (1 - self.eta_self) - \
                        storage.power_discharge.value[j] / storage.eta_discharge + \
                        storage.power_charge.value[j] * storage.eta_charge
            assert isclose(storage.energy.value[i], power_end, rel_tol=0.0001)
        size_bat = storage.size.value
        bat_status = zeros(length)
        bat_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            bat_status[t] = bat_status[t - 1] * (1 - self.eta_self)
            p = self.pv_data.produced_power.power[t - 1] * self.max_pv * self.eta_electrolysis
            d = self.h2_demand.demand[t - 1]
            bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t - 1] / self.eta_discharge +
                                storage.power_charge.value[t - 1] * self.eta_charge, 0)
            p_buy[t - 1] = max(d - p - storage.power_discharge.value[t - 1] + storage.power_charge.value[t - 1], 0)
            p_sell[t - 1] = max(p - d + storage.power_discharge.value[t - 1] - storage.power_charge.value[t - 1], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_tp_cyclic_backward(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        ran_end = range(self.ds.tp_data.hours_per_period - 1, length, self.ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] /
                           storage.eta_discharge - storage.power_charge.value[i] * storage.eta_charge)
            assert isclose(power_start, storage.energy.value[j] * (1 - self.eta_self), rel_tol=0.0001)
        size_bat = storage.size.value
        bat_status = zeros(length)
        bat_status[0] = (start_value * size_bat - storage.power_discharge.value[0] /
                         storage.eta_discharge + storage.power_charge.value[0] * storage.eta_charge) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            bat_status[t] = bat_status[t - 1] * (1 - self.eta_self)
            p = self.pv_data.produced_power.power[t] * self.max_pv * self.eta_electrolysis
            d = self.h2_demand.demand[t]
            bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t] / self.eta_discharge +
                                storage.power_charge.value[t] * self.eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_tp_cyclic_central(self, storage: Storage, buy: array, sell: array, start_value: float = None):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        ran_end = range(self.ds.tp_data.hours_per_period - 1, length, self.ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] / storage.eta_discharge / 2
                           - storage.power_charge.value[i] * storage.eta_charge / 2)
            power_end = (storage.energy.value[j] * (1 - self.eta_self) - storage.power_discharge.value[j] /
                         storage.eta_discharge / 2 + storage.power_charge.value[j] * storage.eta_charge / 2)
            assert isclose(power_start, power_end, rel_tol=0.0001)
        size_bat = storage.size.value
        bat_status = zeros(length)
        bat_status[0] = (start_value * size_bat - storage.power_discharge.value[0] / storage.eta_discharge / 2
                         + storage.power_charge.value[0] * storage.eta_charge / 2) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            bat_status[t] = bat_status[t - 1] * (1 - self.eta_self)
            p = self.pv_data.produced_power.power[t] * self.max_pv * self.eta_electrolysis
            d = self.h2_demand.demand[t]
            bat_status[t] = max(bat_status[t] - (storage.power_discharge.value[t] +
                                                 storage.power_discharge.value[t - 1]) / 2
                                / self.eta_discharge + (storage.power_charge.value[t]
                                                   + storage.power_charge.value[t - 1]) / 2 * self.eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def test_typical_cyclic_forward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.h2_data.start = False
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_cyclic_forward(self.h2_data, self.h2_grid.power_buy.value, self.h2_grid.power_sell.value)
        self.check_fail(self.check_tp_cyclic_backward, self.h2_data, self.h2_grid.power_buy.value,
                   self.h2_grid.power_sell.value)
        self.check_fail(self.check_tp_cyclic_central, self.h2_data, self.h2_grid.power_buy.value, self.h2_grid.power_sell.value)

        print('test of cyclic hydrogen storage with forward euler successful!\n')

    def test_typical_cyclic_forward_with_start_value(self):
        self.h2_data.set_start(self.start_val)
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_cyclic_forward(self.h2_data, self.h2_grid.power_buy.value,
                                     self.h2_grid.power_sell.value,
                                     self.start_val)
        self.check_fail(self.check_tp_cyclic_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)

        print('test of cyclic hydrogen storage with forward euler with start value successful!\n')

    def test_typical_cyclic_backward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.h2_data.start = False
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_cyclic_backward(self.h2_data, self.h2_grid.power_buy.value,
                                      self.h2_grid.power_sell.value)
        self.check_fail(self.check_tp_cyclic_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)
        self.check_fail(self.check_tp_cyclic_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)

        print('test of cyclic hydrogen storage with backward euler successful!\n')

    def test_typical_cyclic_backward_with_start_value(self):
        self.h2_data.set_start(self.start_val)
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_cyclic_backward(self.h2_data, self.h2_grid.power_buy.value,
                                      self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_central, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)

        print('test of cyclic hydrogen storage with backward euler with start value successful!\n')

    def test_typical_cyclic_central(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.h2_data.start = False
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_cyclic_central(self.h2_data, self.h2_grid.power_buy.value,
                                     self.h2_grid.power_sell.value)
        self.check_fail(self.check_tp_cyclic_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)
        self.check_fail(self.check_tp_cyclic_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value)

        print('test of cyclic hydrogen storage with central euler successful!\n')

    def test_typical_cyclic_central_with_start_value(self):
        self.h2_data.set_start(self.start_val)
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_cyclic_central(self.h2_data, self.h2_grid.power_buy.value,
                                     self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_forward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)
        self.check_fail(self.check_tp_cyclic_backward, self.h2_data, self.h2_grid.power_buy.value,
                        self.h2_grid.power_sell.value, self.start_val)

        print('test of cyclic hydrogen storage with central euler with start value successful!\n')

    @staticmethod
    def compare_ds_results_storage(ds_first: DataStorage, ds_second: DataStorage):
        h2_data1 = ds_first.H2Storage[0]
        h2_grid1 = ds_first.district_hydro[0]
        h2_data2 = ds_second.H2Storage[0]
        h2_grid2 = ds_second.district_hydro[0]
        assert isclose(ds_first.res['Obj'], ds_second.res['Obj'], rel_tol=0.000001)
        assert isclose(h2_data1.size.value[0], h2_data2.size.value[0], rel_tol=0.000001)

        for t in range(ds_first.raw.shape[0]):
            assert isclose(round(h2_data1.power_charge.value[t], 2), round(h2_data2.power_charge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(h2_data1.power_discharge.value[t], 2), round(h2_data2.power_discharge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(h2_grid1.power_sell.value[t], 2), round(h2_grid2.power_sell.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(h2_grid1.power_buy.value[t], 2), round(h2_grid2.power_buy.value[t], 2),
                           rel_tol=0.00001)

    def check_tp_seasonal(self, storage: Storage, dgl: int):
        length = self.ds.raw.shape[0]
        ran_start = range(0, length, self.ds.tp_data.hours_per_period)
        f = 0.5 if dgl == 3 else 1
        for i in ran_start:
            power_start = storage.power_discharge.value[i] / self.eta_discharge - \
                          storage.power_charge.value[i] * self.eta_charge if dgl > 1 else 0
            assert round(storage.energy.value[i] + power_start * f, 2) == 0

        period = self.ds.tp_data.matching['PeriodNum'][0]
        period_hour = self.ds.tp_data.matching['TimeStep'][0]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        bat_status = zeros(length)
        power_start = (storage.power_discharge.value[t_old] / self.eta_discharge -
                       storage.power_charge.value[t_old] * self.eta_charge) if dgl > 1 else 0
        bat_status[0] = storage.energy_d.value[0] - power_start * f
        day = 0
        hour = 0
        for i in range(1, length):
            period = self.ds.tp_data.matching['PeriodNum'][i]
            period_hour = self.ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            bat_status[i] = bat_status[i - 1] * (1 - self.eta_self)
            power_last = 0 if dgl == 2 else (storage.power_charge.value[t_old] * self.eta_charge -
                                             storage.power_discharge.value[t_old] / self.eta_discharge)
            power_current = 0 if dgl == 1 else (storage.power_charge.value[t] * self.eta_charge -
                                                storage.power_discharge.value[t] / self.eta_discharge)
            bat_status[i] = bat_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(bat_status[i], 2) >= 0
            assert round(bat_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(
                round(storage.energy_d.value[day] * (1 - self.eta_self) ** period_hour + storage.energy.value[t], 2),
                round(bat_status[i], 2), rel_tol=0.0001)

    def test_typical_seasonal_backward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_seasonal(self.h2_data, 2)
        print('test of seasonal hydrogen storage with backward euler successful!\n')

        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()
        ds1.calc()

        self.compare_ds_results_storage(self.ds, ds1)
        print('test of seasonal+ hydrogen storage with backward euler successful!\n')

    def test_typical_seasonal_forward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_seasonal(self.h2_data, 1)
        print('test of seasonal hydrogen storage with forward euler successful!\n')

        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()
        ds1.calc()

        self.compare_ds_results_storage(self.ds, ds1)
        print('test of seasonal+ hydrogen storage with forward euler successful!\n')

    def test_typical_seasonal_central(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_seasonal(self.h2_data, 3)
        print('test of seasonal hydrogen storage with central euler successful!\n')

        ds1 = deepcopy(self.ds)
        ds1.tp_data.tp_seasonal_plus = True
        ds1.tp_data.aggregate_tp_seasonal()
        ds1.calc()

        self.compare_ds_results_storage(self.ds, ds1)
        print('test of seasonal+ hydrogen storage with central euler successful!\n')

    def check_tp_seasonal_simple(self, storage: Storage, dgl: int):
        f = 0.5 if dgl == 3 else 1

        period = self.ds.tp_data.matching['PeriodNum'][-1]
        period_hour = self.ds.tp_data.matching['TimeStep'][-1]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        bat_status = zeros(length)
        bat_status[-1] = storage.energy.value[-1]
        day = 0
        hour = 0
        for i in range(length):
            period = self.ds.tp_data.matching['PeriodNum'][i]
            period_hour = self.ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            bat_status[i] = bat_status[i - 1] * (1 - self.eta_self)
            power_last = 0 if dgl == 2 else (
                        storage.power_charge.value[t_old] * self.eta_charge - storage.power_discharge.value[
                    t_old] / self.eta_discharge)
            power_current = 0 if dgl == 1 else (
                        storage.power_charge.value[t] * self.eta_charge - storage.power_discharge.value[
                    t] / self.eta_discharge)
            bat_status[i] = bat_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(bat_status[i], 2) >= 0
            assert round(bat_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(round(storage.energy.value[i], 2), round(bat_status[i], 2), rel_tol=0.0001)

    def test_typical_seasonal_simple_forward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.tp_seasonal_simple = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(1)
        self.ds.calc()

        self.check_tp_seasonal_simple(self.h2_data, 1)

        print('test of simple seasonal hydrogen storage with forward euler successful!\n')

    def test_typical_seasonal_simple_backward(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.tp_seasonal_simple = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(2)
        self.ds.calc()

        self.check_tp_seasonal_simple(self.h2_data, 2)

        print('test of simple seasonal hydrogen storage with backward euler successful!\n')

    def test_typical_seasonal_simple_central(self):
        self.ds.import_typical_periods('typical_periods.pkl')
        self.ds.tp_data.tp_seasonal = True
        self.ds.tp_data.tp_seasonal_simple = True
        self.ds.tp_data.aggregate_tp_seasonal()
        self.ds.set_de_model(3)
        self.ds.calc()

        self.check_tp_seasonal_simple(self.h2_data, 3)

        print('test of simple seasonal hydrogen storage with central euler successful!\n')
