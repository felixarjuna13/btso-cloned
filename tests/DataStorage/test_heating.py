from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the district heating node and the heating demands
"""


class TestHeating(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    heat_line: str = 'Heating'
    buy_line: str = 'Buy'
    buy_heat: float = 0.08
    buy_el: float = 0.30
    power_price: float = 25.
    lifetime: int = 20
    # variables for multiple demand checking
    heat_line_2: str = 'Electricity'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_stc: float = 100_000.
    sell_stc: float = 0.06
    price_lin: float = 250.
    stc_line: str = 'STCPowerHot'
    sell_line: str = 'Sell'

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)
        self.ds.add_electrical_grid(self.buy_el)

        # set solver
        self.ds.set_txt_solver()

    def test_constant_price(self):
        self.ds.add_single_heating_demand(self.heat_line)
        district_heat = self.ds.add_district_heating(self.buy_heat, None, self.power_price)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = self.df[self.heat_line].sum() * self.buy_heat * self.lifetime + self.power_price * self.df[self.heat_line].max() * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of district heating constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price(self):
        self.ds.add_single_heating_demand(self.heat_line)
        district_heat = self.ds.add_district_heating(self.buy_heat, None, self.power_price)
        district_heat.use_power_price = False
        district_heat.set_flexible_price(self.buy_line)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = sum(self.df[self.heat_line] * self.df[self.buy_line]) * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of district heating flexible price successful: {int(c)} == {int(obj)}')

    def test_constant_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_heating_demand(self.heat_line)
        district_heat = self.ds.add_district_heating(self.buy_heat)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = self.df[self.heat_line].sum() * self.buy_heat * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of district heating constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_heating_demand(self.heat_line)

        district_heat = self.ds.add_district_heating(self.buy_heat)
        district_heat.set_flexible_price(self.buy_line)

        self.ds.raw[self.buy_line] = self.ds.raw['ElHeat'] / self.ds.raw[self.heat_line].clip(1)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = sum(self.df[self.heat_line] * self.df[self.buy_line]) * self.lifetime
        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of district heating flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')

    def test_multiple_demand_1(self):
        self.ds.import_typical_periods("./typical_periods.pkl")

        district_heat = self.ds.add_district_heating(self.buy_heat)
        district_heat.set_flexible_price(self.buy_line)
        
        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_multiple_heating_demand([self.heat_line, self.heat_line], [self.price_1, self.price_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.df[self.heat_line].sum() * self.buy_heat * self.lifetime + min(self.price_1, self.price_2)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple heating demands 1 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_2(self):
        self.ds.import_typical_periods("./typical_periods.pkl")

        district_heat = self.ds.add_district_heating(self.buy_heat)
        district_heat.set_flexible_price(self.buy_line)

        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_multiple_heating_demand([self.heat_line, self.heat_line_2], [self.price_1, self.price_1])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = min(self.df[self.heat_line].sum(), self.df[self.heat_line_2].sum()) * self.buy_heat * self.lifetime + self.price_1

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple heating demands 2 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_3(self):
        self.ds.import_typical_periods("./typical_periods.pkl")

        district_heat = self.ds.add_district_heating(self.buy_heat)
        district_heat.set_flexible_price(self.buy_line)

        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_multiple_heating_demand([self.heat_line, self.heat_line], [self.price_1, self.price_1], [self.life_1, self.life_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.df[self.heat_line].sum() * self.buy_heat * self.lifetime + min(self.price_1 / self.life_1 * self.lifetime, self.price_1 / self.life_2 * self.lifetime)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple heating demands 3 successful: {int(c)} == {int(obj)}')

    def test_constant_sell_price_with_stc(self):
        self.ds.add_district_heating(self.buy_heat, self.sell_stc)
        stc_data = self.ds.add_solar_thermal_collector(self.price_lin, 0, 0, self.lifetime)
        stc_data.get_power_from_data(self.stc_line)
        stc_data.set_connections(True, False, False, False)
        stc_data.set_max(self.max_stc)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c = self.max_stc * self.price_lin - self.df[self.stc_line].where(self.df[self.stc_line] > 0).sum() * self.max_stc / 1000 * self.sell_stc * self.lifetime
        c_sell = self.price_lin / self.df[self.stc_line].where(self.df[self.stc_line] > 0).sum() / self.lifetime * 1000

        self.assertAlmostEqual(stc_data.size.value, self.max_stc, delta=0.000001*stc_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))
        self.assertLess(c_sell, self.sell_stc)

        print(f'Validation of district heating constant reward successful: {c_sell} < {self.sell_stc} and {int(c)} == 'f'{int(obj)}')

    def test_flexible_sell_price_with_stc(self):
        self.ds.add_district_heating(self.buy_line, self.sell_line)
        stc_data = self.ds.add_solar_thermal_collector(self.price_lin, 0, 0, self.lifetime)
        stc_data.get_power_from_data(self.stc_line)
        stc_data.set_connections(True, False, False, False)
        stc_data.set_max(self.max_stc)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c_sell = self.df[self.stc_line].clip(0) * self.df[self.sell_line]
        c = self.max_stc * self.price_lin - c_sell.where(self.df[self.sell_line] > 0).sum() * self.max_stc / 1000 * self.lifetime

        self.assertAlmostEqual(stc_data.size.value, self.max_stc, delta=0.000001*stc_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))

        print(f'Validation of district heating flexible reward successful: {int(c)} == {int(obj)}')