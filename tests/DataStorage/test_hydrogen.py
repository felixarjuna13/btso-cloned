from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the district hydrogen node and the hydrogen demands
"""


class TestHydrogen(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    hydro_line: str = 'Electricity'
    buy_line: str = 'Buy'
    buy_hydro: float = 1.05
    buy_el: float = 0.15
    power_price: float = 5.
    lifetime: int = 20
    # variables for multiple demand checking
    hydro_line_2: str = 'Heating'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_es: float = 100.
    sell_es: float = 0.7
    price_lin: float = 200.
    eta_el: float = 0.4
    sell_line: str = 'Buy'
    buy_el_sell: float = 0.02

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)
        self.ds.add_electrical_grid(self.buy_el)

        # set solver
        self.ds.set_txt_solver()

    def test_constant_price(self):
        self.ds.add_single_hydrogen_demand(self.hydro_line)
        district_hydro = self.ds.add_district_hydro(self.buy_hydro, None, self.power_price)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = self.df[self.hydro_line].sum() * self.buy_hydro * self.lifetime + self.df[self.hydro_line].max() * self.power_price * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of district hydrogen constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price(self):
        self.ds.add_single_hydrogen_demand(self.hydro_line)
        district_hydro = self.ds.add_district_hydro(self.buy_hydro, None, self.power_price)
        district_hydro.use_power_price = False
        district_hydro.set_flexible_price(self.buy_line)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = sum(self.df[self.hydro_line] * self.df[self.buy_line]) * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of district hydrogen flexible price successful: {int(c)} == {int(obj)}')

    def test_constant_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_hydrogen_demand(self.hydro_line)
        district_hydro = self.ds.add_district_hydro(self.buy_hydro)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = self.df[self.hydro_line].sum() * self.buy_hydro * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of district hydrogen constant price successful: {int(c)} == {int(obj)}')

    def test_flexible_price_with_typical_periods(self):
        self.ds.import_typical_periods("./typical_periods.pkl")
        self.ds.add_single_hydrogen_demand(self.hydro_line)

        district_hydro = self.ds.add_district_hydro(self.buy_hydro)
        district_hydro.set_flexible_price(self.buy_line)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = sum(self.df[self.hydro_line] * self.df[self.buy_line]) * self.lifetime
        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of district hydrogen flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')

    def test_multiple_demand_1(self):
        self.ds.import_typical_periods("./typical_periods.pkl")

        self.ds.add_district_hydro(self.buy_hydro)
        self.ds.add_multiple_hydrogen_demand([self.hydro_line, self.hydro_line], [self.price_1, self.price_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.df[self.hydro_line].sum() * self.buy_hydro * self.lifetime + min(self.price_1, self.price_2)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple hydrogen demands 1 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_2(self):
        self.ds.import_typical_periods("./typical_periods.pkl")

        self.ds.add_district_hydro(self.buy_hydro)
        self.ds.add_multiple_hydrogen_demand([self.hydro_line, self.hydro_line_2], [self.price_1, self.price_1])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = min(self.df[self.hydro_line].sum(), self.df[self.hydro_line_2].sum()) * self.buy_hydro * self.lifetime + self.price_1

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple hydrogen demands 2 successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_3(self):
        self.ds.import_typical_periods("./typical_periods.pkl")

        self.ds.add_district_hydro(self.buy_hydro)
        self.ds.add_multiple_hydrogen_demand([self.hydro_line, self.hydro_line], [self.price_1, self.price_1], [self.life_1, self.life_2])

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        err = min(self.ds.tp_data.error['MAE']['Buy'], self.ds.tp_data.error['MAE']['Fos'])
        c = self.df[self.hydro_line].sum() * self.buy_hydro * self.lifetime + min(self.price_1 / self.life_1 * self.lifetime, self.price_1 / self.life_2 * self.lifetime)

        self.assertAlmostEqual(c, obj, delta=err*abs(obj))
        print(f'Validation of multiple hydrogen demands 3 successful: {int(c)} == {int(obj)}')

    def test_constant_sell_price_with_es(self):
        self.ds.add_district_hydro(self.buy_hydro, self.sell_es)
        es_data = self.ds.add_electrolysis(self.price_lin, 0, 0, self.lifetime)
        es_data.set_electrical_efficiency(self.eta_el)
        es_data.set_max(self.max_es)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c = self.max_es * self.price_lin - (self.sell_es * self.eta_el - self.buy_el) * self.max_es * 8760 * self.lifetime
        c_sell = (self.price_lin + self.buy_el * 8760 * self.lifetime) / (self.sell_es * self.eta_el * 8760 * self.lifetime)

        self.assertAlmostEqual(es_data.size.value, self.max_es, delta=0.000001*es_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))
        self.assertLess(c_sell, self.sell_es)

        print(f'Validation of district hydrogen constant reward successful: {c_sell} < {self.sell_es} and {int(c)} == '
              f'{int(obj)}')

    def test_flexible_sell_price_with_es(self):
        self.ds.add_electrical_grid(self.buy_el_sell)
        self.ds.add_district_hydro(self.buy_line, self.sell_line)
        es_data = self.ds.add_electrolysis(self.price_lin, 0, 0, self.lifetime)
        es_data.set_electrical_efficiency(self.eta_el)
        es_data.set_max(self.max_es)

        self.ds.calc()

        obj = self.ds.res['Obj']

        c = self.max_es * self.price_lin
        for i in self.df[self.sell_line]:
            if i * self.eta_el >= self.buy_el_sell:
                c += self.max_es * (self.buy_el_sell - i * self.eta_el) * self.lifetime

        self.assertAlmostEqual(es_data.size.value, self.max_es, delta=0.000001*es_data.size.value)
        self.assertAlmostEqual(c, obj, delta=0.000001*abs(obj))

        print(f'Validation of district hydrogen flexible reward successful: {int(c)} == {int(obj)}')