from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the multiple demands
"""


class TestMultipleDemands(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    el_line: str = 'Electricity'
    heat_line: str = 'Heating'
    cool_line: str = 'Cooling'
    hydro_line: str = 'PV'
    el_line_2: str = 'PV'
    heat_line_2: str = 'Electricity'
    cool_line_2: str = 'Heating'
    hydro_line_2: str = 'Cooling'
    buy_el: float = 0.18
    buy_heat: float = 0.08
    buy_cool: float = 0.06
    buy_hydro: float = 1.05
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    lifetime: int = 20

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)

        # set solver
        self.ds.set_txt_solver()

    def test_multiple_linked_demands_1(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_2], [self.lifetime, self.lifetime], [self.el_line, self.el_line], [self.heat_line, self.heat_line], [self.cool_line, self.cool_line], [self.hydro_line, self.hydro_line])
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_district_hydro(self.buy_hydro)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = (self.df[self.el_line].sum() * self.buy_el + self.df[self.heat_line].sum() * self.buy_heat - self.df[self.cool_line].sum() * self.buy_cool +
             self.df[self.hydro_line].sum() * self.buy_hydro) * self.lifetime + min(self.price_1, self.price_2)

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of multiple linked demands 1 successful: {int(c)} == {int(obj)}')

    def test_multiple_linked_demands_2(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_1], [self.lifetime, self.lifetime], [self.el_line, self.el_line_2],
                                       [self.heat_line, self.heat_line_2], [self.cool_line, self.cool_line_2], [self.hydro_line, self.hydro_line_2])
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_district_hydro(self.buy_hydro)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        # Calculate total costs manually
        c = min((self.df[self.el_line].sum() * self.buy_el + self.df[self.heat_line].sum() * self.buy_heat - self.df[self.cool_line].sum() * self.buy_cool +
                 self.df[self.hydro_line].sum() * self.buy_hydro), (self.df[self.el_line_2].sum() * self.buy_el + self.df[self.heat_line_2].sum() * self.buy_heat + self.df[self.cool_line_2].sum() * self.buy_cool - self.df[self.hydro_line_2].sum() * self.buy_hydro)) * self.lifetime + self.price_1

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of multiple linked demands 2 successful: {int(c)} == {int(obj)}')

    def test_multiple_linked_demands_3(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_1], [self.life_1, self.life_2], [self.el_line, self.el_line], [self.heat_line, self.heat_line],
                                       [self.cool_line, self.cool_line], [self.hydro_line, self.hydro_line])
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.add_district_heating(self.buy_heat)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_district_hydro(self.buy_hydro)

        self.ds.calc()

        obj = self.ds.res['Obj']
        c = (self.df[self.el_line].sum() * self.buy_el + self.df[self.heat_line].sum() * self.buy_heat - self.df[self.cool_line].sum() * self.buy_cool +
             self.df[self.hydro_line].sum() * self.buy_hydro) * self.lifetime + min(self.price_1 / self.life_1 * self.lifetime, self.price_1 / self.life_2 * self.lifetime)

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of multiple linked demands 3 successful: {int(c)} == {int(obj)}')

    def test_multiple_demands_just_electrical(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_1], [self.life_1, self.life_1], column_names_electrical=[self.heat_line, self.el_line])
        self.ds.add_electrical_grid(self.buy_el)
        self.ds.calc()

        obj = self.ds.res['Obj']

        c = (min(self.df[self.heat_line].sum(), self.df[self.el_line].sum()) * self.buy_el) * self.lifetime + self.price_1 / self.life_1 * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of multiple linked demands just electrical successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_just_heating(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_1], [self.life_1, self.life_1], column_names_heating=[self.heat_line, self.el_line])
        self.ds.add_district_heating(self.buy_heat)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        c = (min(self.df[self.heat_line].sum(), self.df[self.el_line].sum()) * self.buy_heat) * self.lifetime + self.price_1 / self.life_1 * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of multiple linked demands just heating successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_just_cooling(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_1], [self.life_1, self.life_1], column_names_cooling=[self.heat_line, self.el_line])
        self.ds.add_district_cooling(self.buy_cool)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        c = (min(self.df[self.heat_line].sum(), self.df[self.el_line].sum()) * self.buy_cool) * self.lifetime + self.price_1 / self.life_1 * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of multiple linked demands just cooling successful: {int(c)} == {int(obj)}')

    def test_multiple_demand_just_hydrogen(self):
        self.ds.add_linked_multiple_demands([self.price_1, self.price_1], [self.life_1, self.life_1], column_names_hydrogen=[self.heat_line, self.el_line])
        self.ds.add_district_hydro(self.buy_hydro)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        c = (min(self.df[self.heat_line].sum(), self.df[self.el_line].sum()) * self.buy_hydro) * self.lifetime + self.price_1 / self.life_1 * self.lifetime

        self.assertAlmostEqual(c, obj, delta=0.0001*abs(obj))
        print(f'Validation of multiple linked demands just hydrogen successful: {int(c)} == {int(obj)}')
