from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv

"""
validation script for the district cooling node and the cooling demands
"""


class TestNodeConnections(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    demand_line: str = 'Demand'
    demand_line_2: str = 'Electricity'
    reward: float = 0.08
    buy: float = 0.30
    price_lin = 200
    price_con = 500
    lifetime: int = 20
    eta_start_2_end = 0.96
    eta_end_2_start = 0.9

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0, self.lifetime, self.lifetime, 8760)

        # set solver
        self.ds.set_txt_solver()

    def test_electrical_node_with_reward(self):
        self.first_node = self.ds.add_electrical_node()
        self.second_node = self.ds.add_electrical_node()

        self.demand_data = self.ds.add_single_electrical_demand(self.demand_line)
        self.demand_data.set_node(self.second_node)

        self.external_connection = self.ds.add_electrical_grid(self.buy, self.reward)
        self.external_connection.set_node(self.first_node)

        self.node_connection = self.ds.add_inverter(self.price_lin, self.price_con)
        self.node_connection.set_nodes(self.first_node, self.second_node)
        self.node_connection.activate_bidirectional()
        self.node_connection.set_efficiencies(self.eta_start_2_end, self.eta_end_2_start)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line]
        costs = (demand.clip(0).sum() * self.buy / self.eta_start_2_end + demand.clip(None, 0).sum() * self.reward * self.eta_end_2_start
                 ) * self.lifetime + self.price_lin * max(max(demand) / self.eta_start_2_end, -1 * min(demand)) + self.price_con

        self.assertAlmostEqual(costs, obj, delta=0.00001*abs(obj))

        print(f'Validation of electrical node connection with reward successful: {costs:_.0f} == {obj:_.0f}')

    def test_standard_electrical_node(self):
        self.first_node = self.ds.add_electrical_node()
        self.second_node = self.ds.add_electrical_node()

        self.demand_data = self.ds.add_single_electrical_demand(self.demand_line_2)
        self.demand_data.set_node(self.second_node)

        self.external_connection = self.ds.add_electrical_grid(self.buy)
        self.external_connection.set_node(self.first_node)

        self.node_connection = self.ds.add_inverter(self.price_lin, self.price_con)
        self.node_connection.set_nodes(self.first_node, self.second_node)
        self.node_connection.set_efficiencies(self.eta_start_2_end)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line_2]
        costs = demand.sum() * self.buy / self.eta_start_2_end * self.lifetime + self.price_lin * max(demand) / self.eta_start_2_end + (self.price_con)

        self.assertAlmostEqual(costs, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrical node connection successful: {costs:_.0f} == {obj:_.0f}')

    def test_fuel_node_with_reward(self):
        demand_data = self.ds.add_single_fuel_demand(self.demand_line)
        external_connection = self.ds.add_fuel_connection(self.buy, self.reward)
        node_connection = self.ds.add_fuel_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_fuel_node()
        second_node = self.ds.add_fuel_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.activate_bidirectional()
        node_connection.set_efficiencies(self.eta_start_2_end, self.eta_end_2_start)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line]
        costs = (demand.clip(0).sum() * self.buy / self.eta_start_2_end + demand.clip(None, 0).sum() * self.reward * self.eta_end_2_start) * self.lifetime + self.price_lin * max(max(demand) / self.eta_start_2_end, -1 * min(demand)) + self.price_con

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))

        print(f'Validation of fuel node connection with reward successful: {costs:_.0f} == {obj:_.0f}')

    def test_standard_fuel_node(self):
        demand_data = self.ds.add_single_fuel_demand(self.demand_line_2)
        external_connection = self.ds.add_fuel_connection(self.buy)
        node_connection = self.ds.add_fuel_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_fuel_node()
        second_node = self.ds.add_fuel_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.set_efficiencies(self.eta_start_2_end)

        self.ds.calc()

        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line_2]
        costs = demand.sum() * self.buy / self.eta_start_2_end * self.lifetime + self.price_lin * max(demand) / self.eta_start_2_end + (self.price_con)

        self.assertAlmostEqual(costs, obj, delta=0.00001*abs(obj))
        print(f'Validation of fuel node connection successful: {costs:_.0f} == {obj:_.0f}')

    def test_hydrogen_node_with_reward(self):
        demand_data = self.ds.add_single_hydrogen_demand(self.demand_line)
        external_connection = self.ds.add_district_hydro(self.buy, self.reward)
        node_connection = self.ds.add_hydrogen_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_hydrogen_node()
        second_node = self.ds.add_hydrogen_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.activate_bidirectional()
        node_connection.set_efficiencies(self.eta_start_2_end, self.eta_end_2_start)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line]
        costs = (demand.clip(0).sum() * self.buy / self.eta_start_2_end + demand.clip(None,0).sum() * self.reward * self.eta_end_2_start) * self.lifetime + self.price_lin * max(
            max(demand) / self.eta_start_2_end, -1 * min(demand)) + self.price_con

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))
        print(f'Validation of hydrogen node connection with reward successful: {costs:_.0f} == {obj:_.0f}')

    def test_standard_hydrogen_node(self):
        demand_data = self.ds.add_single_hydrogen_demand(self.demand_line_2)
        external_connection = self.ds.add_district_hydro(self.buy)
        node_connection = self.ds.add_hydrogen_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_hydrogen_node()
        second_node = self.ds.add_hydrogen_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.set_efficiencies(self.eta_start_2_end)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line_2]
        costs = demand.sum() * self.buy / self.eta_start_2_end * self.lifetime + self.price_lin * max(demand) / self.eta_start_2_end + (self.price_con)

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))
        print(f'Validation of hydrogen node connection successful: {costs:_.0f} == {obj:_.0f}')

    def test_heating_node_with_reward(self):
        demand_data = self.ds.add_single_heating_demand(self.demand_line)
        external_connection = self.ds.add_district_heating(self.buy, self.reward)
        node_connection = self.ds.add_heating_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_heating_node()
        second_node = self.ds.add_heating_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.activate_bidirectional()
        node_connection.set_efficiencies(self.eta_start_2_end,self. eta_end_2_start)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line]
        costs = (demand.clip(0).sum() * self.buy / self.eta_start_2_end + demand.clip(None,0).sum() * self.reward * self.eta_end_2_start) * self.lifetime + self.price_lin * max(
            max(demand) / self.eta_start_2_end, -1 * min(demand)) + self.price_con

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))
        print(f'Validation of hydrogen node connection successful: {costs:_.0f} == {obj:_.0f}')

    def test_standard_heating_node(self):
        demand_data = self.ds.add_single_heating_demand(self.demand_line_2)
        external_connection = self.ds.add_district_heating(self.buy)
        node_connection = self.ds.add_heating_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_heating_node()
        second_node = self.ds.add_heating_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.set_efficiencies(self.eta_start_2_end)

        self.ds.calc()

        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line_2]
        costs = demand.sum() * self.buy / self.eta_start_2_end * self.lifetime + self.price_lin * max(demand) / self.eta_start_2_end + (self.price_con)

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))

        print(f'Validation of heating node connection successful: {costs:_.0f} == {obj:_.0f}')

    def test_cooling_node_with_reward(self):
        demand_data = self.ds.add_single_cooling_demand(self.demand_line)
        external_connection = self.ds.add_district_cooling(self.buy, self.reward)
        node_connection = self.ds.add_cooling_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_cooling_node()
        second_node = self.ds.add_cooling_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.activate_bidirectional()
        node_connection.set_efficiencies(self.eta_start_2_end, self.eta_end_2_start)

        self.ds.calc()

        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line]
        costs = (demand.clip(0).sum() * self.buy / self.eta_start_2_end + demand.clip(None,0).sum() * self.reward * self.eta_end_2_start) * self.lifetime + self.price_lin * max(
            max(demand) / self.eta_start_2_end, -1 * min(demand)) + self.price_con

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))

        print(f'Validation of cooling node connection with reward successful: {costs:_.0f} == {obj:_.0f}')

    def test_standard_cooling_node(self):
        demand_data = self.ds.add_single_cooling_demand(self.demand_line_2)
        external_connection = self.ds.add_district_cooling(self.buy)
        node_connection = self.ds.add_cooling_pipe(self.price_lin, self.price_con)
        first_node = self.ds.add_cooling_node()
        second_node = self.ds.add_cooling_node()
        demand_data.set_node(second_node)
        external_connection.set_node(first_node)
        node_connection.set_nodes(first_node, second_node)
        node_connection.set_efficiencies(self.eta_start_2_end)

        self.ds.calc()

        obj = self.ds.res['Obj']

        demand = self.df[self.demand_line_2]
        costs = demand.sum() * self.buy / self.eta_start_2_end * self.lifetime + self.price_lin * max(demand) / self.eta_start_2_end + (self.price_con)

        self.assertAlmostEqual(costs, obj, delta=0.00001 * abs(obj))

        print(f'Validation of cooling node connection successful: {costs:_.0f} == {obj:_.0f}')