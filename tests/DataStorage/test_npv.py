from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv
from numpy import ones, zeros

"""
validation script for the npv
"""


class TestNPV(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    el_line: str = 'Electricity'
    pv_line: str = 'PV'
    buy_el: float = 0.25
    lifetime: int = 60
    life_pv: int = 20
    rate_of_interest: float = 0.05
    max_pv: float = 100_000.
    sell_pv: float = 0.15
    price_lin: float = 250.
    price_con: float = 2500.
    price_1: float = 1000_000.
    price_2: float = 555_000.
    life_1: int = 20
    life_2: int = 10
    maintenance: float = 0.01

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, self.rate_of_interest*100, self.lifetime, self.lifetime, 8760)
        self.ds.add_electrical_grid(self.buy_el)

        # set solver
        self.ds.set_txt_solver()

    def calc_buy_electricity(self):
        buy_el_arr = ones(self.lifetime) * self.buy_el
        buy_el_npv = 0
        for idx, b in enumerate(buy_el_arr):
            buy_el_npv += b / ((1 + self.rate_of_interest) ** idx)

        return buy_el_npv

    def calc_price_npv(self):
        price_1_arr = zeros(self.lifetime)
        price_1_arr[0:int(self.lifetime):int(self.life_1)] = self.price_1
        price_1_npv = 0
        for idx, p in enumerate(price_1_arr):
            price_1_npv += p / ((1 + self.rate_of_interest) ** idx)

        return price_1_npv

    def test_standard_constant_price_without_pv(self):
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        buy_el_npv = self.calc_buy_electricity()

        # Calculate total costs manually
        c = self.df[self.el_line].sum() * buy_el_npv

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))

        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')

    def test_standard_constant_price(self):
        self.ds.add_single_electrical_demand(self.el_line)

        pv_data = self.ds.add_photovoltaic(self.price_lin, self.price_con, self.maintenance * 100, self.life_pv)
        pv_data.produced_power.get_power_from_data(self.pv_line, 1 / 1000)
        pv_data.selling_power.set_constant_selling_price(self.sell_pv)
        pv_data.set_max(self.max_pv)

        self.ds.calc()

        # get total costs result
        obj = self.ds.res['Obj']

        sell_pv_arr = ones(self.lifetime) * self.sell_pv
        sell_pv_npv = 0
        for idx, s in enumerate(sell_pv_arr):
            sell_pv_npv += s / ((1 + self.rate_of_interest) ** idx)

        price_lin_arr = zeros(self.lifetime)
        price_lin_arr[0:int(self.lifetime):int(self.life_pv)] = self.price_lin
        # add maintenance costs
        price_lin_arr[:] += self.maintenance * self.price_lin
        price_lin_npv = 0
        for idx, p in enumerate(price_lin_arr):
            price_lin_npv += p / ((1 + self.rate_of_interest) ** idx)

        price_con_arr = zeros(self.lifetime)
        price_con_arr[0:int(self.lifetime):int(self.life_pv)] = self.price_con
        # add maintenance costs
        price_con_arr[:] += self.maintenance * self.price_con
        price_con_npv = 0
        for idx, p in enumerate(price_con_arr):
            price_con_npv += p / ((1 + self.rate_of_interest) ** idx)

        demand = self.df[self.el_line]
        buy_el_npv = self.calc_buy_electricity()
        e_diff = demand - self.df[self.pv_line] * self.max_pv / 1000
        c_buy = e_diff.where(e_diff > 0).sum() * buy_el_npv
        c_sell = e_diff.where(e_diff < 0).sum() * sell_pv_npv
        c = c_buy + c_sell + self.max_pv * price_lin_npv + price_con_npv

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')

    def test_multiple_electrical_demand(self):
        self.ds.add_multiple_electrical_demand([self.el_line, self.el_line], [self.price_1, self.price_2], [self.life_1, self.life_1])

        self.ds.calc()

        price_1_npv = self.calc_price_npv()

        price_2_arr = zeros(self.lifetime)
        price_2_arr[0:int(self.lifetime):int(self.life_1)] = self.price_2
        price_2_npv = 0
        for idx, p in enumerate(price_2_arr):
            price_2_npv += p / ((1 + self.rate_of_interest) ** idx)

        # get total costs result
        obj = self.ds.res['Obj']

        buy_el_npv = self.calc_buy_electricity()
        # Calculate total costs manually
        c = self.df[self.el_line].sum() * buy_el_npv + min(price_1_npv, price_2_npv)

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')

    def test_multiple_electrical_demand_same_price(self):
        self.ds.add_multiple_electrical_demand([self.el_line, self.el_line], [self.price_1, self.price_1], [self.life_1, self.life_2])

        self.ds.calc()

        price_2_arr = zeros(self.lifetime)
        price_2_arr[0:int(self.lifetime):int(self.life_2)] = self.price_1
        price_2_npv = 0
        for idx, p in enumerate(price_2_arr):
            price_2_npv += p / ((1 + self.rate_of_interest) ** idx)

        # get total costs result
        obj = self.ds.res['Obj']

        buy_el_npv = self.calc_buy_electricity()
        price_1_npv = self.calc_price_npv()
        # Calculate total costs manually
        c = self.df[self.el_line].sum() * buy_el_npv + min(price_1_npv, price_2_npv)

        self.assertAlmostEqual(c, obj, delta=0.00001*abs(obj))
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
