from btso.DataStorage import DataStorage
from btso import FOLDER
from unittest import TestCase
from pandas import read_csv
from scipy import optimize
from numpy import ones, ndarray

"""
validation script for the photovoltaic
"""


class TestPV(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    pv_line: str = 'PV'
    el_line: str = 'Electricity'
    sell_line: str = 'Sell'
    buy_line: str = 'Buy'
    max_pv: float = 100_000.
    sell_pv: float = 0.063
    buy_el: float = 0.18
    price_lin: float = 221.
    power_frac: float = 0.001
    lifetime: int = 20

    ds = DataStorage()
    df = None
    demand = None

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)

        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.el_line] + self.power_frac * self.max_pv * 2

    def test_standard_and_multiple_panels(self):
        pv_data = self.ds.add_photovoltaic(self.price_lin, 0, 0, self.lifetime)
        pv_data.produced_power.get_power_from_data(self.pv_line, 0.001)
        pv_data.selling_power.set_constant_selling_price(self.sell_pv)
        pv_data.set_max(self.max_pv)
        pv_data.set_electrical_demand(self.power_frac)
        pv_data2 = self.ds.add_photovoltaic(self.price_lin, 0, 0, self.lifetime)
        pv_data2.produced_power.get_power_from_data(self.pv_line, 0.001)
        pv_data2.selling_power.set_constant_selling_price(self.sell_pv)
        pv_data2.set_max(self.max_pv)
        pv_data2.set_electrical_demand(self.power_frac)
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        e_diff = self.demand - self.df[self.pv_line] * self.max_pv * 2 / 1000
        c_buy = e_diff.where(e_diff > 0).sum() * self.lifetime * self.buy_el
        c_sell = e_diff.where(e_diff < 0).sum() * self.lifetime * self.sell_pv
        c = c_buy + c_sell + self.max_pv * 2 * self.price_lin
        obj = self.ds.res['Obj']
        c_sell = self.price_lin / sum(self.df[self.pv_line] - self.power_frac * 1000) / self.lifetime * 1000
        self.assertAlmostEqual(pv_data.size.value, self.max_pv, delta=self.max_pv * 0.000001)
        self.assertAlmostEqual(pv_data2.size.value, self.max_pv, delta=self.max_pv * 0.000001)
        self.assertAlmostEqual(c, obj, delta=abs(obj) * 0.000001)
        self.assertLessEqual(c_sell, self.sell_pv)

        print(
            f'Validation of PV with constant selling successful: {c_sell} < {self.sell_pv} and {int(c)} == {int(obj)}')

    def test_standard_without_sell_price(self):
        self.setUpClass()
        sell_pv_neu = 0
        pv_data = self.ds.add_photovoltaic(self.price_lin, 0, 0, self.lifetime)
        pv_data.produced_power.get_power_from_data(self.pv_line, 1 / 1000)
        pv_data.set_max(self.max_pv)
        pv_data.selling_power.set_constant_selling_price(sell_pv_neu)
        self.ds.add_single_electrical_demand(self.el_line)
        el_grid = self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs, ones(1), method='SLSQP')
        c = res.fun

        obj = int(self.ds.res["Obj"])
        self.assertAlmostEqual(c, obj, delta=abs(obj)*0.0001)
        print(f'Validation of PV without selling successful : {int(obj)} == {int(c)}')

    def calc_costs(self, size: float) -> float:
        e_diff_f = self.df[self.el_line] - self.df[self.pv_line] * size / 1000
        return e_diff_f.where(e_diff_f > 0).sum() * self.lifetime * self.buy_el + size * self.price_lin

    def test_flexible_price(self):
        pv_data = self.ds.add_photovoltaic(self.price_lin, 0, 0, self.lifetime)
        pv_data.produced_power.get_power_from_data(self.pv_line, 1 / 1000)
        pv_data.set_max(self.max_pv)
        pv_data.selling_power.set_flexible_selling_price(self.sell_line)
        el_grid = self.ds.add_electrical_grid(self.buy_el)
        el_grid.set_flexible_price(self.buy_line)
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.calc()

        e_diff = self.df[self.el_line] - self.df[self.pv_line] * self.max_pv / 1000
        e_buy = e_diff * self.df[self.buy_line]
        e_sell = e_diff * self.df[self.sell_line]
        e_sell_2 = e_sell.where(e_diff < 0)
        c_buy = e_buy.where(e_diff >= 0).sum() * self.lifetime
        c_sell = e_sell_2.where(self.df[self.sell_line] > 0).sum() * self.lifetime
        c = c_buy + c_sell + self.max_pv * self.price_lin
        obj = self.ds.res['Obj']

        self.assertAlmostEqual(pv_data.size.value, self.max_pv, delta=0.0000001*self.max_pv)
        self.assertAlmostEqual(c, obj, delta=abs(c)*0.0000001)
        print(f'Validation of PV with flexible selling successful: {int(c)} == {int(obj)}')