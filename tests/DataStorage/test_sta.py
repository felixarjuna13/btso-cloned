from btso.DataStorage import DataStorage
from btso import FOLDER
from pandas import read_csv
from unittest import TestCase
from scipy import optimize
from numpy import ones, ndarray

"""
validation script for the solar thermal absorber
"""


class TestSTA(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    power_warm_line: str = 'STAPowerHot'
    power_cold_line: str = 'STAPowerCold'
    heat_line: str = 'Heating'
    cool_line: str = 'Cooling'
    buy_el: float = 0.18
    buy_cool: float = 0.11
    price_lin: float = 200.
    price_lin_cold: float = 2.
    price_con: float = 50.
    lifetime: int = 20

    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)

        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.heat_line]

    def test_heating_warm_side(self):
        sta_data = self.ds.add_solar_thermal_absorber(self.price_lin, self.price_con, 0, self.lifetime)
        sta_data.get_power_from_data(self.power_warm_line)
        sta_data.set_connections(True, False, False, False)
        eh_data = self.ds.add_electrical_heater(0, 0, 0, self.lifetime)
        eh_data.set_efficiency(1)
        self.ds.add_single_heating_demand(self.heat_line)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs_warm_side, ones(1), method='SLSQP')
        c = res.fun
        obj = self.ds.res['Obj']

        self.assertAlmostEqual(obj, c, delta=0.0001 * abs(obj))
        print(f'Validation of solar thermal absorber heating warm side successful : {int(obj)} == {int(c)}')

    def calc_costs_warm_side(self, size: float) -> float:
        q_sta = self.df[self.power_warm_line].clip(0) / 1000
        e_diff = self.demand - q_sta * size
        return e_diff.where(e_diff > 0).sum() * self.lifetime * self.buy_el + size * self.price_lin + self.price_con

    def test_cooling_heat_side(self):
        sta_data = self.ds.add_solar_thermal_collector(self.price_lin_cold, 0, 0, self.lifetime)
        sta_data.get_power_from_data(self.power_warm_line, self.power_cold_line)
        sta_data.set_connections(False, False, False, True)
        self.ds.add_single_cooling_demand(self.cool_line)
        self.ds.add_district_cooling(self.buy_cool)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs_cold_side, ones(1), method='SLSQP', bounds=((0, 20_000_000),))
        c = res.fun
        obj = self.ds.res['Obj']

        self.assertAlmostEqual(obj, c, delta=0.0001 * abs(obj))

        print(f'Validation of solar thermal collector absorber cold side successful : {int(obj)} == {int(c)}')

    def calc_costs_cold_side(self, size: ndarray) -> float:
        demand = self.df[self.cool_line] * -1
        q_sta = (self.df[self.power_cold_line] * -1).clip(0) / 1000
        e_diff = demand - q_sta * size[0]
        return e_diff.where(e_diff > 0).sum() * self.lifetime * self.buy_cool + size * self.price_lin_cold
