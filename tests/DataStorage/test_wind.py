from btso.DataStorage import DataStorage
from btso import FOLDER
from pandas import read_csv
from unittest import TestCase
from scipy import optimize
from numpy import ones

"""
validation script for the wind turbine
"""


class TestWind(TestCase):
    # read data from csv file
    data = f'{FOLDER}/data/data.csv'
    wind_line: str = 'WindPower'
    sell_line: str = 'Sell'
    buy_line: str = 'Buy'
    el_line: str = 'Electricity'
    max_wind: float = 100_000.
    sell_wind: float = 0.06
    buy_el: float = 0.18
    price_lin: float = 1900.
    lifetime: int = 20

    df = None
    demand = None
    ds = DataStorage()

    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        print('\n')
        self.df = read_csv(self.data)
        self.ds = DataStorage()
        self.ds.set_file_rate_time_steps(self.data, 0.0, self.lifetime, self.lifetime, 8760)
        # set solver
        self.ds.set_txt_solver()

        self.demand = self.df[self.el_line]

    def test_standard_and_multiple_turbines(self):
        wind_data = self.ds.add_wind_turbine(self.price_lin * 1_000, 0, 0, self.lifetime)
        wind_data.produced_power.get_power_from_data(self.wind_line)
        wind_data.selling_power.set_constant_selling_price(self.sell_wind)
        wind_data.set_max(self.max_wind)
        wind_data2 = self.ds.add_wind_turbine(self.price_lin, 0, 0, self.lifetime)
        wind_data2.produced_power.get_power_from_data(self.wind_line)
        wind_data2.selling_power.set_constant_selling_price(self.sell_wind)
        wind_data2.set_max(self.max_wind)
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.add_electrical_grid(self.buy_el)
        # set solver
        self.ds.set_txt_solver()

        self.ds.calc()

        e_diff = self.df[self.el_line] - self.df[self.wind_line]*self.max_wind
        c_buy = e_diff.where(e_diff > 0).sum()*self.lifetime*self.buy_el
        c_sell = e_diff.where(e_diff < 0).sum()*self.lifetime*self.sell_wind
        c = c_buy + c_sell + self.max_wind * self.price_lin
        obj = self.ds.res['Obj']
        c_sell = self.price_lin / sum(self.df[self.wind_line]) / self.lifetime
        self.assertAlmostEqual(wind_data2.size.value, self.max_wind, delta=self.max_wind*0.000001)
        self.assertAlmostEqual(c, obj, delta=abs(obj) * 0.000001)
        self.assertLessEqual(c_sell, self.sell_wind)

        print(f'Validation of Wind with constant selling successful: {c_sell} < {self.sell_wind} and {int(c)} == {int(obj)}')

    def test_standard_without_sell_price(self):
        wind_data = self.ds.add_wind_turbine(self.price_lin, 0, 0, self.lifetime)
        wind_data.produced_power.get_power_from_data(self.wind_line)
        wind_data.set_max(self.max_wind)
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.add_electrical_grid(self.buy_el)

        self.ds.calc()

        res = optimize.minimize(self.calc_costs, ones(1), method='SLSQP', bounds=((0, 100_000),))
        c = res.fun

        obj = int(self.ds.res["Obj"])
        self.assertAlmostEqual(wind_data.size.value[0], res.x, delta=0.000001*res.x)
        self.assertAlmostEqual(c, obj, delta=0.0000001 * obj)
        print(f'Validation of wind without selling successful : {int(obj)} == {int(c)}')

    def calc_costs(self, size: float) -> float:
        e_diff_f = self.df[self.el_line] - self.df[self.wind_line] * size
        return e_diff_f.where(e_diff_f > 0).sum()*self.lifetime*self.buy_el+size*self.price_lin

    def test_flexible_price(self):
        wind_data = self.ds.add_wind_turbine(self.price_lin, 0, 0, self.lifetime)
        wind_data.produced_power.get_power_from_data(self.wind_line)
        wind_data.set_max(self.max_wind)
        wind_data.selling_power.set_flexible_selling_price(self.sell_line)
        self.ds.add_single_electrical_demand(self.el_line)
        self.ds.add_electrical_grid(self.buy_line)
        self.ds.calc()

        e_diff = self.df[self.el_line] - self.df[self.wind_line] * self.max_wind
        e_buy = e_diff * self.df[self.buy_line]
        e_sell = e_diff * self.df[self.sell_line]
        e_sell_2 = e_sell.where(e_diff < 0)
        c_buy = e_buy.where(e_diff >= 0).sum() * self.lifetime
        c_sell = e_sell_2.where(self.df[self.sell_line] > 0).sum() * self.lifetime
        c = c_buy + c_sell + self.max_wind * self.price_lin
        obj = self.ds.res['Obj']
        self.assertAlmostEqual(wind_data.size.value[0], self.max_wind, delta=0.0000001 * self.max_wind)
        self.assertAlmostEqual(c, obj, delta=abs(c) * 0.0000001)
        print(f'Validation of wind with flexible selling successful: {int(c)} == {int(obj)}')


if __name__ == '__main__':
    wind = TestWind()
    wind.test_standard_and_multiple_turbines()
    wind.test_standard_without_sell_price()
    wind.test_flexible_price()
