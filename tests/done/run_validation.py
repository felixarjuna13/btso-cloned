import os
from btso.DataStorage import DataStorage
from pickle import dump, HIGHEST_PROTOCOL


def main():
    """
    lifetime = 20
    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.calculate_typical_periods(15, 24, 4, 2, ['Heating', 'Cooling', 'Electricity', 'TAmb'], ['TAmb'], solver='gurobi')
    ds.calc()

    with open("typical_periods.pkl", "wb") as f:
        dump(ds.tp_data, f, HIGHEST_PROTOCOL)
        print('Export successful')
    """
    for solver in ['gurobi']:#, 'glpk']:
        with open('solver.txt', 'w') as f:
            f.write(solver)
        for filename in [f for f in os.listdir("") if f != os.path.basename(__file__)]:
            if filename.endswith(".py"):
                res = os.system(filename)
                if res > 0:
                    break


if __name__ == "__main__":
    main()
