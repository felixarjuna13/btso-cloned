from src.btso.DataStorage import DataStorage, Storage
from math import isclose
from pandas import read_csv, DataFrame
from scipy import optimize
from numpy import ones, array, zeros
from typing import Callable
from copy import deepcopy


def main():
    df: DataFrame = read_csv('../../src/btso/data/data.csv')
    stc_line: str = 'STAPowerCold'
    cool_line: str = 'Cooling'
    max_stc: float = 100_000.
    sell_stc: float = 0.06
    buy_cool: float = 0.18
    eta_self: float = 0.01
    eta_charge: float = 0.97
    eta_discharge: float = 0.95
    price_lin_stc: float = 200.
    price_con_stc: float = 2000.
    price_lin_ct: float = 5.
    price_con_ct: float = 350.
    lifetime: int = 20
    start_val: float = 0.2

    ds: DataStorage = DataStorage()
    ds.set_file_rate_time_steps('../../src/btso/data/data.csv', 0.0, lifetime, lifetime, 8760)

    stc_data = ds.add_solar_thermal_collector(price_lin_stc, price_con_stc, 0, lifetime)
    stc_data.get_power_from_data(stc_line, stc_line)
    stc_data.set_connections(False, False, False, True)
    stc_data.set_max(max_stc)
    cold_tank_data = ds.add_cold_tank(price_lin_ct, price_con_ct, 0, lifetime)
    cold_tank_data.set_values(eta_self * 100, eta_charge * 100, eta_discharge * 100)
    cold_tank_data.set_start(start_val)
    ds.set_de_model(2)
    cool_demand = ds.add_single_cooling_demand(cool_line, -1)
    district_cooling = ds.add_district_cooling(buy_cool, sell_stc)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    if not isclose(stc_data.size.value, max_stc, rel_tol=0.0001):
        print(f'Validation cold tank system failed: {stc_data.size.value} != {max_stc}')
        raise AssertionError

    demand = -1 * df[cool_line]
    p_stc = (-1 * df[stc_line]).clip(0) * max_stc / 1000
    len_ye = len(demand)
    ran_yr: range = range(len_ye)

    def calc_costs_backward(size_bat: array, eta_self_internal: float):
        size_bat = size_bat[0]
        charge_status = start_val * size_bat
        e_sell = 0
        e_buy = 0
        for p, d in zip(p_stc, demand):
            charge_status = charge_status * (1 - eta_self_internal)
            bat_status_old = charge_status
            if p > d:
                charge_status = min(charge_status + (p - d) * eta_charge, size_bat)
                e_sell += max(p - d - (charge_status - bat_status_old) / eta_charge, 0)
                continue
            charge_status = max(charge_status + (p - d) / eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - charge_status) * eta_discharge, 0)

        return price_lin_stc * max_stc + price_con_stc + price_lin_ct * size_bat + price_con_ct + (
                e_buy * buy_cool - e_sell * sell_stc) * lifetime

    def calc_costs_backward_with_self_discharge(size_bat: array):
        return calc_costs_backward(size_bat, eta_self)

    def calc_costs_backward_without_self_discharge(size_bat: array):
        return calc_costs_backward(size_bat, 0)

    def calc_costs_forward(size_bat: float, eta_self_internal: float):
        charge_status = start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in ran_yr:
            p = p_stc[t - 1] if t > 0 else 0
            d = demand[t - 1] if t > 0 else 0
            charge_status = charge_status * (1 - eta_self_internal)
            bat_status_old = charge_status
            if p > d:
                charge_status = min(charge_status + (p - d) * eta_charge, size_bat)
                e_sell += max(p - d - (charge_status - bat_status_old) / eta_charge, 0)
                continue
            charge_status = max(charge_status + (p - d) / eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - charge_status) * eta_discharge, 0)

        return price_lin_stc * max_stc + price_con_stc + price_lin_ct * size_bat + price_con_ct + (
                e_buy * buy_cool - e_sell * sell_stc) * lifetime

    def calc_costs_forward_with_self_discharge(size_bat: array):
        return calc_costs_forward(size_bat, eta_self)

    def calc_costs_forward_without_self_discharge(size_bat: array):
        return calc_costs_forward(size_bat, 0)

    def calc_costs_center(size_bat: float, eta_self_internal: float):
        charge_status = start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in ran_yr:
            p = p_stc[t - 1] / 2 + p_stc[t] / 2 if t > 0 else p_stc[t] / 2
            d = demand[t - 1] / 2 + demand[t] / 2 if t > 0 else demand[t] / 2
            charge_status = charge_status * (1 - eta_self_internal)
            bat_status_old = charge_status
            if p > d:
                charge_status = min(charge_status + (p - d) * eta_charge, size_bat)
                e_sell += max(p - d - (charge_status - bat_status_old) / eta_charge, 0)
                continue
            charge_status = max(charge_status + (p - d) / eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - charge_status) * eta_discharge, 0)

        return price_lin_stc * max_stc + price_con_stc + price_lin_ct * size_bat + price_con_ct + (
                e_buy * buy_cool - e_sell * sell_stc) * lifetime

    def calc_costs_center_with_self_discharge(size_bat: array):
        return calc_costs_center(size_bat, eta_self)

    def calc_costs_center_without_self_discharge(size_bat: array):
        return calc_costs_center(size_bat, 0)

    def check_backward(storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        charge_status = zeros(8760)
        charge_status[0] = start_value * size_bat * (1 - eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(8760):
            charge_status[t] = charge_status[0] if t == 0 else charge_status[t - 1] * (1 - eta_self)
            bat_status_old = charge_status[t]
            p = p_stc[t]
            d = demand[t]
            charge_status[t] = max(min(charge_status[t] + storage.power_charge.value[t] * eta_charge -
                                       storage.power_discharge.value[t] / eta_discharge, size_bat), 0)
            p_sell[t] = max(p - d - (storage.power_charge.value[t] - storage.power_discharge.value[t]), 0)
            p_buy[t] = max(d - p + (storage.power_charge.value[t] - storage.power_discharge.value[t]), 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_forward(storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        charge_status = zeros(8760)
        charge_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[8759] * (1 - eta_self)
        charge_status[0] = charge_status[0] + (storage.power_charge.value[-1] * eta_charge - 
                                               storage.power_discharge.value[-1] / eta_discharge)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(1, 8760):
            charge_status[t] = charge_status[t - 1] * (1 - eta_self)
            bat_status_old = charge_status[t]
            p = p_stc[t - 1]
            d = demand[t - 1]
            if p > d:
                charge_status[t] = min(charge_status[t] + storage.power_charge.value[t - 1] * eta_charge, size_bat)
                p_sell[t - 1] = max(p - d - (charge_status[t] - bat_status_old) / eta_charge, 0)
            else:
                charge_status[t] = max(charge_status[t] - storage.power_discharge.value[t - 1] / eta_discharge, 0)
                p_buy[t - 1] = max(d - p - (bat_status_old - charge_status[t]) * eta_discharge, 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_central(storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        charge_status = zeros(8760)
        charge_status[0] = start_value * size_bat * (1 - eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        p = (p_stc[0])
        d = (demand[0])
        charge_status[0] = max(min(charge_status[0] + (storage.power_charge.value[0]) / 2 * eta_charge -
                                   (storage.power_discharge.value[0]) / 2 / eta_discharge, size_bat), 0)
        p_sell[0] = max(p - d - storage.power_charge.value[0] + storage.power_discharge.value[0], 0)
        p_buy[0] += max(d - p - storage.power_discharge.value[0] + storage.power_charge.value[0], 0)
        charge_status[0] = max(min(charge_status[0] + (storage.power_charge.value[-1]) / 2 * eta_charge -
                                   (storage.power_discharge.value[-1]) / 2 / eta_discharge, size_bat), 0)
        assert isclose(round(charge_status[0], 2), round(storage.energy.value[0], 2), rel_tol=0.0001)
        assert isclose(round(p_buy[0], 2), round(buy[0], 2), rel_tol=0.0001)
        assert isclose(round(p_sell[0], 2), round(sell[0], 2), rel_tol=0.0001)
        for t in range(1, 8760):
            charge_status[t] = charge_status[t - 1] * (1 - eta_self)
            p = p_stc[t]
            d = demand[t]
            charge_status[t] = max(min(charge_status[t] +
                                       (storage.power_charge.value[t] + storage.power_charge.value[t - 1]) / 2 * eta_charge
                                       - (storage.power_discharge.value[t] + storage.power_discharge.value[t - 1]) / 2 /
                                       eta_discharge, size_bat), 0)

            p_sell[t] = max(p - d - storage.power_charge.value[t] + storage.power_discharge.value[t], 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_fail(funct: Callable, storage: Storage, buy: array, sell: array, start_value: float = None):
        try:
            funct(storage, buy, sell, start_value)
        except AssertionError:
            return
        raise AssertionError

    check_backward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)

    res_with = optimize.minimize(calc_costs_backward_with_self_discharge, ones(1), method='SLSQP', bounds=[[0, 100_000]])
    c_calc_with = res_with.fun
    res_without = optimize.minimize(calc_costs_backward_without_self_discharge, ones(1), method='SLSQP',
                                    bounds=[[0, 100_000]])
    c_calc_without = res_without.fun
    c_opt = ds.res['Obj']

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of cold tank size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
              f' <= {int(c_calc_with)}')
    else:
        print(f'Validation of cold tank size with backward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    cold_tank_data.start = False
    ds.calc()
    c_opt = ds.res['Obj']

    check_backward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of cold tank size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
              f' <= {int(c_calc_with)}')
    else:
        print(f'Validation of cold tank size with backward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    cold_tank_data.set_start(start_val)
    cold_tank_data.set_max(100_000)
    ds.set_de_model(1)
    ds.calc()
    check_forward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    res_with = optimize.minimize(calc_costs_forward_with_self_discharge, ones(1), method='SLSQP',
                                 bounds=[[0, 100_000]])
    c_calc_with = res_with.fun
    res_without = optimize.minimize(calc_costs_forward_without_self_discharge, ones(1), method='SLSQP',
                                    bounds=[[0, 100_000]])
    c_calc_without = res_without.fun
    c_opt = ds.res['Obj']

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of cold tank size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
              f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of cold tank size with forward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    cold_tank_data.start = False
    ds.calc()
    c_opt = ds.res['Obj']

    check_forward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)

    if c_calc_without <= c_opt <= c_calc_with:
        print(
            f'Validation of cold tank size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of cold tank size with forward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    cold_tank_data.set_start(start_val)
    ds.set_de_model(3)
    ds.calc()
    check_central(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    res_with = optimize.minimize(calc_costs_center_with_self_discharge, ones(1), method='SLSQP',
                                 bounds=[[0, 100_000]])
    c_calc_with = res_with.fun
    res_without = optimize.minimize(calc_costs_center_without_self_discharge, ones(1), method='SLSQP',
                                    bounds=[[0, 100_000]])
    c_calc_without = res_without.fun
    c_opt = ds.res['Obj']

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of cold tank size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
              f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of cold tank size with central euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    cold_tank_data.start = False
    ds.calc()
    c_opt = ds.res['Obj']

    check_central(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)

    if c_calc_without <= c_opt <= c_calc_with:
        print(
            f'Validation of cold tank size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of cold tank size with central euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    cold_tank_data.start = False
    ds.import_typical_periods('../typical_periods.pkl')
    ds.set_de_model(1)
    ds.calc()

    def check_tp_forward(storage: Storage, buy: array, sell: array, start_value: float = None):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        ran_end = range(ds.tp_data.hours_per_period - 1, length, ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_end = storage.energy.value[j] * (1 - eta_self) - \
                        storage.power_discharge.value[j] / storage.eta_discharge + \
                        storage.power_charge.value[j] * storage.eta_charge
            assert isclose(round(storage.energy.value[i], 2), round(power_end, 2), rel_tol=0.00001)
        size_bat = storage.size.value
        charge_status = zeros(length)
        charge_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            charge_status[t] = charge_status[t - 1] * (1 - eta_self)
            p = max(-1 * stc_data.power_cold[t - 1], 0) / 1000. * stc_data.size.value
            d = cool_demand.demand[t - 1]
            charge_status[t] = max(charge_status[t] - storage.power_discharge.value[t - 1] / eta_discharge +
                                   storage.power_charge.value[t - 1] * eta_charge, 0)
            p_buy[t - 1] = max(d - p - storage.power_discharge.value[t - 1] + storage.power_charge.value[t - 1], 0)
            p_sell[t - 1] = max(p - d + storage.power_discharge.value[t - 1] - storage.power_charge.value[t - 1], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_tp_backward(storage: Storage, buy: array, sell: array, start_value: float = None):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        ran_end = range(ds.tp_data.hours_per_period - 1, length, ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] /
                           storage.eta_discharge - storage.power_charge.value[i] * storage.eta_charge)
            assert isclose(power_start, storage.energy.value[j] * (1 - eta_self), rel_tol=0.0001)
        size_bat = storage.size.value
        charge_status = zeros(length)
        charge_status[0] = (start_value * size_bat - storage.power_discharge.value[0] /
                         storage.eta_discharge + storage.power_charge.value[0] * storage.eta_charge) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            charge_status[t] = charge_status[t - 1] * (1 - eta_self)
            p = max(-1 * stc_data.power_cold[t], 0) / 1000. * stc_data.size.value
            d = cool_demand.demand[t]
            charge_status[t] = max(charge_status[t] - storage.power_discharge.value[t] / eta_discharge +
                                   storage.power_charge.value[t] * eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_tp_central(storage: Storage, buy: array, sell: array, start_value: float = None):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        ran_end = range(ds.tp_data.hours_per_period - 1, length, ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] / storage.eta_discharge / 2
                           - storage.power_charge.value[i] * storage.eta_charge / 2)
            power_end = (storage.energy.value[j] * (1 - eta_self) - storage.power_discharge.value[j] /
                         storage.eta_discharge / 2 + storage.power_charge.value[j] * storage.eta_charge / 2)
            assert isclose(round(power_start, 2), round(power_end, 2), rel_tol=0.0001)
        size_bat = storage.size.value
        charge_status = zeros(length)
        charge_status[0] = (start_value * size_bat - storage.power_discharge.value[0] / storage.eta_discharge / 2
                            + storage.power_charge.value[0] * storage.eta_charge / 2) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            charge_status[t] = charge_status[t - 1] * (1 - eta_self)
            p = max(-1 * stc_data.power_cold[t], 0) / 1000. * stc_data.size.value
            d = cool_demand.demand[t]
            charge_status[t] = max(charge_status[t] -
                                   (storage.power_discharge.value[t] + storage.power_discharge.value[t - 1]) / 2
                                   / eta_discharge + (storage.power_charge.value[t]
                                                      + storage.power_charge.value[t - 1]) / 2 * eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(charge_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    check_tp_forward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_tp_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_tp_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)

    cold_tank_data.set_start(start_val)
    ds.calc()

    check_tp_forward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_tp_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value,
               start_val)
    check_fail(check_tp_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value,
               start_val)

    ds.set_de_model(2)
    cold_tank_data.start = False
    ds.calc()

    check_tp_backward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_tp_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_tp_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)

    cold_tank_data.set_start(start_val)
    ds.calc()

    check_tp_backward(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_tp_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_tp_central, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)

    ds.set_de_model(3)
    cold_tank_data.start = False
    ds.calc()

    check_tp_central(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_tp_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)
    check_fail(check_tp_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value)

    cold_tank_data.set_start(start_val)
    ds.calc()

    check_tp_central(cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value, start_val)
    check_fail(check_tp_backward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value,
               start_val)
    check_fail(check_tp_forward, cold_tank_data, district_cooling.power_buy.value, district_cooling.power_sell.value,
               start_val)

    def check_tp_seasonal(storage: Storage, dgl: int):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        f = 0.5 if dgl == 3 else 1
        for i in ran_start:
            power_start = storage.power_discharge.value[i] / eta_discharge - \
                          storage.power_charge.value[i] * eta_charge if dgl > 1 else 0
            assert round(storage.energy.value[i] + power_start * f, 2) == 0

        period = ds.tp_data.matching['PeriodNum'][0]
        period_hour = ds.tp_data.matching['TimeStep'][0]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        charge_status = zeros(length)
        power_start = (storage.power_discharge.value[t_old] / eta_discharge -
                       storage.power_charge.value[t_old] * eta_charge) if dgl > 1 else 0
        charge_status[0] = storage.energy_d.value[0] - power_start * f
        day = 0
        hour = 0
        for i in range(1, length):
            period = ds.tp_data.matching['PeriodNum'][i]
            period_hour = ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            charge_status[i] = charge_status[i - 1] * (1 - eta_self)
            power_last = 0 if dgl == 2 else (storage.power_charge.value[t_old] * eta_charge -
                                             storage.power_discharge.value[t_old] / eta_discharge)
            power_current = 0 if dgl == 1 else (storage.power_charge.value[t] * eta_charge -
                                                storage.power_discharge.value[t] / eta_discharge)
            charge_status[i] = charge_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(charge_status[i], 2) >= 0
            assert round(charge_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(round(storage.energy_d.value[day] * (1 - eta_self) ** period_hour + storage.energy.value[t], 2),
                           round(charge_status[i], 2), rel_tol=0.0001)

    ds.import_typical_periods('../typical_periods.pkl')
    ds.tp_data.tp_seasonal = True
    ds.tp_data.aggregate_tp_seasonal()
    ds.set_de_model(2)
    ds.calc()

    check_tp_seasonal(cold_tank_data, 2)

    ds1 = deepcopy(ds)
    ds1.tp_data.tp_seasonal_plus = True
    ds1.tp_data.aggregate_tp_seasonal()
    ds1.calc()

    def compare_ds_results_storage(ds_first: DataStorage, ds_second: DataStorage):
        ct_data1 = ds_first.CT[0]
        ds_cool1 = ds_first.district_cool[0]
        ct_data2 = ds_second.CT[0]
        ds_cool2 = ds_second.district_cool[0]
        assert isclose(ds_first.res['Obj'], ds_second.res['Obj'], rel_tol=0.000001)
        assert isclose(ct_data1.size.value[0], ct_data2.size.value[0], rel_tol=0.000001)

        for t in range(ds1.raw.shape[0]):
            assert isclose(round(ct_data1.power_charge.value[t], 2), round(ct_data2.power_charge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(ct_data1.power_discharge.value[t], 2), round(ct_data2.power_discharge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(ds_cool1.power_sell.value[t], 2), round(ds_cool2.power_sell.value[t], 2), rel_tol=0.00001)
            assert isclose(round(ds_cool1.power_buy.value[t], 2), round(ds_cool2.power_buy.value[t], 2), rel_tol=0.00001)

    compare_ds_results_storage(ds, ds1)

    ds.set_de_model(1)
    ds.calc()
    check_tp_seasonal(cold_tank_data, 1)

    ds1.set_de_model(1)
    ds1.calc()

    compare_ds_results_storage(ds, ds1)

    ds.set_de_model(3)
    ds.calc()
    check_tp_seasonal(cold_tank_data, 3)

    ds1.set_de_model(3)
    ds1.calc()

    compare_ds_results_storage(ds, ds1)

    def check_tp_seasonal_simple(storage: Storage, dgl: int):
        f = 0.5 if dgl == 3 else 1

        period = ds.tp_data.matching['PeriodNum'][-1]
        period_hour = ds.tp_data.matching['TimeStep'][-1]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        charge_status = zeros(length)
        charge_status[-1] = storage.energy.value[-1]
        day = 0
        hour = 0
        for i in range(length):
            period = ds.tp_data.matching['PeriodNum'][i]
            period_hour = ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            charge_status[i] = charge_status[i - 1] * (1 - eta_self)
            power_last = 0 if dgl == 2 else (storage.power_charge.value[t_old] * eta_charge -
                                             storage.power_discharge.value[t_old] / eta_discharge)
            power_current = 0 if dgl == 1 else (storage.power_charge.value[t] * eta_charge -
                                                storage.power_discharge.value[t] / eta_discharge)
            charge_status[i] = charge_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(charge_status[i], 2) >= 0
            assert round(charge_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(round(storage.energy.value[i], 2), round(charge_status[i], 2), rel_tol=0.0001)

    ds.tp_data.tp_seasonal_simple = True
    ds.tp_data.aggregate_tp_seasonal()
    ds.set_de_model(1)
    ds.calc()

    check_tp_seasonal_simple(cold_tank_data, 1)

    ds.set_de_model(2)
    ds.calc()

    check_tp_seasonal_simple(cold_tank_data, 2)

    ds.set_de_model(3)
    ds.calc()

    check_tp_seasonal_simple(cold_tank_data, 3)


if __name__ == '__main__':
    main()
