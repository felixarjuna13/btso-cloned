from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
"""
validation script for the district cooling node and the cooling demands
"""


def main():
    # read pv_data from csv file
    df = read_csv('../../src/btso/data/data.csv')
    # set variables which are used later on
    cool_line: str = 'Cooling'
    buy_line: str = 'Buy'
    buy_cool: float = 0.08
    buy_el: float = 0.30
    power_price = 20.
    lifetime: int = 20
    # variables for multiple demand checking
    cool_line_2: str = 'Electricity'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_sta: float = 100_000.
    sell_sta: float = 0.06
    price_lin: float = 200.
    sta_line: str = 'STAPowerCold'
    sell_line: str = 'Sell'
    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_single_cooling_demand(cool_line, -1)
    district_cool = ds.add_district_cooling(buy_cool, None, power_price)
    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    demand = df[cool_line] * -1
    c = demand.sum() * buy_cool * lifetime + power_price * demand.max() * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district cooling constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district cooling constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError
    # change constant to flexible pricing
    district_cool.use_power_price = False
    district_cool.set_flexible_price(buy_line)
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = sum(demand * df[buy_line]) * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district cooling flexible price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district cooling flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.import_typical_periods("./typical_periods.pkl")
    ds.add_single_cooling_demand(cool_line)
    district_cool = ds.add_district_cooling(buy_cool)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    obj = ds.res['Obj']
    c = demand.sum() * buy_cool * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district cooling constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district cooling constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    district_cool.set_flexible_price(buy_line)
    ds.calc()

    obj = ds.res['Obj']
    c = sum(demand * df[buy_line]) * lifetime
    err = min(ds.tp_data.error['MAE']['Buy'], ds.tp_data.error['MAE']['Fos'])
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of district cooling flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')
    else:
        print(f'Validation of district cooling flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_cooling(buy_cool)
    ds.add_multiple_cooling_demand([cool_line, cool_line], [price_1, price_2])
    ds.calc()

    c = demand.sum() * buy_cool * lifetime + min(price_1, price_2)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of multiple cooling demands 1 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple cooling demands 1  failed: {c} != {obj}')
        raise AssertionError

    ds.add_multiple_cooling_demand([cool_line, cool_line_2], [price_1, price_1])
    ds.calc()

    c = c + min(demand.sum(), df[cool_line_2].sum()) * buy_cool * lifetime + price_1
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of multiple cooling demands 2 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple cooling demands 2  failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_cooling(buy_cool)
    ds.add_multiple_cooling_demand([cool_line, cool_line], [price_1, price_1], [life_1, life_2])
    ds.calc()

    c = demand.sum() * buy_cool * lifetime + min(price_1 / life_1 * lifetime, price_1 / life_2 * lifetime)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of multiple cooling demands 3 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple cooling demands 3 failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_cooling(buy_cool, sell_sta)
    sta_data = ds.add_solar_thermal_absorber(price_lin, 0, 0, lifetime)
    sta_data.get_power_from_data(column_name_cold_side=sta_line)
    sta_data.set_connections(False, False, False, True)
    sta_data.set_max(max_sta)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    c = max_sta * price_lin - df[sta_line].where(df[sta_line] < 0).sum() * -1 * max_sta / 1000 * sell_sta * lifetime
    obj = ds.res['Obj']
    c_sell = price_lin / df[sta_line].where(df[sta_line] < 0).sum() / lifetime * 1000 * -1
    if isclose(sta_data.size.value, max_sta, rel_tol=0.00001) and isclose(c, obj, rel_tol=0.00001) and c_sell < sell_sta:
        print(f'Validation of district cooling constant reward successful: {c_sell} < {sell_sta} and {int(c)} == '
              f'{int(obj)}')
    else:
        print(f'Validation of district cooling constant reward failed: {c_sell} > {sell_sta} or {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_line)
    ds.add_district_cooling(buy_line, sell_line)
    sta_data = ds.add_solar_thermal_absorber(price_lin, 0, 0, lifetime)
    sta_data.get_power_from_data(column_name_cold_side=sta_line)
    sta_data.set_connections(False, False, False, True)
    sta_data.set_max(max_sta)
    ds.calc()

    c_sell = (df[sta_line] * -1).clip(0) * df[sell_line]
    c = max_sta * price_lin - c_sell.where(df[sell_line] > 0).sum() * max_sta / 1000 * lifetime
    obj = ds.res['Obj']
    if isclose(sta_data.size.value, max_sta, rel_tol=0.00001) and isclose(c, obj, rel_tol=0.00001):
        print(f'Validation of district cooling flexible reward successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district cooling flexible reward failed: {c} != {obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
