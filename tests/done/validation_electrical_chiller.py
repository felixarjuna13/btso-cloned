from btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
from scipy import optimize
from numpy import ndarray, array, ones


def main():
    df = read_csv('../../src/btso/data/data.csv')
    cool_line: str = 'Cooling'
    buy_el: float = 0.18
    buy_cool: float = 0.1
    cop: float = 3.
    price_lin: float = 100.
    price_con: float = 1_000.
    power_el: float = 200.
    thermal_power_line: str = 'ElectricChiller'
    lifetime: int = 20

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)

    ec_data = ds.add_electrical_chiller(price_lin, 0, 0, lifetime)
    ec_data.set_constant_cop(cop)
    ds.add_single_cooling_demand(cool_line)
    ds.add_electrical_grid(buy_el)
    ds.add_district_cooling(buy_cool)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = -1 * df[cool_line]
    sum_d = sum(demand)

    def calc_costs(size: ndarray):
        size = size[0]
        q_ec = demand.where(demand <= size).sum() + demand.where(demand > size).count() * size
        return size * price_lin + (sum_d - q_ec) * buy_cool * lifetime + q_ec / cop * buy_el * lifetime

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
    c = res.fun
    obj = ds.res["Obj"]

    if isclose(c, obj, rel_tol=0.0001):
        print(f'Validation of electrical chiller with constant COP successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical chiller with constant COP failed! {int(c)} != {int(obj)}')
        raise AssertionError

    ec_data = ds.add_electrical_chiller(price_lin, price_con, 0, lifetime)
    ec_data.set_flexible_cop(power_el, thermal_power_line)

    ds.calc()

    demand = -1 * df[cool_line]
    sum_d = sum(demand)
    th_power = df[thermal_power_line] / 1000
    cop_flex = th_power/power_el * 1000

    def calc_costs(size: ndarray):
        size = size[0]
        q_ec: ndarray = array([d if d <= (size * th) else th * size for d, th in zip(demand, th_power)])
        p_ec: float = (q_ec / cop_flex).sum()
        q_ec: float = q_ec.sum()
        return size * price_lin + price_con + ((sum_d - q_ec) * buy_cool + p_ec * buy_el) * lifetime

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP', bounds=((0, 10 ** 10),))
    c = res.fun
    obj = ds.res["Obj"]

    if isclose(c, obj, rel_tol=0.0001):
        print(f'Validation of electrical chiller with flexible COP successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical chiller with flexible COP failed! {int(c)} != {int(obj)}')
        raise AssertionError


if __name__ == '__main__':
    main()
