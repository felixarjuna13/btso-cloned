from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
"""
validation script for the electrical grid node and the electrical demands
"""


def main():
    # read pv_data from csv file
    df = read_csv('../../src/btso/data/data.csv')
    # set variables which are used later on
    el_line: str = 'Electricity'
    buy_line: str = 'Buy'
    buy_el: float = 0.18
    buy_el_2: float = 0.36
    power_price: float = 15
    base_price: float = 9 * 12
    base_price_2: float = 9 * 12 * 2
    lifetime: int = 20
    # variables for multiple demand checking
    el_line_2: str = 'Heating'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_pv: float = 100_000.
    sell_pv: float = 0.06
    price_lin: float = 250.
    pv_line: str = 'PV'
    sell_line: str = 'Sell'
    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_single_electrical_demand(el_line)
    el_grid = ds.add_electrical_grid(buy_el, None, power_price, base_price)
    ds.add_electrical_grid(buy_el_2, None, power_price, base_price_2)
    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()

    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = df[el_line].sum() * buy_el * lifetime + power_price * df[el_line].max() * lifetime + base_price * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError
    # change constant to flexible pricing
    el_grid.use_power_price = False
    el_grid.use_base_price = False
    el_grid.set_flexible_price(buy_line)
    # start calculation
    ds.calc()

    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = sum(df[el_line] * df[buy_line]) * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid flexible price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.import_typical_periods("./typical_periods.pkl")
    ds.add_single_electrical_demand(el_line)
    el_grid = ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    obj = ds.res['Obj']
    c = df[el_line].sum() * buy_el * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    el_grid.set_flexible_price(buy_line)
    ds.raw[buy_line] = ds.raw['BuyEl'] / ds.raw[el_line]
    ds.calc()

    obj = ds.res['Obj']
    c = sum(df[el_line] * df[buy_line]) * lifetime
    err = min(ds.tp_data.error['MAE']['Buy'], ds.tp_data.error['MAE']['Fos'])
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')
    else:
        print(f'Validation of electrical grid flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_multiple_electrical_demand([el_line, el_line], [price_1, price_2])
    ds.calc()

    c = df[el_line].sum() * buy_el * lifetime + min(price_1, price_2)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of multiple electrical demands 1 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple electrical demands 1  failed: {c} != {obj}')
        raise AssertionError

    ds.add_multiple_electrical_demand([el_line, el_line_2], [price_1, price_1])
    ds.calc()

    c = c + min(df[el_line].sum(), df[el_line_2].sum()) * buy_el * lifetime + price_1
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of multiple electrical demands 2 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple electrical demands 2  failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_multiple_electrical_demand([el_line, el_line], [price_1, price_1], [life_1, life_2])
    ds.calc()

    c = df[el_line].sum() * buy_el * lifetime + min(price_1/life_1*lifetime, price_1/life_2*lifetime)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of multiple electrical demands 3 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple electrical demands 3 failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el, sell_pv)
    pv_data = ds.add_photovoltaic(price_lin, 0, 0, lifetime)
    pv_data.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data.selling_power.set_constant_selling_price(0)
    pv_data.set_max(max_pv)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    c = max_pv * price_lin - df[pv_line].sum() * max_pv / 1000 * sell_pv * lifetime
    obj = ds.res['Obj']
    c_sell = price_lin / sum(df[pv_line]) / lifetime * 1000
    if isclose(pv_data.size.value, max_pv, rel_tol=0.00001) and isclose(c, obj, rel_tol=0.00001) and c_sell < sell_pv:
        print(f'Validation of electrical grid constant reward successful: {c_sell} < {sell_pv} and {int(c)} == '
              f'{int(obj)}')
    else:
        print(f'Validation of electrical grid constant reward failed: {c_sell} > {sell_pv} or {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_line, sell_line)
    pv_data = ds.add_photovoltaic(price_lin, 0, 0, lifetime)
    pv_data.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data.selling_power.set_constant_selling_price(0)
    pv_data.set_max(max_pv)

    ds.calc()

    c_sell = df[pv_line] * df[sell_line]
    c = max_pv * price_lin - c_sell.where(df[sell_line] > 0).sum() * max_pv / 1000 * lifetime
    obj = ds.res['Obj']
    if pv_data.size.value == max_pv and isclose(c, obj, rel_tol=0.00001):
        print(f'Validation of electrical grid flexible reward successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid flexible reward failed: {c} != {obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
