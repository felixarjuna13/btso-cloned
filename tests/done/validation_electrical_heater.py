from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose


def main():
    df = read_csv('../../src/btso/data/data.csv')
    heat_line: str = 'Heating'
    buy_el: float = 0.18
    eta: float = 0.96
    price_lin: float = 100.
    price_con: float = 1_000.
    lifetime: int = 20

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)

    eh_data = ds.add_electrical_heater(price_lin, 0, 0, lifetime)
    eh_data.set_efficiency(eta)
    ds.add_single_heating_demand(heat_line)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = df[heat_line]
    sum_d = sum(demand)
    c = sum_d / eta * buy_el * lifetime + demand.max() * price_lin

    if isclose(c, ds.res['Obj'], rel_tol=0.0001):
        print(f'Validation of electrical heater successful: {int(c)} == {int(ds.res["Obj"])}')
    else:
        print(f'Validation of electrical heater failed! {int(c)} != {int(ds.res["Obj"])}')
        raise AssertionError

    eh_data = ds.add_electrical_heater(price_lin, price_con, 0, lifetime)
    eh_data.set_efficiency(eta)

    ds.calc()

    c = sum_d / eta * buy_el * lifetime + demand.max() * price_lin + price_con

    if isclose(c, ds.res['Obj'], rel_tol=0.0001):
        print(f'Validation of electrical chiller with constant price successful: {int(c)} == {int(ds.res["Obj"])}')
    else:
        print(f'Validation of electrical chiller with constant price failed! {int(c)} != {int(ds.res["Obj"])}')
        raise AssertionError


if __name__ == '__main__':
    main()
