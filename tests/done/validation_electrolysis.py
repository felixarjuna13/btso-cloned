from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
from scipy import optimize
from numpy import ones, ndarray
"""
validation script for the electrolysis
"""


def main():
    # read pv_data from csv file
    df = read_csv('../../src/btso/data/data.csv')
    # set variables which are used later on
    hydro_line: str = 'Electricity'
    buy_hydro: float = 1.05
    buy_el: float = 0.15
    buy_heat: float = 0.08
    sell_heat: float = 0.06
    lifetime: int = 20
    # electrolysis variables
    price_lin: float = 200.
    price_con: float = 2000.
    eta_el: float = 0.4
    eta_th: float = 0.5
    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_single_hydrogen_demand(hydro_line)
    ds.add_district_hydro(buy_hydro)
    ds.add_electrical_grid(buy_el)
    electrolysis_data = ds.add_electrolysis(price_lin, price_con, 0, lifetime)
    electrolysis_data.set_electrical_efficiency(eta_el)
    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    demand = df[hydro_line]
    sum_d: float = demand.sum()

    def calc_costs(size: ndarray) -> float:
        size = size[0]
        size_m = size * eta_el
        q_es = demand.where(demand <= size_m).sum() + demand.where(demand > size_m).count() * size_m
        return size * price_lin + price_con + (q_es / eta_el * buy_el + (sum_d - q_es) * buy_hydro) * lifetime

    def calc_costs_wo_constant(size: ndarray) -> float:
        size = size[0]
        size_m = size * eta_el
        q_es = demand.where(demand <= size_m).sum() + demand.where(demand > size_m).count() * size_m
        return size * price_lin + (q_es / eta_el * buy_el + (sum_d - q_es) * buy_hydro) * lifetime

    def calc_costs_waste_heat(size: ndarray) -> float:
        size = size[0]
        size_m = size * eta_el
        q_es = demand.where(demand <= size_m).sum() + demand.where(demand > size_m).count() * size_m
        return size * price_lin + price_con + (q_es / eta_el * buy_el + (sum_d - q_es) * buy_hydro - q_es / eta_el *
                                               eta_th * sell_heat) * lifetime

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP')
    c = res.fun
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001) and isclose(electrolysis_data.size.value, res.x, rel_tol=0.001):
        print(f'Validation of electrolysis successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrolysis failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    electrolysis_data = ds.add_electrolysis(price_lin, 0, 0, lifetime)
    electrolysis_data.set_electrical_efficiency(eta_el)
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    res = optimize.minimize(calc_costs_wo_constant, ones(1), method='SLSQP')
    c = res.fun
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001) and isclose(electrolysis_data.size.value, res.x, rel_tol=0.001):
        print(f'Validation of electrolysis without constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrolysis without constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    electrolysis_data = ds.add_electrolysis(price_lin, price_con, 0, lifetime)
    electrolysis_data.set_electrical_efficiency(eta_el)
    electrolysis_data.set_use_waste_heat(eta_th)
    ds.add_district_heating(buy_heat, sell_heat)
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    res = optimize.minimize(calc_costs_waste_heat, ones(1), method='SLSQP')
    c = res.fun
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001) and isclose(electrolysis_data.size.value, res.x, rel_tol=0.001):
        print(f'Validation of electrolysis with waste heat use successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrolysis with waste heat use failed: {c} != {obj}; {c - obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
