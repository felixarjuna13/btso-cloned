import sys

sys.path.append("/src")

from src.btso.DataStorage import DataStorage
from pandas import read_csv, to_datetime as pd_to_datetime
from math import isclose
from numpy import trapz
from src.btso import FOLDER

"""
validation script for the district heating node and the heating demands
"""


def main():
    # read pv_data from csv file
    df = read_csv(f'{FOLDER}/data/data_flex_time_step.csv')
    # set variables which are used later on
    heat_line: str = 'Heating'
    date: str = 'Date'
    buy_line: str = 'Buy'
    buy_heat: float = 0.08
    buy_el: float = 0.30
    power_price: float = 25.
    lifetime: int = 20
    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data_flex_time_step.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_single_heating_demand(heat_line)
    ds.add_district_heating(buy_heat, None, power_price)
    ds.add_electrical_grid(buy_el)
    ds.set_integration_rule(1)
    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    df[date] = pd_to_datetime(df[date])
    df.set_index(date, inplace=True)
    # Calculate total costs manually
    sample_points = df.reset_index()[date].diff().dt.total_seconds().fillna(0).cumsum().values
    e_tot = trapz(df[heat_line], x=sample_points)/3600
    c = e_tot * buy_heat * lifetime + power_price * df[heat_line].max() * \
        lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of flexible time step calculation trapezoid rule successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of flexible time step calculation trapezoid rule failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    ds.set_integration_rule(0)
    ds.calc()

    obj = ds.res['Obj']
    sample_points = df.reset_index()[date].diff().dt.total_seconds().fillna(0).values
    e_tot = sum(df[heat_line] * sample_points)/3600 + df[heat_line][0] * sample_points[1]/3600
    c = e_tot * buy_heat * lifetime + power_price * df[heat_line].max() * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of flexible time step calculation Riemann sum successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of flexible time step calculation Riemann sum failed: {c} != {obj}; {c - obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
