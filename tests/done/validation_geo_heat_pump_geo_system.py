from btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
from scipy import optimize
from numpy import ones, ndarray


def main():
    df = read_csv('../src/btso/data/data.csv')
    cop = 5.1
    cop_2 = 3.2
    heat_line: str = 'Heating'
    heat_line_2: str = 'Electricity'
    cool_line: str = 'Cooling'
    gain_geo: float = 50.
    frac_el: float = 0.02
    buy_el: float = 0.18
    buy_cool: float = 0.08
    price_lin_hp: float = 900.
    price_lin_geo: float = 50.
    price_con_hp: float = 1200.
    price_con_geo: float = 2000.
    lifetime: int = 20

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)

    geo_hp_data = ds.add_geothermal_heat_pump(price_lin_hp, price_con_hp, 0, lifetime)
    geo_hp_data.set_constant_cop(cop)
    geo_data = ds.add_geothermal_system(price_lin_geo, price_con_geo, 0, lifetime)
    geo_data.set_constant_gain(gain_geo)
    geo_data.set_electrical_demand(frac_el)
    eh_data = ds.add_electrical_heater(0, 0, 0, lifetime)
    eh_data.set_efficiency(1)
    ds.add_single_heating_demand(heat_line)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()
    demand = df[heat_line]
    sum_d = sum(demand)
    c_old = sum_d*buy_el*lifetime
    for size in demand.sort_values(ascending=False):
        q_hp = demand.where(demand <= size).sum() + demand.where(demand > size).count() * size
        c_new = size * price_lin_hp + price_con_hp + size * (1 - 1 / cop) / gain_geo * 1000 * price_lin_geo + (
                price_con_geo) + (q_hp / cop + q_hp * (1 - 1 / cop) * frac_el + (sum_d - q_hp)) * buy_el * lifetime
        if c_new < c_old:
            c_old = c_new
            continue
        if c_new > c_old:
            break

    if isclose(c_old, ds.res['Obj'], rel_tol=0.0001):
        print(f'Validation of geothermal systems successful: {int(c_old):_.0f} == {int(ds.res["Obj"]):_.0f}')
    else:
        print(f'Validation of geothermal systems failed: {int(c_old):_.0f} != {int(ds.res["Obj"]):_.0f}; '
              f'{int(c_old) - int(ds.res["Obj"]):_.0f}')
        raise AssertionError
    """
    geo_hp_data.activate_binary_status()

    ds.calc()
    demand = df[heat_line]
    sum_d = sum(demand)
    c_old = sum_d * buy_el * lifetime

    def calc_costs(size_j: ndarray):
        size = size_j[0]
        q_hp = demand.where(demand >= size).count() * size
        return size * price_lin_hp + price_con_hp + size * (1 - 1 / cop) / gain_geo * 1000 * price_lin_geo + (
            price_con_geo) + (q_hp / cop + q_hp * (1 - 1 / cop) * frac_el + (sum_d - q_hp)) * buy_el * lifetime

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP')
    c = res.fun

    if isclose(c, ds.res['Obj'], rel_tol=ds.res['GAP']):
        print(f'Validation of geothermal systems successful: {int(c):_.0f} ~= {int(ds.res["Obj"]):_.0f} in {ds.res["GAP"]}')
    else:
        print(f'Validation of geothermal systems failed: {int(c):_.0f} != {int(ds.res["Obj"]):_.0f}; '
              f'{int(c) - int(ds.res["Obj"]):_.0f}')
        raise AssertionError
    """

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    geo_data = ds.add_geothermal_system(price_lin_geo, 0, 0, lifetime)
    geo_data.set_constant_gain(gain_geo)
    ds.add_single_cooling_demand(cool_line)
    ds.add_district_cooling(buy_cool)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()
    demand = df[cool_line] * - 1
    sum_d = sum(demand)
    c_old = sum_d * buy_el * lifetime
    for size in demand.sort_values(ascending=False):
        q_hp = demand.where(demand <= size).sum() + demand.where(demand > size).count() * size
        c_new = size / gain_geo * 1000 * price_lin_geo + ((sum_d - q_hp) * buy_cool) * lifetime
        if c_new < c_old:
            c_old = c_new
            continue
        if c_new > c_old:
            break

    if isclose(c_old, ds.res['Obj'], rel_tol=0.0001):
        print(f'Validation of geothermal system for cooling successful: {int(c_old):_.0f} == {int(ds.res["Obj"]):_.0f}')
    else:
        print(f'Validation of geothermal system for cooling failed: {int(c_old):_.0f} != {int(ds.res["Obj"]):_.0f}; '
              f'{int(c_old) - int(ds.res["Obj"]):_.0f}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    geo_hp_data = ds.add_geothermal_heat_pump(price_lin_hp, price_con_hp, 0, lifetime)
    geo_hp_data.set_constant_cop(cop)
    geo_data = ds.add_geothermal_system(price_lin_geo, 0, 0, lifetime)
    geo_data.set_constant_gain(gain_geo)
    geo_data.set_node(ds.electrical_nodes[0], ds.heat_nodes[0], ds.heat_nodes[0])
    ds.add_single_cooling_demand(cool_line)
    ds.add_district_cooling(buy_cool)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = df[cool_line] * - 1
    sum_d = sum(demand)

    def calc_costs(size_j: ndarray):
        size_j = size_j[0]
        q_hp = demand.where(demand <= size_j * (1 - 1 / cop)).sum() + demand.where(
            demand > size_j * (1 - 1 / cop)).count() * size_j * (1 - 1 / cop)
        return size_j * price_lin_hp + price_con_hp + size_j / gain_geo * 1000 * price_lin_geo + (
                q_hp / (1 - 1 / cop) / cop) * buy_el * lifetime + (sum_d - q_hp) * buy_cool * lifetime

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP')
    c = res.fun

    if isclose(c, ds.res['Obj'], rel_tol=0.0001):
        print(f'Validation of geothermal system for cooling with heat pump use successful:'
              f' {int(c):_.0f} == {int(ds.res["Obj"]):_.0f}')
    else:
        print(f'Validation of geothermal system for cooling with heat pump use  failed: '
              f'{int(c):_.0f} != {int(ds.res["Obj"]):_.0f}; {int(c) - int(ds.res["Obj"]):_.0f}')
        print(f'{res.x} {geo_hp_data.size.value}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    first_node = ds.add_heating_node()
    second_node = ds.add_heating_node()
    geo_hp_data = ds.add_geothermal_heat_pump(price_lin_hp, price_con_hp, 0, lifetime)
    geo_hp_data.activate_second_node(first_node, second_node, cop, cop_2)
    geo_data = ds.add_geothermal_system(price_lin_geo, price_con_geo, 0, lifetime)
    geo_data.set_constant_gain(gain_geo)
    heat_d = ds.add_single_heating_demand(heat_line)
    heat_d.set_node(first_node)
    heat_d_2 = ds.add_single_heating_demand(heat_line_2)
    heat_d_2.set_node(second_node)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = df[heat_line]
    demand_2 = df[heat_line_2]

    q_hp = demand.sum()
    q_hp_2 = demand_2.sum()
    q_geo = max(demand_2 * (1 - 1 / cop_2) + demand * (1 - 1 / cop))
    size_j = max(demand + demand_2)
    costs = size_j * price_lin_hp + price_con_hp + q_geo / gain_geo * 1000 * price_lin_geo + (
        price_con_geo) + (q_hp / cop + q_hp_2 / cop_2) * buy_el * lifetime

    if isclose(costs, ds.res['Obj'], rel_tol=0.0001):
        print(f'Validation of geothermal heat pump with two heat nodes successful:'
              f' {int(costs):_.0f} == {int(ds.res["Obj"]):_.0f}')
    else:
        print(f'Validation of geothermal heat pump with two heat nodes failed: '
              f'{int(costs):_.0f} != {int(ds.res["Obj"]):_.0f}; {int(costs) - int(ds.res["Obj"]):_.0f}')
        print(f'{size_j} {geo_hp_data.size.value}')
        print(f'{q_geo / gain_geo * 1000}: {geo_data.size.value}')
        raise AssertionError


if __name__ == '__main__':
    main()
