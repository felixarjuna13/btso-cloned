import sys

sys.path.append("/src")

from src.btso.DataStorage import DataStorage, Storage
from src.btso import FOLDER
from math import isclose
from pandas import read_csv, DataFrame
from scipy import optimize
from numpy import ones, array, zeros
from typing import Callable
from copy import deepcopy


def main():
    df: DataFrame = read_csv(f'{FOLDER}/data/data.csv')
    pv_line: str = 'PV'
    h2_line: str = 'Electricity'
    max_pv: float = 100_000.
    sell_h2: float = 0.06
    buy_h2: float = 0.32
    eta_self: float = 0.01
    eta_charge: float = 0.97
    eta_discharge: float = 0.95
    price_lin_pv: float = 200.
    price_con_pv: float = 2000.
    price_lin_h2: float = 5.
    price_con_h2: float = 350.
    eta_electrolysis: float = 0.7
    price_lin_electrolysis: float = 500.
    price_con_electrolysis: float = 3500.
    p_pv = df[pv_line] * max_pv / 1000
    max_electrolysis = max(p_pv)
    lifetime: int = 20
    start_val: float = 0.2

    ds: DataStorage = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)

    pv_data = ds.add_photovoltaic(price_lin_pv, price_con_pv, 0, lifetime)
    pv_data.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data.selling_power.set_constant_selling_price(0)
    pv_data.set_min(max_pv)
    pv_data.set_max(max_pv)
    h2_data = ds.add_hydrogen_storage(price_lin_h2, price_con_h2, 0, lifetime)
    h2_data.set_values(eta_self * 100, eta_charge * 100, eta_discharge * 100)
    h2_data.set_start(start_val)
    electrolysis_data = ds.add_electrolysis(price_lin_electrolysis, price_con_electrolysis, 0, lifetime)
    electrolysis_data.set_min(max_electrolysis)
    electrolysis_data.set_electrical_efficiency(eta_electrolysis)
    ds.set_de_model(2)
    h2_demand = ds.add_single_hydrogen_demand(h2_line)
    h2_grid = ds.add_district_hydro(buy_h2, sell_h2)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    if not isclose(pv_data.size.value, max_pv, rel_tol=0.0001):
        print(f'pv_data.size.value: {pv_data.size.value}')
        print(f'h2_data.size.value: {h2_data.size.value}')
        print(f'electrolysis_data.size.value: {electrolysis_data.size.value}')
        print(f'Validation PV and battery system failed: {pv_data.size.value} != {max_pv}')
        raise AssertionError

    demand = df[h2_line]
    p_pv = df[pv_line] * max_pv / 1000 * eta_electrolysis
    len_ye = len(demand)
    ran_yr: range = range(len_ye)

    def calc_costs_backward(size_bat: array, eta_self_internal: float):
        size_bat = size_bat[0]
        bat_status = start_val * size_bat
        e_sell = 0
        e_buy = 0
        for p, d in zip(p_pv, demand):
            bat_status = bat_status * (1 - eta_self_internal)
            bat_status_old = bat_status
            if p > d:
                bat_status = min(bat_status + (p - d) * eta_charge, size_bat)
                e_sell += max(p - d - (bat_status - bat_status_old) / eta_charge, 0)
                continue
            bat_status = max(bat_status + (p - d) / eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - bat_status) * eta_discharge, 0)

        return price_lin_pv * max_pv + price_con_pv + price_lin_h2 * size_bat + price_con_h2 + (
               price_lin_electrolysis * max_electrolysis + price_con_electrolysis) + (
                e_buy * buy_h2 - e_sell * sell_h2) * lifetime

    def calc_costs_backward_with_self_discharge(size_bat: array):
        return calc_costs_backward(size_bat, eta_self)

    def calc_costs_backward_without_self_discharge(size_bat: array):
        return calc_costs_backward(size_bat, 0)

    def calc_costs_forward(size_bat: float, eta_self_internal: float):
        bat_status = start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in ran_yr:
            p = p_pv[t - 1] if t > 0 else 0
            d = demand[t - 1] if t > 0 else 0
            bat_status = bat_status * (1 - eta_self_internal)
            bat_status_old = bat_status
            if p > d:
                bat_status = min(bat_status + (p - d) * eta_charge, size_bat)
                e_sell += max(p - d - (bat_status - bat_status_old) / eta_charge, 0)
                continue
            bat_status = max(bat_status + (p - d) / eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - bat_status) * eta_discharge, 0)

        return price_lin_pv * max_pv + price_con_pv + price_lin_h2 * size_bat + price_con_h2 + (
               price_lin_electrolysis * max_electrolysis + price_con_electrolysis) + (
                e_buy * buy_h2 - e_sell * sell_h2) * lifetime

    def calc_costs_forward_with_self_discharge(size_bat: array):
        return calc_costs_forward(size_bat, eta_self)

    def calc_costs_forward_without_self_discharge(size_bat: array):
        return calc_costs_forward(size_bat, 0)

    def calc_costs_center(size_bat: float, eta_self_internal: float):
        bat_status = start_val * size_bat
        e_sell = 0
        e_buy = 0
        for t in ran_yr:
            p = p_pv[t - 1] / 2 + p_pv[t] / 2 if t > 0 else p_pv[t] / 2
            d = demand[t - 1] / 2 + demand[t] / 2 if t > 0 else demand[t] / 2
            bat_status = bat_status * (1 - eta_self_internal)
            bat_status_old = bat_status
            if p > d:
                bat_status = min(bat_status + (p - d) * eta_charge, size_bat)
                e_sell += max(p - d - (bat_status - bat_status_old) / eta_charge, 0)
                continue
            bat_status = max(bat_status + (p - d) / eta_discharge, 0)
            e_buy += max(d - p - (bat_status_old - bat_status) * eta_discharge, 0)

        return price_lin_pv * max_pv + price_con_pv + price_lin_h2 * size_bat + price_con_h2 + (
               price_lin_electrolysis * max_electrolysis + price_con_electrolysis) + (
                e_buy * buy_h2 - e_sell * sell_h2) * lifetime

    def calc_costs_center_with_self_discharge(size_bat: array):
        return calc_costs_center(size_bat, eta_self)

    def calc_costs_center_without_self_discharge(size_bat: array):
        return calc_costs_center(size_bat, 0)

    def check_backward(storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        bat_status = zeros(8760)
        bat_status[0] = start_value * size_bat * (1 - eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(8760):
            bat_status[t] = bat_status[0] if t == 0 else bat_status[t - 1] * (1 - eta_self)
            bat_status_old = bat_status[t]
            p = p_pv[t]
            d = demand[t]
            if p > d:
                bat_status[t] = min(bat_status[t] + storage.power_charge.value[t] * eta_charge, size_bat)
                p_sell[t] = max(p - d - (bat_status[t] - bat_status_old) / eta_charge, 0)
            else:
                bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t] / eta_discharge, 0)
                p_buy[t] = max(d - p - (bat_status_old - bat_status[t]) * eta_discharge, 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_forward(storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        bat_status = zeros(8760)
        bat_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[8759] * (1 - eta_self)
        bat_status[0] = bat_status[0] + (storage.power_charge.value[-1] * eta_charge -
                                         storage.power_discharge.value[-1] / eta_discharge)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        for t in range(1, 8760):
            bat_status[t] = bat_status[t-1] * (1 - eta_self)
            bat_status_old = bat_status[t]
            p = p_pv[t - 1]
            d = demand[t - 1]
            if p > d:
                bat_status[t] = min(bat_status[t] + storage.power_charge.value[t - 1] * eta_charge, size_bat)
                p_sell[t - 1] = max(p - d - (bat_status[t] - bat_status_old) / eta_charge, 0)
            else:
                bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t - 1] / eta_discharge, 0)
                p_buy[t - 1] = max(d - p - (bat_status_old - bat_status[t]) * eta_discharge, 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_central(storage: Storage, buy: array, sell: array, start_value: float = None):
        size_bat = storage.size.value
        bat_status = zeros(8760)
        bat_status[0] = start_value * size_bat * (1 - eta_self) if start_value is not None else \
            storage.energy.value[8759] * (1 - eta_self)
        p_sell = zeros(8760)
        p_buy = zeros(8760)
        p = (p_pv[0])
        d = (demand[0])
        if p > d:
            bat_status[0] = min(bat_status[0] + (storage.power_charge.value[0]) / 2 * eta_charge, size_bat)
            p_sell[0] = max(p - d - storage.power_charge.value[0], 0)
        else:
            bat_status[0] = max(bat_status[0] - (storage.power_discharge.value[0]) / 2 / eta_discharge, 0)
            p_buy[0] += max(d - p - storage.power_discharge.value[0], 0)
        p = (p_pv[8759])
        d = (demand[8759])
        if p > d:
            bat_status[0] = min(bat_status[0] + (storage.power_charge.value[-1]) / 2 * eta_charge, size_bat)
        else:
            bat_status[0] = max(bat_status[0] - (storage.power_discharge.value[-1]) / 2 / eta_discharge, 0)
        assert isclose(round(bat_status[0], 2), round(storage.energy.value[0], 2), rel_tol=0.0001)
        assert isclose(round(p_buy[0], 2), round(buy[0], 2), rel_tol=0.0001)
        assert isclose(round(p_sell[0], 2), round(sell[0], 2), rel_tol=0.0001)
        for t in range(1, 8760):
            bat_status[t] = bat_status[t-1] * (1 - eta_self)
            p = p_pv[t]
            d = demand[t]
            if p > d:
                bat_status[t] = min(bat_status[t] + (storage.power_charge.value[t] + storage.power_charge.value[t - 1]) / 2
                                    * eta_charge - (storage.power_discharge.value[t] +
                                                    storage.power_discharge.value[t - 1]) / 2 / eta_discharge, size_bat)
                p_sell[t] = max(p - d - storage.power_charge.value[t] + storage.power_discharge.value[t], 0)
            else:
                bat_status[t] = max(bat_status[t] - (storage.power_discharge.value[t] +
                                                     storage.power_discharge.value[t - 1]) / 2 / eta_discharge +
                                    (storage.power_charge.value[t] + storage.power_charge.value[t - 1]) / 2 * eta_charge, 0)
                p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_fail(funct: Callable, storage: Storage, buy: array, sell: array, start_value: float = None):
        try:
            funct(storage, buy, sell, start_value)
        except AssertionError:
            return
        raise AssertionError

    check_backward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)

    res_with = optimize.minimize(calc_costs_backward_with_self_discharge, ones(1), method='SLSQP',
                                 bounds=[[0, 100_000]])
    c_calc_with = res_with.fun
    res_without = optimize.minimize(calc_costs_backward_without_self_discharge, ones(1), method='SLSQP',
                                    bounds=[[0, 100_000]])
    c_calc_without = res_without.fun
    c_opt = ds.res['Obj']

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of battery size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
              f' <= {int(c_calc_with)}')
    else:
        print(f'Validation of battery size with backward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    h2_data.start = False
    ds.calc()
    c_opt = ds.res['Obj']

    check_backward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of battery size with backward euler solver successful: {int(c_calc_without)} <= {int(c_opt)}'
              f' <= {int(c_calc_with)}')
    else:
        print(f'Validation of battery size with backward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    h2_data.set_start(start_val)
    ds.set_de_model(1)
    ds.calc()
    check_forward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    res_with = optimize.minimize(calc_costs_forward_with_self_discharge, ones(1), method='SLSQP',
                                 bounds=[[0, 100_000]])
    c_calc_with = res_with.fun
    res_without = optimize.minimize(calc_costs_forward_without_self_discharge, ones(1), method='SLSQP',
                                    bounds=[[0, 100_000]])
    c_calc_without = res_without.fun
    c_opt = ds.res['Obj']

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of battery size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
              f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of battery size with forward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    h2_data.start = False
    ds.calc()
    c_opt = ds.res['Obj']

    check_forward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)

    if c_calc_without <= c_opt <= c_calc_with:
        print(
            f'Validation of battery size with forward euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of battery size with forward euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    h2_data.set_start(start_val)
    ds.set_de_model(3)
    ds.calc()
    check_central(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    res_with = optimize.minimize(calc_costs_center_with_self_discharge, ones(1), method='SLSQP',
                                 bounds=[[0, 100_000]])
    c_calc_with = res_with.fun
    res_without = optimize.minimize(calc_costs_center_without_self_discharge, ones(1), method='SLSQP',
                                    bounds=[[0, 100_000]])
    c_calc_without = res_without.fun
    c_opt = ds.res['Obj']

    if c_calc_without <= c_opt <= c_calc_with:
        print(f'Validation of battery size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
              f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of battery size with central euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    h2_data.start = False
    ds.calc()
    c_opt = ds.res['Obj']

    check_central(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)

    if c_calc_without <= c_opt <= c_calc_with:
        print(
            f'Validation of battery size with central euler solver successful: {int(c_calc_without)} <= {int(c_opt)} '
            f'<= {int(c_calc_with)}')
    else:
        print(f'Validation of battery size with central euler solver failed: {int(c_calc_without)} >= {int(c_opt)} '
              f'>= {int(c_calc_with)}')
        raise AssertionError

    h2_data.start = False
    ds.import_typical_periods('../typical_periods.pkl')
    ds.set_de_model(1)
    ds.calc()

    def check_tp_forward(storage: Storage, buy: array, sell: array, start_value: float = None):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        ran_end = range(ds.tp_data.hours_per_period - 1, length, ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_end = storage.energy.value[j] * (1 - eta_self) - \
                        storage.power_discharge.value[j] / storage.eta_discharge + \
                        storage.power_charge.value[j] * storage.eta_charge
            assert isclose(storage.energy.value[i], power_end, rel_tol=0.0001)
        size_bat = storage.size.value
        bat_status = zeros(length)
        bat_status[0] = start_value * size_bat if start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            bat_status[t] = bat_status[t - 1] * (1 - eta_self)
            p = pv_data.produced_power.power[t - 1] * max_pv * eta_electrolysis
            d = h2_demand.demand[t - 1]
            bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t - 1] / eta_discharge +
                                storage.power_charge.value[t - 1] * eta_charge, 0)
            p_buy[t - 1] = max(d - p - storage.power_discharge.value[t - 1] + storage.power_charge.value[t - 1], 0)
            p_sell[t - 1] = max(p - d + storage.power_discharge.value[t - 1] - storage.power_charge.value[t - 1], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t - 1], 2), round(buy[t - 1], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t - 1], 2), round(sell[t - 1], 2), rel_tol=0.0001)

    def check_tp_backward(storage: Storage, buy: array, sell: array, start_value: float = None):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        ran_end = range(ds.tp_data.hours_per_period - 1, length, ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] /
                           storage.eta_discharge - storage.power_charge.value[i] * storage.eta_charge)
            assert isclose(power_start, storage.energy.value[j] * (1 - eta_self), rel_tol=0.0001)
        size_bat = storage.size.value
        bat_status = zeros(length)
        bat_status[0] = (start_value * size_bat - storage.power_discharge.value[0] /
                         storage.eta_discharge + storage.power_charge.value[0] * storage.eta_charge) if \
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            bat_status[t] = bat_status[t - 1] * (1 - eta_self)
            p = pv_data.produced_power.power[t] * max_pv * eta_electrolysis
            d = h2_demand.demand[t]
            bat_status[t] = max(bat_status[t] - storage.power_discharge.value[t] / eta_discharge +
                                storage.power_charge.value[t] * eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)

    def check_tp_central(storage: Storage, buy: array, sell: array, start_value: float = None):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        ran_end = range(ds.tp_data.hours_per_period - 1, length, ds.tp_data.hours_per_period)
        for i, j in zip(ran_start, ran_end):
            power_start = (storage.energy.value[i] + storage.power_discharge.value[i] / storage.eta_discharge / 2
                           - storage.power_charge.value[i] * storage.eta_charge / 2)
            power_end = (storage.energy.value[j] * (1 - eta_self) - storage.power_discharge.value[j] /
                         storage.eta_discharge / 2 + storage.power_charge.value[j] * storage.eta_charge / 2)
            assert isclose(power_start, power_end, rel_tol=0.0001)
        size_bat = storage.size.value
        bat_status = zeros(length)
        bat_status[0] = (start_value * size_bat - storage.power_discharge.value[0] / storage.eta_discharge / 2
                         + storage.power_charge.value[0] * storage.eta_charge / 2) if\
            start_value is not None else storage.energy.value[0]
        p_sell = zeros(length)
        p_buy = zeros(length)
        for t in range(1, length):
            bat_status[t] = bat_status[t - 1] * (1 - eta_self)
            p = pv_data.produced_power.power[t] * max_pv * eta_electrolysis
            d = h2_demand.demand[t]
            bat_status[t] = max(bat_status[t] - (storage.power_discharge.value[t] +
                                                 storage.power_discharge.value[t - 1]) / 2
                                / eta_discharge + (storage.power_charge.value[t]
                                                   + storage.power_charge.value[t - 1]) / 2 * eta_charge, 0)
            p_buy[t] = max(d - p - storage.power_discharge.value[t] + storage.power_charge.value[t], 0)
            p_sell[t] = max(p - d + storage.power_discharge.value[t] - storage.power_charge.value[t], 0)
            assert isclose(round(bat_status[t], 2), round(storage.energy.value[t], 2), rel_tol=0.0001)
            assert isclose(round(p_buy[t], 2), round(buy[t], 2), rel_tol=0.0001)
            assert isclose(round(p_sell[t], 2), round(sell[t], 2), rel_tol=0.0001)
    check_tp_forward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_tp_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_tp_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)

    h2_data.set_start(start_val)
    ds.calc()

    check_tp_forward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_tp_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_tp_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)

    ds.set_de_model(2)
    h2_data.start = False
    ds.calc()

    check_tp_backward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_tp_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_tp_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)

    h2_data.set_start(start_val)
    ds.calc()

    check_tp_backward(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_tp_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_tp_central, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)

    ds.set_de_model(3)
    h2_data.start = False
    ds.calc()

    check_tp_central(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_tp_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)
    check_fail(check_tp_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value)

    h2_data.set_start(start_val)
    ds.calc()
    
    check_tp_central(h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_tp_backward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)
    check_fail(check_tp_forward, h2_data, h2_grid.power_buy.value, h2_grid.power_sell.value, start_val)

    def check_tp_seasonal(storage: Storage, dgl: int):
        length = ds.raw.shape[0]
        ran_start = range(0, length, ds.tp_data.hours_per_period)
        f = 0.5 if dgl == 3 else 1
        for i in ran_start:
            power_start = storage.power_discharge.value[i] / eta_discharge - \
                          storage.power_charge.value[i] * eta_charge if dgl > 1 else 0
            assert round(storage.energy.value[i] + power_start * f, 2) == 0

        period = ds.tp_data.matching['PeriodNum'][0]
        period_hour = ds.tp_data.matching['TimeStep'][0]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        bat_status = zeros(length)
        power_start = (storage.power_discharge.value[t_old] / eta_discharge -
                       storage.power_charge.value[t_old] * eta_charge) if dgl > 1 else 0
        bat_status[0] = storage.energy_d.value[0] - power_start * f
        day = 0
        hour = 0
        for i in range(1, length):
            period = ds.tp_data.matching['PeriodNum'][i]
            period_hour = ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            bat_status[i] = bat_status[i - 1] * (1 - eta_self)
            power_last = 0 if dgl == 2 else (storage.power_charge.value[t_old] * eta_charge -
                                             storage.power_discharge.value[t_old] / eta_discharge)
            power_current = 0 if dgl == 1 else (storage.power_charge.value[t] * eta_charge -
                                                storage.power_discharge.value[t] / eta_discharge)
            bat_status[i] = bat_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(bat_status[i], 2) >= 0
            assert round(bat_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(round(storage.energy_d.value[day] * (1 - eta_self) ** period_hour + storage.energy.value[t], 2),
                           round(bat_status[i], 2), rel_tol=0.0001)

    ds.import_typical_periods('../typical_periods.pkl')
    ds.tp_data.tp_seasonal = True
    ds.tp_data.aggregate_tp_seasonal()
    ds.set_de_model(2)
    ds.calc()

    check_tp_seasonal(h2_data, 2)

    ds1 = deepcopy(ds)
    ds1.tp_data.tp_seasonal_plus = True
    ds1.tp_data.aggregate_tp_seasonal()
    ds1.calc()

    def compare_ds_results_storage(ds_first: DataStorage, ds_second: DataStorage):
        h2_data1 = ds_first.H2Storage[0]
        h2_grid1 = ds_first.district_hydro[0]
        h2_data2 = ds_second.H2Storage[0]
        h2_grid2 = ds_second.district_hydro[0]
        assert isclose(ds_first.res['Obj'], ds_second.res['Obj'], rel_tol=0.000001)
        assert isclose(h2_data1.size.value[0], h2_data2.size.value[0], rel_tol=0.000001)

        for t in range(ds_first.raw.shape[0]):
            assert isclose(round(h2_data1.power_charge.value[t], 2), round(h2_data2.power_charge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(h2_data1.power_discharge.value[t], 2), round(h2_data2.power_discharge.value[t], 2),
                           rel_tol=0.00001)
            assert isclose(round(h2_grid1.power_sell.value[t], 2), round(h2_grid2.power_sell.value[t], 2), rel_tol=0.00001)
            assert isclose(round(h2_grid1.power_buy.value[t], 2), round(h2_grid2.power_buy.value[t], 2), rel_tol=0.00001)

    compare_ds_results_storage(ds, ds1)

    ds.set_de_model(1)
    ds.calc()
    check_tp_seasonal(h2_data, 1)

    ds1.set_de_model(1)
    ds1.calc()

    compare_ds_results_storage(ds, ds1)

    ds.set_de_model(3)
    ds.calc()
    check_tp_seasonal(h2_data, 3)

    ds1.set_de_model(3)
    ds1.calc()

    compare_ds_results_storage(ds, ds1)

    def check_tp_seasonal_simple(storage: Storage, dgl: int):
        f = 0.5 if dgl == 3 else 1

        period = ds.tp_data.matching['PeriodNum'][-1]
        period_hour = ds.tp_data.matching['TimeStep'][-1]
        t_old = period_hour + period * 24
        size_bat = storage.size.value
        length = 8760
        bat_status = zeros(length)
        bat_status[-1] = storage.energy.value[-1]
        day = 0
        hour = 0
        for i in range(length):
            period = ds.tp_data.matching['PeriodNum'][i]
            period_hour = ds.tp_data.matching['TimeStep'][i]
            t = period_hour + period * 24
            bat_status[i] = bat_status[i - 1] * (1 - eta_self)
            power_last = 0 if dgl == 2 else (storage.power_charge.value[t_old] * eta_charge -
                                             storage.power_discharge.value[t_old] / eta_discharge)
            power_current = 0 if dgl == 1 else (storage.power_charge.value[t] * eta_charge -
                                                storage.power_discharge.value[t] / eta_discharge)
            bat_status[i] = bat_status[i] + (power_last + power_current) * f
            t_old = t

            assert round(bat_status[i], 2) >= 0
            assert round(bat_status[i], 2) <= size_bat.round(2)
            if hour < 23:
                hour += 1
            else:
                day += 1
                hour = 0
            assert isclose(round(storage.energy.value[i], 2), round(bat_status[i], 2), rel_tol=0.0001)

    ds.tp_data.tp_seasonal_simple = True
    ds.tp_data.aggregate_tp_seasonal()
    ds.set_de_model(1)
    ds.calc()

    check_tp_seasonal_simple(h2_data, 1)

    ds.set_de_model(2)
    ds.calc()

    check_tp_seasonal_simple(h2_data, 2)

    ds.set_de_model(3)
    ds.calc()

    check_tp_seasonal_simple(h2_data, 3)


if __name__ == '__main__':
    main()
