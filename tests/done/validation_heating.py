import sys

sys.path.append("/src")

from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
from src.btso import FOLDER
"""
validation script for the district heating node and the heating demands
"""


def main():
    # read pv_data from csv file
    df = read_csv(f'{FOLDER}/data/data.csv')
    # set variables which are used later on
    heat_line: str = 'Heating'
    buy_line: str = 'Buy'
    buy_heat: float = 0.08
    buy_el: float = 0.30
    power_price: float = 25.
    lifetime: int = 20
    # variables for multiple demand checking
    heat_line_2: str = 'Electricity'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_stc: float = 100_000.
    sell_stc: float = 0.06
    price_lin: float = 250.
    stc_line: str = 'STCPowerHot'
    sell_line: str = 'Sell'
    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_single_heating_demand(heat_line)
    district_heat = ds.add_district_heating(buy_heat, None, power_price)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = df[heat_line].sum() * buy_heat * lifetime + power_price * df[heat_line].max() * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district heating constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district heating constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError
    # change constant to flexible pricing
    district_heat.use_power_price = False
    district_heat.set_flexible_price(buy_line)
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = sum(df[heat_line] * df[buy_line]) * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district heating flexible price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district heating flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.import_typical_periods("./typical_periods.pkl")

    ds.add_single_heating_demand(heat_line)
    district_heat = ds.add_district_heating(buy_heat)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    obj = ds.res['Obj']
    c = df[heat_line].sum() * buy_heat * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district heating constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district heating constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    district_heat.set_flexible_price(buy_line)
    ds.raw[buy_line] = ds.raw['ElHeat'] / ds.raw[heat_line].clip(1)
    ds.calc()

    obj = ds.res['Obj']
    c = sum(df[heat_line] * df[buy_line]) * lifetime
    err = min(ds.tp_data.error['MAE']['Buy'], ds.tp_data.error['MAE']['Fos'])
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of district heating flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')
    else:
        print(f'Validation of district heating flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_heating(buy_heat)
    ds.add_multiple_heating_demand([heat_line, heat_line], [price_1, price_2])
    ds.calc()

    c = df[heat_line].sum() * buy_heat * lifetime + min(price_1, price_2)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of multiple heating demands 1 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple heating demands 1  failed: {c} != {obj}')
        raise AssertionError

    ds.add_multiple_heating_demand([heat_line, heat_line_2], [price_1, price_1])
    ds.calc()

    c = c + min(df[heat_line].sum(), df[heat_line_2].sum()) * buy_heat * lifetime + price_1
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of multiple heating demands 2 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple heating demands 2  failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_heating(buy_heat)
    ds.add_multiple_heating_demand([heat_line, heat_line], [price_1, price_1], [life_1, life_2])
    ds.calc()

    c = df[heat_line].sum() * buy_heat * lifetime + min(price_1 / life_1 * lifetime, price_1 / life_2 * lifetime)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of multiple heating demands 3 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple heating demands 3 failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_heating(buy_heat, sell_stc)
    stc_data = ds.add_solar_thermal_collector(price_lin, 0, 0, lifetime)
    stc_data.get_power_from_data(stc_line)
    stc_data.set_connections(True, False, False, False)
    stc_data.set_max(max_stc)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    c = max_stc * price_lin - df[stc_line].where(df[stc_line] > 0).sum() * max_stc / 1000 * sell_stc * lifetime
    obj = ds.res['Obj']
    c_sell = price_lin / df[stc_line].where(df[stc_line] > 0).sum() / lifetime * 1000
    if isclose(stc_data.size.value, max_stc, rel_tol=0.000001) and isclose(c, obj) and c_sell < sell_stc:
        print(f'Validation of district heating constant reward successful: {c_sell} < {sell_stc} and {int(c)} == '
              f'{int(obj)}')
    else:
        print(f'Validation of district heating constant reward failed: {c_sell} > {sell_stc} or {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_line)
    ds.add_district_heating(buy_line, sell_line)
    stc_data = ds.add_solar_thermal_collector(price_lin, 0, 0, lifetime)
    stc_data.get_power_from_data(stc_line)
    stc_data.set_connections(True, False, False, False)
    stc_data.set_max(max_stc)
    ds.calc()

    c_sell = df[stc_line].clip(0) * df[sell_line]
    c = max_stc * price_lin - c_sell.where(df[sell_line] > 0).sum() * max_stc / 1000 * lifetime
    obj = ds.res['Obj']
    if isclose(stc_data.size.value, max_stc, rel_tol=0.000001) and isclose(c, obj):
        print(f'Validation of district heating flexible reward successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district heating flexible reward failed: {c} != {obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
