from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
"""
validation script for the district hydrogen node and the hydrogen demands
"""


def main():
    # read pv_data from csv file
    df = read_csv('../../src/btso/data/data.csv')
    # set variables which are used later on
    hydro_line: str = 'Electricity'
    buy_line: str = 'Buy'
    buy_hydro: float = 1.05
    buy_el: float = 0.15
    power_price: float = 5.
    lifetime: int = 20
    # variables for multiple demand checking
    hydro_line_2: str = 'Heating'
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    # variables for sale checking
    max_es: float = 100.
    sell_es: float = 0.7
    price_lin: float = 200.
    eta_el: float = 0.4
    sell_line: str = 'Buy'
    buy_el_sell: float = 0.02
    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_single_hydrogen_demand(hydro_line)
    district_hydro = ds.add_district_hydro(buy_hydro, None, power_price)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = df[hydro_line].sum() * buy_hydro * lifetime + df[hydro_line].max() * power_price * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district hydrogen constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district hydrogen constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError
    # change constant to flexible pricing
    district_hydro.use_power_price = False
    district_hydro.set_flexible_price(buy_line)
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = sum(df[hydro_line] * df[buy_line]) * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district hydrogen flexible price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district hydrogen flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.import_typical_periods("./typical_periods.pkl")
    ds.add_single_hydrogen_demand(hydro_line)
    district_hydro = ds.add_district_hydro(buy_hydro)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    obj = ds.res['Obj']
    c = df[hydro_line].sum() * buy_hydro * lifetime
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of district hydrogen constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district hydrogen constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    district_hydro.set_flexible_price(buy_line)
    ds.calc()

    obj = ds.res['Obj']
    c = sum(df[hydro_line] * df[buy_line]) * lifetime
    err = min(ds.tp_data.error['MAE']['Buy'], ds.tp_data.error['MAE']['Fos'])
    # check if validation worked
    if isclose(obj, c, rel_tol=err):
        print(f'Validation of district hydrogen flexible price successful: {int(c)} == {int(obj)} in error of '
              f'{err * 100} %')
    else:
        print(f'Validation of district hydrogen flexible price failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_hydro(buy_hydro)
    ds.add_multiple_hydrogen_demand([hydro_line, hydro_line], [price_1, price_2])
    ds.calc()

    c = df[hydro_line].sum() * buy_hydro * lifetime + min(price_1, price_2)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of multiple hydrogen demands 1 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple hydrogen demands 1  failed: {c} != {obj}')
        raise AssertionError

    ds.add_multiple_hydrogen_demand([hydro_line, hydro_line_2], [price_1, price_1])
    ds.calc()

    c = c + min(df[hydro_line].sum(), df[hydro_line_2].sum()) * buy_hydro * lifetime + price_1
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of multiple hydrogen demands 2 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple hydrogen demands 2  failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_hydro(buy_hydro)
    ds.add_multiple_hydrogen_demand([hydro_line, hydro_line], [price_1, price_1], [life_1, life_2])
    ds.calc()

    c = df[hydro_line].sum() * buy_hydro * lifetime + min(price_1 / life_1 * lifetime, price_1 / life_2 * lifetime)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of multiple hydrogen demands 3 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple hydrogen demands 3 failed: {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el)
    ds.add_district_hydro(buy_hydro, sell_es)
    es_data = ds.add_electrolysis(price_lin, 0, 0, lifetime)
    es_data.set_electrical_efficiency(eta_el)
    es_data.set_max(max_es)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    c = max_es * price_lin - (sell_es * eta_el - buy_el) * max_es * 8760 * lifetime
    obj = ds.res['Obj']
    c_sell = (price_lin + buy_el * 8760 * lifetime) / (sell_es * eta_el * 8760 * lifetime)
    if isclose(es_data.size.value, max_es, rel_tol=0.00001) and isclose(c, obj, rel_tol=0.00001) and c_sell < sell_es:
        print(f'Validation of district hydrogen constant reward successful: {c_sell} < {sell_es} and {int(c)} == '
              f'{int(obj)}')
    else:
        print(f'Validation of district hydrogen constant reward failed: {c_sell} > {sell_es} or {c} != {obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_electrical_grid(buy_el_sell)
    ds.add_district_hydro(buy_line, sell_line)
    es_data = ds.add_electrolysis(price_lin, 0, 0, lifetime)
    es_data.set_electrical_efficiency(eta_el)
    es_data.set_max(max_es)
    ds.calc()

    c = max_es * price_lin
    for i in df[sell_line]:
        if i * eta_el >= buy_el_sell:
            c += max_es * (buy_el_sell - i * eta_el) * lifetime
    obj = ds.res['Obj']
    if isclose(es_data.size.value, max_es, rel_tol=0.00001) and isclose(c, obj, rel_tol=0.00001):
        print(f'Validation of district hydrogen flexible reward successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of district hydrogen flexible reward failed: {c} != {obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
