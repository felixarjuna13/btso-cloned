from btso.DataStorage import DataStorage
from btso import FOLDER
from pandas import read_csv
from math import isclose


def main():
    df = read_csv(f'{FOLDER}/data/data.csv')
    el_line: str = 'Electricity'
    heat_line: str = 'Heating'
    cool_line: str = 'Cooling'
    hydro_line: str = 'PV'
    el_line_2: str = 'PV'
    heat_line_2: str = 'Electricity'
    cool_line_2: str = 'Heating'
    hydro_line_2: str = 'Cooling'
    buy_el: float = 0.18
    buy_heat: float = 0.08
    buy_cool: float = 0.06
    buy_hydro: float = 1.05
    price_1: float = 1000_000.
    price_2: float = 2000_000.
    life_1: int = 20
    life_2: int = 10
    lifetime: int = 20

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_2], [lifetime, lifetime], [el_line, el_line], [heat_line, heat_line],
                                   [cool_line, cool_line], [hydro_line, hydro_line])
    ds.add_electrical_grid(buy_el)
    ds.add_district_heating(buy_heat)
    ds.add_district_cooling(buy_cool)
    ds.add_district_hydro(buy_hydro)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    c = (df[el_line].sum() * buy_el + df[heat_line].sum() * buy_heat - df[cool_line].sum() * buy_cool +
         df[hydro_line].sum() * buy_hydro) * lifetime + min(price_1, price_2)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands 1 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands 1  failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_1], [lifetime, lifetime], [el_line, el_line_2],
                                   [heat_line, heat_line_2], [cool_line, cool_line_2], [hydro_line, hydro_line_2])
    ds.add_electrical_grid(buy_el)
    ds.add_district_heating(buy_heat)
    ds.add_district_cooling(buy_cool)
    ds.add_district_hydro(buy_hydro)
    # set solver
    ds.set_txt_solver()
    ds.calc()
    c = min((df[el_line].sum() * buy_el + df[heat_line].sum() * buy_heat - df[cool_line].sum() * buy_cool +
             df[hydro_line].sum() * buy_hydro), (df[el_line_2].sum() * buy_el + df[heat_line_2].sum() * buy_heat +
                                                 df[cool_line_2].sum() * buy_cool - df[hydro_line_2].sum() *
                                                 buy_hydro)) * lifetime + price_1
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands 2 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands 2  failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_1], [life_1, life_2], [el_line, el_line], [heat_line, heat_line],
                                   [cool_line, cool_line], [hydro_line, hydro_line])
    ds.add_electrical_grid(buy_el)
    ds.add_district_heating(buy_heat)
    ds.add_district_cooling(buy_cool)
    ds.add_district_hydro(buy_hydro)
    # set solver
    ds.set_txt_solver()
    ds.calc()
    c = (df[el_line].sum() * buy_el + df[heat_line].sum() * buy_heat - df[cool_line].sum() * buy_cool +
         df[hydro_line].sum() * buy_hydro) * lifetime + min(price_1 / life_1 * lifetime, price_1 / life_2 * lifetime)
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands 3 successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands 3  failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_1], [life_1, life_1], column_names_electrical=[heat_line, el_line])
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    c = (min(df[heat_line].sum(), df[el_line].sum()) * buy_el) * lifetime + price_1 / life_1 * lifetime
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands just electrical successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands just electrical failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_1], [life_1, life_1], column_names_heating=[heat_line, el_line])
    ds.add_district_heating(buy_heat)
    # set solver
    ds.set_txt_solver()
    ds.calc()
    c = (min(df[heat_line].sum(), df[el_line].sum()) * buy_heat) * lifetime + price_1 / life_1 * lifetime
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands just heating successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands just heating failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_1], [life_1, life_1], column_names_cooling=[heat_line, el_line])
    ds.add_district_cooling(buy_cool)
    # set solver
    ds.set_txt_solver()
    ds.calc()
    c = (min(df[heat_line].sum(), df[el_line].sum()) * buy_cool) * lifetime + price_1 / life_1 * lifetime
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands just cooling successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands just cooling failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    ds.add_linked_multiple_demands([price_1, price_1], [life_1, life_1], column_names_hydrogen=[heat_line, el_line])
    ds.add_district_hydro(buy_hydro)
    # set solver
    ds.set_txt_solver()
    ds.calc()
    c = (min(df[heat_line].sum(), df[el_line].sum()) * buy_hydro) * lifetime + price_1 / life_1 * lifetime
    obj = ds.res['Obj']
    # check if validation worked
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of multiple linked demands just hydrogen successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of multiple linked demands just hydrogen failed: {c} != {obj}: difference {c - obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
