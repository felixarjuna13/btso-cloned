import sys

sys.path.append("/src")

from src.btso.DataStorage import DataStorage
from src.btso import FOLDER
from pandas import read_csv
from math import isclose
"""
validation script for the district cooling node and the cooling demands
"""


def main():
    # read pv_data from csv file
    df = read_csv(f'{FOLDER}/data/data.csv')
    # set variables which are used later on
    demand_line: str = 'Demand'
    demand_line_2: str = 'Electricity'
    reward: float = 0.08
    buy: float = 0.30
    price_lin = 200
    price_con = 500
    lifetime: int = 20
    eta_start_2_end = 0.96
    eta_end_2_start = 0.9

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)

    first_node = ds.add_electrical_node()
    second_node = ds.add_electrical_node()
    demand_data = ds.add_single_electrical_demand(demand_line)
    external_connection = ds.add_electrical_grid(buy, reward)
    node_connection = ds.add_inverter(price_lin, price_con)
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.activate_bidirectional()
    node_connection.set_efficiencies(eta_start_2_end, eta_end_2_start)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line]
    costs = (demand.clip(0).sum() * buy / eta_start_2_end + demand.clip(None, 0).sum() * reward * eta_end_2_start
             ) * lifetime + price_lin * max(max(demand) / eta_start_2_end, -1 * min(demand)) + price_con

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of electrical node connection with reward successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of electrical node connection with reward failed: '
              f'{costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_electrical_demand(demand_line_2)
    external_connection = ds.add_electrical_grid(buy)
    node_connection = ds.add_inverter(price_lin, price_con)
    first_node = ds.add_electrical_node()
    second_node = ds.add_electrical_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.set_efficiencies(eta_start_2_end)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line_2]
    costs = demand.sum() * buy / eta_start_2_end * lifetime + price_lin * max(demand) / eta_start_2_end + (
            price_con)

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of electrical node connection successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of electrical node connection failed: {costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_fuel_demand(demand_line)
    external_connection = ds.add_fuel_connection(buy, reward)
    node_connection = ds.add_fuel_pipe(price_lin, price_con)
    first_node = ds.add_fuel_node()
    second_node = ds.add_fuel_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.activate_bidirectional()
    node_connection.set_efficiencies(eta_start_2_end, eta_end_2_start)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line]
    costs = (demand.clip(0).sum() * buy / eta_start_2_end + demand.clip(None, 0).sum() * reward * eta_end_2_start
             ) * lifetime + price_lin * max(max(demand) / eta_start_2_end, -1 * min(demand)) + price_con

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of fuel node connection with reward successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of fuel node connection with reward failed: '
              f'{costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_fuel_demand(demand_line_2)
    external_connection = ds.add_fuel_connection(buy)
    node_connection = ds.add_fuel_pipe(price_lin, price_con)
    first_node = ds.add_fuel_node()
    second_node = ds.add_fuel_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.set_efficiencies(eta_start_2_end)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line_2]
    costs = demand.sum() * buy / eta_start_2_end * lifetime + price_lin * max(demand) / eta_start_2_end + (
        price_con)

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of fuel node connection successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of fuel node connection failed: {costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_hydrogen_demand(demand_line)
    external_connection = ds.add_district_hydro(buy, reward)
    node_connection = ds.add_hydrogen_pipe(price_lin, price_con)
    first_node = ds.add_hydrogen_node()
    second_node = ds.add_hydrogen_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.activate_bidirectional()
    node_connection.set_efficiencies(eta_start_2_end, eta_end_2_start)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line]
    costs = (demand.clip(0).sum() * buy / eta_start_2_end + demand.clip(None, 0).sum() * reward * eta_end_2_start
             ) * lifetime + price_lin * max(max(demand) / eta_start_2_end, -1 * min(demand)) + price_con

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of hydrogen node connection with reward successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of hydrogen node connection with reward failed: '
              f'{costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_hydrogen_demand(demand_line_2)
    external_connection = ds.add_district_hydro(buy)
    node_connection = ds.add_hydrogen_pipe(price_lin, price_con)
    first_node = ds.add_hydrogen_node()
    second_node = ds.add_hydrogen_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.set_efficiencies(eta_start_2_end)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line_2]
    costs = demand.sum() * buy / eta_start_2_end * lifetime + price_lin * max(demand) / eta_start_2_end + (
        price_con)

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of hydrogen node connection successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of hydrogen node connection failed: {costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_heating_demand(demand_line)
    external_connection = ds.add_district_heating(buy, reward)
    node_connection = ds.add_heating_pipe(price_lin, price_con)
    first_node = ds.add_heating_node()
    second_node = ds.add_heating_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.activate_bidirectional()
    node_connection.set_efficiencies(eta_start_2_end, eta_end_2_start)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line]
    costs = (demand.clip(0).sum() * buy / eta_start_2_end + demand.clip(None, 0).sum() * reward * eta_end_2_start
             ) * lifetime + price_lin * max(max(demand) / eta_start_2_end, -1 * min(demand)) + price_con

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of heating node connection with reward successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of heating node connection with reward failed: '
              f'{costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_heating_demand(demand_line_2)
    external_connection = ds.add_district_heating(buy)
    node_connection = ds.add_heating_pipe(price_lin, price_con)
    first_node = ds.add_heating_node()
    second_node = ds.add_heating_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.set_efficiencies(eta_start_2_end)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line_2]
    costs = demand.sum() * buy / eta_start_2_end * lifetime + price_lin * max(demand) / eta_start_2_end + (
        price_con)

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of heating node connection successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of heating node connection failed: {costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_cooling_demand(demand_line)
    external_connection = ds.add_district_cooling(buy, reward)
    node_connection = ds.add_cooling_pipe(price_lin, price_con)
    first_node = ds.add_cooling_node()
    second_node = ds.add_cooling_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.activate_bidirectional()
    node_connection.set_efficiencies(eta_start_2_end, eta_end_2_start)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line]
    costs = (demand.clip(0).sum() * buy / eta_start_2_end + demand.clip(None, 0).sum() * reward * eta_end_2_start
             ) * lifetime + price_lin * max(max(demand) / eta_start_2_end, -1 * min(demand)) + price_con

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of cooling node connection with reward successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of cooling node connection with reward failed: '
              f'{costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError

    # set variables in Datastorage
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    demand_data = ds.add_single_cooling_demand(demand_line_2)
    external_connection = ds.add_district_cooling(buy)
    node_connection = ds.add_cooling_pipe(price_lin, price_con)
    first_node = ds.add_cooling_node()
    second_node = ds.add_cooling_node()
    demand_data.set_node(second_node)
    external_connection.set_node(first_node)
    node_connection.set_nodes(first_node, second_node)
    node_connection.set_efficiencies(eta_start_2_end)

    # set solver
    ds.set_txt_solver()
    # start calculation
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    demand = df[demand_line_2]
    costs = demand.sum() * buy / eta_start_2_end * lifetime + price_lin * max(demand) / eta_start_2_end + (
        price_con)

    # check if validation worked
    if isclose(obj, costs, rel_tol=0.00001):
        print(f'Validation of cooling node connection successful: {costs:_.0f} == {obj:_.0f}')
    else:
        print(f'Validation of cooling node connection failed: {costs:_.0f} != {obj:_.0f}; {costs - obj:_.0f}')
        raise AssertionError


if __name__ == '__main__':
    main()
