from src.btso.DataStorage import DataStorage
from pandas import read_csv
from math import isclose
from numpy import ones, zeros


def main():
    df = read_csv('../../src/btso/data/data.csv')
    el_line: str = 'Electricity'
    pv_line: str = 'PV'
    buy_el: float = 0.25
    lifetime: int = 60
    life_pv: int = 20
    rate_of_interest: float = 0.05
    max_pv: float = 100_000.
    sell_pv: float = 0.15
    price_lin: float = 250.
    price_con: float = 2500.
    price_1: float = 1000_000.
    price_2: float = 555_000.
    life_1: int = 20
    life_2: int = 10
    maintenance: float = 0.01

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', rate_of_interest*100, lifetime, lifetime, 8760)
    ds.add_single_electrical_demand(el_line)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    buy_el_arr = ones(lifetime) * buy_el
    buy_el_npv = 0
    for idx, b in enumerate(buy_el_arr):
        buy_el_npv += b/((1+rate_of_interest)**idx)
    # Calculate total costs manually
    c = df[el_line].sum() * buy_el_npv
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    pv_data = ds.add_photovoltaic(price_lin, price_con, maintenance * 100, life_pv)
    pv_data.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data.selling_power.set_constant_selling_price(sell_pv)
    pv_data.set_max(max_pv)
    ds.calc()
    # get total costs result
    obj = ds.res['Obj']

    sell_pv_arr = ones(lifetime) * sell_pv
    sell_pv_npv = 0
    for idx, s in enumerate(sell_pv_arr):
        sell_pv_npv += s / ((1 + rate_of_interest) ** idx)

    price_lin_arr = zeros(lifetime)
    price_lin_arr[0:int(lifetime):int(life_pv)] = price_lin
    # add maintenance costs
    price_lin_arr[:] += maintenance * price_lin
    price_lin_npv = 0
    for idx, p in enumerate(price_lin_arr):
        price_lin_npv += p / ((1 + rate_of_interest) ** idx)

    price_con_arr = zeros(lifetime)
    price_con_arr[0:int(lifetime):int(life_pv)] = price_con
    # add maintenance costs
    price_con_arr[:] += maintenance * price_con
    price_con_npv = 0
    for idx, p in enumerate(price_con_arr):
        price_con_npv += p / ((1 + rate_of_interest) ** idx)

    demand = df[el_line]
    e_diff = demand - df[pv_line] * max_pv / 1000
    c_buy = e_diff.where(e_diff > 0).sum() * buy_el_npv
    c_sell = e_diff.where(e_diff < 0).sum() * sell_pv_npv
    c = c_buy + c_sell + max_pv * price_lin_npv + price_con_npv
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', rate_of_interest * 100, lifetime, lifetime, 8760)
    ds.add_multiple_electrical_demand([el_line, el_line], [price_1, price_2], [life_1, life_1])
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    price_1_arr = zeros(lifetime)
    price_1_arr[0:int(lifetime):int(life_1)] = price_1
    price_1_npv = 0
    for idx, p in enumerate(price_1_arr):
        price_1_npv += p / ((1 + rate_of_interest) ** idx)

    price_2_arr = zeros(lifetime)
    price_2_arr[0:int(lifetime):int(life_1)] = price_2
    price_2_npv = 0
    for idx, p in enumerate(price_2_arr):
        price_2_npv += p / ((1 + rate_of_interest) ** idx)

    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = df[el_line].sum() * buy_el_npv + min(price_1_npv, price_2_npv)
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps('../data/data.csv', rate_of_interest * 100, lifetime, lifetime, 8760)
    ds.add_multiple_electrical_demand([el_line, el_line], [price_1, price_1], [life_1, life_2])
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    price_2_arr = zeros(lifetime)
    price_2_arr[0:int(lifetime):int(life_2)] = price_1
    price_2_npv = 0
    for idx, p in enumerate(price_2_arr):
        price_2_npv += p / ((1 + rate_of_interest) ** idx)

    # get total costs result
    obj = ds.res['Obj']
    # Calculate total costs manually
    c = df[el_line].sum() * buy_el_npv + min(price_1_npv, price_2_npv)
    # check if validation worked
    if isclose(obj, c, rel_tol=0.00001):
        print(f'Validation of electrical grid constant price successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of electrical grid constant price failed: {c} != {obj}; {c - obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
