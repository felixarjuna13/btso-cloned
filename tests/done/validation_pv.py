import sys

sys.path.append("/src")
from src.btso.DataStorage import DataStorage
from src.btso import FOLDER
from pandas import read_csv
from math import isclose
from scipy import optimize
from numpy import ones


def main():
    df = read_csv(f'{FOLDER}/data/data.csv')
    pv_line: str = 'PV'
    el_line: str = 'Electricity'
    sell_line: str = 'Sell'
    buy_line: str = 'Buy'
    max_pv: float = 100_000.
    sell_pv: float = 0.063
    buy_el: float = 0.18
    price_lin: float = 221.
    power_frac: float = 0.001
    lifetime: int = 20

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)

    pv_data = ds.add_photovoltaic(price_lin, 0, 0, lifetime)
    pv_data.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data.selling_power.set_constant_selling_price(sell_pv)
    pv_data.set_max(max_pv)
    pv_data.set_electrical_demand(power_frac)
    pv_data2 = ds.add_photovoltaic(price_lin, 0, 0, lifetime)
    pv_data2.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data2.selling_power.set_constant_selling_price(sell_pv)
    pv_data2.set_max(max_pv)
    pv_data2.set_electrical_demand(power_frac)
    ds.add_single_electrical_demand(el_line)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = df[el_line] + power_frac * max_pv * 2
    e_diff = demand - df[pv_line] * max_pv * 2 / 1000
    c_buy = e_diff.where(e_diff > 0).sum() * lifetime * buy_el
    c_sell = e_diff.where(e_diff < 0).sum() * lifetime * sell_pv
    c = c_buy + c_sell + max_pv * 2 * price_lin
    obj = ds.res['Obj']
    c_sell = price_lin / sum(df[pv_line] - power_frac * 1000) / lifetime * 1000
    if pv_data.size.value == max_pv and pv_data2.size.value == max_pv and isclose(c, obj) and c_sell < sell_pv:
        print(f'Validation of PV with constant selling successful: {c_sell} < {sell_pv} and {int(c)} == {int(obj)}')
    else:
        print(f'Validation of PV with constant selling failed: {c_sell} > {sell_pv} or {c} != {obj}')
        raise AssertionError

    sell_pv_neu = 0
    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    pv_data = ds.add_photovoltaic(price_lin, 0, 0, lifetime)
    pv_data.produced_power.get_power_from_data(pv_line, 1/1000)
    pv_data.set_max(max_pv)
    pv_data.selling_power.set_constant_selling_price(sell_pv_neu)
    ds.add_single_electrical_demand(el_line)
    el_grid = ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()
    ds.calc()

    def calc_costs(size: float) -> float:
        e_diff_f = df[el_line] - df[pv_line] * size / 1000
        return e_diff_f.where(e_diff_f > 0).sum() * lifetime * buy_el + size * price_lin

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP')
    c = res.fun

    obj = int(ds.res["Obj"])
    if isclose(c, obj, rel_tol=0.0001):
        print(f'Validation of PV without selling successful : {int(obj)} == {int(c)}')
    else:
        print(f'Validation of PV without selling failed!\n {obj} != {c}')
        raise AssertionError

    pv_data.selling_power.set_flexible_selling_price(sell_line)
    el_grid.set_flexible_price(buy_line)
    ds.calc()

    e_diff = df[el_line] - df[pv_line] * max_pv / 1000
    e_buy = e_diff * df[buy_line]
    e_sell = e_diff * df[sell_line]
    e_sell_2 = e_sell.where(e_diff < 0)
    c_buy = e_buy.where(e_diff >= 0).sum() * lifetime
    c_sell = e_sell_2.where(df[sell_line] > 0).sum() * lifetime
    c = c_buy + c_sell + max_pv * price_lin
    obj = ds.res['Obj']
    if pv_data.size.value == max_pv and isclose(c, obj, rel_tol=0.000001):
        print(f'Validation of PV with flexible selling successful: {int(c)} == {int(obj)}')
    else:
        print(f'Validation of PV with flexible selling failed: {c} != {obj}')
        raise AssertionError


if __name__ == '__main__':
    main()
