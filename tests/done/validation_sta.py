import sys
sys.path.append("/src")

from src.btso.DataStorage import DataStorage
from src.btso import FOLDER
from pandas import read_csv
from math import isclose
from scipy import optimize
from numpy import ones, ndarray


def main():
    df = read_csv(f'{FOLDER}/data/data.csv')
    power_warm_line: str = 'STAPowerHot'
    power_cold_line: str = 'STAPowerCold'
    heat_line: str = 'Heating'
    cool_line: str = 'Cooling'
    buy_el: float = 0.18
    buy_cool: float = 0.11
    price_lin: float = 200.
    price_lin_cold: float = 2.
    price_con: float = 50.
    lifetime: int = 20

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)

    sta_data = ds.add_solar_thermal_absorber(price_lin, price_con, 0, lifetime)
    sta_data.get_power_from_data(power_warm_line)
    sta_data.set_connections(True, False, False, False)
    eh_data = ds.add_electrical_heater(0, 0, 0, lifetime)
    eh_data.set_efficiency(1)
    ds.add_single_heating_demand(heat_line)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = df[heat_line]
    q_sta = df[power_warm_line].clip(0) / 1000

    def calc_costs(size: float) -> float:
        e_diff = demand - q_sta * size
        return e_diff.where(e_diff > 0).sum() * lifetime * buy_el + size * price_lin + price_con

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP')
    c = res.fun
    obj = ds.res['Obj']
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of solar thermal absorber heating warm side successful : {int(obj)} == {int(c)}')
    else:
        print(f'Validation of solar thermal absorber heating warm side failed!\n {obj} != {c}')
        raise AssertionError

    ds = DataStorage()
    ds.set_file_rate_time_steps(f'{FOLDER}/data/data.csv', 0.0, lifetime, lifetime, 8760)
    sta_data = ds.add_solar_thermal_collector(price_lin_cold, 0, 0, lifetime)
    sta_data.get_power_from_data(power_warm_line, power_cold_line)
    sta_data.set_connections(False, False, False, True)
    ds.add_single_cooling_demand(cool_line)
    ds.add_district_cooling(buy_cool)
    ds.add_electrical_grid(buy_el)
    # set solver
    ds.set_txt_solver()

    ds.calc()

    demand = df[cool_line] * -1
    q_sta = (df[power_cold_line] * -1).clip(0) / 1000

    def calc_costs(size: ndarray) -> float:
        e_diff = demand - q_sta * size[0]
        return e_diff.where(e_diff > 0).sum() * lifetime * buy_cool + size * price_lin_cold

    res = optimize.minimize(calc_costs, ones(1), method='SLSQP', bounds=((0, 20_000_000),))
    c = res.fun
    obj = ds.res['Obj']
    if isclose(obj, c, rel_tol=0.0001):
        print(f'Validation of solar thermal collector absorber cold side successful : {int(obj)} == {int(c)}')
    else:
        print(f'Validation of solar thermal collector absorber cold side failed!\n {obj} != {c}')
        raise AssertionError


if __name__ == '__main__':
    main()
